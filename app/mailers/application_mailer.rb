class ApplicationMailer < ActionMailer::Base
  default from: 'Pencil Venues <venues@pencil.one>'
  layout 'mailer'
end
