class UserMailer < ApplicationMailer
  def director_registration(options = {})
    @trading_name = options[:trading_name].to_s
    @business_name = options[:business_name].to_s
    @director_email = options[:director_email].to_s
    @director_first_name = options[:director_first_name].to_s
    @company_name = options[:company_name].to_s
    @set_password_link = options[:set_password_link].to_s

    # Get email template
    email_template = EmailTemplate.get_director_registration
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.subject.gsub! '{{business_name}}', @business_name
    email_template.html_content.gsub! '{{director_first_name}}', @director_first_name
    email_template.html_content.gsub! '{{company_name}}', @company_name
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{business_name}}', @business_name
    email_template.html_content.gsub! '{{set_password_link}}', @set_password_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @director_email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def ascf_apply_send_to_supplier(options = {})
    @ascf = options[:ascf]
    @vaf = @ascf.vaf
    @created_by = @ascf.created_by
    @supplier = @ascf.supplier
    @firstname = @created_by[:firstname] || "anonymous"
    @lastname = @created_by[:lastname] || ""
    @market_segment = @vaf.market_segment[:name]
    @company_name = @vaf[:company_name] +" " + @vaf[:trading_name]
    @review_link = @ascf.get_review_url
    @email = @supplier[:email]

    # Get email template
    email_template = EmailTemplate.get_ascf_apply
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{firstname}}', @firstname
    email_template.subject.gsub! '{{lastname}}', @lastname
    email_template.subject.gsub! '{{company_name}}', @company_name
    email_template.subject.gsub! '{{market_segment}}', @market_segment
    email_template.html_content.gsub! '{{firstname}}', @firstname
    email_template.html_content.gsub! '{{lastname}}', @lastname
    email_template.html_content.gsub! '{{company_name}}', @company_name
    email_template.html_content.gsub! '{{market_segment}}', @market_segment
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def pg_signed_apply_send_to_supplier(options = {})
    @ascf = options[:ascf]
    @vaf = @ascf.vaf
    @personal_guarantee = options[:personal_guarantee]
    @supplier = @ascf.supplier
    @firstname = @personal_guarantee[:firstname] || "anonymous"
    @lastname = @personal_guarantee[:lastname] || ""
    @market_segment = @vaf.market_segment[:name]
    @company_name = @vaf[:company_name] +" " + @vaf[:trading_name]
    @review_link = @ascf.get_review_url
    @email = @supplier[:email]

    # Get email template
    email_template = EmailTemplate.get_ascf_pg_signed_apply
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{firstname}}', @firstname
    email_template.subject.gsub! '{{lastname}}', @lastname
    email_template.subject.gsub! '{{company_name}}', @company_name
    email_template.subject.gsub! '{{market_segment}}', @market_segment
    email_template.html_content.gsub! '{{firstname}}', @firstname
    email_template.html_content.gsub! '{{lastname}}', @lastname
    email_template.html_content.gsub! '{{company_name}}', @company_name
    email_template.html_content.gsub! '{{market_segment}}', @market_segment
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  # pga: personal_guarantee_application
  def pga_for_supplier_credit(options = {})
    @personal_guarantee_email = options[:personal_guarantee_email].to_s
    @personal_guarantee_firstname = options[:personal_guarantee_firstname].to_s
    @personal_guarantee_lastname = options[:personal_guarantee_lastname].to_s
    @trading_name = options[:trading_name].to_s
    @company_name = options[:company_name].to_s
    @set_password_link = options[:set_password_link].to_s

    # Get email template
    email_template = EmailTemplate.get_pga_for_supplier_credit
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{firstname}}', @personal_guarantee_firstname
    email_template.html_content.gsub! '{{lastname}}', @personal_guarantee_lastname
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{company_name}}', @company_name
    email_template.html_content.gsub! '{{set_password_link}}', @set_password_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @personal_guarantee_email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def send_form_link(options = {})
    @form_link = options[:form_link].to_s
    @email = options[:email].to_s

    # Get email template
    email_template = EmailTemplate.get_venue_form
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.html_content.gsub! '{{form_link}}', @form_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def send_ascf_form_link(options = {})
    @form_link = options[:form_link].to_s
    @email = options[:email].to_s

    # Get email template
    email_template = EmailTemplate.get_ascf_form
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.html_content.gsub! '{{form_link}}', @form_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  # Notify directors and PGs that supplier T&Cs has been changed
  def send_ascf_form_link_supplier_terms_condition_changed(options = {})
    form_link = options[:form_link].to_s
    supplier_company_name = options[:supplier_company_name]
    emails = options[:emails]

    # Get email template
    email_template = EmailTemplate.get_notify_directors_pgs_supplier_terms_condition_changed
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{supplier_company_name}}', supplier_company_name
    email_template.html_content.gsub! '{{form_link}}', form_link
    email_template.html_content.gsub! '{{supplier_company_name}}', supplier_company_name

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def director_signed_send_all_directors(options = {})
    @director = options[:director]
    @vaf = options[:vaf]
    @firstname = @director[:firstname]
    @lastname = @director[:lastname]
    @trading_name = @vaf[:trading_name]
    @review_link = @vaf.get_review_url
    @emails = @vaf.directors.map(&:email)

    # Get email template
    email_template = EmailTemplate.get_director_sign_venue_form
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{firstname}}', @firstname
    email_template.subject.gsub! '{{lastname}}', @lastname
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{firstname}}', @firstname
    email_template.html_content.gsub! '{{lastname}}', @lastname
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def directors_signed_all(options = {})
    @vaf = options[:vaf]
    @trading_name = @vaf[:trading_name]
    @review_link = @vaf.get_review_url
    @directors = @vaf.directors
    @emails = @directors.map(&:email)
    # Get email template
    email_template = EmailTemplate.get_all_directors_signed_venue_form
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    attachments['pencil_all_directors_signed.pdf'] = WickedPdf.new.pdf_from_string(
      render_to_string(
        :pdf => "pencil_all_directors_signed",
        :template => 'venue_application_form/signed_all_director.html.erb',
        :layout => 'pdf.html',
        viewport_size: '1280x1024'
      )
    )

    mail(subject: email_template.subject, to: @emails, bbc: User.all_admin.pluck(:email)) do |format|
      format.html {render html: ERB::Util.html_escape(email_theme.html_content.html_safe)}
    end
  end

  def pg_signed_send_all_pgs(options = {})
    @personal_guarantee = options[:personal_guarantee]
    @ascf = options[:ascf]
    @vaf = @ascf.vaf
    @supplier = @ascf.supplier
    @firstname = @personal_guarantee[:firstname]
    @lastname = @personal_guarantee[:lastname]
    @trading_name = @vaf[:trading_name]
    @supplier_name = @supplier[:company_name]
    @review_link = @ascf.get_review_url
    @emails = @ascf.personal_guarantees.map(&:email)

    # Get email template
    email_template = EmailTemplate.get_pg_sign_ascf
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{firstname}}', @firstname
    email_template.subject.gsub! '{{lastname}}', @lastname
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.subject.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{firstname}}', @firstname
    email_template.html_content.gsub! '{{lastname}}', @lastname
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def all_pg_signed_send_to_supplier_main(options = {})
    @ascf = options[:ascf]
    @vaf = @ascf.vaf
    @supplier = @ascf.supplier
    @asc_personal_guarantees = @ascf.apply_supplier_credit_personal_guarantees
    @directors = @vaf.directors
    @trade_references = @ascf.trade_references
    @terms_and_condition = @supplier.get_terms_and_condition(@vaf[:id], @ascf.trading_terms_setting_id)

    @trading_name = @vaf[:trading_name]
    @supplier_name = @supplier[:company_name]
    @review_link = @ascf.get_review_url
    @emails = @supplier[:email]

    # Get email template
    email_template = EmailTemplate.get_all_pg_signed_send_to_supplier_main
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.subject.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    attachments['all_personal_guarantees_signed.pdf'] = WickedPdf.new.pdf_from_string(
      render_to_string(
        :pdf => "all_personal_guarantees_signed",
        :template => 'admin/venue_suppliers/all_personal_guarantees_signed.html.erb',
        :layout => 'pdf.html',
        viewport_size: '1280x1024'
      )
    )

    mail(subject: email_template.subject, to: @emails, bbc: User.all_admin.pluck(:email)) do |format|
      format.html {render html: ERB::Util.html_escape(email_theme.html_content.html_safe)}
    end
  end

  def all_pg_signed_send_to_pgs(options = {})
    @ascf = options[:ascf]
    @vaf = @ascf.vaf
    @supplier = @ascf.supplier
    @asc_personal_guarantees = @ascf.apply_supplier_credit_personal_guarantees
    @directors = @vaf.directors
    @trade_references = @ascf.trade_references
    @terms_and_condition = @supplier.get_terms_and_condition(@vaf[:id], @ascf.trading_terms_setting_id)
    @trading_name = @vaf[:trading_name]
    @supplier_name = @supplier[:company_name]
    @review_link = @ascf.get_review_url
    @emails = @ascf.personal_guarantees.map(&:email)

    # Get email template
    email_template = EmailTemplate.get_all_pg_signed_send_to_pgs
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', @trading_name
    email_template.subject.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{trading_name}}', @trading_name
    email_template.html_content.gsub! '{{supplier_name}}', @supplier_name
    email_template.html_content.gsub! '{{review_link}}', @review_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    attachments['all_personal_guarantees_signed.pdf'] = WickedPdf.new.pdf_from_string(
      render_to_string(
        :pdf => "all_personal_guarantees_signed",
        :template => 'admin/venue_suppliers/all_personal_guarantees_signed.html.erb',
        :layout => 'pdf.html',
        viewport_size: '1280x1024'
      )
    )

    mail(subject: email_template.subject, to: @emails, bbc: User.all_admin.pluck(:email)) do |format|
      format.html {render html: ERB::Util.html_escape(email_theme.html_content.html_safe)}
    end
  end

  def supplier_registration(options = {})
    @contact_name = options[:contact_name].to_s
    @email = options[:email].to_s
    @first_name = options[:first_name].to_s
    @last_name = options[:last_name].to_s
    @company_name = options[:company_name].to_s
    @set_password_link = options[:set_password_link].to_s

    # Get email template
    email_template = EmailTemplate.get_supplier_registration
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{contact_name}}', @contact_name
    email_template.html_content.gsub! '{{first_name}}', @first_name
    email_template.html_content.gsub! '{{last_name}}', @last_name
    email_template.html_content.gsub! '{{company_name}}', @company_name
    email_template.html_content.gsub! '{{contact_name}}', @contact_name
    email_template.html_content.gsub! '{{set_password_link}}', @set_password_link

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @email, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def ascf_new_credit_limit_send_all_directors_pgs(options = {})
    ascf = options[:ascf]
    supplier = ascf.supplier
    emails = ascf.personal_guarantees.map(&:email)
    emails << ascf.vaf.directors.map(&:email)
    emails.flatten!
    emails.uniq!
    emails << supplier[:email]
    venue_credit_limit = CommonLib.number_to_currency(ascf[:venue_credit_limit])
    supplier_credit_limit = CommonLib.number_to_currency(ascf[:supplier_credit_limit])

    # Get email template
    email_template = EmailTemplate.get_ascf_new_credit_limit
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{venue_credit_limit}}', venue_credit_limit
    email_template.html_content.gsub! '{{supplier_credit_limit}}', supplier_credit_limit

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def ascf_approved_send_all_directors_pgs_supplier(options = {})
    ascf = options[:ascf]
    approved_by_user = options[:approved_by]
    supplier = ascf.supplier
    emails = ascf.personal_guarantees.map(&:email)
    emails << ascf.vaf.directors.map(&:email)
    emails.flatten!
    emails.uniq!
    emails << supplier[:email]

    # Get email template
    email_template = EmailTemplate.get_ascf_approved
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{approved_by}}', approved_by_user.friendly_name

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  def ascf_rejected_send_all_directors_pgs_supplier(options = {})
    ascf = options[:ascf]
    rejected_by_user = options[:rejected_by]
    supplier = ascf.supplier
    emails = ascf.personal_guarantees.map(&:email)
    emails << ascf.vaf.directors.map(&:email)
    emails.flatten!
    emails.uniq!
    emails << supplier[:email]

    # Get email template
    email_template = EmailTemplate.get_ascf_rejected
    return if email_template.blank?
    email_template = set_values(email_template)
    email_template.subject.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{trading_name}}', supplier[:company_name]
    email_template.html_content.gsub! '{{rejected_by}}', rejected_by_user.friendly_name

    # Get email theme
    email_theme = init_theme_values(email_template)

    send_mail(to: @emails, body: email_theme.html_content,
      content_type: 'text/html',
      subject: email_template.subject)
  end

  private

  def send_mail(options = {})
    # Send all emails to admin
    options[:bcc] = User.all_admin.pluck(:email)
    mail(options)
  end

  def set_values(email_template)
    email_template.subject.gsub! '{{base_url}}', $prod_root_url
    email_template.html_content.gsub! '{{base_url}}', $prod_root_url
    email_template
  end

  def init_theme_values(email_template)
    email_theme = EmailTheme.get_default
    logo = ActionController::Base.helpers.asset_path('pencil-logo.png')
    email_theme.html_content.gsub! '{{logo}}', logo
    email_theme.html_content.gsub! '{{base_url}}', $prod_root_url
    email_theme.html_content.gsub! '{{yield}}', email_template.html_content
    email_theme
  end
end
