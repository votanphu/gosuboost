module ApplicationHelper
  def pretty_format_date(date_obj, format = '%d/%m/%Y', default_date = Time.now)
    date_obj = default_date if date_obj.blank?
    date_obj.strftime(format)
  end
end
