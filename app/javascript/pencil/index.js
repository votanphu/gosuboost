import React, { Component } from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './configs/configureStore'
import {
  BrowserRouter,
  Route,
  HashRouter
} from 'react-router-dom'
import VenueRegistrationStep1 from './containers/VenueRegistrationStep1'
import VenueRegistrationStep2 from './containers/VenueRegistrationStep2'
import VenueRegistrationStep3 from './containers/VenueRegistrationStep3'
import VenueRegistrationStep4 from './containers/VenueRegistrationStep4'
import VenueRegistrationStep5 from './containers/VenueRegistrationStep5'
import VenueRegistrationStep6 from './containers/VenueRegistrationStep6'
import VenueRegistrationStep7 from './containers/VenueRegistrationStep7'
import VenueRegistrationStep8 from './containers/VenueRegistrationStep8'
import VenueRegistrationStep9 from './containers/VenueRegistrationStep9'
import FormLink from './containers/FormLink'
import VRInfoReview from './containers/VRInfoReview'

const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <HashRouter>
          <div>
            <Route exact path='/' component={VenueRegistrationStep1} />
            <Route path='/venue-registration' component={VenueRegistrationStep1} />
            <Route path='/step-1' component={VenueRegistrationStep1} />
            <Route path='/step-2' component={VenueRegistrationStep2} />
            <Route path='/step-3' component={VenueRegistrationStep3} />
            <Route path='/step-4' component={VenueRegistrationStep4} />
            <Route path='/step-5' component={VenueRegistrationStep5} />
            <Route path='/step-6' component={VenueRegistrationStep6} />
            <Route path='/step-7' component={VenueRegistrationStep7} />
            <Route path='/step-8' component={VenueRegistrationStep8} />
            <Route path='/step-9' component={VenueRegistrationStep9} />
            <Route path='/save-form' component={FormLink} />
            <Route path='/venue-registration-information-review/:token' component={VRInfoReview} />
          </div>
        </HashRouter>
      </Provider>
    );
  }
}
