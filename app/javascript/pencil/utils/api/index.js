import { FETCHING_DATA, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE } from '../../constants'

const defaultHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

// General state
export function getData() {
    return {
        type: FETCHING_DATA
    }
}

export function getDataSuccess(data) {
    return {
        type: FETCHING_DATA_SUCCESS,
        data,
    }
}

export function getDataFailure() {
    return {
        type: FETCHING_DATA_FAILURE
    }
}

// General method to call API
export function fetchGet(url) {
    return fetch(url, {
        method: 'GET',
        headers: defaultHeaders
    })
        .then((response) => response.json())
}

export function fetchPost(url, body = {}, token = '') {
    if (token) {
        defaultHeaders['Authorization'] = 'Token ' + token
    }
    return fetch(url, {
        method: 'POST',
        headers: defaultHeaders,
        body: body
    })
        .then((response) => response.json())
}
