/**
 * Created by Nhat Minh
 */

'use strict';

export function isEmptyString(text) {
    return text == undefined || text == null || text == '' || text == 'null';
}

export function isObjectUndefinedOrNull(obj) {
    return obj == undefined || obj == null || obj == 'null';
}

export function validateEmail(email) {
    if (this.isObjectUndefinedOrNull(email) || this.isEmptyString(email)) {
        return true;
    }
    /*eslint-disable */
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    /*eslint-enable */
    return pattern.test(email);
}

export function validatePhone(phone) {
    if (this.isObjectUndefinedOrNull(phone) || this.isEmptyString(phone)) {
        return true;
    }

    let pattern = /^[0-9]*$/;
    return pattern.test(phone);
}


