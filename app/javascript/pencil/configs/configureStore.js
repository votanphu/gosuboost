import { createStore, applyMiddleware } from 'redux'
import app from '../reducers'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

export default function configureStore() {
    let middleware = [thunk]
    console.log('process.env.NODE_ENV = ', process.env.NODE_ENV)

    if (process.env.NODE_ENV != 'production') {
      middleware = [...middleware, logger]
    }

    let store = createStore(
      app, 
      applyMiddleware(...middleware)
    )

    return store
}
