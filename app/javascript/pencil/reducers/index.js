import { combineReducers } from 'redux'
import venueRegistrationReducer from '../containers/VenueRegistration/reducer'

const rootReducer = combineReducers({
  venueRegistrationReducer
})

export default rootReducer
