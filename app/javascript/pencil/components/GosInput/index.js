import { withFormsy } from 'formsy-react'
import React from 'react'

class GosInput extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)

    this.state = {
      errorMessage: '',
      inputClass: this.props.inputClass,
      labelClass: this.props.labelClass,
      smallClass: this.props.smallClass
    }
  }

  componentDidMount() {
    if (this.props.isRequired()) {
      this.setState({ 
        labelClass: this.props.labelClass + ' required',
        smallClass: this.props.smallClass + ' required'
      })
    }

    this.props.setValue(this.props.defaultValue)

    if (this.props.inputmask) {
      $('#' + this.props.id).inputmask(this.props.inputmask)
    }
  }

  handleRequired() {
    if (this.props.isRequired() && !this.props.getValue()) {
      this.setState({ errorMessage: this.props.errorMessage || 'This is required.' })
    } else {
      this.setState({ errorMessage: '' })
    }

    if (!this.props.isValid()) {
      this.setState({ inputClass: this.props.inputClass + ' is-invalid' })
    } else {
      this.setState({ inputClass: this.props.inputClass })
    }
  }

  handleChange(e) {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    this.props.setValue(e.currentTarget.value)
    this.handleRequired()
    this.props.onChange(e)
  }

  handleBlur(e) {
    this.handleChange(e)
  }

  render() {
    const fsErrorMessage = this.props.getErrorMessage()

    return (
      <div>
        <div className={this.props.wrapperClass}>
          {
            this.props.type != 'checkbox' && this.props.textLabel && <label htmlFor={this.props.id} className={this.state.labelClass}>{this.props.textLabel}</label>
          }
          <input 
            type={this.props.type || 'text'} 
            className={this.state.inputClass}
            id={this.props.id}
            name={this.props.name}
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            value={this.props.getValue() || ''} />
          {
            this.props.iconRightClass && <div className="input-group-append">
              <span className="input-group-text">
                <i className="fa fa-calendar" aria-hidden="true"></i>
              </span>
            </div>
          }
          {
            this.props.textSmall && <small className={this.state.smallClass}>{this.props.textSmall}</small>
          }
          {
            this.props.type == 'checkbox' && <label className={this.state.labelClass} htmlFor={this.props.id}>{this.props.textLabel}</label>
          }
          <div className="invalid-feedback">{[this.state.errorMessage, fsErrorMessage].join(" ")}</div>
        </div>
      </div>
    )
  }
}

export default withFormsy(GosInput)
