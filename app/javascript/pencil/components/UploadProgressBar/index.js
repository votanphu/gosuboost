import React from 'react'

class UploadProgressBar extends React.Component {
  constructor(props) {
    super(props)
    this.props.currentStep ? this.props.currentStep : 0
    this.props.totalSteps ? this.props.totalSteps : 100
    this.props.min ? this.props.min : 0
    this.props.max ? this.props.max : 100
  }

  render() {
    let width = this.props.currentStep
    const style = {
      width: width + "%"
    }

    return (
      <div>
        <div className="progress mt-2">
          <div 
            className="progress-bar progress-bar-striped progress-bar-animated" 
            role="progressbar" style={style} 
            aria-valuenow={width} aria-valuemin={this.props.min} 
            aria-valuemax={this.props.max}>
            {this.props.currentStep} %
          </div>
        </div>
      </div>
    )
  }
}

export default UploadProgressBar
