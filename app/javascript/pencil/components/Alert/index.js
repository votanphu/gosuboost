import React from 'react'

class Alert extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        {
          this.props.loading && <p>Loading</p>
        }
        {
          this.props.message && <p className="alert alert-success">{this.props.message}</p>
        }
      </div>
    )
  }
}

export default Alert
