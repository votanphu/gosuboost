import React from 'react'
import Header from '../Header'
import Alert from '../Alert'
import ProgressBar from '../ProgressBar'
import { addValidationRule } from 'formsy-react';

var _isExisty = function _isExisty(value) {
  return value !== null && value !== undefined
}
var isEmpty = function isEmpty(value) {
  return value === ''
}

addValidationRule('isMobileAU', function (values, value) {
  var regexp = /^\d{4}-\d{3}-\d{3}$/i
  return !_isExisty(value) || isEmpty(value) || regexp.test(value);
})

class Master extends React.Component {
  constructor(props) {
    super(props)
    document.title = this.props.title
  }

  render() {
    return (
      <div>
        <Alert
          loading={this.props.loading} 
          message={this.props.message} />
        {
          this.props.currentStep && <ProgressBar currentStep={this.props.currentStep} 
            totalSteps={this.props.totalSteps}
            title={this.props.stepTitle} />
        }
        {this.props.children}
      </div>
    )
  }
}

export default Master
