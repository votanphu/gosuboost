import React from 'react'

class ProgressBar extends React.Component {
  constructor(props) {
    super(props)
    this.props.currentStep ? this.props.currentStep : 1
    this.props.totalSteps ? this.props.totalSteps : 10
    this.props.min ? this.props.min : 0
    this.props.max ? this.props.max : 100
  }

  render() {
    let width = (this.props.currentStep / this.props.totalSteps) * 100

    const style = {
      width: width + "%"
    }

    return (
      <div>
        <h1 className="text-uppercase">{this.props.title}</h1>
        <h6>Step {this.props.currentStep} of {this.props.totalSteps}</h6>
        <div className="progress mb-9">
          <div 
            className="progress-bar progress-bar-striped progress-bar-animated" 
            role="progressbar" style={style} 
            aria-valuenow={width} aria-valuemin={this.props.min} 
            aria-valuemax={this.props.max}>
            {this.props.currentStep} / {this.props.totalSteps}
          </div>
        </div>
      </div>
    )
  }
}

export default ProgressBar
