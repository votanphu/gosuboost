import React from 'react'

class Header extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-dark border-bottom box-shadow">
        <h5 className="my-0 mr-md-auto font-weight-normal">
          <a className="logo" href="/">
            <img src={logo_path} />
          </a>
        </h5>
        <nav className="my-2 my-md-0 mr-md-3">
          <a className="p-2 text-white text-uppercase" href="/venue-registration">Venue Registration</a>
          <a className="p-2 text-white text-uppercase" href="#">Supplier Registration</a>
          <a className="p-2 text-white text-uppercase" href="#">About Us</a>
        </nav>
      </div>
    )
  }
}

export default Header
