import React from 'react'

const STREET_TYPE_OPTION = [
  "STREET", "ROAD", "ALLEY", "AMBLE", "APPROACH", "ARCADE", "ARTERIAL", 
  "AVENUE", "BAY", "BEND", "BRAE", "BREAK", "BOULEVARD", "BOARDWALK", "BOWL", "BYPASS", 
  "CIRCLE", "CIRCUS", "CIRCUIT", "CHASE", "CLOSE", "CORNER", "COMMON", "CONCOURSE", 
  "CRESCENT", "CROSS", "COURSE", "CREST", "CRUISEWAY", "COURT/S", "COVE", "DALE", "DELL", 
  "DENE", "DIVIDE", "DOMAIN", "DRIVE", "EAST", "EDGE", "ENTRANCE", "ESPLANADE", "EXTENSION", 
  "FLATS", "FORD", "FREEWAY", "GATE", "GARDEN/S", "GLADE/S", "GLEN", "GULLY", "GRANGE", 
  "GREEN", "GROVE", "GATEWAY", "HILL", "HOLLOW", "HEATH", "HEIGHTS", "HUB", "HIGHWAY", 
  "ISLAND", "JUNCTION", "LANE", "LINK", "LOOP", "LOWER", "LANEWAY", "MALL", "MEW", "MEWS", 
  "NOOK", "NORTH", "OUTLOOK", "PATH", "PARADE", "POCKET", "PARKWAY", "PLACE", "PLAZA", 
  "PROMENADE", "PASS", "PASSAGE", "POINT", "PURSUIT", "PATHWAY", "QUADRANT", "QUAY", 
  "REACH", "RIDGE", "RESERVE", "REST", "RETREAT", "RIDE", "RISE", "ROUND", "ROW", "RISING", 
  "RETURN", "RUN", "SLOPE", "SQUARE", "SOUTH", "STRIP", "STEPS", "SUBWAY", "TERRACE", 
  "THROUGHWAY", "TOR", "TRACK", "TRAIL", "TURN", "TOLLWAY", "UPPER", "VALLEY", "VISTA", 
  "VIEW/S", "WAY", "WOOD", "WEST", "WALK", "WALKWAY", "WATERS", "WATERWAY", "WYND"
]
class StreetType extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <select className="form-control" 
        {...this.props} >
        <option key="-1" value=''>Select one from the list</option>
        {
          STREET_TYPE_OPTION.map(opt => <option key={opt} value={opt}>{opt}</option>)
        }
      </select>
    )
  }
}

export default StreetType
