import { withFormsy } from 'formsy-react'
import React from 'react'

class FSInput extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleBlur = this.handleBlur.bind(this)

    this.state = {
      errorMessage: '',
      inputClass: this.props.inputClass,
      labelClass: this.props.labelClass
    }
  }

  componentDidMount() {
    if (this.props.isRequired()) {
      this.setState({ labelClass: this.props.labelClass + ' required' })
    }

    this.props.setValue(this.props.defaultValue)
  }

  handleRequired() {
    if (this.props.isRequired() && !this.props.getValue()) {
      this.setState({ errorMessage: this.props.errorMessage || 'This is required' })
    } else {
      this.setState({ errorMessage: '' })
    }

    if (!this.props.isValid()) {
      this.setState({ inputClass: this.props.inputClass + ' is-invalid' })
    } else {
      this.setState({ inputClass: this.props.inputClass })
    }
  }

  handleChange(e) {
    // setValue() will set the value of the component, which in
    // turn will validate it and the rest of the form
    // Important: Don't skip this step. This pattern is required
    // for Formsy to work.
    this.props.setValue(e.currentTarget.value)
    this.handleRequired()
    this.props.onChange(e.currentTarget.value)
  }

  handleBlur(e) {
    this.handleChange(e)
  }

  render() {
    // An error message is returned only if the component is invalid
    const errorMessage = this.props.getErrorMessage()

    return (
      <div>
        <input
          onChange={this.handleChange}
          type={this.props.type || "text"}
          value={this.props.getValue() || ""}
          onBlur={this.handleBlur}
        />
        <span>{errorMessage}</span>

        <div className={this.props.wrapperClass}>
          <label htmlFor={this.props.id} className={this.state.labelClass}>{this.props.textLabel}</label>
          <input 
            type={this.props.type || 'text'} 
            className={this.state.inputClass}
            id={this.props.id}
            placeholder={this.props.placeholder}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            value={this.props.getValue() || ''} />
          <div className="invalid-feedback">{this.state.errorMessage}</div>
          <div className="invalid-feedback">{errorMessage}</div>
        </div>
      </div>
    );
  }
}

export default withFormsy(FSInput)
