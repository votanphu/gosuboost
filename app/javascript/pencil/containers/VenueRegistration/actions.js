import { 
  API_URL,
  CLIENT_URL,
  GETTING_VENUE_APPLICATION_FORM, 
  GET_VENUE_APPLICATION_FORM_SUCCESS, 
  GET_VENUE_APPLICATION_FORM_FAILURE,
  SET_VENUE_APPLICATION_FORM_SUCCESS,
  POSTING_VENUE_APPLICATION_FORM, 
  POST_VENUE_APPLICATION_FORM_SUCCESS, 
  POST_VENUE_APPLICATION_FORM_FAILURE,
  UPDATING_VENUE_APPLICATION_FORM,
  UPDATE_VENUE_APPLICATION_FORM_SUCCESS,
  UPDATE_VENUE_APPLICATION_FORM_FAILURE,
  CREATING_USERS,
  CREATE_USERS_SUCCESS,
  CREATE_USERS_FAILURE,
  GETTING_ABN, 
  GET_ABN_SUCCESS, 
  GET_ABN_FAILURE
} from '../../constants'

// ================================
// Security
export const csrfToken = document.querySelector("meta[name=csrf-token]").content;
const config = { 'X-CSRF-Token': csrfToken, credentials: 'same-origin' };

// ================================
// Actions
export function gettingVenueApplicationForm() {
  return {
    type: GETTING_VENUE_APPLICATION_FORM
  }
}

export function getVenueApplicationFormSuccess(data) {
  return {
    type: GET_VENUE_APPLICATION_FORM_SUCCESS,
    data,
  }
}

export function getVenueApplicationFormFailure() {
  return {
    type: GET_VENUE_APPLICATION_FORM_FAILURE
  }
}

export function setVenueApplicationFormSuccess(data) {
  return {
    type: SET_VENUE_APPLICATION_FORM_SUCCESS,
    data,
  }
}

export function postingVenueApplicationForm() {
  return {
    type: POSTING_VENUE_APPLICATION_FORM
  }
}

export function postVenueApplicationFormSuccess(data) {
  return {
    type: POST_VENUE_APPLICATION_FORM_SUCCESS,
    data,
  }
}

export function postVenueApplicationFormFailure() {
  return {
    type: POST_VENUE_APPLICATION_FORM_FAILURE
  }
}

export function updatingVenueApplicationForm() {
  return {
    type: UPDATING_VENUE_APPLICATION_FORM
  }
}

export function updateVenueApplicationFormSuccess(data) {
  return {
    type: UPDATE_VENUE_APPLICATION_FORM_SUCCESS,
    data,
  }
}

export function updateVenueApplicationFormFailure() {
  return {
    type: UPDATE_VENUE_APPLICATION_FORM_FAILURE
  }
}

export function creatingUsers() {
  return {
    type: CREATING_USERS
  }
}

export function createUsersSuccess(data) {
  return {
    type: CREATE_USERS_SUCCESS,
    data,
  }
}

export function createUsersFailure() {
  return {
    type: CREATE_USERS_FAILURE
  }
}

export function gettingABN() {
  return {
    type: GETTING_ABN
  }
}

export function getABNSuccess(data) {
  return {
    type: GET_ABN_SUCCESS,
    data,
  }
}

export function getABNFailure() {
  return {
    type: GET_ABN_FAILURE
  }
}

// ================================
// Call API or handling
export function setVenueApplicationForm(data) {
  return (dispatch) => {
    return dispatch(setVenueApplicationFormSuccess(data))
  }
}

export function getVenueApplicationForm(token) {
  const url = `${API_URL}/venue-registration/token/${token}.json`
  
  const config = {
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    method: 'GET'
  };

  return (dispatch) => {
    dispatch(gettingVenueApplicationForm())
    return fetch(url, config)
      .then((response) => response.json())
      .then((data) => {
        dispatch(getVenueApplicationFormSuccess(data))
      })
      .catch((err) => {
        dispatch(getVenueApplicationFormFailure())
      })
  }
}

export function postVenueApplicationForm(data) {
  const url = `${API_URL}/venue-registration.json`
  
  const config = {
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    method: 'POST', 
    body: JSON.stringify({ venue_application_form: data })
  };

  return (dispatch) => {
    dispatch(setVenueApplicationFormSuccess(data))
    dispatch(postingVenueApplicationForm())
    return fetch(url, config)
      .then((response) => response.json())
      .then((payload) => {
        dispatch(postVenueApplicationFormSuccess(payload))
      })
      .catch((err) => {
        dispatch(postVenueApplicationFormFailure())
      })
  }
}

export function updateVenueApplicationForm(data) {
  const url = `${API_URL}/venue-registration/${data.id}.json`
  
  const config = {
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    method: 'PUT', 
    body: JSON.stringify({ venue_application_form: data })
  };

  return (dispatch) => {
    dispatch(setVenueApplicationFormSuccess(data))
    dispatch(updatingVenueApplicationForm())
    return fetch(url, config)
      .then((response) => response.json())
      .then((payload) => {
        dispatch(updateVenueApplicationFormSuccess(payload))
      })
      .catch((err) => {
        dispatch(updateVenueApplicationFormFailure())
      })
  }
}

export function createUsers(data) {
  const url = `${API_URL}/users/create-users.json`
  
  const config = {
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    method: 'POST',
    body: JSON.stringify({ users: data })
  };

  return (dispatch) => {
    dispatch(creatingUsers())
    return fetch(url, config)
      .then((response) => response.json())
      .then((payload) => {
        dispatch(createUsersSuccess(payload))
      })
      .catch((err) => {
        dispatch(createUsersFailure())
      })
  }
}

// searchABN
export function searchABN(abn) {
  const url = `${API_URL}/abn/abn-details/${abn}.json`

  const config = {
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    method: 'GET'
  };

  return (dispatch) => {
    dispatch(gettingABN())
    return fetch(url, config)
      .then((response) => response.json())
      .then((data) => {
        dispatch(getABNSuccess(data))
      })
      .catch((err) => {
        dispatch(getABNFailure())
      })
  }
}

// Send email link to form.
export function sendEmailLinkForm(email, save_token) {
  const payload = {
    email: email,
    token: save_token
  }

  const url = `${CLIENT_URL}/venue-registration/send-email/`

  const config = {
    method: 'POST',
    credentials: 'same-origin',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'X-CSRF-Token': csrfToken
    },
    body: JSON.stringify(payload)
  }

  return () => {
    return fetch(url, config)
    .then((response) => response.json())
  }
}
