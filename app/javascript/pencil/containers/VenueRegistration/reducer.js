import {
  GETTING_VENUE_APPLICATION_FORM, 
  GET_VENUE_APPLICATION_FORM_SUCCESS, 
  GET_VENUE_APPLICATION_FORM_FAILURE,
  SET_VENUE_APPLICATION_FORM_SUCCESS,
  POSTING_VENUE_APPLICATION_FORM, 
  POST_VENUE_APPLICATION_FORM_SUCCESS, 
  POST_VENUE_APPLICATION_FORM_FAILURE,
  UPDATING_VENUE_APPLICATION_FORM,
  UPDATE_VENUE_APPLICATION_FORM_SUCCESS,
  UPDATE_VENUE_APPLICATION_FORM_FAILURE,
  CREATING_USERS,
  CREATE_USERS_SUCCESS,
  CREATE_USERS_FAILURE,
  GETTING_ABN, 
  GET_ABN_SUCCESS, 
  GET_ABN_FAILURE
} from '../../constants'

const initialState = {
  venueRegistrationForm: {},
  loading: false,
  error: false,
  message: '',
  abn: {}
}

export default function venueRegistrationReducer(state = initialState, action) {
  switch (action.type) {
    case GETTING_VENUE_APPLICATION_FORM:
      return {
        ...state,
        loading: true,
        error: false,
        message: 'Loading...'
      }
    case GET_VENUE_APPLICATION_FORM_SUCCESS:
      return {
        ...state,
        loading: false,
        venueRegistrationForm: action.data.venue_application_form,
        message: ''
      }
    case GET_VENUE_APPLICATION_FORM_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.data.message
      }
    case SET_VENUE_APPLICATION_FORM_SUCCESS:
      return {
        ...state,
        venueRegistrationForm: action.data,
        loading: false,
        error: false,
        message: ''
      }
    case POSTING_VENUE_APPLICATION_FORM:
      return {
        ...state,
        loading: true,
        error: false,
        message: 'Loading...'
      }
    case POST_VENUE_APPLICATION_FORM_SUCCESS:
      return {
        ...state,
        venueRegistrationForm: action.data.venue_application_form,
        loading: false,
        error: false,
        message: action.data.message
      }
    case POST_VENUE_APPLICATION_FORM_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.data.message
      }
    case UPDATING_VENUE_APPLICATION_FORM:
      return {
        ...state,
        loading: true,
        error: false,
        message: 'Loading...'
      }
    case UPDATE_VENUE_APPLICATION_FORM_SUCCESS:
      return {
        ...state,
        venueRegistrationForm: action.data.venue_application_form,
        loading: false,
        error: false,
        message: action.data.message
      }
    case UPDATE_VENUE_APPLICATION_FORM_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.data.message
      }
    case CREATING_USERS:
      return {
        ...state,
        loading: true,
        error: false,
        message: 'Loading...'
      }
    case CREATE_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        message: action.data.message
      }
    case CREATE_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: action.data.message
      }
    case GETTING_ABN:
      return {
        ...state,
        loading: true,
        error: false,
        message: 'Loading...'
      }
    case GET_ABN_SUCCESS:
      return {
        ...state,
        loading: false,
        abn: action.data.abn,
        message: action.data.message
      }
    case GET_ABN_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        message: 'Cannot search ABN.'
      }
    default:
      return state
  }
}
