import Formsy from 'formsy-react'
import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import Geosuggest from 'react-geosuggest'
import StreetType from '../../components/StreetType'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm 
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'
import {STATE_OPTIONS} from '../../constants/index'

class VenueRegistrationStep2 extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)
    this.validEvent = this.validEvent.bind(this)
    this.invalidEvent = this.invalidEvent.bind(this)

    this.state = {
      currentStep: 2,
      totalSteps: 9,
      title: 'Venue Registration - Step 2 of 9',
      canSubmit: false,
      errorMessageTradingStreetType: "",
      classTradingStreetType: "form-control"
    }
  }

  componentDidMount() {
    $("#checkboxTermsConditions").prop("disabled", true)
    $("#checkboxTermsConditions").css("opacity", 0.5)
    $("label[for=checkboxTermsConditions]").css("opacity", 0.5)

    $("div#divTermsConditions").on("scroll", function() {
      if ($(this).scrollTop() < $(this).height()) {
        $("#checkboxTermsConditions").prop("disabled", true)
        $("#checkboxTermsConditions").css("opacity", 0.5)
        $("label[for=checkboxTermsConditions]").css("opacity", 0.5)
      } else {
        $("#checkboxTermsConditions").prop("disabled", false)
        $("#checkboxTermsConditions").css("opacity", 1)
        $("label[for=checkboxTermsConditions]").css("opacity", 1)
      }
    })

    this.updateMaskForInput()
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-3')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-1')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validSubmitEvent(model) {
    this.saveForm()
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  validEvent() {
    let canSubmit = $('#checkboxTermsConditions').prop('checked') && 
      $('#inputRequestedTradingTerms').val() && this.refs.form.state.isValid
      && this.props.venueRegistrationForm.trading_street_type

      if(this.props.venueRegistrationForm.trading_street_type) {
        this.setState({ 
          errorMessageTradingStreetType: "",
          classTradingStreetType: "form-control"
        })
      } else {
        this.setState({ 
          errorMessageTradingStreetType: "This is required.",
          classTradingStreetType: "form-control is-invalid"
        })
      }

    canSubmit ? this.enableButton() : this.disableButton()
  }

  invalidEvent() {
    this.disableButton()
  }

  handleChange = (e) => {
    let attributes = ['firstname', 'lastname', 'date_of_birth', 'email',
      'address_line_1', 'address_line_2', 'suburb', 'state', 'postcode',
      'mobile', 'phone', 'drivers_license_number', 'license_expiry', 
      'front_license_file', 'back_license_file', 'signature_image', 
      'trading_unit_number', 'trading_street_number', 'trading_street_name', 'trading_state',
      'trading_postcode']
    
    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      this.props.venueRegistrationForm[e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Validate email
    if (validations.indexOf('email') !== -1) {
      if (validations.indexOf('email') && (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i).test(value)) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is not valid email.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Digits
    if (validations.indexOf('digits-eq-10') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 10) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This must be 10 digits.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    if (validations.indexOf('digits-eq-4') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 4) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Must enter 4 numbers')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // alphanumeric
    if (validations.indexOf('alphanumeric') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')
      let isInvalid = temp && /[^a-zA-Z0-9]/i.test(value);

      if (!isInvalid) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Alphanumberic')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  updateMaskForInput() {
    $('.mobile').inputmask('0499 999 999')
    $('.phone').inputmask('(09) 9999 9999')
    $('#inputTradingPostcode').inputmask('9999')
  }

  render() {

    const divTermsConditions = (
      <div className="form-control mb-2 pre-scrollable" style={{maxHeight:'250px'}} id="divTermsConditions">
        <p>pencil is committed to protecting the privacy of visitors to its website. Except in the circumstances set out in this Privacy Statement, pencil will not attempt to identify you. Your name and other identifying personal details are not collected (unless you specifically provide them to us).</p>
        <p>pencil gathers information on this website in 3 ways;</p>
        <ol className="pl-4">
          <li><h6>Information is gathered indirectly through the site’s technology</h6>
            <p>When you look at our website, our standard practice is not to monitor any of your browsing activities. From time to time, pencil may make a statistical record of that visit and logs the information for the purpose of evaluating our website. The types of information we collect may be:</p>
            <ul className="list-unstyled mb-2">
              <li><small><i className="fa fa-asterisk pr-2"></i></small>your server address only (for example, 203.147.56.66)</li>
              <li><small><i className="fa fa-asterisk pr-2"></i></small>your top level domain name (for example, .com, .gov, .au, .uk)</li>
              <li><small><i className="fa fa-asterisk pr-2"></i></small>the date and time of your visit to the site</li>
              <li><small><i className="fa fa-asterisk pr-2"></i></small>the pages you accessed and documents downloaded</li>
              <li><small><i className="fa fa-asterisk pr-2"></i></small>the type of browser you are using</li>
              <li><small><i className="fa fa-asterisk pr-2"></i></small>the previous site you visited</li>
            </ul>
            <p>For the purposes of such evaluation, we will NOT collect any personal details which may identify you.</p>
          </li>
          <li><h6>Information is collected when you voluntarily provide it.</h6>
            <p>pencil collects personal information to enable users to access certain sections of the site or participate in promotional offers and other marketing activity. Use and disclosure of personal information in such circumstances will be governed by the Privacy Act 1988 (Cth) (as amended) and subject to any terms and conditions or provisions specific to these offers.</p>
          </li>
          <li><h6>Use of cookies on this website</h6>
            <p>This site uses cookies. (A cookie is a small data file that certain web sites write to your computer's hard drive when you visit them and when a user requests the website to remember password details). Cookies are used only for the purposes of remembering a user’s password and is a voluntary option.</p>
            <p>Full terms and privacy details can be accessed at <a href="https://wwww.pencil.one">www.pencil.one</a>.</p>
          </li>
        </ol>
      </div>
    )

    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Applicant</h3>
        <Formsy 
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.validEvent} 
          onInvalid={this.invalidEvent}
          ref="form" >
          <div className="gos-form-group">
            <div className="form-row">
              <div className="form-group col-md-4">
                <input type="text" className="form-control" 
                  id="inputFirstName" placeholder="First Name"
                  onChange={(e) => { this.props.venueRegistrationForm.firstname = e.target.value }}
                  defaultValue={this.props.venueRegistrationForm.firstname} />
                <small className="font-weight-bold">First Name</small>
              </div>
              <div className="form-group col-md-4">
                <input type="text" className="form-control" 
                  id="inputLastName" placeholder="Last Name" 
                  onChange={(e) => { this.props.venueRegistrationForm.lastname = e.target.value }}
                  defaultValue={this.props.venueRegistrationForm.lastname} />
                <small className="font-weight-bold">Last Name</small>
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputMobile" className="font-weight-bold required">
                Mobile Number
              </label>
              <input type="text" className="form-control mobile" 
                id="inputMobile" name="inputMobile"
                placeholder="Mobile Number"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.mobile}
                data-name="mobile" 
                data-validations={['required', 'digits-eq-10']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputPhone" className="font-weight-bold">Phone (Landline)</label>
              <input type="text" className="form-control phone" 
                id="inputPhone" name="inputPhone" 
                placeholder="Phone (Landline)"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.phone}
                data-name="phone"
                data-validations={['digits-eq-10']} />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="gos-form-group">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label className="font-weight-bold">Trading Address</label>
                <input 
                  placeholder="Building/Property Name" 
                  className="form-control"
                  id="inputTradingAddressLine1"
                  ref={(input) => { this.inputTradingAddressLine1 = input }}
                  onChange={(e) => { 
                    this.props.venueRegistrationForm.trading_address_line_1 = e.target.value
                    // this.validEvent()
                  }}
                  onBlur={(e) => { 
                    this.props.venueRegistrationForm.trading_address_line_1 = e.target.value
                    // this.validEvent()
                  }}
                  defaultValue={this.props.venueRegistrationForm.trading_address_line_1} 
                  required
                />
                <small className="font-weight-bold">Building/Property Name</small>
                <div className="invalid-feedback"></div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="unitNumber" placeholder="Unit number"
                  data-validations={['alphanumeric']}
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.trading_unit_number} 
                  data-name="trading_unit_number" 
                  required />
                <small className="font-weight-bold">Unit Number</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="streetNumber" placeholder="Street number"
                  data-validations={['required', 'alphanumeric']}
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  data-name="trading_street_number" 
                  defaultValue={this.props.venueRegistrationForm.trading_street_number} required />
                <small className="font-weight-bold required">Street Number</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-3">
                <input type="text" className="form-control" 
                  id="streetName" placeholder="Street name"
                  data-validations={['required', 'alphanumeric']}
                  data-name="trading_street_name" 
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.trading_street_name} required />
                <small className="font-weight-bold required">Street Name</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-3">
                <StreetType
                  onChange={e => {
                    this.props.venueRegistrationForm.trading_street_type = e.target.value
                    this.validEvent();
                  }}
                  defaultValue={this.props.venueRegistrationForm.trading_street_type}
                  className={this.state.classTradingStreetType}
                  />
                <small className="font-weight-bold required">Street Type</small>
                <div className="invalid-feedback">{this.state.errorMessageTradingStreetType}</div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <GosInput 
                  wrapperClass=""
                  inputClass="form-control"
                  type="text"
                  id="inputTradingSuburb"
                  name="inputTradingSuburb"
                  placeholder="Suburb"
                  smallClass="font-weight-bold"
                  textSmall="Suburb"
                  innerRef={(c) => { this.inputTradingSuburb = c }}
                  onChange={(e) => { this.props.venueRegistrationForm.trading_suburb = e.currentTarget.value }}
                  defaultValue={this.props.venueRegistrationForm.trading_suburb}
                  required />
              </div>
              <div className="form-group col-md-2">
                <select 
                  className="form-control"
                  id="inputTradingState"
                  name="inputTradingState"
                  ref={(c) => { this.inputTradingState = c }}
                  data-validations={['required']}
                  data-name="trading_state" 
                  onChange={this.handleChange}
                  defaultValue={this.props.venueRegistrationForm.trading_state}>
                  <option value="">Select one from the list</option>
                  {
                    STATE_OPTIONS.map(opt => <option key={opt} value={opt}>{opt}</option>)
                  }
                </select>
                <small className="font-weight-bold required">State</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="inputTradingPostcode" 
                  placeholder="Postcode"
                  data-validations={['required', 'digits-eq-4']}
                  data-name="trading_postcode" 
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.trading_postcode} required />
                <small className="font-weight-bold required">Postcode</small>
                <div className="invalid-feedback"></div>
              </div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-6">
              <GosInput 
                wrapperClass=""
                inputClass="form-control"
                type="text"
                id="inputEstimatedWeeklyExpectedPurchased"
                name="inputEstimatedWeeklyExpectedPurchased"
                placeholder="Estimated Weekly Expected Purchases $"
                labelClass="font-weight-bold"
                textLabel="Estimated Weekly Expected Purchases $"
                innerRef={(c) => { this.inputEstimatedWeeklyExpectedPurchased = c }}
                onChange={(e) => { this.props.venueRegistrationForm.estimated_weekly_expected_purchased = e.currentTarget.value }}
                defaultValue={this.props.venueRegistrationForm.estimated_weekly_expected_purchased}
                validations="isNumeric"
                validationError="This is the number."
                required />
            </div>
            <div className="form-group col-md-6">
              <div className="form-group">
                <label htmlFor="inputRequestedTradingTerms" className="font-weight-bold required">Requested Trading Terms</label>
                <select id="inputRequestedTradingTerms" className="form-control" 
                  onChange={(e) => { 
                    this.props.venueRegistrationForm.requested_trading_terms = e.target.value
                    this.validEvent()
                  }}
                  defaultValue={this.props.venueRegistrationForm.requested_trading_terms} required >
                  <option value=''>Terms of credit requested</option>
                  <option value='Cash on delivery'>Cash on delivery</option>
                  <option value='7 days'>7 days</option>
                  <option value='14 days'>14 days</option>
                  <option value='30 days'>30 days</option>
                </select>
              </div>
            </div>
          </div>
          <div className="gos-form-group">
            <label className="font-weight-bold required">Privacy and credit check consent</label>
            { divTermsConditions }
            <div className="form-check">
              <input type="checkbox" className="form-check-input" 
                id="checkboxTermsConditions"
                onChange={(e) => this.validEvent() } />
              <label className="form-check-label" htmlFor="checkboxTermsConditions">
                I have read and understand the terms and conditions
              </label>
            </div>
          </div>
          
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep2 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep2)
