import Formsy from 'formsy-react'
import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm 
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VenueRegistrationStep5 extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)
    this.validEvent = this.validEvent.bind(this)
    this.invalidEvent = this.invalidEvent.bind(this)

    this.state = {
      currentStep: 5,
      totalSteps: 9,
      title: 'Venue Registration - Step 5 of 9',
      canSubmit: false
    }
  }

  componentDidMount() {
    this.updateMaskForInput()
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-6')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-4')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validSubmitEvent(model) {
    this.saveForm()
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  validEvent() {
    let canSubmit = this.refs.form.state.isValid
    canSubmit ? this.enableButton() : this.disableButton()
  }

  invalidEvent() {
    this.disableButton()
  }

  handleChange = (e) => {
    let attributes = ['account_contact_name', 'account_contact_email',
      'account_contact_number', 'delivery_contact_name', 'delivery_contact_number']
    
    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      this.props.venueRegistrationForm[e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Validate email
    if (validations.indexOf('email') !== -1) {
      if (validations.indexOf('email') && (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i).test(value)) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is not valid email.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Digits
    if (validations.indexOf('digits-eq-10') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 10) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This must be 10 digits.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  updateMaskForInput() {
    $('.account_contact_number').inputmask('9{10}')
    $('.delivery_contact_number').inputmask('9{10}')
    
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Accounts & Delivery</h3>
        <Formsy 
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.validEvent} 
          onInvalid={this.invalidEvent}
          ref="form" >
          <div className="form-row">
            <div className="form-group col-md-5">
              <label htmlFor="inputAccountsPersonContactNumber" className="font-weight-bold required">
                Accounts Person Contact Name
              </label>
              <input type="text" className="form-control" 
                id="inputAccountsPersonContactName" name="inputAccountsPersonContactName"
                placeholder="Accounts Person Contact Name"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.account_contact_name}
                data-name="account_contact_name" 
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-5">
              <label htmlFor="inputAccountsPersonContactNumber" className="font-weight-bold required">
                Accounts Person Contact Email
              </label>
              <input type="text" className="form-control" 
                id="inputAccountsPersonContactEmail" name="inputAccountsPersonContactEmail"
                placeholder="Accounts Person Contact Email"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.account_contact_email}
                data-name="account_contact_email" 
                data-validations={['required', 'email']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-5">
              <label htmlFor="inputAccountsPersonContactNumber" className="font-weight-bold required">
                Accounts Person Contact Number
              </label>
              <input type="text" className="form-control account_contact_number" 
                id="inputAccountsPersonContactNumber" name="inputAccountsPersonContactNumber"
                placeholder="Accounts Person Contact Number"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.account_contact_number}
                data-name="account_contact_number" 
                data-validations={['required', 'digits-eq-10']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-5">
              <GosInput 
                wrapperClass=""
                labelClass="font-weight-bold"
                inputClass="form-control"
                type="text"
                id="inputDeliveryContactName"
                name="inputDeliveryContactName"
                placeholder="Delivery Contact Name"
                textLabel="Delivery Contact Name"
                innerRef={(c) => { this.inputDeliveryContactName = c }}
                onChange={(e) => { this.props.venueRegistrationForm.delivery_contact_name = e.currentTarget.value }}
                defaultValue={this.props.venueRegistrationForm.delivery_contact_name} />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-5">
              <label htmlFor="inputAccountsPersonContactNumber" className="font-weight-bold">
                Delivery Contact Number
              </label>
              <input type="text" className="form-control delivery_contact_number" 
                id="inputDeliveryContactNumber" name="inputDeliveryContactNumber"
                placeholder="Delivery Contact Number"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.delivery_contact_number}
                data-name="delivery_contact_number" />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep5 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep5)
