import React from 'react'
import Formsy from 'formsy-react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import Alert from '../../components/Alert'
import { CLIENT_URL } from '../../constants'
import { 
  csrfToken,
  sendEmailLinkForm
} from '../VenueRegistration/actions'

class FormLink extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)

    if (this.props.stateData.venueRegistrationForm) {
      this.venueRegistrationForm = this.props.stateData.venueRegistrationForm
    }

    this.state = {
      title: 'Venue Registration - Save form',
      canSubmit: false,
      email: '',
      isSubmittedSuccessfully: null
    }
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validSubmitEvent(model) {

    // send an email
    const email = this.state.email

    const save_token = this.props.stateData.venueRegistrationForm.save_token
    
    this.props.sendEmailLinkForm(email, save_token)
      .then((data) => {
        console.log(data)
        this.setState({ 
          isSubmittedSuccessfully: true,
          email: ''
        })
      })
      .catch((err) => {
        console.log(err.message)
        this.setState({ 
          isSubmittedSuccessfully: false
        })
      })
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  render() {
    const formLink = `${CLIENT_URL}/venue-registration/#/?token=${this.props.stateData.venueRegistrationForm.save_token}`

    const showSuccessMessage = () => (<div className="col-md-11 alert alert-success mt-2" role="alert">Congratulations!</div>)

    const showFailMessage = () => (<div className="col-md-11 alert alert-danger mt-2" role="alert">Sorry! Cannot send the email!</div>)

    const renderMessage = () => {
      if (this.state.isSubmittedSuccessfully === true) {
        return showSuccessMessage()
      } else if (this.state.isSubmittedSuccessfully === false) {
        return showFailMessage()
      } else {
        return null
      }
    }

    return (
      <Master title={this.state.title} {...this.props} >
        <Alert
          loading={this.props.stateData.loading} 
          message={this.props.stateData.message} />
        <Formsy
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.enableButton} 
          onInvalid={this.disableButton} >
          <div className="card">
            <div className="card-body text-center container">
              <p>Enter your email address to send this link to you by email. Please note that this link will expire after 30 days. Thanks.</p>
              <p><a href={formLink} target="_blank">{formLink}</a></p>
              <div className="form-row justify-content-md-center">
                <div className="col-md-8 mt-2">
                  <GosInput 
                    wrapperClass="form-group"
                    inputClass="form-control"
                    type="email"
                    id="inputEmail"
                    name="inputEmail"
                    placeholder="Enter Email"
                    value={this.state.email}
                    onChange={e => this.setState({email: e.currentTarget.value })}
                    validations="isEmail"
                    validationError="This is not a valid email."
                    required />
                </div>
                <div className="col-md-3 mt-2 actions">
                  <button type="submit" className="btn btn-block btn-primary text-uppercase" disabled={!this.state.canSubmit}>Send Email Link</button>
                </div>
                { renderMessage() }
              </div>
            </div>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    sendEmailLinkForm: (email, save_token) => dispatch(sendEmailLinkForm(email, save_token))
  }
}

export default FormLink = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormLink)
