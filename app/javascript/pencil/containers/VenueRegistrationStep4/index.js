import Formsy from 'formsy-react'
import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm 
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VenueRegistrationStep4 extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)
    this.validEvent = this.validEvent.bind(this)
    this.invalidEvent = this.invalidEvent.bind(this)

    this.state = {
      currentStep: 4,
      totalSteps: 9,
      title: 'Venue Registration - Step 4 of 9',
      canSubmit: false
    }
  }

  componentDidMount() {
    this.updateMaskForInput()
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-5')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-3')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validSubmitEvent(model) {
    this.saveForm()
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  validEvent() {
    let canSubmit = this.refs.form.state.isValid
    canSubmit ? this.enableButton() : this.disableButton()
  }

  invalidEvent() {
    this.disableButton()
  }

  handleChange = (e) => {
    let attributes = ['bank_branch', 'bsb', 'account_number']
    
    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      this.props.venueRegistrationForm[e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Validate email
    if (validations.indexOf('email') !== -1) {
      if (validations.indexOf('email') && (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i).test(value)) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is not valid email.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Digits
    if (validations.indexOf('digits-eq-10') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 10) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This must be 10 digits.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  updateMaskForInput() {
    $('.bsb, .account-number').inputmask('9{1,20}')
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Banking</h3>
        <Formsy 
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.validEvent} 
          onInvalid={this.invalidEvent}
          ref="form" >
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputBankBranch" className="font-weight-bold required">
                Bank & Branch
              </label>
              <input type="text" className="form-control bank_branch" 
                id="inputBankBranch" name="inputBankBranch"
                placeholder="Bank & Branch"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.bank_branch}
                data-name="bank_branch" 
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputBsb" className="font-weight-bold required">
                BSB
              </label>
              <input type="text" className="form-control bsb" 
                id="inputBsb" name="inputBsb"
                placeholder="BSB"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.bsb}
                data-name="bsb" 
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <label htmlFor="inputAccountNumber" className="font-weight-bold required">
                Account Number
              </label>
              <input type="text" className="form-control mobile" 
                id="inputAccountNumber" name="inputAccountNumber"
                placeholder="Account Number"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.props.venueRegistrationForm.account_number}
                data-name="account_number" 
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="gos-form-group">
            <label className="font-weight-bold">If you want to pay by credit card, please check the box</label>
            <div className="form-check">
              <input type="checkbox" className="form-check-input" 
                id="checkboxPayByCreditCard" ref={(input) => this.checkboxPayByCreditCard = input}
                onChange={(e) => { 
                  this.props.venueRegistrationForm.pay_by_credit_card = e.target.value
                  this.validEvent()
                }}
                defaultValue={this.props.venueRegistrationForm.pay_by_credit_card} />
              <label className="form-check-label" htmlFor="checkboxPayByCreditCard">
                I want to pay by credit card
              </label>
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep4 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep4)
