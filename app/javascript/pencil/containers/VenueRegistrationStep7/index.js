import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import {
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm 
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VenueRegistrationStep7 extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentStep: 7,
      totalSteps: 9,
      title: 'Venue Registration - Step 7 of 9',
      venueRegistrationForm: this.props.venueRegistrationForm,
      canSubmit: false
    }
  }

  componentDidMount() {
    this.updateMaskForInput()
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.state.venueRegistrationForm)
    this.props.history.push('step-8')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.state.venueRegistrationForm)
    this.props.history.push('step-6')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.state.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.state.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.state.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  handleChange = (e) => {
    let venueRegistrationForm = this.state.venueRegistrationForm
    let attributes = ['trade_reference_1_business_name', 
      'trade_reference_1_contact_person', 
      'trade_reference_1_contact_number']
    
    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      venueRegistrationForm[e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }

    this.setState({ venueRegistrationForm: venueRegistrationForm })
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Digits
    if (validations.indexOf('digits-eq-10') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 10) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This must be 10 digits.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  updateMaskForInput() {
    $('.trade_reference_1_contact_number').inputmask('9{10}')
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Trade Reference</h3>
        <form>
          <div className="form-row">
            <div className="col-md-5">
              <label htmlFor="inputTradeReference1BusinessName" className="font-weight-bold required">Trade Reference 1 Business Name</label>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <input type="text" className="form-control" 
                id="inputTradeReference1BusinessName" placeholder="Trade Reference 1 Business Name"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.state.venueRegistrationForm.trade_reference_1_business_name}
                data-name="trade_reference_1_business_name"
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="col-md-5">
              <label htmlFor="inputTradeReference1ContactPerson" className="font-weight-bold required">Trade Reference 1 Contact Person</label>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <input type="text" className="form-control" 
                id="inputTradeReference1ContactPerson" placeholder="Trade Reference 1 Contact Person"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.state.venueRegistrationForm.trade_reference_1_contact_person}
                data-name="trade_reference_1_contact_person"
                data-validations={['required']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="form-row">
            <div className="col-md-5">
              <label htmlFor="inputTradeReference1ContactNumber" className="font-weight-bold required">Trade Reference 1 Contact Number</label>
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <input type="text" className="form-control trade_reference_1_contact_number" 
                id="inputTradeReference1ContactNumber" placeholder="Trade Reference 1 Contact Number"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={this.state.venueRegistrationForm.trade_reference_1_contact_number}
                data-name="trade_reference_1_contact_number"
                data-validations={['required', 'digits-eq-10']}
                required />
              <div className="invalid-feedback"></div>
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </form>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep7 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep7)
