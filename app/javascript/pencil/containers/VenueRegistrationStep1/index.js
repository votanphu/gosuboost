import Formsy from 'formsy-react'
import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VenueRegistrationStep1 extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)

    this.state = {
      currentStep: 1,
      totalSteps: 9,
      title: 'Venue Registration - Step 1 of 9',
      canSubmit: false
    }
  }

  componentDidMount() {
    let paramsString = this.props.location.search
    const params = new URLSearchParams(paramsString)
    const token = params.get('token')
    var self = this

    if (token) {
      this.props.getVenueApplicationForm(token)
        .then(() => {
          self.inputTradingName.props.setValue(self.props.venueRegistrationForm.trading_name)
          self.inputEmail.props.setValue(self.props.venueRegistrationForm.email)
        })
    }
  }

  nextClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-2')
  }

  saveForm = () => {
    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validSubmitEvent(model) {
    this.saveForm()
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Let's Start!</h3>
        <Formsy 
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.enableButton} 
          onInvalid={this.disableButton} >
          <div className="form-row">
            <div className="form-group col-md-6">
              <GosInput 
                wrapperClass="form-group"
                labelClass="font-weight-bold"
                inputClass="form-control"
                type="text"
                id="inputTradingName"
                name="inputTradingName"
                placeholder="Enter Trading Name"
                textLabel="Trading Name"
                innerRef={(c) => { this.inputTradingName = c }}
                onChange={(e) => { this.props.venueRegistrationForm.trading_name = e.currentTarget.value }}
                defaultValue={this.props.venueRegistrationForm.trading_name}
                required />
            </div>
            <div className="form-group col-md-6">
              <GosInput 
                wrapperClass="form-group"
                labelClass="font-weight-bold"
                inputClass="form-control"
                type="email"
                id="inputEmail"
                name="inputEmail"
                placeholder="Enter Email"
                textLabel="Email"
                innerRef={(c) => { this.inputEmail = c }}
                onChange={(e) => { this.props.venueRegistrationForm.email = e.currentTarget.value }}
                defaultValue={this.props.venueRegistrationForm.email}
                validations="isEmail"
                validationError="This is not a valid email."
                required />
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-primary text-uppercase" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="submit" className="btn btn-link" disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep1 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep1)
