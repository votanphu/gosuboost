import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import UploadProgressBar from '../../components/UploadProgressBar'
import Geosuggest from 'react-geosuggest'
import { UploadField, Uploader } from '@navjobs/upload'
import { API_URL, STATE_OPTIONS } from '../../constants'
import StreetType from '../../components/StreetType'
import { 
  csrfToken,
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm,
  createUsers
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VenueRegistrationStep6 extends React.Component {
  constructor(props) {
    super(props)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)
    this.convertDateToDDMMYYY = this.convertDateToDDMMYYY.bind(this)

    let directors = [
      {
        firstname: '',
        lastname: '',
        date_of_birth: ''
      }
    ]

    if (this.props.venueRegistrationForm && this.props.venueRegistrationForm.directors) {
      directors = this.props.venueRegistrationForm.directors
    } else {
      this.props.venueRegistrationForm.directors = directors
    }

    this.state = {
      directors: directors,
      currentStep: 6,
      totalSteps: 9,
      title: 'Venue Registration - Step 6 of 9',
      user_signed_in: user_signed_in,
      current_user_email: current_user_email,
      canSubmit: false
    }
  }

  componentDidMount() {
    var self = this

    $('.datepicker').datepicker({
      format: 'dd/mm/yyyy',
      autoclose: true
    })

    $('.datepicker').on('changeDate', function(e) {
      self.handleChange(e)
    })

    this.updateMaskForInput()
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-7')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-5')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  addMore = () => {
    var self = this

    this.setState({
      directors: this.state.directors.concat([
        {
          firstname: '',
          lastname: '',
          date_of_birth: ''
        }
      ])
    })

    this.props.venueRegistrationForm.directors = this.state.directors
    var initDatepickerCounter = 0

    var initDatepicker = setInterval(function(){
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
      })

      self.updateMaskForInput()

      initDatepickerCounter++

      if (initDatepickerCounter === 1) {
          clearInterval(initDatepicker)
      }
    }, 1000)

    // var signature_image = this.refs.signature_image
    // console.log(signature_image.getCanvas().toDataURL())
    // console.log(signature_image.getTrimmedCanvas())
  }

  handleChange = (e) => {
    let directors = this.state.directors
    let attributes = ['firstname', 'lastname', 'date_of_birth', 'email',
      'address_line_1', 'address_line_2', 'suburb', 'state', 'postcode',
      'mobile', 'phone', 'drivers_license_number', 'license_expiry', 
      'front_license_file', 'back_license_file', 'signature_image',
      'unit_number', 'street_number', 'street_name', 'street_type', 
      'state',
    ]

    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      directors[e.target.dataset.index][e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }

    // // e.target.dataset.name == 'signature_image' ? directors[e.target.dataset.index].signature_image = e.target.getCanvas().toDataURL() : 0
    // e.target.dataset.name == 'signature_image' ? console.log('signature_image', e) : ''

    this.setState({
      directors: directors
    })

    this.props.venueRegistrationForm.directors = this.state.directors
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Validate email
    if (validations.indexOf('email') !== -1) {
      if (validations.indexOf('email') && (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i).test(value)) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is not valid email.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    if (validations.indexOf('digits-eq-4') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 4) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Must enter 4 numbers')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // alphanumeric
    if (validations.indexOf('alphanumeric') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')
      let isInvalid = temp && /[^a-zA-Z0-9]/i.test(value);

      if (!isInvalid) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Alphanumberic')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  updateMaskForInput() {
    $('.mobile').inputmask('0499 999 999')
    $('.phone').inputmask('(09) 9999 9999')
    $('.driver-license').inputmask('9{1,20}')
    $('.inputDirectorPostcode').inputmask('9999')
  }

  convertDateToDDMMYYY(value) {
    let value_date = new Date(value)
    let dd = value_date.getDate()
    let mm = ("0" + (value_date.getMonth() + 1)).slice(-2)
    let yyyy = value_date.getFullYear()
    return `${dd}/${mm}/${yyyy}`
  }

  render() {
    var self = this

    const styles = {
      disable: {
        backgroundColor: '#e9ecef',
        opacity: 0.5
      }
    }

    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Directors</h3>
        <form ref="form">
          {
            this.state.directors.map((director, index) => {
              let num_index = index + 1
              if (director.date_of_birth && director.date_of_birth.indexOf('/') === -1) {
                director.date_of_birth = this.convertDateToDDMMYYY(director.date_of_birth)
              }

              if (director.license_expiry && director.license_expiry.indexOf('/') === -1) {
                director.license_expiry = this.convertDateToDDMMYYY(director.license_expiry)
              }

              return (
                <div className="gos-form-group border-top-1 border-top-dotted mt-3 pt-3" key={index} data-index={index}>
                  <div className="gos-form-group">
                    <label className="font-weight-bold required">Director {num_index} Personal Details</label>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <input type="text" className="form-control" 
                          id={'inputDirectorFirstName_' + index} placeholder="First Name"
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.firstname}
                          data-index={index}
                          data-name="firstname"
                          data-validations={['required']}
                          required />
                        <small className="font-weight-bold">First Name</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-4">
                        <input type="text" className="form-control" 
                          id={'inputDirectorLastName_' + index} placeholder="Last Name"
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.lastname}
                          data-index={index}
                          data-name="lastname"
                          data-validations={['required']}
                          required />
                        <small className="font-weight-bold">Last Name</small>
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor={'inputEmail_' + index} className="font-weight-bold required">Email</label>
                      <input className="form-control"
                        type="email" placeholder="Email"
                        id={'inputEmail_' + index}
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        defaultValue={director.email} 
                        data-index={index}
                        data-name="email" 
                        data-validations={['required', 'email']}
                        required />
                      <div className="invalid-feedback"></div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor={'inputDateOfBirth_' + index} className="font-weight-bold required">Date of Birth</label>
                      <div className="input-group">
                        <input className="form-control datepicker"
                          type="text" placeholder="DD/MM/YYYY"
                          id={'inputDateOfBirth_' + index}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.date_of_birth} 
                          data-index={index}
                          data-name="date_of_birth" 
                          data-validations={['required']}
                          required />
                        <div className="input-group-append">
                          <span className="input-group-text">
                            <i className="fa fa-calendar" aria-hidden="true"></i>
                          </span>
                        </div>
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                  </div>
                  <div className="gos-form-group">
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label className="font-weight-bold">Address</label>
                        <input 
                          placeholder="Building/Property Name" 
                          className="form-control"
                          id={"inputTradingAddressLine1_" + index}
                          // ref={(input) => { this.inputTradingAddressLine1 = input }}
                          data-index={index}
                          data-name="address_line_1"
                          // data-validations={[]}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.address_line_1} 
                          required
                        />
                        <small className="font-weight-bold">Building/Property Name</small>
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-2">
                        <input type="text" className="form-control" 
                          id={"unitNumber_"+index} placeholder="Unit number"
                          data-validations={['alphanumeric']}
                          data-index={index}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.unit_number} 
                          data-name="unit_number" 
                          required />
                        <small className="font-weight-bold">Unit Number</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-2">
                        <input type="text" className="form-control" 
                          id={"streetNumber"+index} placeholder="Street number"
                          data-validations={['required', 'alphanumeric']}
                          data-index={index}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          data-name="street_number" 
                          defaultValue={director.street_number} required />
                        <small className="font-weight-bold required">Street Number</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-3">
                        <input type="text" className="form-control" 
                          id={"streetName"+index} placeholder="Street name"
                          data-validations={['required', 'alphanumeric']}
                          data-index={index}
                          data-name="street_name" 
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.street_name} required />
                        <small className="font-weight-bold required">Street Name</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-3">
                        <StreetType
                          data-validations={['required', 'alphanumeric']}
                          data-index={index}
                          data-name="street_type" 
                          onChange={this.handleChange}
                          defaultValue={director.street_type}
                          />
                        <small className="font-weight-bold required">Street Type</small>
                        <div className="invalid-feedback">{this.state.errorMessageTradingStreetType}</div>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <input type="text" className="form-control" 
                          id={'inputDirectorSuburb_' + index} placeholder="Suburb"
                          ref={(input) => { this.inputDirectorSuburb = input }}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.suburb}
                          data-index={index}
                          data-name="suburb"
                          data-validations={['required']}
                          required />
                        <small className="font-weight-bold required">Suburb</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-2">
                      <select 
                        className="form-control"
                        id={"inputState_"+index}
                        name="inputTradingState"
                        data-validations={['required']}
                        data-index={index}
                        data-name="state" 
                        onChange={this.handleChange}
                        defaultValue={director.state}>
                        <option value="">Select one from the list</option>
                        {
                          STATE_OPTIONS.map(opt => <option key={opt} value={opt}>{opt}</option>)
                        }
                      </select>
                        <small className="font-weight-bold required">State</small>
                        <div className="invalid-feedback"></div>
                      </div>
                      <div className="form-group col-md-2">
                        <input type="text" className="form-control inputDirectorPostcode" 
                          id={'inputDirectorPostcode_' + index} placeholder="Postcode"
                          ref={(input) => { this.inputDirectorPostcode = input }}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.postcode}
                          data-index={index}
                          data-name="postcode"
                          data-validations={['required', 'digits-eq-4']}
                          required />
                        <small className="font-weight-bold required">Postcode</small>
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor={'inputDirectorMobile_' + index} className="font-weight-bold required">
                        Mobile Number
                      </label>
                      <input type="text" className="form-control mobile" 
                        id={'inputDirectorMobile_' + index} placeholder="Mobile Number"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        defaultValue={director.mobile}
                        data-index={index}
                        data-name="mobile" 
                        data-validations={['required']}
                        required />
                      <div className="invalid-feedback"></div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor={'inputDirectorPhone_' + index} className="font-weight-bold">
                        Home Phone
                      </label>
                      <input type="text" className="form-control phone" 
                        id={'inputDirectorPhone_' + index} placeholder="Home Phone"
                        onChange={this.handleChange}
                        onBlur={this.handleBlur}
                        defaultValue={director.phone}
                        data-index={index}
                        data-name="phone" />
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <div className="form-group">
                        <label htmlFor={'inputDriversLicenseNumber_' + index} className="font-weight-bold required">
                          Driver's License
                        </label>
                        <input type="text" className="form-control driver-license" 
                          id={'inputDriversLicenseNumber_' + index} placeholder="Driver's License"
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.drivers_license_number}
                          data-index={index}
                          data-name="drivers_license_number" 
                          data-validations={['required']}
                          required />
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-4">
                      <label htmlFor={'inputDirectorLicenseExpiry_' + index} className="font-weight-bold required">License Expiry</label>
                      <div className="input-group">
                        <input className="form-control datepicker"
                          type="text" placeholder="DD/MM/YYYY"
                          id={'inputDirectorLicenseExpiry_' + index}
                          onChange={this.handleChange}
                          onBlur={this.handleBlur}
                          defaultValue={director.license_expiry} 
                          data-index={index}
                          data-name="license_expiry" 
                          data-validations={['required']}
                          required />
                        <div className="input-group-append">
                          <span className="input-group-text">
                            <i className="fa fa-calendar" aria-hidden="true"></i>
                          </span>
                        </div>
                        <div className="invalid-feedback"></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-8">
                      <div className="form-group">
                        <label htmlFor={'inputUploadDirectorFrontLicense_' + index} className="font-weight-bold">
                          Upload Front Of License
                        </label>
                        <Uploader
                          request={{
                            fileName: 'file',
                            url: `${API_URL}/directors/upload-front-license-file.json`,
                            method: 'POST',
                            fields: director,
                            headers: {
                              'X-CSRF-Token': csrfToken
                            },
                            withCredentials: false,
                          }}
                          onComplete={({ response, status }) => {
                            let directors = self.state.directors
                            directors[index] = response.director
                            self.setState({ directors: directors })
                          }}
                          onError={(error) => {
                          }}
                          //upload on file selection, otherwise use `startUpload`
                          uploadOnSelection={true}
                        >
                          {({ onFiles, progress, complete }) => {
                          return (
                            <div>
                              <UploadField 
                                onFiles={onFiles}
                                id={'inputUploadDirectorFrontLicense_' + index}
                                data-index={index}
                                data-name="front_license_file" 
                                containerProps={{
                                  className: 'custom-file',
                                  style: {}
                                }}
                                uploadProps={{
                                  accept: '.png,.jpg,.jpeg,.gif,.bmp',
                                  className: 'custom-file-input',
                                  style: {},
                                  disabled: !director.email
                                }}>
                                <label className="custom-file-label" htmlFor="customFile" 
                                  style={director.email ? {} : styles.disable}>
                                  { director.front_license_file_file_name || "Click here to select an image" }
                                </label>
                              </UploadField>
                              {
                                progress && <UploadProgressBar currentStep={progress} totalSteps={100} />
                              }
                              {
                                complete && <span>Upload successful!</span>
                              }
                            </div>
                          )}}
                        </Uploader>
                        {
                          director.front_license_original_url && <img src={director.front_license_original_url} 
                            alt={director.front_license_file_file_name} className="img-thumbnail" />
                        }
                      </div>
                    </div>
                  </div>
                  <div className="form-row">
                    <div className="form-group col-md-8">
                      <div className="form-group">
                        <label htmlFor={'inputUploadDirectorBackLicense_' + index} className="font-weight-bold">
                          Upload Back Of License
                        </label>
                        <Uploader
                          request={{
                            fileName: 'file',
                            url: `${API_URL}/directors/upload-back-license-file.json`,
                            method: 'POST',
                            fields: director,
                            headers: {
                              'X-CSRF-Token': csrfToken
                            },
                            withCredentials: false,
                          }}
                          onComplete={({ response, status }) => {
                            let directors = self.state.directors
                            directors[index] = response.director
                            self.setState({ directors: directors })
                          }}
                          onError={(error) => {
                          }}
                          //upload on file selection, otherwise use `startUpload`
                          uploadOnSelection={true}
                        >
                          {({ onFiles, progress, complete }) => {
                          return (
                            <div>
                              <UploadField 
                                onFiles={onFiles}
                                id={'inputUploadDirectorBackLicense_' + index}
                                data-index={index}
                                data-name="back_license_file" 
                                containerProps={{
                                  className: 'custom-file',
                                  style: {}
                                }}
                                uploadProps={{
                                  accept: '.png,.jpg,.jpeg,.gif,.bmp',
                                  className: 'custom-file-input',
                                  style: {},
                                  disabled: !director.email
                                }}>
                                <label className="custom-file-label" htmlFor="customFile"
                                  style={director.email ? {} : styles.disable}>
                                  { director.back_license_file_file_name || "Click here to select an image" }
                                </label>
                              </UploadField>
                              {
                                progress && <UploadProgressBar currentStep={progress} totalSteps={100} />
                              }
                              {
                                complete && <span>Upload successful!</span>
                              }
                            </div>
                          )}}
                        </Uploader>
                        {
                          director.back_license_original_url && <img src={director.back_license_original_url} 
                            alt={director.back_license_file_file_name} className="img-thumbnail" />
                        }
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }
          <div className="actions mt-3 pt-3 text-right">
            <button type="button" className="btn btn-primary text-uppercase" onClick={this.addMore}>Add More</button>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </form>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data)),
    createUsers: (data) => dispatch(createUsers(data))
  }
}

export default VenueRegistrationStep6 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep6)
