import React from 'react'
import { connect } from 'react-redux'
import SignatureCanvas from 'react-signature-canvas'
import Master from '../../components/Master'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm  
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'

class VRInfoReview extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentStep: 8,
      totalSteps: 9,
      title: 'Venue Registration - Step 8 of 9',
      user_signed_in: user_signed_in,
      current_user_email: current_user_email
    }
  }

  componentDidMount() {
    $("#checkboxTermsConditions").prop("disabled", true)
    $("#checkboxTermsConditions").css("opacity", 0.5)
    $("label[for=checkboxTermsConditions]").css("opacity", 0.5)

    $("textarea#textareaTermsConditions").on("scroll", function() {
      if ($(this).scrollTop() < $(this).height()) {
        $("#checkboxTermsConditions").prop("disabled", true)
        $("#checkboxTermsConditions").css("opacity", 0.5)
        $("label[for=checkboxTermsConditions]").css("opacity", 0.5)
      } else {
        $("#checkboxTermsConditions").prop("disabled", false)
        $("#checkboxTermsConditions").css("opacity", 1)
        $("label[for=checkboxTermsConditions]").css("opacity", 1)
      }
    })

    const token = this.props.match.params.token
    var self = this

    if (token) {
      this.props.getVenueApplicationForm(token)
        .then(() => {
          // TODO
        })
    }
  }

  saveForm = () => {
    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  renderDirectors = () => {
    return (
      <div className="card mb-3">
        <div className="card-body pb-1">
          <h5 className="card-title">Directors</h5>
          { this.renderOneDirectorDetails() }
        </div>
      </div>
    )
  }

  renderOneDirectorDetails = () => {
    if (this.props.venueRegistrationForm.directors !== undefined)
    {
      return this.props.venueRegistrationForm.directors.map( (director, index) => (
        <div key={index}>
          <h6 className="font-weight-bold mt-4 mb-4">Director {index+1} Personal Details</h6>
          <p>First Name: {director.firstname}</p>
          <p>Last Name: {director.lastname}</p>
          <p>Email: {director.email}</p>
          <p>Date of Birth: {this.convertDateToDDMMYYY(director.date_of_birth)}</p>
          <p>Street Address: {director.address_line_1}</p>
          <p>Address Line 2: {director.address_line_2}</p>
          <p>Suburb: {director.suburb}</p>
          <p>State: {director.state}</p>
          <p>Postcode: {director.postcode}</p>
          <p>Mobile Number: {director.mobile}</p>
          <p>Home Phone: {director.phone}</p>
          <p>Driver's License: {director.drivers_license_number}</p>
          <p>License Expiry: {this.convertDateToDDMMYYY(director.license_expiry)}</p>
          <p>Upload Front Of License:</p>
          <p>Upload Back Of License:</p>
          <div className="form-group">
            {
              this.state.user_signed_in && this.state.current_user_email == director.email && <SignatureCanvas 
                penColor='green'
                canvasProps={{width: 500, height: 200, className: 'signCanvas'}}
                ref={(ref) => { this.signature_image = ref }}
                onEnd={(e) => { 
                  let param = {
                    target: {
                      dataset: {
                        name: 'signature_image',
                        index: index
                      },
                      value: e
                    }
                  }

                  this.handleChange(param)
                }}
                data-index={index}
                data-name="signature_image" />
            }
            {
              (!this.state.user_signed_in || this.state.current_user_email != director.email) && <a 
                href={`${login_link}?form_token=${this.props.match.params.token}`}>Login to sign</a>
            }
          </div>
        </div>
      )
      )
    } else {
      return <p className="font-italic">Empty</p>
    }
    
  }

  convertDateToDDMMYYY(value) {
    let value_date = new Date(value)
    let dd = value_date.getDate()
    let mm = ("0" + (value_date.getMonth() + 1)).slice(-2)
    let yyyy = value_date.getFullYear()
    return `${dd}/${mm}/${yyyy}`
  }

  handleChange = (e) => {
    let directors = this.props.venueRegistrationForm.directors
    e.target.dataset.name == 'signature_image' ? directors[e.target.dataset.index].signature_image = this.signature_image.getCanvas().toDataURL() : ''
    this.setState({ directors: directors })
    this.props.venueRegistrationForm.directors = this.state.directors
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={false} 
        totalSteps={0}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Information Review</h3>
        <form>
          <div className="form-group">
            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Let's Start</h5>
                <p>Trading name: {this.props.venueRegistrationForm.trading_name}</p>
                <p>Email: {this.props.venueRegistrationForm.email}</p>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Applicant</h5>
                <p>First Name: {this.props.venueRegistrationForm.firstname}</p>
                <p>Last Name: {this.props.venueRegistrationForm.lastname}</p>
                <p>Mobile: {this.props.venueRegistrationForm.mobile}</p>
                <p>Phone: {this.props.venueRegistrationForm.phone}</p>
                <p>Trading Address</p>
                <p>Street Address: {this.props.venueRegistrationForm.trading_address_line_1}</p>
                <p>Address Line 2: {this.props.venueRegistrationForm.trading_address_line_2}</p>
                <p>Suburb: {this.props.venueRegistrationForm.trading_suburb}</p>
                <p>State: {this.props.venueRegistrationForm.trading_state}</p>
                <p>Postcode: {this.props.venueRegistrationForm.trading_postcode}</p>
                <p>Estimated Weekly Expected Purchased: ${this.props.venueRegistrationForm.estimated_weekly_expected_purchased}</p>
                <p>Requested Trading Terms: {this.props.venueRegistrationForm.requested_trading_terms}</p>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Company</h5>
                <p>Company Name: {this.props.venueRegistrationForm.company_name}</p>
                <p>Company ABN: {this.props.venueRegistrationForm.company_abn}</p>
                <p>Operating Structure: {this.props.venueRegistrationForm.operating_structure}</p>
                <p>Registered Address</p>
                <p>Street Address: {this.props.venueRegistrationForm.registered_address_line_1}</p>
                <p>Address Line 2: {this.props.venueRegistrationForm.registered_address_line_2}</p>
                <p>Suburb: {this.props.venueRegistrationForm.registered_suburb}</p>
                <p>State: {this.props.venueRegistrationForm.registered_state}</p>
                <p>Postcode: {this.props.venueRegistrationForm.registered_postcode}</p>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Banking</h5>
                <p>Bank & Branch: {this.props.venueRegistrationForm.bank_branch}</p>
                <p>BSB: {this.props.venueRegistrationForm.bsb}</p>
                <p>Account Number: {this.props.venueRegistrationForm.account_number}</p>
                <p>Pay by Credit Card: {this.props.venueRegistrationForm.pay_by_credit_card}</p>
              </div>
            </div>
            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Accounts & Delivery</h5>
                <p>Account Contact Name: {this.props.venueRegistrationForm.account_contact_name}</p>
                <p>Account Contact Email: {this.props.venueRegistrationForm.account_contact_email}</p>
                <p>Account Contact Number: {this.props.venueRegistrationForm.account_contact_number}</p>
                <p>Delivery Contact Name: {this.props.venueRegistrationForm.delivery_contact_name}</p>
                <p>Delivery Contact Number: {this.props.venueRegistrationForm.delivery_contact_number}</p>
              </div>
            </div>
            
            { this.renderDirectors() }

            <div className="card mb-3">
              <div className="card-body pb-1">
                <h5 className="card-title">Trade Reference</h5>
                <p>Trade Reference 1 Business Name: {this.props.venueRegistrationForm.trade_reference_1_business_name}</p>
                <p>Trade Reference 1 Contact Person: {this.props.venueRegistrationForm.trade_reference_1_contact_person}</p>
                <p>Trade Reference 1 Contact Number: {this.props.venueRegistrationForm.trade_reference_1_contact_number}</p>
              </div>
            </div>
          </div>
          <div className="form-group">
            <label className="font-weight-bold">Terms & Conditions</label>
            <textarea className="form-control mb-2" id="textareaTermsConditions" rows="8" 
              defaultValue="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
              Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
              Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus">
            </textarea>
          </div>
          <div className="form-group">
            <label className="font-weight-bold required">Credit Limit Request</label>
            <div className="form-check">
              <input type="checkbox" className="form-check-input" id="checkboxTermsConditions" />
              <label className="form-check-label" htmlFor="checkboxTermsConditions">
                I have read and understand the terms and conditions
              </label>
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-primary text-uppercase" onClick={this.saveForm}>Save</button>
          </div>
        </form>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VRInfoReview = connect(
  mapStateToProps,
  mapDispatchToProps
)(VRInfoReview)
