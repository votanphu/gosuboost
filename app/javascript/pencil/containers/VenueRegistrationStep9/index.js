import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import Geosuggest from 'react-geosuggest'
import StreetType from '../../components/StreetType'
import {
  setVenueApplicationForm,
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm
} from '../VenueRegistration/actions'
import { STATE_OPTIONS } from '../../constants'
import '../VenueRegistration/index.scss'

const DEFAULT_DIRECTORS = []

const inputMask = () => {
  setTimeout(() => {
    $('.inputDirectorPostcode').inputmask('9999')
  }, 200)
}
class VenueRegistrationStep9 extends React.Component {
  constructor(props) {
    super(props)

    let personal_guarantees = {
      personal: [
        {
          firstname: '',
          lastname: '',
          has_different_postal_address: false
        }
      ],
      directors: [{}]
    }

    let selectedEmail = [];

    if (this.props.venueRegistrationForm && this.props.venueRegistrationForm.personal_guarantees) {
      personal_guarantees = this.props.venueRegistrationForm.personal_guarantees

      if (this.props.venueRegistrationForm.is_director_guarantees == true && personal_guarantees.directors.length > 1) {
        selectedEmail = personal_guarantees.directors.map(d => d.email);
      }
    } else {
      this.props.venueRegistrationForm.personal_guarantees = personal_guarantees
    }

    this.state = {
      personal_guarantees: personal_guarantees,
      currentStep: 9,
      totalSteps: 9,
      title: 'Venue Registration - Step 9 of 9',
      selectedEmail: selectedEmail
    }
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-8')
  }

  saveForm = () => {
    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }

    this.props.history.push('save-form')
  }

  addMore = () => {
    let {personal_guarantees} = this.state
    personal_guarantees.personal.push({
      firstname: '',
      lastname: ''
    })
    this.setState({
      personal_guarantees: personal_guarantees
    })

    this.props.venueRegistrationForm.personal_guarantees = personal_guarantees
    inputMask();
  }

  handleChange = (e) => {
    let personal_guarantees = this.state.personal_guarantees
    let personal = personal_guarantees.personal
    e.target.dataset.name == 'firstname' ? personal[e.target.dataset.index].firstname = e.target.value : 0
    e.target.dataset.name == 'lastname' ? personal[e.target.dataset.index].lastname = e.target.value : 0
    e.target.dataset.name == 'home_address_line_1' ? personal[e.target.dataset.index].home_address_line_1 = e.target.value : 0
    e.target.dataset.name == 'home_unit_number' ? personal[e.target.dataset.index].home_unit_number = e.target.value : 0
    e.target.dataset.name == 'home_street_number' ? personal[e.target.dataset.index].home_street_number = e.target.value : 0
    e.target.dataset.name == 'home_street_name' ? personal[e.target.dataset.index].home_street_name = e.target.value : 0
    e.target.dataset.name == 'home_street_type' ? personal[e.target.dataset.index].home_street_type = e.target.value : 0
    e.target.dataset.name == 'home_suburb' ? personal[e.target.dataset.index].home_suburb = e.target.value : 0
    e.target.dataset.name == 'home_state' ? personal[e.target.dataset.index].home_state = e.target.value : 0
    e.target.dataset.name == 'home_postcode' ? personal[e.target.dataset.index].home_postcode = e.target.value : 0
    e.target.dataset.name == 'postal_address_line_1' ? personal[e.target.dataset.index].postal_address_line_1 = e.target.value : 0
    e.target.dataset.name == 'postal_unit_number' ? personal[e.target.dataset.index].postal_unit_number = e.target.value : 0
    e.target.dataset.name == 'postal_street_number' ? personal[e.target.dataset.index].postal_street_number = e.target.value : 0
    e.target.dataset.name == 'postal_street_name' ? personal[e.target.dataset.index].postal_street_name = e.target.value : 0
    e.target.dataset.name == 'postal_street_type' ? personal[e.target.dataset.index].postal_street_type = e.target.value : 0
    e.target.dataset.name == 'postal_suburb' ? personal[e.target.dataset.index].postal_suburb = e.target.value : 0
    e.target.dataset.name == 'postal_state' ? personal[e.target.dataset.index].postal_state = e.target.value : 0
    e.target.dataset.name == 'postal_postcode' ? personal[e.target.dataset.index].postal_postcode = e.target.value : 0
    e.target.dataset.name == 'mobile' ? personal[e.target.dataset.index].mobile = e.target.value : 0
    e.target.dataset.name == 'phone' ? personal[e.target.dataset.index].phone = e.target.value : 0
    e.target.dataset.name == 'email' ? personal[e.target.dataset.index].email = e.target.value : 0
    e.target.dataset.name == 'has_different_postal_address' ? personal[e.target.dataset.index].has_different_postal_address = e.target.checked : 0

    this.setState({
      personal_guarantees: personal_guarantees
    })

    this.props.venueRegistrationForm.personal_guarantees = personal_guarantees
    this.validateInput(e.target, e.target.dataset.validations)
    if(e.target.dataset.name == 'has_different_postal_address' && e.target.checked === true) {
      inputMask();
    }
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // Validate email
    if (validations.indexOf('email') !== -1) {
      if (validations.indexOf('email') && (/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i).test(value)) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is not valid email.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    if (validations.indexOf('digits-eq-4') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 4) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Must enter 4 numbers')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // alphanumeric
    if (validations.indexOf('alphanumeric') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')
      let isInvalid = temp && /[^a-zA-Z0-9]/i.test(value);

      if (!isInvalid) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Alphanumberic')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  handleSelectDirector(value, index) {
    let { personal_guarantees } = this.state;
    let director_guarantees = personal_guarantees.directors
    let { stateData } = this.props;
    let directors = stateData.venueRegistrationForm && stateData.venueRegistrationForm.directors || DEFAULT_DIRECTORS
    director_guarantees[index] = directors.filter(d => d.email == value)[0] || {};
    director_guarantees[index].is_director_guarantee = true
    let selectedEmail = director_guarantees.map(d => d.email)

    this.setState({
      personal_guarantees: personal_guarantees,
      selectedEmail: selectedEmail || []
    })

    this.props.venueRegistrationForm.personal_guarantees = personal_guarantees
  }

  setGuaranteesType(is_director_guarantees) {
    let { personal_guarantees } = this.state
    this.props.venueRegistrationForm.is_director_guarantees = is_director_guarantees
    this.setState({ personal_guarantees })
    this.props.venueRegistrationForm.personal_guarantees = personal_guarantees
    inputMask();
  }

  renderSelectGuarantee() {
    let {personal_guarantees} = this.state
    let director_guarantees = personal_guarantees.directors
    let {stateData} = this.props
    let directors = stateData.venueRegistrationForm && stateData.venueRegistrationForm.directors || DEFAULT_DIRECTORS

    return  (
      <div style={{marginTop: 10 }}>
        <p>Select each director who will act as a personal guarantee</p>
        {
          director_guarantees.map((director, index) => {
            return (
              <div style={{marginBottom: 30}} key={index}>
                <div className="form-row justify-content-md-center">
                  <div className="col-md-6">
                    <div className="form-group">
                      {/* <label htmlFor="selectOperatingStructure" className="font-weight-bold required">Operating Structure</label> */}
                      <select id="selectDirector" className="form-control"
                        defaultValue={director.email} required
                        onChange={(e) => {
                          this.handleSelectDirector(e.target.value, index)
                          // this.validEvent()
                        }}
                        required >
                        <option key="-1" value=''>Select one from the list</option>
                        {directors.filter(
                                      (d, idx) =>  d.email == director.email || (this.state.selectedEmail.indexOf(d.email) == -1)
                                  ).map(d =>
                                    <option key={d.email} value={d.email}>
                                      {`${d.firstname} ${d.lastname}`}
                                    </option>
                                  )
                        }
                      </select>
                    </div>
                  </div>
                </div>
                {!(Object.keys(director).length === 0) &&
                  <div className="col-md-12">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5 pull-left">Name</strong>
                          <span className="col-md-7 pull-left">{`${director.firstname} ${director.lastname}`}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Birthday</strong>
                          <span className="col-md-7">{director.date_of_birth}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Email</strong>
                          <span className="col-md-7">{director.email}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Phone</strong>
                          <span className="col-md-7">{director.phone}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Address 1</strong>
                          <span className="col-md-7">{director.address_line_1}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Address 2</strong>
                          <span className="col-md-7">{director.address_line_2}</span></div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">Drivers license number</strong>
                          <span className="col-md-7">{director.drivers_license_number}</span>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="row">
                          <strong className="col-md-5">License expiry</strong>
                          <span className="col-md-7">{director.license_expiry}</span></div>
                      </div>
                    </div>
                  </div>
                }
              </div>
            )
          })
        }
        <div className="text-center">
          <button type="button" className="btn btn-link"
            disabled={director_guarantees.filter(d => Object.keys(d).length === 0).length != 0}
            onClick={() => {
              director_guarantees.push({})
              this.setState({director_guarantees})
            }}
            >
            Add another director
          </button>
        </div>
        <div className="gos-form-group">
            <label className="font-weight-bold required">Credit Request Terms & Conditions</label>
            <textarea className="form-control mb-2" id="textareaTermsConditions" rows="8"
              defaultValue="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
              Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus
              Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus">
            </textarea>
            <div className="form-check">
              <input type="checkbox" className="form-check-input"
                id="checkboxTermsConditions"
                // onChange={(e) => this.validEvent() }
                />
              <label className="form-check-label" htmlFor="checkboxTermsConditions">
                I have read and understand the terms and conditions
              </label>
            </div>
          </div>
      </div>
    )
  }

  renderAddress(index, addressType, personal_guarantee, prefix) {
    return (
      <div className="gos-form-group">
        <label className="font-weight-bold required">Address ({addressType})</label>
        <div className="form-row">
          <div className="form-group col-md-8">
            <input
              placeholder="Building/Property Name"
              className="form-control"
              id={"inputTradingAddressLine1_" + index}
              data-index={index}
              data-name={prefix + "address_line_1"}
              // data-validations={[]}
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              defaultValue={personal_guarantee[prefix + 'address_line_1']}
              required
            />
            <small className="font-weight-bold">Building/Property Name</small>
            <div className="invalid-feedback"></div>
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-2">
            <input type="text" className="form-control"
              id={"unitNumber_"+index} placeholder="Unit number"
              data-validations={['alphanumeric']}
              data-index={index}
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              defaultValue={personal_guarantee[prefix + 'unit_number']}
              data-name={prefix + "unit_number"}
              required />
            <small className="font-weight-bold">Unit Number</small>
            <div className="invalid-feedback"></div>
          </div>
          <div className="form-group col-md-2">
            <input type="text" className="form-control"
              id="streetNumber" placeholder="Street number"
              data-validations={['required', 'alphanumeric']}
              data-index={index}
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              data-name={prefix + "street_number" }
              defaultValue={personal_guarantee[prefix + 'street_number']} required />
            <small className="font-weight-bold required">Street Number</small>
            <div className="invalid-feedback"></div>
          </div>
          <div className="form-group col-md-3">
            <input type="text" className="form-control"
              id="streetName" placeholder="Street name"
              data-validations={['required', 'alphanumeric']}
              data-index={index}
              data-name={prefix + "street_name" }
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              defaultValue={personal_guarantee[prefix + 'street_name']} required />
            <small className="font-weight-bold required">Street Name</small>
            <div className="invalid-feedback"></div>
          </div>
          <div className="form-group col-md-3">
            <StreetType
              data-validations={['required']}
              data-index={index}
              data-name={prefix + "street_type" }
              onChange={this.handleChange}
              defaultValue={personal_guarantee[prefix + 'street_type']}
              />
            <small className="font-weight-bold required">Street Type</small>
            <div className="invalid-feedback"></div>
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-4">
            <input type="text" className="form-control"
              id={"inputTradingSuburb_"+index} placeholder="Suburb"
              onChange={this.handleChange}
              onBlur={this.handleBlur}
              defaultValue={personal_guarantee[prefix + 'suburb']}
              data-index={index}
              data-name={prefix + "suburb"}
              data-validations={['required']} />
            <small className="font-weight-bold required">Suburb</small>
            <div className="invalid-feedback"></div>
          </div>
          <div className="form-group col-md-2">
            <select
              className="form-control"
              id={"inputState_"+index}
              name="inputTradingState"
              data-validations={['required']}
              data-index={index}
              data-name={prefix + "state" }
              onChange={this.handleChange}
              defaultValue={personal_guarantee[prefix + 'state']}>
              <option value="">Select one from the list</option>
              {
                STATE_OPTIONS.map(opt => <option key={opt} value={opt}>{opt}</option>)
              }
            </select>
            <small className="font-weight-bold required">State</small>
            <div className="invalid-feedback"></div>
          </div>
          <div className="form-group col-md-2">
            <div className="form-group">
              <input type="text" className="form-control inputDirectorPostcode"
                id={'inputDirectorPostcode_' + index} placeholder="Postcode"
                onChange={this.handleChange}
                onBlur={this.handleBlur}
                defaultValue={personal_guarantee[prefix + 'postcode']}
                data-index={index}
                data-name={prefix + "postcode"}
                data-validations={['required', 'digits-eq-4']}
                required />
              <small className="font-weight-bold required">Postcode</small>
              <div className="invalid-feedback"></div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  render() {
    let {personal_guarantees} = this.state
    let {is_director_guarantees} = this.props.venueRegistrationForm
    return (
      <Master
        title={this.state.title}
        loading={this.props.stateData.loading}
        message={this.props.stateData.message}
        currentStep={this.state.currentStep}
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        <h3 className="step-title mb-6">Personal Guarantee & Indemnity</h3>
        <form>
          <p>Will one or more of the registered directors act as the personal guarantee?</p>
          <div className="action text-center">
            <button type="button"
              className={'btn action-button ' + (is_director_guarantees === true ? 'btn-primary' : 'btn-outline-primary')}
              onClick={() => this.setGuaranteesType(true)}
              >Yes</button>
            <button type="button"
              className={'btn action-button ' + (is_director_guarantees === false ? 'btn-primary' : 'btn-outline-primary')}
              onClick={() => this.setGuaranteesType(false)}
              >No</button>
          </div>
          {/* YES */}
          {is_director_guarantees === true &&
            this.renderSelectGuarantee()
          }
          {/* NO */}
          {is_director_guarantees === false &&
            <div>
              <h5 className="font-weight-bold mb-2">Personal Guarantees</h5>
              {personal_guarantees.personal.map((personal_guarantee, index) => {
                let num_index = index + 1

                return (
                  <div className="gos-form-group" key={index} data-index={index}>
                    <div className="gos-form-group">
                      <label className="font-weight-bold required">Personal Guarantee {num_index}</label>
                      <div className="form-row">
                        <div className="form-group col-md-3">
                          <input type="text" className="form-control"
                            id={'inputPersonalGuaranteeFirstName' + index} placeholder="First Name"
                            onChange={this.handleChange}
                            defaultValue={personal_guarantee.firstname}
                            data-index={index}
                            data-name="firstname" />
                          <small className="font-weight-bold">First Name</small>
                        </div>
                        <div className="form-group col-md-3">
                          <input type="text" className="form-control"
                            id={'inputPersonalGuaranteeLastName' + index} placeholder="Last Name"
                            onChange={this.handleChange}
                            defaultValue={personal_guarantee.lastname}
                            data-index={index}
                            data-name="lastname" />
                          <small className="font-weight-bold">Last Name</small>
                        </div>
                      </div>
                    </div>
                    {/* Personal guarantee Home */}
                    {
                      this.renderAddress(index, 'Home', personal_guarantee, 'home_')
                    }
                    {/* Personal guarantee Postal */}
                    <div className="gos-form-group">
                      {/* Check box */}
                      <div className="form-check" style={{marginBottom: 10}}>
                        <input type="checkbox" className="form-check-input"
                          id={"checkboxHasPostalAdress" + index}
                          checked={personal_guarantee.has_different_postal_address || false}
                          onChange={this.handleChange}
                          data-index={index}
                          data-name="has_different_postal_address"
                        />
                        <label className="form-check-label" htmlFor={"checkboxHasPostalAdress" + index}>
                          My postal address is different to my residential address
                        </label>
                      </div>
                      {personal_guarantee.has_different_postal_address === true &&
                        this.renderAddress(index, 'Postal', personal_guarantee, 'postal_')
                      }
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <label htmlFor={'inputMobile' + index} className="font-weight-bold required">
                            Mobile Number
                          </label>
                          <input type="text" className="form-control"
                            id={'inputMobile' + index} placeholder="Mobile Number"
                            onChange={this.handleChange}
                            defaultValue={personal_guarantee.mobile}
                            data-index={index}
                            data-name="mobile" required />
                        </div>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <label htmlFor={'inputPhone' + index} className="font-weight-bold">
                            Home Phone
                          </label>
                          <input type="text" className="form-control"
                            id={'inputPhone' + index} placeholder="Home Phone"
                            onChange={this.handleChange}
                            defaultValue={personal_guarantee.phone}
                            data-index={index}
                            data-name="phone" />
                        </div>
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-4">
                        <div className="form-group">
                          <label htmlFor={'inputEmail' + index} className="font-weight-bold required">Email</label>
                          <input className="form-control"
                            type="email" placeholder="Email"
                            id={'inputEmail' + index}
                            onChange={this.handleChange}
                            defaultValue={personal_guarantee.email}
                            data-index={index}
                            data-name="email" required />
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
              <div className="actions mt-3 pt-3 text-right">
                <button type="button" className="btn btn-primary text-uppercase" onClick={this.addMore}>Add More</button>
              </div>
            </div>
          }
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm}>Save and Continue Later</button>
          </div>
        </form>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data))
  }
}

export default VenueRegistrationStep9 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep9)
