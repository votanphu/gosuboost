import Formsy from 'formsy-react'
import React from 'react'
import { connect } from 'react-redux'
import Master from '../../components/Master'
import GosInput from '../../components/GosInput'
import Geosuggest from 'react-geosuggest'
import StreetType from '../../components/StreetType'
import { 
  setVenueApplicationForm, 
  postVenueApplicationForm,
  getVenueApplicationForm,
  updateVenueApplicationForm,
  searchABN
} from '../VenueRegistration/actions'
import '../VenueRegistration/index.scss'
import {STATE_OPTIONS} from '../../constants/index'

class VenueRegistrationStep3 extends React.Component {
  constructor(props) {
    super(props)
    this.submitEvent = this.submitEvent.bind(this)
    this.validSubmitEvent = this.validSubmitEvent.bind(this)
    this.invalidSubmitEvent = this.invalidSubmitEvent.bind(this)
    this.disableButton = this.disableButton.bind(this)
    this.enableButton = this.enableButton.bind(this)
    this.validEvent = this.validEvent.bind(this)
    this.invalidEvent = this.invalidEvent.bind(this)
    this.checkAbnClick = this.checkAbnClick.bind(this)

    this.state = {
      currentStep: 3,
      totalSteps: 9,
      title: 'Venue Registration - Step 3 of 9',
      canSubmit: false,
      errorMessageRegisteredStreetType: "",
      classRegisteredStreetType: "form-control",
      abnActive: "",
      abnMessage: "",
      showCompanyName: false
    }
  }

  componentDidMount() {
    $('#inputRegisteredPostcode').inputmask('9999')
  }

  nextClick = () => {
    if (!this.validateForm()) {
      return
    }

    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-4')
  }

  previousClick = () => {
    this.props.setVenueApplicationForm(this.props.venueRegistrationForm)
    this.props.history.push('step-2')
  }

  saveForm = () => {
    if (!this.validateForm()) {
      return
    }

    if (this.props.venueRegistrationForm.id) {
      this.props.updateVenueApplicationForm(this.props.venueRegistrationForm)
    } else {
      this.props.postVenueApplicationForm(this.props.venueRegistrationForm)
    }
    
    this.props.history.push('save-form')
  }

  checkAbnClick = () => {
    let company_abn = this.props.venueRegistrationForm.company_abn.replace(/\s+/g, '')

    this.props.searchABN(company_abn).then(() => {
      if (this.props.abn.EntityName) {
        this.setState({ showCompanyName: true })
        this.inputCompanyName.props.setValue(this.props.abn.EntityName)
      } else {
        this.setState({ showCompanyName: false })
      }

      this.setState({ abnActive: this.props.abn.AbnStatus })
      this.setState({ abnMessage: this.props.abn.Message })
    })
  }

  disableButton() {
    this.setState({ canSubmit: false })
  }

  enableButton() {
    this.setState({ canSubmit: true })
  }

  handleChange = (e) => {
    let attributes = [
      'registered_unit_number', 'registered_street_number', 'registered_street_name', 'registered_state',
      'registered_postcode'
    ]
    
    if (attributes.indexOf(e.target.dataset.name) !== -1) {
      this.props.venueRegistrationForm[e.target.dataset.name] = e.target.value
      this.validateInput(e.target, e.target.dataset.validations)
    }
  }

  handleBlur = (e) => {
    this.handleChange(e)
  }

  validateInput(element, validations) {
    let wrapper = $(element).closest('.form-group')
    let specialObjects = ['Geosuggest']
    let value =  specialObjects.indexOf(element.dataset.object) !== -1 ? element.value : $(element).val()
    value = value ? value.trim() : ''
    validations = validations ? validations : []

    if (specialObjects.indexOf(element.dataset.object) !== -1) {
      wrapper = $('#' + element.dataset.id).closest('.form-group')
    }

    // Validate required
    if (validations.indexOf('required') !== -1) {
      if (value) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('This is required.')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    if (validations.indexOf('digits-eq-4') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')

      if (temp && temp.length === 4) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else if (temp) {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Must enter 4 numbers')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    // alphanumeric
    if (validations.indexOf('alphanumeric') !== -1) {
      let temp = value.replace(/\s+|_|\(|\)/g, '')
      let isInvalid = temp && /[^a-zA-Z0-9]/i.test(value);

      if (!isInvalid) {
        wrapper.find('.invalid-feedback').removeClass('d-block').text('')
        wrapper.find('input').removeClass('is-invalid')
      } else {
        wrapper.find('.invalid-feedback').addClass('d-block').text('Alphanumberic')
        wrapper.find('input').addClass('is-invalid')
        this.disableButton()
        return false
      }
    }

    this.enableButton()
    return true
  }

  validSubmitEvent(model) {
    this.saveForm()
  }

  submitEvent(model) {
  }

  invalidSubmitEvent(model) {
  }

  validEvent() {
    let canSubmit = this.refs.form.state.isValid &&  $('#selectOperatingStructure').val()
      && this.props.venueRegistrationForm.registered_street_type

      if(this.props.venueRegistrationForm.registered_street_type) {
        this.setState({ 
          errorMessageRegisteredStreetType: "",
          classRegisteredStreetType: "form-control"
        })
      } else {
        this.setState({ 
          errorMessageRegisteredStreetType: "This is required.",
          classRegisteredStreetType: "form-control is-invalid"
        })
      }

    canSubmit ? this.enableButton() : this.disableButton()
  }

  invalidEvent() {
    this.disableButton()
  }

  validateForm() {
    var valid = true

    $('.form-control').each((index, element) => {
      $(element).focus()

      if ($(element).hasClass('datepicker')) {
        $('.datepicker-dropdown').hide()
      }
    })

    $('.form-control').each((index, element) => {
      if ($(element).hasClass('is-invalid')) {
        $(element).focus()
        valid = false
        return false
      }
    })

    return valid
  }

  render() {
    return (
      <Master 
        title={this.state.title} 
        loading={this.props.stateData.loading} 
        message={this.props.stateData.message}
        currentStep={this.state.currentStep} 
        totalSteps={this.state.totalSteps}
        stepTitle="Venue Registration"
        {...this.props} >
        {/* ABN has been found, please confirm the Company Name is correct */}
        <h3 className="step-title mb-6">Company</h3>
        <Formsy 
          onSubmit={this.submitEvent}
          onValidSubmit={this.validSubmitEvent} 
          onInvalidSubmit={this.invalidSubmitEvent}
          onValid={this.validEvent} 
          onInvalid={this.invalidEvent}
          ref="form" >
          <div>
            <label className="font-weight-bold required">Company ABN</label>
            <div className="form-row">
              <div className="form-group col-md-4">
                <GosInput 
                  wrapperClass=""
                  labelClass="font-weight-bold"
                  inputClass="form-control"
                  type="text"
                  id="inputCompanyAbn"
                  name="inputCompanyAbn"
                  placeholder="Company ABN"
                  textLabel=""
                  innerRef={(c) => { this.inputCompanyAbn = c }}
                  onChange={(e) => { this.props.venueRegistrationForm.company_abn = e.currentTarget.value }}
                  defaultValue={this.props.venueRegistrationForm.company_abn}
                  required />
                <small>{this.state.abnMessage}</small>
              </div>
              <div className="form-group col-md-4">
                <button type="button" className="btn btn-outline-primary text-uppercase" 
                  onClick={this.checkAbnClick}>Check ABN</button>
              </div>
            </div>
          </div>
          {
            this.state.showCompanyName &&
              <div className="form-row">
                <div className="form-group col-md-8">
                  <GosInput 
                    wrapperClass=""
                    labelClass="font-weight-bold"
                    inputClass="form-control"
                    type="text"
                    id="inputCompanyName"
                    name="inputCompanyName"
                    placeholder="Company Name"
                    textLabel="Company Name"
                    innerRef={(c) => { this.inputCompanyName = c }}
                    onChange={(e) => { this.props.venueRegistrationForm.company_name = e.currentTarget.value }}
                    defaultValue={this.props.venueRegistrationForm.company_name}
                    required />
                  <small>{this.state.abnActive}</small>
                </div>
              </div>
          }
          <div className="form-group">
            <div className="form-row">
              <div className="form-group col-md-8">
                <label htmlFor="selectOperatingStructure" className="font-weight-bold required">Operating Structure</label>
                <select id="selectOperatingStructure" className="form-control" 
                  onChange={(e) => { 
                    this.props.venueRegistrationForm.operating_structure = e.target.value 
                    this.validEvent()
                  }}
                  defaultValue={this.props.venueRegistrationForm.operating_structure} required >
                  <option value=''>Select one from the list</option>
                  <option value='PTY LTD'>PTY LTD</option>
                  <option value='PARTNERSHIP'>PARTNERSHIP</option>
                  <option value='SOLE TRADER'>SOLE TRADER</option>
                  <option value='CORPORATION AS TRUSTEE'>CORPORATION AS TRUSTEE</option>
                  <option value='Individual(s) as Trustee'>Individual(s) as Trustee</option>
                  <option value='Public Company'>Public Company</option>
                </select>
              </div>
            </div>
          </div>
          <div className="gos-form-group">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label className="font-weight-bold">Registered Address</label>
                <input 
                  placeholder="Building/Property Name" 
                  className="form-control"
                  id="inputRegisteredAddressLine1"
                  onChange={(e) => { 
                    this.props.venueRegistrationForm.registered_address_line_1 = e.target.value
                    // this.validEvent()
                  }}
                  onBlur={(e) => { 
                    this.props.venueRegistrationForm.registered_address_line_1 = e.target.value
                    // this.validEvent()
                  }}
                  defaultValue={this.props.venueRegistrationForm.registered_address_line_1} 
                  required
                />
                <small className="font-weight-bold">Building/Property Name</small>
                <div className="invalid-feedback"></div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="unitNumber" placeholder="Unit number"
                  data-validations={['alphanumeric']}
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.registered_unit_number} 
                  data-name="registered_unit_number" 
                  required />
                <small className="font-weight-bold">Unit Number</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="streetNumber" placeholder="Street number"
                  data-validations={['required', 'alphanumeric']}
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  data-name="registered_street_number" 
                  defaultValue={this.props.venueRegistrationForm.registered_street_number} required />
                <small className="font-weight-bold required">Street Number</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-3">
                <input type="text" className="form-control" 
                  id="streetName" placeholder="Street name"
                  data-validations={['required', 'alphanumeric']}
                  data-name="registered_street_name" 
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.registered_street_name} required />
                <small className="font-weight-bold required">Street Name</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-3">
                <StreetType
                  onChange={e => {
                    this.props.venueRegistrationForm.registered_street_type = e.target.value
                    this.validEvent();
                  }}
                  defaultValue={this.props.venueRegistrationForm.registered_street_type}
                  className={this.state.classRegisteredStreetType}
                  />
                <small className="font-weight-bold required">Street Type</small>
                <div className="invalid-feedback">{this.state.errorMessageRegisteredStreetType}</div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <GosInput 
                  wrapperClass=""
                  inputClass="form-control"
                  type="text"
                  id="inputRegisteredSuburb"
                  name="inputRegisteredSuburb"
                  placeholder="Suburb"
                  smallClass="font-weight-bold"
                  textSmall="Suburb"
                  innerRef={(c) => { this.inputRegisteredSuburb = c }}
                  onChange={(e) => { this.props.venueRegistrationForm.registered_suburb = e.currentTarget.value }}
                  defaultValue={this.props.venueRegistrationForm.registered_suburb}
                  required />
              </div>
              <div className="form-group col-md-2">
                <select 
                  className="form-control"
                  id="inputRegisteredState"
                  name="inputRegisteredState"
                  data-validations={['required']}
                  data-name="registered_state" 
                  onChange={this.handleChange}
                  defaultValue={this.props.venueRegistrationForm.registered_state}>
                  <option value="">Select one from the list</option>
                  {
                    STATE_OPTIONS.map(opt => <option key={opt} value={opt}>{opt}</option>)
                  }
                </select>
                <small className="font-weight-bold required">State</small>
                <div className="invalid-feedback"></div>
              </div>
              <div className="form-group col-md-2">
                <input type="text" className="form-control" 
                  id="inputRegisteredPostcode" 
                  placeholder="Postcode"
                  data-validations={['required', 'digits-eq-4']}
                  data-name="registered_postcode" 
                  onChange={this.handleChange}
                  onBlur={this.handleBlur}
                  defaultValue={this.props.venueRegistrationForm.registered_postcode} required />
                <small className="font-weight-bold required">Postcode</small>
                <div className="invalid-feedback"></div>
              </div>
            </div>
          </div>
          <div className="actions border-top-1 border-top-dotted mt-3 pt-3">
            <button type="button" className="btn btn-outline-primary text-uppercase" onClick={this.previousClick}>Previous</button>
            <button type="button" className="btn btn-primary text-uppercase ml-3" onClick={this.nextClick} disabled={!this.state.canSubmit}>Next</button>
            <button type="button" className="btn btn-link" onClick={this.saveForm} disabled={!this.state.canSubmit}>Save and Continue Later</button>
          </div>
        </Formsy>
      </Master>
    )
  }
}

function mapStateToProps(state) {
  return {
    stateData: state.venueRegistrationReducer,
    venueRegistrationForm: state.venueRegistrationReducer.venueRegistrationForm,
    abn: state.venueRegistrationReducer.abn
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setVenueApplicationForm: (data) => dispatch(setVenueApplicationForm(data)),
    postVenueApplicationForm: (data) => dispatch(postVenueApplicationForm(data)),
    getVenueApplicationForm: (token) => dispatch(getVenueApplicationForm(token)),
    updateVenueApplicationForm: (data) => dispatch(updateVenueApplicationForm(data)),
    searchABN: (abn) => dispatch(searchABN(abn))
  }
}

export default VenueRegistrationStep3 = connect(
  mapStateToProps,
  mapDispatchToProps
)(VenueRegistrationStep3)
