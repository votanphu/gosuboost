import React from 'react'
import ReactDOM from 'react-dom'
import App from 'pencil'

const pencil_root = document.querySelector('#pencil_root')
ReactDOM.render(<App />, pencil_root)
