# The User class is responsible for the  the management of all type of users.
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :registerable,
         :confirmable, :lockable, :timeoutable,
         :recoverable, :rememberable, :trackable, :validatable,
         :two_factor_authenticatable,
         otp_secret_encryption_key: ENV['TWO_FACTOR_SECRET']

  has_many :directors, dependent: :destroy
  has_many :venue_application_forms, through: :directors, dependent: :destroy
  has_many :personal_guarantees, dependent: :destroy
  has_many :email_templates, foreign_key: 'created_by_id', class_name: 'EmailTemplate', dependent: :destroy
  has_many :email_themes, foreign_key: 'created_by_id', class_name: 'EmailTheme', dependent: :destroy
  has_many :apply_supplier_credit_forms, foreign_key: 'created_by_id', class_name: 'ApplySupplierCreditForm', dependent: :destroy

  validates :email, format: { with: Const::VALID_EMAIL_REGEX, on: :create }
  validate :password_complexity

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" },
    processors: [:cropper],
    default_url: '/assets/default-avatar.png',
    url: "/media/users/:id/:style/:hash.:extension",
    path: ":rails_root/public/media/users/:id/:style/:hash.:extension",
    hash_secret: "G0sl3tm31n"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :processed

  before_save :downcase_fields
  after_update :reprocess_avatar, if: :cropping?

  def get_setting_password_url(options = {})
    url = "#{$prod_root_url}/users/set-password?user_token=#{id}"
    url = "#{url}&form_token=#{options[:form_token]}" if options[:form_token].present?
    url = "#{url}&ascf_token=#{options[:ascf_token]}" if options[:ascf_token].present?
    url
  end

  def is_admin?
    self.role_to_a.include?(Const::ROLES[:admin])
  end

  def is_supplier?
    self.role_to_a.include?(Const::ROLES[:supplier])
  end

  def is_director?
    self.role_to_a.include?(Const::ROLES[:director])
  end

  def is_personal_guarantee?
    self.role_to_a.include?(Const::ROLES[:personal_guarantee])
  end

  def is_venue_user?
    self.is_director? || self.is_personal_guarantee?
  end

  def is_only_personal_guarantee?
    self.role == Const::ROLES[:personal_guarantee]
  end

  def role_to_a
    self.role.split(',')
  end

  def add_role(new_role)
    self.role = (role_to_a << new_role).uniq.join(',')
    self.save
  end

  def is_only_supplier?
    role == Const::ROLES[:supplier]
  end

  def remove_role(role)
    self.role = (role_to_a.reject { |item| item == role }).join(',')
    self.save
  end

  def remove_supplier_role
    remove_role(Const::ROLES[:supplier])
    self.parent_ids = ""
    self.save
  end

  def friendly_name
    return self.name if self.name.present?
    value = self.email
    value = "#{self.firstname} #{self.lastname}" if self.firstname.present? || self.lastname.present?
    value
  end

  def director_from_vaf(vaf_id)
    vaf = VenueApplicationForm.includes(:venue_application_form_directors).find(vaf_id)
    vaf_director_ids = vaf.venue_application_form_directors.map(&:director_id)
    directors.where(id: vaf_director_ids).first
  end

  def get_venue_user_venue_application_forms
    venue_forms = []

    directors.includes(:venue_application_form_directors,
      :venue_application_forms).each do |director|
      venue_forms << director.venue_application_forms.order(object_index: :desc)
    end

    personal_guarantees.includes(:apply_supplier_credit_personal_guarantees,
      :apply_supplier_credit_forms).each do |pg|
      venue_forms << pg.get_venue_application_forms
    end

    venue_forms.flatten!
    venue_forms.uniq! { |f| f[:id] }
    venue_forms
  end

  # Check if the venue user has any venues
  def has_venues?
    return self.get_venue_user_venue_application_forms.present?
  end

  # ascf: apply_supplier_credit_forms
  def all_apply_supplier_credit_forms
    venue_forms_ids = get_venue_user_venue_application_forms.map(&:id)

    if is_only_personal_guarantee?
      ascf_ids = personal_guarantees.includes(:apply_supplier_credit_personal_guarantees)
        .pluck(:apply_supplier_credit_form_id)
      ApplySupplierCreditForm.where(id: ascf_ids)
    else
      ApplySupplierCreditForm.where(vaf_id: venue_forms_ids)
    end
  end

  # Check whether user is exist if not create one
  def self.create_user(email, role, user_attrs)
    new_password = SecureRandom.base64(16)
    temp_name = email.split("@")[0]
    firstname = user_attrs['firstname'].present? ? user_attrs['firstname'] : temp_name
    lastname = user_attrs['lastname'].present? ? user_attrs['lastname'] : ""

    user_attributes = {
      email: email,
      password: new_password,
      password_confirmation: new_password,
      role: role,
      firstname: firstname,
      lastname: lastname,
      name: "#{firstname} #{lastname}"
    }

    user = User.new(user_attributes)
    user.skip_confirmation!
    user.save!
    return user
  end

  def self.all_admin
    User.where("role LIKE '%ADMIN%'")
  end

  # Get the list ids from supplier users
  def self.get_supplier_user_ids(user_id)
    User.where("parent_ids like ? or id = ?", "#{sanitize_sql_like(user_id)}%", user_id).ids
  end

  # Get the list supplier users
  def self.get_supplier_users(user_id)
    supplier_user_ids = get_supplier_user_ids(user_id)
    return User.where(id: supplier_user_ids)
  end

  def root_parent_id
    parent_ids.split("|").first
  end

  # Set supplier login
  def supplier
    supplier = Supplier.where(user_id: id).first
    return supplier if supplier.present?
    Supplier.where(user_id: root_parent_id).first
  end

  # Override send mail of devise, force send via deliver_later, using queue
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  # vaf: Venue Application Form
  # ascf: Apply Supplier Credit Form
  def vafs_has_not_ascf
    vaf_ids = get_venue_user_venue_application_forms.map(&:id)
    vaf_ids_has_ascf = all_apply_supplier_credit_forms.map(&:vaf_id)
    vaf_ids_has_not_ascf = vaf_ids - vaf_ids_has_ascf
    VenueApplicationForm.where(id: vaf_ids_has_not_ascf)
  end

  # vaf: Venue Application Form
  # ascf: Apply Supplier Credit Form
  def vafs_valid_to_create_ascf
    vaf_ids = get_venue_user_venue_application_forms.map(&:id)
    vaf_ids_has_ascf = all_apply_supplier_credit_forms.map(&:vaf_id)
    vaf_ids_invalid_has_ascf = vaf_ids_has_ascf.reject { |vaf_id| Supplier.get_supplier_valid_for_vaf(vaf_id).count > 0 }
    vaf_ids_has_not_ascf = vaf_ids - vaf_ids_invalid_has_ascf
    VenueApplicationForm.where(id: vaf_ids_has_not_ascf)
  end

  def default_avatar
    avatar.url(:medium) || avatar.default_url
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end

  def delete_avatar
    self.avatar = nil
    self.save
  end

  def avatar_geometry(style = :medium)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(avatar.path(style))
  end

  # Password complexity requirement not met
  def password_complexity
    if password.present? && role == "ADMIN"
      if !password.match(/^(?=.*[a-z])/)
         errors.add :password, "requires at least one low case character"
      end

      if !password.match(/^(?=.*[A-Z])/)
        errors.add :password, "requires at least one upper case character"
      end

      if !password.match(/^(?=.*[! @ # $ % ^ & * ( ) _ - + = ])/)
        errors.add :password, "requires at least one special character"
      end

      if !password.match(/^(?=.*[1-9])/)
        errors.add :password, "requires at least one number"
      end
    end
  end

  def self.user_by_email(email)
    User.where(email: email).first
  end

  # Create new user
  def self.new_user(options = {})
    user = User.user_by_email(options[:email])
    return user if user.present?
    new_password = SecureRandom.base64(16)

    user_attributes = {
      name: options[:name],
      email: options[:email],
      password: new_password,
      password_confirmation: new_password,
      role: options[:role]
    }

    user = User.new(user_attributes)
    user.skip_confirmation! if options[:is_skip_confirmation].present?
    user.save!
    user
  end

  def activate_otp
    self.otp_required_for_login = true
    self.otp_secret = unconfirmed_otp_secret
    self.unconfirmed_otp_secret = nil
    save!
  end

  # Create new children user
  def self.new_children_user(options = {})
    user = User.user_by_email(options[:email])

    if user.blank?
      new_password = SecureRandom.base64(16)

      user_attributes = {
        firstname: options['firstname'],
        lastname: options['lastname'],
        name: "#{options['firstname']} #{options['lastname']}",
        email: options['email'],
        password: new_password,
        password_confirmation: new_password,
        role: 'SUPPLIER',
        parent_ids: options[:parent_user_id]
      }

      user = User.new(user_attributes)
    else
      if user[:parent_ids].blank?
        user[:parent_ids] = options[:parent_user_id]
        user.add_role('SUPPLIER')
      end
    end

    user.skip_confirmation! if options[:is_skip_confirmation]
    user.save!
    user
  end

  # Check exist parent id
  def exist_parent_id(parent_user_id)
    parent_ids.include? parent_user_id
  end

  def self.validate_user_opt(user, password)
    return user.present? && user.valid_password?(password) && user.role == 'ADMIN'
  end

  def update_otp_secret
    self.unconfirmed_otp_secret = User.generate_otp_secret
    self.save!
  end

  private
    def reprocess_avatar
      return if processed
      avatar.reprocess!
    end
end
