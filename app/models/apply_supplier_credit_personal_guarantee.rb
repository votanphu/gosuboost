class ApplySupplierCreditPersonalGuarantee < ApplicationRecord
  # Attribute Encrypted
  # attr_encrypted :signature_image, key: :attribute_encrypted_key
  # attr_encrypted :signed_at, key: :attribute_encrypted_key
  # signed_ip
  # form_data

  belongs_to :apply_supplier_credit_form
  belongs_to :personal_guarantee

  def set_signature(signature_image, remote_ip, request_latitude, request_longitude)
    self.signature_image = signature_image
    self.is_signed = true
    self.signed_at = DateTime.now
    self.signed_ip = remote_ip
    self.latitude  = request_latitude
    self.longitude = request_longitude
  end
end
