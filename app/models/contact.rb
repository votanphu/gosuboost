class Contact < ApplicationRecord
  before_save :downcase_fields

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, presence: true, length: { minimum: 10 }, format: { with: Const::VALID_EMAIL_REGEX }
  validates :subject, presence: true
  validates :message, presence: true
end
