class TradeReference < ApplicationRecord
  belongs_to :ascf, class_name: "ApplySupplierCreditForm"

  def self.trade_reference_attributes
    %w{business_name contact_person contact_number}
  end

  def has_missing_information?
    self.attributes.values_at(*TradeReference.trade_reference_attributes).any? { |value| value.blank? }
  end

  # Validate present value, remove if invalid
  def self.valid_trade_references_attributes(trade_references_attrs, ascf, errors_report)
    return trade_references_attrs if trade_references_attrs["contact_number"].blank?
    trade_references_attrs["contact_number"] = trade_references_attrs["contact_number"].gsub(/\s+/, ' ').strip

    identify = trade_references_attrs["contact_number"]
    identify = trade_references_attrs["contact_person"] if trade_references_attrs["contact_person"].present?
    identify = trade_references_attrs["business_name"] if trade_references_attrs["business_name"].present?

    unless CommonLib.validate_phoneAU(trade_references_attrs["contact_number"]) || CommonLib.validate_mobileAU(trade_references_attrs["contact_number"])
      trade_references_attrs["contact_number"] = nil
      errors_report[:invalid_error] << "#{ascf.vaf[:company_abn]} -
                                        #{ascf.supplier[:abn]} -
                                        Invalid trade_reference[#{identify}][contact_number]"
    end

    return trade_references_attrs
  end

  def self.trading_references_attributes_from_csv_hash(trade_references_hash)
    trade_references_attributes = []
    trade_references_hash.reject! { |key, value| value.blank? }

    keys = trade_references_hash.map do |key, value|
      key.split("]").count >= 2 ? key.split("]")[0] : ""
    end

    keys.uniq!
    keys.reject! { |key| key.blank? }

    # Convert CSv hash to director attributes
    keys.each do |key|
      temp_d = trade_references_hash.select { |hash_key, v| hash_key.split("]")[0] == key }
      trade_reference_attributes = {}

      temp_d.each do |k, value|
        tr_attr = k.split("]")[1]
        tr_attr = tr_attr.split("[")[1]
        trade_reference_attributes[tr_attr] = value
      end

      trade_references_attributes << trade_reference_attributes
    end

    trade_references_attributes.uniq! { |trade_reference| trade_reference['business_name'] }
    return trade_references_attributes
  end

  # Create Trade Reference form import Apply Supplier Credit Form
  def self.create_from_import_ascf(trade_references_hash, ascf, errors_report)
    trade_references_attributes = trading_references_attributes_from_csv_hash(trade_references_hash)

    # Create trade_reference and need_send_mail = false
    trade_references_attributes.each do |tr_attrs|
      # Validate present value, remove if invalid
      tr_attrs = valid_trade_references_attributes(tr_attrs, ascf, errors_report)
      ascf.trade_references.create(tr_attrs)
    end
  end
end
