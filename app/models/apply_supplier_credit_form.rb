class ApplySupplierCreditForm < ApplicationRecord
  include AscfSendEmailsConcern
  include AscfTermsConditionConcern
  include AscfWizardConcern
  include AscfImportConcern
  include AscfExportConcern
  include AscfApproveRejectConcern

  belongs_to :vaf, class_name: "VenueApplicationForm"
  belongs_to :supplier
  has_many :apply_supplier_credit_personal_guarantees, dependent: :destroy
  has_many :personal_guarantees, through: :apply_supplier_credit_personal_guarantees
  belongs_to :created_by, class_name: "User"
  belongs_to :trading_terms_setting
  has_many :trade_references, foreign_key: 'ascf_id', class_name: 'TradeReference', dependent: :destroy

  after_create :generate_form_token
  before_save :downcase_fields

  STEP_REVIEW = 'review'

  def validate_credit_limit
    unless validate_currency(venue_credit_limit) || validate_currency(credit_limit)
      errors.add(:credit_limit, 'Invalid value!')
    end
  end

  def validate_currency(currency, precision = 15)
    currency.to_s.gsub(".", "").length <= precision
  end

  def generate_form_token
    new_token = SecureRandom.urlsafe_base64(24)

    loop do
      break new_token unless ApplySupplierCreditForm.exists?(save_token: new_token)
      new_token = SecureRandom.urlsafe_base64(24)
    end

    self.save_token = new_token
    self.save_at = Time.now
    self.save
  end

  def sign_in_to_form_url(user = nil)
    url = "#{$prod_root_url}/users/sign_in?ascf_token=#{save_token}"
    url = "#{url}&user_token=#{user[:id]}" if user.present?
    url
  end

  def is_signed_by_personal_guarantee?
    apply_supplier_credit_personal_guarantees.where(is_signed: true).count > 0
  end

  def is_signed_by_all_personal_guarantees?
    if is_signed_by_personal_guarantee?
      signed_count = apply_supplier_credit_personal_guarantees.where(is_signed: true).count
      return signed_count == apply_supplier_credit_personal_guarantees.count
    else
      return false
    end
  end

  def get_quantity_of_personal_guarantees_have_signed
    apply_supplier_credit_personal_guarantees.where(is_signed: true).count
  end

  def get_personal_guarantee_by_user(user)
    personal_guarantees.where(user_id: user[:id]).first
  end

  def get_ascpg_by_personal_guarantee(personal_guarantee)
    apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
  end

  def is_signed_by_user(user)
    return false if user.blank?
    personal_guarantee = get_personal_guarantee_by_user(user)
    return false if personal_guarantee.blank?
    get_ascpg_by_personal_guarantee(personal_guarantee)[:is_signed]
  end

  # Directors as Personal Guarantees
  def director_ids_as_pgs
    apply_supplier_credit_personal_guarantees.where(is_director: true).map(&:director_id)
  end

  def asc_personal_guarantees_as_director
    apply_supplier_credit_personal_guarantees.where(is_director: true)
  end

  def asc_personal_guarantees_ids_as_director
    asc_personal_guarantees_as_director.map(&:personal_guarantee_id)
  end

  def personal_guarantees_as_director
    PersonalGuarantee.where(id: asc_personal_guarantees_ids_as_director)
  end

  def is_directors?
    asc_personal_guarantees_as_director.count > 0
  end

  def get_review_url
    "#{$prod_root_url}/admin/venue_suppliers/#{id}/edit/?step=review"
  end

  def self.get_ascf_by_token(token)
    ApplySupplierCreditForm.where(save_token: token).first
  end

  def is_include_user(user)
    personal_guarantees.map(&:email).include?(user[:email])
  end

  def is_signed
    apply_supplier_credit_personal_guarantees.where(is_signed: true).count > 0
  end

  def get_signed_at(personal_guarantee)
    ascpg = apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
    return nil if ascpg.blank?
    ascpg[:signed_at]
  end

  def get_signed_ip(personal_guarantee)
    ascpg = apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
    return nil if ascpg.blank?
    ascpg[:signed_ip]
  end

  def get_geo(personal_guarantee)
    ascpg = apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
    return nil if ascpg.blank?
    "#{ascpg[:latitude]}, #{ascpg[:longitude]}"
  end

  def signature_image(personal_guarantee)
    ascpg = apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
    return nil if ascpg.blank?
    ascpg[:signature_image]
  end

  def get_pg_by_user(user)
    personal_guarantees.where(user_id: user[:id]).first
  end

  def get_ascfpg_by_pg(personal_guarantee)
    apply_supplier_credit_personal_guarantees.where(personal_guarantee_id: personal_guarantee[:id]).first
  end

  def get_status_name
    case self[:status]
    when Const::STATUS[:draft][:value]
      return Const::STATUS[:draft][:name]
    when Const::STATUS[:pending_pg_signature][:value]
      return Const::STATUS[:pending_pg_signature][:name]
    when Const::STATUS[:no_signature_required][:value]
      return Const::STATUS[:no_signature_required][:name]
    when Const::STATUS[:all_signed][:value]
      return Const::STATUS[:all_signed][:name]
    when Const::STATUS[:approved][:value]
      return Const::STATUS[:approved][:name]
    when Const::STATUS[:rejected][:value]
      return Const::STATUS[:rejected][:name]
    when Const::STATUS[:completed][:value]
      return Const::STATUS[:completed][:name]
    else
      return ""
    end
  end

  def is_belongs_to_supplier?(supplier)
    self[:supplier_id].include?(supplier[:id])
  end

  def is_draft?
    Const::STATUS[:draft][:value].include?(self[:status])
  end

  def is_pending_pg_signature?
    Const::STATUS[:pending_pg_signature][:value].include?(self[:status])
  end

  def is_approved?
    Const::STATUS[:approved][:value].include?(self[:status])
  end

  def is_no_signature_required?
    Const::STATUS[:no_signature_required][:value].include?(self[:status])
  end

  def is_completed?
    Const::STATUS[:completed][:value].include?(self[:status])
  end

  def get_quantity_of_requrired_directors
    if self.trading_terms_setting[:directors].include?("All")
      return 1 # director at least = 1
    end

    if self.trading_terms_setting[:directors].to_i > 0
      return self.trading_terms_setting[:directors].to_i
    end

    return 0
  end

  def get_quantity_of_requrired_personal_guarantees
    if self.trading_terms_setting[:personal_guarantees].include?("All")
      return 1 # personal guarantees at least = 1
    end

    if self.trading_terms_setting[:personal_guarantees].to_i > 0
      return self.trading_terms_setting[:personal_guarantees].to_i
    end

    return 0
  end

  def get_quantity_of_requrired_trade_references
    if self.trading_terms_setting[:trade_references].include?("All")
      return 1 # personal guarantees at least = 1
    end

    if self.trading_terms_setting[:trade_references].to_i > 0
      return self.trading_terms_setting[:trade_references].to_i
    end

    return 0
  end

  def has_enough_directors?
    return true if self.trading_terms_setting[:directors].include?("None")

    if self.trading_terms_setting[:directors].include?("All")
      return true if self.vaf.directors.count > 0
    end

    if self.trading_terms_setting[:directors].to_i > 0
      return true if self.vaf.directors.count >= self.trading_terms_setting[:directors].to_i
    end

    return false
  end

  def has_enough_personal_guarantees?
    return true if self.trading_terms_setting[:personal_guarantees].include?("None")

    if self.trading_terms_setting[:personal_guarantees].include?("All")
      return true if self.personal_guarantees.count > 0
    end

    if self.trading_terms_setting[:personal_guarantees].to_i > 0
      return true if self.personal_guarantees.count == self.trading_terms_setting[:personal_guarantees].to_i
    end

    return false
  end

  def has_enough_trade_references?
    return true if self.trading_terms_setting[:trade_references].include?("None")

    if self.trading_terms_setting[:trade_references].include?("All")
      return true if self.trade_references.count > 0
    end

    if self.trading_terms_setting[:trade_references].to_i > 0
      return true if self.trade_references.count == self.trading_terms_setting[:trade_references].to_i
    end

    return false
  end

  def get_personal_guarantees_with_full_required_information
    pgs_list = []

    self.personal_guarantees.each do |pg|
      is_director = ApplySupplierCreditPersonalGuarantee.where(apply_supplier_credit_form_id: self[:id],
                                                personal_guarantee_id: pg[:id]).first[:is_director]
      if is_director
        director = self.vaf.directors.where(email: pg[:email]).first
        pgs_list << pg unless director.has_missing_information?
        next
      end

      next if pg.has_missing_information?
      pgs_list << pg
    end

    return pgs_list
  end

  def get_trade_references_with_full_required_information
    trade_references_list = []

    self.trade_references.each do |trade_reference|
      next if trade_reference.has_missing_information?
      trade_references_list << trade_reference
    end

    return trade_references_list
  end

  def has_missing_information?
    return true if self.vaf.get_directors_with_full_required_information.count < self.get_quantity_of_requrired_directors

    if self.get_personal_guarantees_with_full_required_information.count < self.get_quantity_of_requrired_personal_guarantees
      return true
    end

    if self.get_trade_references_with_full_required_information.count < self.get_quantity_of_requrired_trade_references
      return true
    end

    return false
  end

  def has_all_valid_required_information?
    return false if self[:credit_limit].blank?
    return false unless self.has_enough_directors?
    return false unless self.has_enough_personal_guarantees?
    return false unless self.has_enough_trade_references?
    return false if self.has_missing_information?
    return true
  end

  def self.apply_credit_attributes
    %w{venue_abn supplier_abn trading_term credit_limit}
  end

  def self.get_all_status
    current_statuses = distinct.pluck(:status)
    current_statuses = Const::STATUS.select { |key, value| current_statuses.include?(value[:value]) }
    return current_statuses.map { |key, value| value[:name] }
  end
end
