class Const
  STREET_TYPE_OPTIONS = [
    { name: 'Select one', value: '' },
    { name: 'STREET', value: 'STREET' },
    { name: 'ROAD', value: 'ROAD' },
    { name: 'ALLEY', value: 'ALLEY' },
    { name: 'AMBLE', value: 'AMBLE' },
    { name: 'APPROACH', value: 'APPROACH' },
    { name: 'ARCADE', value: 'ARCADE' },
    { name: 'ARTERIAL', value: 'ARTERIAL' },
    { name: 'AVENUE', value: 'AVENUE' },
    { name: 'BAY', value: 'BAY' },
    { name: 'BEND', value: 'BEND' },
    { name: 'BRAE', value: 'BRAE' },
    { name: 'BREAK', value: 'BREAK' },
    { name: 'BOULEVARD', value: 'BOULEVARD' },
    { name: 'BOARDWALK', value: 'BOARDWALK' },
    { name: 'BOWL', value: 'BOWL' },
    { name: 'BYPASS', value: 'BYPASS' },
    { name: 'CIRCLE', value: 'CIRCLE' },
    { name: 'CIRCUS', value: 'CIRCUS' },
    { name: 'CIRCUIT', value: 'CIRCUIT' },
    { name: 'CHASE', value: 'CHASE' },
    { name: 'CLOSE', value: 'CLOSE' },
    { name: 'CORNER', value: 'CORNER' },
    { name: 'COMMON', value: 'COMMON' },
    { name: 'CONCOURSE', value: 'CONCOURSE' },
    { name: 'CRESCENT', value: 'CRESCENT' },
    { name: 'CROSS', value: 'CROSS' },
    { name: 'COURSE', value: 'COURSE' },
    { name: 'CREST', value: 'CREST' },
    { name: 'CRUISEWAY', value: 'CRUISEWAY' },
    { name: 'COURT/S', value: 'COURT/S' },
    { name: 'COVE', value: 'COVE' },
    { name: 'DALE', value: 'DALE' },
    { name: 'DELL', value: 'DELL' },
    { name: 'DENE', value: 'DENE' },
    { name: 'DIVIDE', value: 'DIVIDE' },
    { name: 'DOMAIN', value: 'DOMAIN' },
    { name: 'DRIVE', value: 'DRIVE' },
    { name: 'EAST', value: 'EAST' },
    { name: 'EDGE', value: 'EDGE' },
    { name: 'ENTRANCE', value: 'ENTRANCE' },
    { name: 'ESPLANADE', value: 'ESPLANADE' },
    { name: 'EXTENSION', value: 'EXTENSION' },
    { name: 'FLATS', value: 'FLATS' },
    { name: 'FORD', value: 'FORD' },
    { name: 'FREEWAY', value: 'FREEWAY' },
    { name: 'GATE', value: 'GATE' },
    { name: 'GARDEN/S', value: 'GARDEN/S' },
    { name: 'GLADE/S', value: 'GLADE/S' },
    { name: 'GLEN', value: 'GLEN' },
    { name: 'GULLY', value: 'GULLY' },
    { name: 'GRANGE', value: 'GRANGE' },
    { name: 'GREEN', value: 'GREEN' },
    { name: 'GROVE', value: 'GROVE' },
    { name: 'GATEWAY', value: 'GATEWAY' },
    { name: 'HILL', value: 'HILL' },
    { name: 'HOLLOW', value: 'HOLLOW' },
    { name: 'HEATH', value: 'HEATH' },
    { name: 'HEIGHTS', value: 'HEIGHTS' },
    { name: 'HUB', value: 'HUB' },
    { name: 'HIGHWAY', value: 'HIGHWAY' },
    { name: 'ISLAND', value: 'ISLAND' },
    { name: 'JUNCTION', value: 'JUNCTION' },
    { name: 'LANE', value: 'LANE' },
    { name: 'LINK', value: 'LINK' },
    { name: 'LOOP', value: 'LOOP' },
    { name: 'LOWER', value: 'LOWER' },
    { name: 'LANEWAY', value: 'LANEWAY' },
    { name: 'MALL', value: 'MALL' },
    { name: 'MEW', value: 'MEW' },
    { name: 'MEWS', value: 'MEWS' },
    { name: 'NOOK', value: 'NOOK' },
    { name: 'NORTH', value: 'NORTH' },
    { name: 'OUTLOOK', value: 'OUTLOOK' },
    { name: 'PATH', value: 'PATH' },
    { name: 'PARADE', value: 'PARADE' },
    { name: 'POCKET', value: 'POCKET' },
    { name: 'PARKWAY', value: 'PARKWAY' },
    { name: 'PLACE', value: 'PLACE' },
    { name: 'PLAZA', value: 'PLAZA' },
    { name: 'PROMENADE', value: 'PROMENADE' },
    { name: 'PASS', value: 'PASS' },
    { name: 'PASSAGE', value: 'PASSAGE' },
    { name: 'POINT', value: 'POINT' },
    { name: 'PURSUIT', value: 'PURSUIT' },
    { name: 'PATHWAY', value: 'PATHWAY' },
    { name: 'QUADRANT', value: 'QUADRANT' },
    { name: 'QUAY', value: 'QUAY' },
    { name: 'REACH', value: 'REACH' },
    { name: 'RIDGE', value: 'RIDGE' },
    { name: 'RESERVE', value: 'RESERVE' },
    { name: 'REST', value: 'REST' },
    { name: 'RETREAT', value: 'RETREAT' },
    { name: 'RIDE', value: 'RIDE' },
    { name: 'RISE', value: 'RISE' },
    { name: 'ROUND', value: 'ROUND' },
    { name: 'ROW', value: 'ROW' },
    { name: 'RISING', value: 'RISING' },
    { name: 'RETURN', value: 'RETURN' },
    { name: 'RUN', value: 'RUN' },
    { name: 'SLOPE', value: 'SLOPE' },
    { name: 'SQUARE', value: 'SQUARE' },
    { name: 'SOUTH', value: 'SOUTH' },
    { name: 'STRIP', value: 'STRIP' },
    { name: 'STEPS', value: 'STEPS' },
    { name: 'SUBWAY', value: 'SUBWAY' },
    { name: 'TERRACE', value: 'TERRACE' },
    { name: 'THROUGHWAY', value: 'THROUGHWAY' },
    { name: 'TOR', value: 'TOR' },
    { name: 'TRACK', value: 'TRACK' },
    { name: 'TRAIL', value: 'TRAIL' },
    { name: 'TURN', value: 'TURN' },
    { name: 'TOLLWAY', value: 'TOLLWAY' },
    { name: 'UPPER', value: 'UPPER' },
    { name: 'VALLEY', value: 'VALLEY' },
    { name: 'VISTA', value: 'VISTA' },
    { name: 'VIEW/S', value: 'VIEW/S' },
    { name: 'WAY', value: 'WAY' },
    { name: 'WOOD', value: 'WOOD' },
    { name: 'WEST', value: 'WEST' },
    { name: 'WALK', value: 'WALK' },
    { name: 'WALKWAY', value: 'WALKWAY' },
    { name: 'WATERS', value: 'WATERS' },
    { name: 'WATERWAY', value: 'WATERWAY' },
    { name: 'WYND', value: 'WYND' }
  ]

  STREET_TYPES = STREET_TYPE_OPTIONS.reject { |item| item[:value].blank? }

  STATE_OPTIONS = [
    { name: 'Select one', value: '' },
    { name: 'VIC', value: 'VIC' },
    { name: 'NSW', value: 'NSW' },
    { name: 'QLD', value: 'QLD' },
    { name: 'ACT', value: 'ACT' },
    { name: 'SA', value: 'SA' },
    { name: 'WA', value: 'WA' },
    { name: 'NT', value: 'NT' },
    { name: 'TAS', value: 'TAS' }
  ]

  STATES = STATE_OPTIONS.reject { |item| item[:value].blank? }

  TRADING_TERMS = ["Cash on delivery", "7 days", "14 days", "30 days"]

  OPERATING_STRUCTURE_OPTIONS = [
    { name: 'Select one', value: '' },
    { name: 'PTY LTD', value: 'PTY LTD' },
    { name: 'PARTNERSHIP', value: 'PARTNERSHIP' },
    { name: 'SOLE TRADER', value: 'SOLE TRADER' },
    { name: 'CORPORATION AS TRUSTEE', value: 'CORPORATION AS TRUSTEE' },
    { name: 'Individual(s) as Trustee', value: 'INDIVIDUAL(S) AS TRUSTEE' },
    { name: 'Public Company', value: 'PUBLIC COMPANY' }
  ]

  OPERATING_STRUCTURES = OPERATING_STRUCTURE_OPTIONS.reject { |item| item[:value].blank? }

  SIGN_QUANTITY_OPTIONS = [
    ['None', 'None'], ['1', '1'], ['2', '2'],
    ['3', '3'], ['4', '4'], ['All', 'All']
  ]

  SIGN_QUANTITIES = {
    none: {
      name: 'None',
      value: 'None'
    },
    one: {
      name: '1',
      value: '1'
    },
    two: {
      name: '2',
      value: '2'
    },
    three: {
      name: '3',
      value: '3'
    },
    four: {
      name: '4',
      value: '4'
    },
    all: {
      name: 'All',
      value: 'All'
    }
  }

  ROLES = {
    :admin => 'ADMIN', :supplier => 'SUPPLIER',
    :director => 'DIRECTOR', :personal_guarantee => 'PERSONAL_GUARANTEE',
    :customer => 'CUSTOMER'
  }

  USER_ROLES = {
    admin: {
      name: 'Admin',
      value: 'ADMIN'
    },
    supplier: {
      name: 'Supplier',
      value: 'SUPPLIER'
    },
    director: {
      name: 'Director',
      value: 'DIRECTOR'
    },
    personal_guarantee: {
      name: 'Personal Guarantee',
      value: 'PERSONAL_GUARANTEE'
    },
    customer: {
      name: 'Customer',
      value: 'CUSTOMER'
    }
  }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  VALID_NUMBERIC_REGEX = /\A[+-]?\d+\z/

  STATUS = {
    draft: {
      name: 'Draft',
      value: 'DRAFT'
    },
    pending_director_signature: {
      name: 'Pending Signature',
      value: 'PENDING_DIRECTOR_SIGNATURE'
    },
    pending_pg_signature: {
      name: 'Pending Signature',
      value: 'PENDING_PERSONAL_GUARANTEE_SIGNATURE'
    },
    approved: {
      name: 'Approved',
      value: 'APPROVED'
    },
    rejected: {
      name: 'Rejected',
      value: 'REJECTED'
    },
    all_signed: {
      name: 'All Signed',
      value: 'ALL_SIGNED'
    },
    no_signature_required: {
      name: 'No Signature Required',
      value: 'NO_SIGNATURE_REQUIRED'
    },
    completed: {
      name: 'Completed',
      value: 'COMPLETED'
    }
  }

  YES_NO_OPTIONS = {
    place_holder: {
      name: 'Please select',
      value: ''
    },
    yes: {
      name: 'Yes',
      value: true
    },
    no: {
      name: 'No',
      value: false
    }
  }
end
