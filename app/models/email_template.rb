class EmailTemplate < ApplicationRecord
  belongs_to :created_by, class_name: "User"
  validates :name, presence: true

  CATEGORY = [
    { name: 'General', value: 'GENERAL' },
    { name: 'Director Registration', value: 'DIRECTOR_REGISTRATION' },
    { name: 'Send venue form link', value: 'VENUE_FORM'},
    { name: 'Send the apply supplier credit form link', value: 'APPLY_SUPPLIER_CREDIT_FORM'},
    { name: 'Personal Guarantee Application for supplier credit', value: 'PERSONAL_GUARANTEE_APPLICATION_FOR_SUPPLIER_CREDIT'},
    { name: 'Director sign venue form', value: 'DIRECTOR_SIGN_VENUE_FORM'},
    { name: 'Personal Guarantee sign Apply Supplier Credit Form', value: 'PERSONAL_GUARANTEE_SIGN_ASCF'},
    { name: 'A venue has applied Supplier Credit Form', value: 'VENUE_APPLY_TO_APPLY_SUPPLIER_CREDIT_FORM'},
    { name: 'Personal Guarantee has signed Apply Supplier Credit Form', value: 'PERSONAL_GUARANTEE_SIGN_SUPPLIER_CREDIT_FORM'},
    { name: 'All Personal Guarantees have signed the supplier credit form', value: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM'},
    { name: 'Supplier registration', value: 'SUPPLIER_REGISTRATION'},
    { name: 'Send new credit limit notify to directors personal guarantees', value: 'APPLY_SUPPLIER_CREDIT_FORM_NEW_CREDIT_LIMIT'},
    { name: 'Send approve notify to directors personal guarantees supplier', value: 'APPLY_SUPPLIER_CREDIT_FORM_APPROVED'},
    { name: 'Send reject notify to directors personal guarantees supplier', value: 'APPLY_SUPPLIER_CREDIT_FORM_REJECTED'},
    { name: 'All directors have signed the venue application form', value: 'ALL_DIRECTORS_SIGNED_VENUE_FORM'},
    { name: 'All Personal Guarantees have signed the supplier credit form send to all personal guarantees',
      value: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM_PGS'},
    { name: 'Notify directors and personal guarantees supplier terms and condition changed',
      value: 'NOTIFY_DIRECTORS_PGS_T&CS_CHANGED'}
  ]

  def self.get_director_registration
    EmailTemplate.where(category: 'DIRECTOR_REGISTRATION').first
  end

  def self.get_venue_form
    EmailTemplate.where(category: 'VENUE_FORM').first
  end

  def self.get_ascf_form
    EmailTemplate.where(category: 'APPLY_SUPPLIER_CREDIT_FORM').first
  end

  # pga: personal_guarantee_application
  def self.get_pga_for_supplier_credit
    EmailTemplate.where(category: 'PERSONAL_GUARANTEE_APPLICATION_FOR_SUPPLIER_CREDIT').first
  end

  def self.get_director_sign_venue_form
    EmailTemplate.where(category: 'DIRECTOR_SIGN_VENUE_FORM').first
  end

  def self.get_pg_sign_ascf
    EmailTemplate.where(category: 'PERSONAL_GUARANTEE_SIGN_ASCF').first
  end

  def self.get_ascf_apply
    EmailTemplate.where(category: 'VENUE_APPLY_TO_APPLY_SUPPLIER_CREDIT_FORM').first
  end

  def self.get_ascf_pg_signed_apply
    EmailTemplate.where(category: 'PERSONAL_GUARANTEE_SIGN_SUPPLIER_CREDIT_FORM').first
  end

  def self.get_all_pg_signed_send_to_supplier_main
    EmailTemplate.where(category: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM').first
  end

  def self.get_supplier_registration
    EmailTemplate.where(category: 'SUPPLIER_REGISTRATION').first
  end

  def self.get_ascf_new_credit_limit
    EmailTemplate.where(category: 'APPLY_SUPPLIER_CREDIT_FORM_NEW_CREDIT_LIMIT').first
  end

  def self.get_ascf_approved
    EmailTemplate.where(category: 'APPLY_SUPPLIER_CREDIT_FORM_APPROVED').first
  end

  def self.get_ascf_rejected
    EmailTemplate.where(category: 'APPLY_SUPPLIER_CREDIT_FORM_REJECTED').first
  end

  def self.get_all_directors_signed_venue_form
    EmailTemplate.where(category: 'ALL_DIRECTORS_SIGNED_VENUE_FORM').first
  end

  def self.get_all_pg_signed_send_to_pgs
    EmailTemplate.where(category: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM_PGS').first
  end

  def self.get_notify_directors_pgs_supplier_terms_condition_changed
    EmailTemplate.where(category: 'NOTIFY_DIRECTORS_PGS_T&CS_CHANGED').first
  end
end
