require 'net/http'
require 'uri'
include Rails.application.routes.url_helpers

class VenueApplicationForm < ApplicationRecord
  # Attribute Encrypted
  # attr_encrypted :bank_branch, key: :attribute_encrypted_key
  # attr_encrypted :bsb, key: :attribute_encrypted_key
  # account_number
  include VenueSendEmailsConcern
  include VenueImportConcern
  include VenueExportConcern

  has_many :venue_application_form_directors, dependent: :destroy
  has_many :directors, through: :venue_application_form_directors
  has_many :venue_application_form_personal_guarantees, dependent: :destroy
  has_many :personal_guarantees, through: :venue_application_form_personal_guarantees
  has_many :suppliers, through: :apply_supplier_credit_form
  belongs_to :market_segment, optional: true
  has_many :apply_supplier_credit_forms, foreign_key: 'vaf_id', class_name: 'ApplySupplierCreditForm', dependent: :destroy

  before_save :downcase_fields
  after_create :generate_form_token

  validates :company_abn, uniqueness: true

  STEP_REVIEW = 'review'

  def generate_form_token
    new_token = SecureRandom.urlsafe_base64(24)

    loop do
      break new_token unless VenueApplicationForm.exists?(save_token: new_token)
      new_token = SecureRandom.urlsafe_base64(24)
    end

    self.save_token = new_token
    self.save_at = Time.now
    self.save
  end

  def get_personal_guarantees
    cf_ids = apply_supplier_credit_forms.map(&:id)
    pg_ids = ApplySupplierCreditPersonalGuarantee.where(apply_supplier_credit_form_id: cf_ids).map(&:personal_guarantee_id)
    PersonalGuarantee.where(id: pg_ids)
  end

  def is_include_user(user)
    directors.map(&:email).include?(user[:email]) || personal_guarantees.map(&:email).include?(user[:email])
  end

  def is_signed_by_director
    venue_application_form_directors.where(is_signed: true).count > 0
  end

  def is_signed_by_all_directors?
    if is_signed_by_director
      signed_count = venue_application_form_directors.where(is_signed: true).count
      return signed_count == directors.count
    else
      return false
    end
  end

  def get_quantity_of_directors_have_signed
    venue_application_form_directors.where(is_signed: true).count
  end

  def has_directors_have_been_bankrupt?
    directors.where(has_been_bankrupt: true).count > 0
  end

  def is_all_directors_have_been_bankrupt?
    if has_directors_have_been_bankrupt?
      count = directors.where(has_been_bankrupt: true).count
      return count == directors.count
    else
      return false
    end
  end

  def get_quantity_of_directors_have_have_been_bankrupt
    directors.where(has_been_bankrupt: true).count
  end

  def get_review_url
    "#{$prod_root_url}/venue-form/token/#{save_token}?step=review"
  end

  def sign_in_to_form_url(user = nil)
    url = "#{$prod_root_url}/users/sign_in?form_token=#{save_token}"
    return "#{url}&user_token=#{user[:id]}" if user.present?
    url
  end

  def send_form_link_to_email(email)
    link_url = "#{$prod_root_url}#{venue_application_form_venue_form_token_path(self[:save_token])}"

    if email.present?
      UserMailer.send_form_link(
        form_link: link_url,
        email: email
      ).deliver_later
    end
  end

  def get_director_by_user(user)
    directors.where(user_id: user[:id]).first
  end

  def get_vafd_by_director(director)
    venue_application_form_directors.where(director_id: director[:id]).first
  end

  def is_signed_by_user(user)
    return false if user.blank?
    director = get_director_by_user(user)
    return false if director.blank?
    get_vafd_by_director(director)[:is_signed]
  end

  def is_signed
    venue_application_form_directors.where(is_signed: true).count > 0 || venue_application_form_personal_guarantees.where(is_signed: true).count > 0
  end

  def signature_image(director)
    vafd = venue_application_form_directors.where(director_id: director[:id]).first
    return nil if vafd.blank?
    vafd[:signature_image]
  end

  def get_signed_at(director)
    vafd = venue_application_form_directors.where(director_id: director[:id]).first
    return nil if vafd.blank?
    vafd[:signed_at]
  end

  def get_signed_ip(director)
    vafd = venue_application_form_directors.where(director_id: director[:id]).first
    return nil if vafd.blank?
    vafd[:signed_ip]
  end

  def get_geo(director)
    vafd = venue_application_form_directors.where(director_id: director[:id]).first
    return nil if vafd.blank?
    "#{vafd[:latitude]}, #{vafd[:longitude]}"
  end

  def get_directors_with_full_required_information
    directors_list = []

    self.directors.each do |director|
      next if director.has_missing_information?
      directors_list << director
    end

    return directors_list
  end

  def has_director?(director_email)
    self.directors.map(&:email).include?(director_email)
  end

  def is_draft?
    Const::STATUS[:draft][:value].include?(self[:status])
  end

  def is_pending_director_signature?
    Const::STATUS[:pending_director_signature][:value].include?(self[:status])
  end

  def has_missing_information?
    self.attributes.values_at(*VenueApplicationForm.required_attributes).any? { |value| value.blank? }
  end

  def has_all_valid_required_information?
    return false if self.has_missing_information?
    return false unless self.directors.count > 0
    return false if self.get_directors_with_full_required_information.count < self.directors.count
    return true
  end

  def self.get_vaf_by_token(token)
    VenueApplicationForm.where(save_token: token).first
  end

  def self.venue_attributes
    %w{trading_name email company_abn company_name operating_structure market_segment_id
      firstname lastname mobile phone trading_property_name trading_unit_number
      trading_street_number trading_street_name trading_street_type trading_suburb trading_state
      trading_postcode registered_property_name registered_unit_number
      registered_street_number registered_street_name registered_street_type registered_suburb
      registered_state registered_postcode
      bank_branch bsb account_number account_contact_name account_contact_email
      account_contact_number delivery_contact_name delivery_contact_number
      }
  end

  def self.required_attributes
    %w{trading_name email company_abn company_name operating_structure market_segment_id
      firstname lastname mobile
      trading_street_number trading_street_name trading_street_type trading_suburb trading_state
      trading_postcode
      registered_street_number registered_street_name registered_street_type registered_suburb
      registered_state registered_postcode
      bank_branch bsb account_number account_contact_name account_contact_email
      account_contact_number delivery_contact_name delivery_contact_number
      }
  end

  def self.minimum_attributes
    %w{trading_name email company_abn}
  end

  # Check present data
  def self.has_present_attributes(venue_attrs)
    has_missing_attributes = venue_attrs.values.any? { |v| v.blank? }
    return !has_missing_attributes
  end

  # Check minimum data
  def self.has_minimum_attributes(venue_minimum_attrs)
    # If venue hash don't have minimum fields
    venue_minimum_attrs = venue_minimum_attrs.select { |key, value| minimum_attributes.include?(key) }
    has_missing_attributes = venue_minimum_attrs.values.any? { |v| v.blank? }
    return !has_missing_attributes
  end

  # Check if abn is numberic string and length is 11
  def self.validate_abn(abn)
    if (abn =~ Const::VALID_NUMBERIC_REGEX).present? && abn.length == 11
      result = check_unique_abn(abn)
      return result[:is_real] && result[:is_active]
    end

    return false
  end

  def self.check_real_abn(abn)
    url = "https://abr.business.gov.au/json/AbnDetails.aspx?abn=#{abn}&guid=#{ENV['ABN_GUID']}"
    page_content = Net::HTTP.get(URI.parse(url))

    if page_content.present?
      page_content.gsub! 'callback(', ''
      page_content.gsub! ')', ''
      response = JSON.parse(page_content)

      if response['AbnStatus'].present?
        return { response: response, is_real: true, message: 'Is a real ABN.' }
      end
    end

    { response: response, is_real: false, message: 'The ABN you entered does not exist in the ASIC database. Please check and try again.' }
  end

  def self.check_active_abn(abn)
    result = check_real_abn(abn)
    result[:is_active] = false
    return result unless result[:is_real]

    if result[:response]["AbnStatus"] == "Active"
      result[:is_active] = true
      result[:message] = 'Is an active ABN.'
      return result
    end

    result[:message] = 'The ABN you entered is not active in the ASIC database. Please check and try again.'
    result
  end

  def self.check_unique_abn(abn, vaf_id = nil)
    result = check_active_abn(abn)
    result[:is_exist] = false
    vaf = VenueApplicationForm.where(company_abn: abn).first
    return result unless result[:is_active]

    if vaf.present? && vaf_id.blank?
      result[:is_exist] = true
      result[:message] = 'The ABN you entered is already in the pencil platform. Please check with your directors to confirm who has applied for pencil.'
      return result
    end

    result[:message] = 'This Company ABN is not exist in the pencil platform. You can use it.'
    result
  end

  def self.get_all_status
    distinct.pluck(:status)
  end

  def self.has_missing_information_error(venue_hash)
    venue_hash.each do |key, value|
      if required_attributes.include?(key) && value.blank?
        return { is_missed_info: true, key: key }
      end
    end

    return { is_missed_info: false, key: nil }
  end

  # Check valid all information of venue application form
  def self.valid_hash_params(hash_params, vaf = nil)
    invalid = false
    vaf_params = hash_params.to_unsafe_h

    # Check minimum required
    unless VenueApplicationForm.has_minimum_attributes(hash_params)
      invalid = true
      message = 'Some information are required or invalid. Please correct them.'
    end

    # Check Company ABN
    check_abn = { is_real: true, is_active: true }

    # Check ABN when create new
    if vaf.blank? || (vaf.present? && vaf[:company_abn] != vaf_params[:company_abn])
      check_abn = VenueApplicationForm.check_unique_abn(vaf_params[:company_abn])
    end

    unless check_abn[:is_real] && check_abn[:is_active] && !check_abn[:is_exist]
      invalid = true
      message = check_abn[:message]
    end

    # Check valid venue form
    venue_hash = VenueApplicationForm.valid_venue_hash(vaf_params)
    missed_info = VenueApplicationForm.has_missing_information_error(venue_hash)

    if missed_info[:is_missed_info]
      invalid = true
      message = "#{missed_info[:key]} is required or invalid. Please correct them."
    end

    { invalid: invalid, message: message }
  end
end
