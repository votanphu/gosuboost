class TradingTermsSetting < ApplicationRecord
  belongs_to :supplier
  has_many :apply_supplier_credit_forms, dependent: :destroy
  has_many :supplier_terms_conditions, dependent: :destroy
  has_many :credit_limits, dependent: :destroy

  def has_enough_personal_guarantees(pgs_quantity)
    has_enough = false

    case self[:personal_guarantees]
    when Const::SIGN_QUANTITIES[:none][:value]
      has_enough = true
    when Const::SIGN_QUANTITIES[:one][:value]
      has_enough = true if pgs_quantity >= 1
    when Const::SIGN_QUANTITIES[:two][:value]
      has_enough = true if pgs_quantity >= 2
    when Const::SIGN_QUANTITIES[:three][:value]
      has_enough = true if pgs_quantity >= 3
    when Const::SIGN_QUANTITIES[:four][:value]
      has_enough = true if pgs_quantity >= 4
    when Const::SIGN_QUANTITIES[:all][:value]
      has_enough = true if pgs_quantity >= 1
    else
      has_enough = false
    end

    has_enough
  end

  def get_required_personal_guarantees_quantity
    quantity = 0

    case self[:personal_guarantees]
    when Const::SIGN_QUANTITIES[:none][:value]
      quantity = 0
    when Const::SIGN_QUANTITIES[:one][:value]
      quantity = 1
    when Const::SIGN_QUANTITIES[:two][:value]
      quantity = 2
    when Const::SIGN_QUANTITIES[:three][:value]
      quantity = 3
    when Const::SIGN_QUANTITIES[:four][:value]
      quantity = 4
    when Const::SIGN_QUANTITIES[:all][:value]
      quantity = 1
    else
      quantity = 0
    end

    quantity
  end

  def get_required_trade_references_quantity
    quantity = 0

    case self[:trade_references]
    when Const::SIGN_QUANTITIES[:none][:value]
      quantity = 0
    when Const::SIGN_QUANTITIES[:one][:value]
      quantity = 1
    when Const::SIGN_QUANTITIES[:two][:value]
      quantity = 2
    when Const::SIGN_QUANTITIES[:three][:value]
      quantity = 3
    when Const::SIGN_QUANTITIES[:four][:value]
      quantity = 4
    when Const::SIGN_QUANTITIES[:all][:value]
      quantity = 1
    else
      quantity = 0
    end

    quantity
  end
end
