class Director < ApplicationRecord
  # Attribute Encrypted
  # attr_encrypted :drivers_license_number, key: :attribute_encrypted_key
  # attr_encrypted :license_expiry, key: :attribute_encrypted_key
  # front_license_file
  # back_license_file
  include DirectorImportConcern

  belongs_to :user
  has_many :venue_application_form_directors, dependent: :destroy
  has_many :venue_application_forms, through: :venue_application_form_directors

  before_save :downcase_fields

  validates :email, presence: true, length: { minimum: 10 }, format: { with: Const::VALID_EMAIL_REGEX }

  has_attached_file :front_license_file, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/media/default/directors/default_front_license_file.png",
    url: "/media/directors/:id/:style/:hash.:extension",
    path: ":rails_root/public/media/directors/:id/:style/:hash.:extension",
    hash_secret: "G0sl3tm31n"
  do_not_validate_attachment_file_type :front_license_file

  has_attached_file :back_license_file, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/media/default/directors/default_back_license_file.png",
    url: "/media/directors/:id/:style/:hash.:extension",
    path: ":rails_root/public/media/directors/:id/:style/:hash.:extension",
    hash_secret: "G0sl3tm31n"
  do_not_validate_attachment_file_type :back_license_file

  Paperclip.interpolates :normalized_front_license_file_file_name do |attachment, style|
    attachment.instance.normalized_front_license_file_file_name
  end

  Paperclip.interpolates :normalized_end_license_file_file_name do |attachment, style|
    attachment.instance.normalized_end_license_file_file_name
  end

  def normalized_front_license_file_file_name
    "#{self.id}-#{self.front_license_file_file_name.gsub( /[^a-zA-Z0-9_\.]/, '_')}"
  end

  def normalized_end_license_file_file_name
    "#{self.id}-#{self.end_license_file_file_name.gsub( /[^a-zA-Z0-9_\.]/, '_')}"
  end

  def set_front_license_file(base64_date, image_name = 'file.png')
    image_data = Paperclip.io_adapters.for(base64_date)
    image_data.original_filename = image_name
    self.front_license_file = image_data
  end

  def set_back_license_file(base64_date, image_name = 'file.png')
    image_data = Paperclip.io_adapters.for(base64_date)
    image_data.original_filename = image_name
    self.back_license_file = image_data
  end

  # vaf: VenueApplicationForm
  def send_director_registration(vaf, is_exist_user = false)
    token = vaf["save_token"]

    if user.created_at == user.updated_at
      set_password_link = user.get_setting_password_url(form_token: token)
    else
      set_password_link = vaf.sign_in_to_form_url(user)
    end

    UserMailer.director_registration(
      business_name: vaf["trading_name"],
      director_email: email,
      director_first_name: firstname,
      company_name: vaf["company_name"],
      set_password_link: set_password_link
    ).deliver_later
  end

  def has_missing_information?
    self.attributes.values_at(*Director.required_director_attributes).any? { |value| value.blank? }
  end

  # Send sign email to email, create director if not added in venue
  def self.send_sign_email(email, venue)
    director = venue.directors.where(email: email).first
    director = create_from_email(email, venue) if director.blank?
    director.send_director_registration(venue)
  end

  # Check email whether has user yet if not create one
  def self.create_from_email(email, venue, director_attributes = {})
    user = User.where(email: email).first
    is_exist_user = true

    if user.blank?
      is_exist_user = false
      user = User.create_user(email, Const::ROLES[:director], director_attributes)
    end

    temp_name = email.split("@")[0]

    director_attributes = {
      email: email,
      firstname: temp_name,
      lastname: ""
    } if director_attributes.blank?

    director = user.directors.create(director_attributes)

    attributes = {
      venue_application_form_id: venue[:id],
      director_id: director[:id]
    }

    VenueApplicationFormDirector.where(venue_application_form_id: venue[:id],
      director_id: director[:id]).first_or_initialize.tap do |item|
      next unless item.new_record?
      item.assign_attributes(attributes)
      item.save!
    end

    director
  end

  # Director attributes
  def self.director_attributes
    %w{firstname middlename lastname mobile phone email property_name unit_number street_number
      street_name street_type suburb state postcode date_of_birth drivers_license_number license_expiry
      has_been_bankrupt
      }
  end

  def self.required_director_attributes
    %w{firstname lastname mobile email street_number street_name street_type suburb state postcode
      date_of_birth drivers_license_number license_expiry
      }
  end

  # Check whether external pg is also director
  def self.get_real_pg_quantity(selected_directors_ids, pgs_params)
    emails = where(id: selected_directors_ids).map(&:email)
    emails << pgs_params.map { |pg_params| pg_params["email"] }
    emails.flatten!
    emails.uniq!
    emails.count
  end
end
