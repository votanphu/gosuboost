class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  include DataConcern
  include EncryptorConcern
end
