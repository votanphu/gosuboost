module AscfWizardConcern
  extend ActiveSupport::Concern

  included do
    # Filter pg params and remove empty item when create ascf
    def self.filter_pg_params(pgs_params)
      pgs_params.select! { |pg_params| pg_params["director_id"].blank? }
      personal_guarantees_params = []

      pgs_params.each do |pg_params|
        pg_params.select! { |key, value| PersonalGuarantee.personal_guarantee_attributes.include?(key) }
        personal_guarantees_params << pg_params if pg_params["email"].present?
      end

      personal_guarantees_params.uniq! { |pg_param| pg_param["email"] }
      personal_guarantees_params
    end
  end
end
