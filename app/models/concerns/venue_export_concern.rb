module VenueExportConcern
  extend ActiveSupport::Concern

  included do
    def self.to_csv_header(venues)
      quantity_directors_arr = venues.map { |venue| venue.directors.count }
      quantity = quantity_directors_arr.count > 0 ? quantity_directors_arr.max : 1
      quantity = 1 unless quantity > 0
      director_attrs =  Director.director_attributes
      attributes = venue_attributes

      quantity.times do |index|
        temp_attrs = director_attrs.map { |attr| "director[#{index + 1}][#{attr}]" }
        attributes << temp_attrs
      end

      attributes.flatten!
      return attributes
    end

    def self.to_csv(venues)
      attributes = to_csv_header(venues)

      CSV.generate(headers: true) do |csv|
        csv << attributes

        venues.each do |venue|
          csv_values = venue.attributes.values_at(*venue_attributes)

          venue.directors.each do |director|
            csv_values << director.attributes.values_at(*Director.director_attributes)
          end

          csv_values.flatten!
          csv << csv_values
        end
      end
    end
  end
end
