module VenueSendEmailsConcern
  extend ActiveSupport::Concern

  # Send save & continue link when venue is draft;
  # Send sign link when when venue is pending signature
  def send_email(emails)
    return if emails.blank?
    # Send save & continue later link to emails if venue is draft
    emails.each { |email| self.send_form_link_to_email(email) } if self.is_draft?
    # Send director sign emails if venue is pending directors signature
    emails.each { |email| Director.send_sign_email(email, self) } if self.is_pending_director_signature?
    # Redirect to venue details
  end

  def send_directors_emails(send_email_params)
    directors_emails = []

    # Collect director emails
    case send_email_params[:directors_option]
    when 'all'
      directors_emails = self.directors.map(&:email)
      self.send_email(directors_emails)
    when 'single_director'
      director = Director.where(id: send_email_params[:director_id]).first if send_email_params[:director_id].present?
      directors_emails << director[:email] if director.present?
      self.send_email(directors_emails)
    when 'email'
      email = send_email_params[:director_email] if send_email_params[:director_email].present?

      if CommonLib.validate_email(email)
        directors_emails << email
        self.send_email(directors_emails)
      end
    when 'sms'
      # Update soon
    end

    directors_emails.present?
  end
end