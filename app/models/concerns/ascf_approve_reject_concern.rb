module AscfApproveRejectConcern
  extend ActiveSupport::Concern

  def is_approvable?
    return false unless self[:active]
    return true if self.is_no_signature_required?
    return false if self.is_draft? || self.is_approved? || self.is_completed?
    return false unless self.is_signed_by_personal_guarantee?
    return false if self.get_quantity_of_personal_guarantees_have_signed < self.get_quantity_of_requrired_personal_guarantees
    return true if self.get_quantity_of_requrired_directors <= 0
    return false unless self.vaf.is_signed_by_director
    return false if self.vaf.get_quantity_of_directors_have_signed < self.get_quantity_of_requrired_directors
    return true
  end

  def send_approve_emails(approved_by)
    UserMailer.ascf_approved_send_all_directors_pgs_supplier(
      ascf: self,
      approved_by: approved_by
    ).deliver_later
  end

  def send_reject_emails(rejected_by)
    UserMailer.ascf_rejected_send_all_directors_pgs_supplier(
      ascf: self,
      rejected_by: rejected_by
    ).deliver_later
  end

  def approve_by_director(user)
    return false unless user.is_director?
    return false unless self.vaf.has_director?(user[:email])
    return false unless self.is_approvable?
    self.update_attributes(status: Const::STATUS[:approved][:value])
    self.send_approve_emails(user)
    true
  end

  def approve_by_supplier(user)
    return false unless user.is_supplier?
    return false unless self.is_belongs_to_supplier?(user.supplier)
    return false unless self.is_approvable?
    self.update_attributes(status: Const::STATUS[:approved][:value])
    self.send_approve_emails(user)
    true
  end

  def reject_by_director(user)
    return false unless user.is_director?
    return false unless self.vaf.has_director?(user[:email])
    return false unless self[:active]
    self.update_attributes(active: false, status: Const::STATUS[:rejected][:value])
    self.send_reject_emails(user)
    true
  end

  def reject_by_supplier(user)
    return false unless user.is_supplier?
    return false unless self.is_belongs_to_supplier?(user.supplier)
    return false unless self[:active]
    self.update_attributes(active: false, status: Const::STATUS[:rejected][:value])
    self.send_approve_emails(user)
    true
  end

  # Supplier send new credit limit
  def send_credit_limit(user, reject_apply_credit_params)
    return false unless user.is_supplier?
    return false unless reject_apply_credit_params[:supplier_credit_limit].present?
    credit_limit = reject_apply_credit_params[:supplier_credit_limit].to_d
    return false unless CommonLib.validate_currency(credit_limit)
    return false unless credit_limit > 0
    return false unless self.is_belongs_to_supplier?(user.supplier)
    return false unless self[:supplier_credit_limit].blank?
    return false unless self.is_approvable?

    self.update_attributes(supplier_credit_limit: reject_apply_credit_params[:supplier_credit_limit],
                            credit_limit: reject_apply_credit_params[:supplier_credit_limit])

    UserMailer.ascf_new_credit_limit_send_all_directors_pgs(
      ascf: self
    ).deliver_later

    true
  end
end
