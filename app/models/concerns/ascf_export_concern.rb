module AscfExportConcern
  extend ActiveSupport::Concern

  included do
    # ascfs: Apply Supplier Credit Forms
    def self.to_csv_header(ascfs)
      attributes = apply_credit_attributes

      # Add trade references headers
      quantity_trs_arr = ascfs.map { |ascf| ascf.trade_references.count }
      tr_quantity = quantity_trs_arr.count > 0 ? quantity_trs_arr.max : 1
      tr_quantity = 1 unless tr_quantity > 0
      tr_attrs =  TradeReference.trade_reference_attributes

      tr_quantity.times do |index|
        temp_attrs = tr_attrs.map { |attr| "trade_reference[#{index + 1}][#{attr}]" }
        attributes << temp_attrs
      end

      # Add personal guarantees headers
      quantity_pgs_arr = ascfs.map { |ascf| ascf.personal_guarantees.count }
      pg_quantity = quantity_pgs_arr.count > 0 ? quantity_pgs_arr.max : 1
      pg_quantity = 1 unless pg_quantity > 0
      pg_attrs =  PersonalGuarantee.personal_guarantee_attributes

      pg_quantity.times do |index|
        temp_attrs = pg_attrs.map { |attr| "personal_guarantee[#{index + 1}][#{attr}]" }
        attributes << temp_attrs
      end

      attributes.flatten!
      return attributes
    end

    def self.to_csv(ascfs)
      attributes = to_csv_header(ascfs)

      CSV.generate(headers: true) do |csv|
        csv << attributes

        ascfs.each do |ascf|
          csv_values = [] << ascf.vaf[:company_abn]
          csv_values << ascf.supplier[:abn]
          csv_values << ascf.trading_terms_setting[:trading_term]
          csv_values << ascf[:credit_limit]

          ascf.trade_references.each do |tr|
            csv_values << tr.attributes.values_at(*TradeReference.trade_reference_attributes)
          end

          ascf.personal_guarantees.each do |pg|
            csv_values << pg.attributes.values_at(*PersonalGuarantee.personal_guarantee_attributes)
          end

          csv_values.flatten!
          csv << csv_values
        end
      end
    end
  end
end