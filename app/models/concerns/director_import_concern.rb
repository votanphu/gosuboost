module DirectorImportConcern
  extend ActiveSupport::Concern

  included do
    # Validate present value, remove if invalid
    def self.valid_directors_attributes(directors_attributes, venue, errors_report)
      directors_attributes.map do |key, value|
        case key
        when 'mobile'
          valid = CommonLib.validate_mobileAU(value)
        when 'phone'
          valid = CommonLib.validate_phoneAU(value)
        when 'street_type'
          valid = CommonLib.validate_street_type(value)
          directors_attributes[key].upcase!
        when 'state'
          valid = CommonLib.validate_state(value)
          directors_attributes[key].upcase!
        when 'postcode'
          valid = CommonLib.validate_postcodeAU(value)
        when 'drivers_license_number'
          valid = CommonLib.validate_number(value)
        when 'date_of_birth'
          valid = CommonLib.validate_datetime(value)
        when 'license_expiry'
          valid = CommonLib.validate_datetime(value)
        else
          valid = true
        end

        directors_attributes[key] = directors_attributes[key].gsub(/\s+/, ' ').strip

        unless valid
          directors_attributes[key] = nil
          errors_report[:invalid_error] << "#{venue[:trading_name]} -
                                            #{venue[:company_abn]} -
                                            #{venue[:email]} - Invalid director[#{directors_attributes['email']}][#{key}]"
        end
      end

      directors_attributes['has_been_bankrupt'] = CommonLib::convert_to_bool(directors_attributes['has_been_bankrupt'])

      return directors_attributes
    end

    def self.directors_attributes_from_csv_hash(directors_hash)
      directors_attributes = []
      directors_hash.reject! { |key, value| value.blank? }

      keys = directors_hash.map do |key, value|
        key.split("]").count >= 2 ? key.split("]")[0] : ""
      end

      keys.uniq!
      keys.reject! { |key| key.blank? } # key sample: director[0; keys: to get quantity of directors

      # Convert CSv hash to director attributes
      keys.each do |key|
        temp_d = directors_hash.select { |hash_key, v| hash_key.split("]")[0] == key }
        director_attrs = {}

        temp_d.each do |k, value|
          d_attr = k.split("]")[1]
          d_attr = d_attr.split("[")[1]
          director_attrs[d_attr] = value
        end

        directors_attributes << director_attrs
      end

      directors_attributes.reject! { |attrs| attrs['email'].blank? }
      directors_attributes.uniq! { |attrs| attrs['email'] }

      return directors_attributes
    end

    # Create Director form import VAF
    def self.create_from_import_vaf(directors_hash, venue, errors_report, is_update = false)
      directors_attributes = directors_attributes_from_csv_hash(directors_hash)

      if is_update
        directors_emails = venue.directors.map(&:email)
        directors_attributes = directors_attributes.reject { |attrs| directors_emails.include?(attrs["email"]) }
      end

      # Create director and need_send_mail = false
      directors_attributes.each do |director_attrs|
        unless CommonLib.validate_email(director_attrs['email'])
          errors_report[:invalid_error] << "#{venue[:trading_name]} -
                                            #{venue[:company_abn]} -
                                            #{venue[:email]} - Invalid director[#{director_attrs['email']}][email]"
          next
        end

        # Validate present value, remove if invalid
        director_attrs = valid_directors_attributes(director_attrs, venue, errors_report)
        create_from_email(director_attrs['email'], venue, director_attrs)
      end
    end
  end
end
