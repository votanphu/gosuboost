module DataConcern
  extend ActiveSupport::Concern

  included do
    def downcase_fields
      exist = self.exist_attribute?('email') && self.email.present?
      self.email.strip! if exist
      self.email.downcase! if exist
      exist = self.exist_attribute?('account_contact_email') && self.account_contact_email.present?
      self.account_contact_email.strip! if exist
      self.account_contact_email.downcase! if exist
    end

    def exist_attribute?(attribute)
      self.attributes.keys.member?(attribute)
    end
  end
end
