module AscfTermsConditionConcern
  extend ActiveSupport::Concern

  def get_supplier_terms_condition
    self.supplier.get_terms_and_condition(self.vaf[:id], self.trading_terms_setting[:id])
  end
end
