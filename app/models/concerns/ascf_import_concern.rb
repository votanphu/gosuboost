module AscfImportConcern
  extend ActiveSupport::Concern

  included do
    def self.has_required_attributes(attributes)
      required_attrs = %w{venue_abn supplier_abn}
      attrs = attributes.select { |key, value| required_attrs.include?(key) }
      has_missing_attributes = attrs.values.any? { |v| v.blank? }
      return !has_missing_attributes
    end

    def self.create_default_from_import_vaf(venue, user)
      supplier = user.supplier
      ascf = venue.apply_supplier_credit_forms.where(supplier_id: supplier[:id]).first

      if ascf.blank?
        trading_term_id = supplier.get_default_trading_terms_setting[:id]
        terms_condition = supplier.get_terms_and_condition_for_create_default_ascf_from_import_vaf(venue)
        trading_term_id = terms_condition[:trading_terms_setting_id] unless terms_condition[:is_all_trading_terms_settings]

        ascf_attrs = {
          vaf_id: venue[:id],
          supplier_id: supplier[:id],
          trading_terms_setting_id: trading_term_id,
          created_by_id: user[:id],
          venue_credit_limit: nil,
          credit_limit: nil,
          status: Const::STATUS[:draft][:value]
        }

        ascf = ApplySupplierCreditForm.create(ascf_attrs)
      end

      return ascf
    end

    # Create from import apply supplier credit form
    def self.create_from_csv(csv_hashes, request_supplier_abn)
      errors_report = { :venue_exist_error => [], :venue_invalid_error => [],
                        :supplier_exist_error => [], :ascf_exist_error => [],
                        :invalid_error => [], :general_error => [], :no_data => false }
      no_data = 0

      csv_hashes.each do |csv_hash|
        apply_credit_hash = csv_hash.select { |key, value| apply_credit_attributes.include?(key) }
        trade_references_hash = csv_hash.select { |key, value| key.split("[")[0].include?("trade_reference") }
        personal_guarantees_hash = csv_hash.select { |key, value| key.split("[")[0].include?("personal_guarantee") }

        if apply_credit_hash.blank?
          no_data += 1
          next
        end

        # If venue hash don't have required fields
        next unless has_required_attributes(apply_credit_hash)

        apply_credit_hash["venue_abn"] = apply_credit_hash["venue_abn"].gsub(/\s+/, "").strip
        apply_credit_hash["supplier_abn"] = apply_credit_hash["supplier_abn"].gsub(/\s+/, "").strip

        venue = VenueApplicationForm.where(company_abn: apply_credit_hash["venue_abn"]).first

        # Add no exist error if venue is not exist
        if venue.blank?
          errors_report[:venue_exist_error] << "#{apply_credit_hash["venue_abn"]} -
                                                #{apply_credit_hash["supplier_abn"]} -
                                                Venue is not exist"
          next
        end

        if venue.directors.count == 0
          errors_report[:venue_invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                                  #{apply_credit_hash["supplier_abn"]} -
                                                  Venue has no director added"
          next
        end

        # If venue not exist, check whether there is apply credit from supplier
        supplier = Supplier.where(abn: apply_credit_hash["supplier_abn"]).first

        if supplier.blank?
          errors_report[:supplier_exist_error] << "#{apply_credit_hash["venue_abn"]} -
                                                  #{apply_credit_hash["supplier_abn"]} -
                                                  Supplier is not exist"
          next
        end

        # Authorize supplier in case current user is supplier
        if request_supplier_abn.present? && supplier[:abn] != request_supplier_abn
          errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Unauthorized supplier"
          next
        end

        ascf = venue.apply_supplier_credit_forms.where(supplier_id: supplier[:id]).first

        if ascf.present?
          errors_report[:ascf_exist_error] << "#{apply_credit_hash["venue_abn"]} -
                                              #{apply_credit_hash["supplier_abn"]} -
                                              Apply supplier credit is exist"
          next
        end

        trading_term = supplier.get_trading_term(apply_credit_hash["trading_term"])

        # trading term present but not valid
        if trading_term.blank?
          errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Trading Term"
          next
        end

        unless supplier.has_terms_and_condition?(venue, trading_term)
          errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Terms and Conditions"
          next
        end

        credit_limit = apply_credit_hash["credit_limit"].to_d

        unless CommonLib.validate_currency(credit_limit)
          errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Credit Limit"
          next
        end

        if apply_credit_hash["credit_limit"].present?
          if credit_limit <= 0
            errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Credit Limit"
            next
          end

          unless supplier.is_valid_credit_limit?(credit_limit, trading_term, venue)
            errors_report[:invalid_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]} -
                                            Credit Limit"
            next
          end
        end

        credit_limit = apply_credit_hash["credit_limit"].present? ? credit_limit : nil

        ascf_attrs = {
          supplier_id: supplier[:id],
          trading_terms_setting_id: trading_term[:id],
          created_by_id: supplier.user[:id],
          venue_credit_limit: credit_limit,
          credit_limit: credit_limit,
          status: Const::STATUS[:draft][:value]
        }

        ascf = venue.apply_supplier_credit_forms.new(ascf_attrs)

        # If venue save successfully
        if ascf.save
          # Create Personal Guarantees
          PersonalGuarantee.create_from_import_ascf(personal_guarantees_hash, ascf, errors_report)
          # Create Trade References
          TradeReference.create_from_import_ascf(trade_references_hash, ascf, errors_report)
        # Add general error when venue is not saved successfully
        else
          errors_report[:general_error] << "#{apply_credit_hash["venue_abn"]} -
                                            #{apply_credit_hash["supplier_abn"]}"
          next
        end

        required_pgs_quantity = ascf.get_quantity_of_requrired_personal_guarantees
        has_all_valid_required_information = ascf.has_all_valid_required_information?

        if has_all_valid_required_information && required_pgs_quantity > 0
          ascf.update_attributes(status: Const::STATUS[:pending_pg_signature][:value])
        end

        if has_all_valid_required_information && required_pgs_quantity <= 0
          ascf.update_attributes(status: Const::STATUS[:no_signature_required][:value])
        end
      end # end each

      errors_report[:no_data] = true if no_data == csv_hashes.count
      return errors_report
    end

    def self.import(file, request_supplier_abn = nil)
      csv_hashes = []

      begin
        CSV.foreach(file.path, headers: true, :encoding => 'UTF-8') do |row|
          csv_hashes << row.to_hash
        end
      rescue
        CSV.foreach(file.path, headers: true, :encoding => 'windows-1251:UTF-8') do |row|
          csv_hashes << row.to_hash
        end
      end

      errors_report = create_from_csv(csv_hashes, request_supplier_abn)
      return errors_report
    end
  end
end