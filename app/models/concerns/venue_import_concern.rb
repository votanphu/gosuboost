module VenueImportConcern
  extend ActiveSupport::Concern

  included do
    # Check if market segment is valid, if not set default
    def self.set_market_segment(market_segment_id)
      # ms_id: market segment id
      ms_id = MarketSegment.order(name: :asc).first[:id]

      if MarketSegment.validate_market_segment_id(market_segment_id)
        ms_id = market_segment_id
      end

      ms_id
    end

    # Validate present value, remove if invalid
    def self.valid_venue_hash(venue_hash, errors_report = nil)
      venue_hash.reject! { |key, value| value.blank? }

      venue_hash.map do |key, value|
        case key
        when 'mobile'
          valid = CommonLib.validate_mobileAU(value)
        when 'phone'
          valid = CommonLib.validate_phoneAU(value)
        when 'trading_street_type'
          valid = CommonLib.validate_street_type(value)
          venue_hash[key].upcase!
        when 'trading_state'
          valid = CommonLib.validate_state(value)
          venue_hash[key].upcase!
        when 'trading_postcode'
          valid = CommonLib.validate_postcodeAU(value)
        when 'operating_structure'
          valid = CommonLib.validate_operating_structure(value)
          venue_hash[key].upcase!
        when 'registered_street_type'
          valid = CommonLib.validate_street_type(value)
          venue_hash[key].upcase!
        when 'registered_state'
          valid = CommonLib.validate_state(value)
          venue_hash[key].upcase!
        when 'registered_postcode'
          valid = CommonLib.validate_postcodeAU(value)
        when 'bsb'
          valid = CommonLib.validate_number(value)
        when 'account_contact_email'
          valid = CommonLib.validate_email(value)
        when 'account_contact_number'
          valid = CommonLib.validate_mobileAU(value) || CommonLib.validate_phoneAU(value)
        when 'delivery_contact_number'
          valid = CommonLib.validate_mobileAU(value) || CommonLib.validate_phoneAU(value)
        else
          valid = true
        end

        venue_hash[key] = venue_hash[key].gsub(/\s+/, ' ').strip

        unless valid
          venue_hash[key] = nil

          if errors_report.present?
            errors_report[:invalid_error] << "#{venue_hash["trading_name"]} -
                                              #{venue_hash["company_abn"]} -
                                              #{venue_hash["email"]} - Invalid #{key}"
          end
        end
      end

      return venue_hash
    end

    # Set minimum errors report
    def self.set_minimum_errors_report(errors_report, venue_minimum_attrs)
      return errors_report unless venue_minimum_attrs.values.any? { |v| v.present? }

      errors_report[:missing_error] << "#{venue_minimum_attrs["trading_name"]} -
                                        [company_abn] -
                                        #{venue_minimum_attrs["email"]} - Missing ABN" if venue_minimum_attrs["company_abn"].blank?
      errors_report[:missing_error] << "[trading_name] -
                                        #{venue_minimum_attrs["company_abn"]} -
                                        #{venue_minimum_attrs["email"]} - Missing Trading Name" if venue_minimum_attrs["trading_name"].blank?
      errors_report[:missing_error] << "#{venue_minimum_attrs["trading_name"]} -
                                        #{venue_minimum_attrs["company_abn"]} -
                                        [email] - Missing Email" if venue_minimum_attrs["email"].blank?

      return errors_report
    end

    def self.create_from_csv(csv_hashes, user)
      errors_report = { :exist_error => [], :missing_error => [],
                        :invalid_error => [], :general_error => [], :no_data => false }
      no_data = 0

      csv_hashes.each do |csv_hash|
        venue_hash = csv_hash.select { |key, value| venue_attributes.include?(key) }
        directors_hash = csv_hash.select { |key, value| key.split("[")[0].include?("director") }

        if venue_hash.blank?
          no_data += 1
          next
        end

        # If venue hash don't have minimum fields
        venue_minimum_attrs = venue_hash.select { |key, value| minimum_attributes.include?(key) }

        unless has_present_attributes(venue_minimum_attrs)
          errors_report = set_minimum_errors_report(errors_report, venue_minimum_attrs)
          next
        end

        venue_hash["company_abn"] = venue_hash["company_abn"].gsub(/\s+/, "").strip
        venue = where(company_abn: venue_hash["company_abn"]).first

        # Add exist error if venue is exist and user is blank
        if venue.present? && user.blank?
          errors_report[:exist_error] << "#{venue_hash["trading_name"]} -
                                          #{venue_hash["company_abn"]} -
                                          #{venue_hash["email"]} - Venue is exist"
          Director.create_from_import_vaf(directors_hash, venue, errors_report, true)
          next
        end

        # if venue is exist and user is present
        if venue.present? && user.present?
          Director.create_from_import_vaf(directors_hash, venue, errors_report, true)
          # Create Apply Supplier Credit user_id present
          ApplySupplierCreditForm.create_default_from_import_vaf(venue, user)
          next
        end

        # If venue is not exist, create new venue
        # If ABN is invalid, add invalid error
        unless validate_abn(venue_hash["company_abn"])
          errors_report[:invalid_error] << "#{venue_hash["trading_name"]} -
                                            #{venue_hash["company_abn"]} -
                                            #{venue_hash["email"]} - Invalid company_abn"
          next
        end

        # If ABN is valid, check email
        unless CommonLib.validate_email(venue_hash["email"])
          errors_report[:invalid_error] << "#{venue_hash["trading_name"]} -
                                            #{venue_hash["company_abn"]} -
                                            #{venue_hash["email"]} - Invalid email"
          next
        end

        venue_hash = valid_venue_hash(venue_hash, errors_report) # Validate present value, remove if invalid
        venue_hash["market_segment_id"] = set_market_segment(venue_hash["market_segment_id"])
        venue_hash = venue_hash.merge(status: Const::STATUS[:draft][:value])
        venue = new(venue_hash)

        # If venue save successfully
        if venue.save
          # Create Directors
          Director.create_from_import_vaf(directors_hash, venue, errors_report)
          # Create Apply Supplier Credit user_id present
          ApplySupplierCreditForm.create_default_from_import_vaf(venue, user) if user.present?
        # Add general error when venue is not saved successfully
        else
          errors_report[:general_error] << venue[:company_abn]
          next
        end

        if venue.has_all_valid_required_information?
          venue.update_attributes(status: Const::STATUS[:pending_director_signature][:value])
        end
      end # end each

      errors_report[:no_data] = true if no_data == csv_hashes.count
      return errors_report
    end

    def self.import(file, user = nil)
      csv_hashes = []

      begin
        CSV.foreach(file.path, headers: true, :encoding => 'UTF-8') do |row|
          csv_hashes << row.to_hash
        end
      rescue
        CSV.foreach(file.path, headers: true, :encoding => 'windows-1251:UTF-8') do |row|
          csv_hashes << row.to_hash
        end
      end

      errors_report = create_from_csv(csv_hashes, user)
      return errors_report
    end
  end
end
