module AscfSendEmailsConcern
  extend ActiveSupport::Concern

  def send_form_link_to_email(email)
    link_url = "#{$prod_root_url}/admin/venue_suppliers/#{id}/edit/"

    if email.present?
      UserMailer.send_ascf_form_link(
        form_link: link_url,
        email: email
      ).deliver_later
    end
  end

  def send_email_directors_pgs_supplier_terms_condition_changed
    link_url = "#{$prod_root_url}/admin/venue_suppliers/#{id}/edit/"
    supplier_company_name = self.supplier[:company_name]
    emails = self.vaf.directors.map(&:email)
    emails = self.personal_guarantees.map(&:email)
    emails.flatten!
    emails.uniq!

    if emails.present?
      UserMailer.send_ascf_form_link_supplier_terms_condition_changed(
        form_link: link_url,
        supplier_company_name: supplier_company_name,
        emails: emails
      ).deliver_later
    end
  end

  # Send save & continue link when ascf is draft;
  # Send sign link when when ascf is pending signature
  def send_email(emails)
    return if emails.blank?
    # Send save and continue later link
    emails.each { |email| self.send_form_link_to_email(email) } if self.is_draft?
    # Send sign emails
    emails.each { |email| PersonalGuarantee.send_sign_email(email, self) } if self.is_pending_pg_signature?
  end

  # pgs: personal guarantees
  # ascf: Apply Supplier Credit Form
  def send_pgs_emails(send_email_params)
    pgs_emails = []

    # Collect personal guarantee emails
    case send_email_params[:pgs_option]
    when "all"
      pgs_emails = self.personal_guarantees.map(&:email)
      self.send_email(pgs_emails)
    when "single_pg"
      if send_email_params[:personal_guarantee_id].present?
        personal_guarantee = PersonalGuarantee.where(id: send_email_params[:personal_guarantee_id]).first
        pgs_emails << personal_guarantee[:email] if personal_guarantee.present?
        self.send_email(pgs_emails)
      end
    when "email"
      email = send_email_params[:personal_guarantee_email] if send_email_params[:personal_guarantee_email].present?

      if CommonLib.validate_email(email)
        pgs_emails << email
        self.send_email(pgs_emails)
      end
    when "pg_single_director"
      if send_email_params[:pg_director_id].present?
        director = Director.where(id: send_email_params[:pg_director_id]).first
        pgs_emails << director[:email] if director.present?
        self.send_email(pgs_emails)
      end
    when "sms"
      # Update soon
    end

    pgs_emails.present?
  end
end