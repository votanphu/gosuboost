module PersonalGuaranteeImportConcern
  extend ActiveSupport::Concern

  included do
    # Validate present value, remove if invalid
    def self.valid_personal_guarantees_attributes(pg_attributes, ascf, errors_report)
      pg_attributes.map do |key, value|
        case key
        when 'mobile'
          valid = CommonLib.validate_mobileAU(value)
        when 'phone'
          valid = CommonLib.validate_phoneAU(value)
        when 'home_street_type'
          valid = CommonLib.validate_street_type(value)
          pg_attributes[key].upcase!
        when 'postal_street_type'
          valid = CommonLib.validate_street_type(value)
          pg_attributes[key].upcase!
        when 'home_state'
          valid = CommonLib.validate_state(value)
          pg_attributes[key].upcase!
        when 'postal_state'
          valid = CommonLib.validate_state(value)
          pg_attributes[key].upcase!
        when 'home_postcode'
          valid = CommonLib.validate_postcodeAU(value)
        when 'postal_postcode'
          valid = CommonLib.validate_postcodeAU(value)
        else
          valid = true
        end

        pg_attributes[key] = pg_attributes[key].gsub(/\s+/, ' ').strip

        unless valid
          pg_attributes[key] = nil
          errors_report[:invalid_error] << "#{ascf.vaf[:company_abn]} -
                                            #{ascf.supplier[:abn]} -
                                            Invalid personal_guarantee[#{pg_attributes['email']}][#{key}]"
        end
      end

      return pg_attributes
    end

    def self.pgs_attributes_from_csv_hash(pgs_hash)
      personal_guarantees_attributes = []
      pgs_hash.reject! { |key, value| value.blank? }

      keys = pgs_hash.map do |key, value|
        key.split("]").count >= 2 ? key.split("]")[0] : ""
      end

      keys.uniq!
      keys.reject! { |key| key.blank? }

      # Convert CSv hash to director attributes
      keys.each do |key|
        temp_d = pgs_hash.select { |hash_key, v| hash_key.split("]")[0] == key }
        personal_guarantee_attributes = {}

        temp_d.each do |k, value|
          pg_attr = k.split("]")[1]
          pg_attr = pg_attr.split("[")[1]
          personal_guarantee_attributes[pg_attr] = value
        end

        personal_guarantees_attributes << personal_guarantee_attributes
      end

      personal_guarantees_attributes.reject! { |pg_attr| pg_attr['email'].blank? }
      personal_guarantees_attributes.uniq! { |pg_attr| pg_attr['email'] }

      return personal_guarantees_attributes
    end

    # Create Personal Guarantee form import Apply Supplier Credit Form
    def self.create_from_import_ascf(pgs_hash, ascf, errors_report)
      personal_guarantees_attributes = pgs_attributes_from_csv_hash(pgs_hash)

      # Create director and need_send_mail = false
      personal_guarantees_attributes.each do |pg_attrs|
        unless CommonLib.validate_email(pg_attrs['email'])
          errors_report[:invalid_error] << "#{ascf.vaf[:company_abn]} -
                                            #{ascf.supplier[:abn]} -
                                            Invalid personal_guarantee[#{pg_attrs['email']}][email]"
          next
        end

        # Validate present value, remove if invalid
        pg_attrs = valid_personal_guarantees_attributes(pg_attrs, ascf, errors_report)
        create_from_email(pg_attrs['email'], ascf, pg_attrs)
      end
    end
  end
end
