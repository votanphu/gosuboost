module EncryptorConcern
  extend ActiveSupport::Concern

  included do
    def attribute_encrypted_key
      ENV['ATTRIBUTE_ENCRYPTED_KEY']
    end
  end
end
