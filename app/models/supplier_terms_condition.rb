class SupplierTermsCondition < ApplicationRecord
  belongs_to :supplier
  belongs_to :trading_terms_setting, optional: true
  has_many :supplier_terms_condition_market_segments, dependent: :destroy
  has_many :market_segments, through: :supplier_terms_condition_market_segments

  after_create :notify_directors_pgs, if: Proc.new { self.content.present? }
  after_update :notify_directors_pgs, if: :is_content_changed?

  def get_ascfs
    # supplier => ascfs
    ascfs = []

    supplier.apply_supplier_credit_forms.each do |ascf|
      ascfs << ascf if ascf.get_supplier_terms_condition[:id] == self[:id]
    end

    return ascfs
  end

  def is_deletable?
    self.get_ascfs.each do |ascf|
      # Return false if there is ascf not draft
      return false if ascf.is_draft?
      # Return false if there is ascf status is pending signature and has not been signed by anyone
      return false if ascf.is_pending_pg_signature? && !ascf.is_signed_by_personal_guarantee?
    end

    return true
  end

  # Notify directors and personal guarantee affected by the change
  def notify_directors_pgs
    self.get_ascfs.each do |ascf|
      ascf.send_email_directors_pgs_supplier_terms_condition_changed
    end
  end

  def self.find_terms_condition_match_market_segment(supplier_terms_conditions, market_segment)
    result = nil

    result = supplier_terms_conditions.find { |stc| stc.market_segments.include?(market_segment) }
    return result if result.present?
    # Find T&C for all market segment
    result = supplier_terms_conditions.find { |stc| stc[:is_all_market] }
    return result if result.present?

    result
  end

  def is_content_changed?
    CommonLib::text_different?(self.content_before_last_save, self.content)
  end

  def self.set_market_segment(attributes, terms_and_condition_params)
    if terms_and_condition_params["market_segments_list"].present? &&
          terms_and_condition_params["market_segments_list"].include?("All")
      attributes = attributes.merge(
        is_all_market: true
      )
    elsif terms_and_condition_params["market_segments_list"].present? &&
            !terms_and_condition_params["market_segments_list"].include?("All")
      attributes = attributes.merge(
        is_all_market: false
      )
    end

    attributes
  end

  def self.set_trading_terms_setting(attributes, terms_and_condition_params, trading_terms_setting_id)
    if terms_and_condition_params["trading_terms_setting_id"].present? &&
        terms_and_condition_params["trading_terms_setting_id"].include?("All")
      attributes = attributes.merge(
        is_all_trading_terms_settings: true,
        trading_terms_setting_id: trading_terms_setting_id
      )
    elsif terms_and_condition_params["trading_terms_setting_id"].present? &&
            !terms_and_condition_params["trading_terms_setting_id"].include?("All")
      attributes = attributes.merge(
        is_all_trading_terms_settings: false,
        trading_terms_setting_id: terms_and_condition_params["trading_terms_setting_id"]
      )
    end

    attributes
  end
end
