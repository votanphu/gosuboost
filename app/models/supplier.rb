class Supplier < ApplicationRecord
  belongs_to :user
  has_many :apply_supplier_credit_forms, dependent: :destroy
  has_many :venue_appication_forms, through: :apply_supplier_credit_forms
  has_many :trading_terms_settings, dependent: :destroy
  has_many :supplier_terms_conditions, dependent: :destroy
  has_many :supplier_terms_condition_market_segments, through: :supplier_terms_conditions, dependent: :destroy
  has_many :credit_limits, dependent: :destroy

  before_save :downcase_fields
  after_create :send_supplier_registration, :init_default_trading_terms_settings

  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" },
    default_url: "/assets/non-logo.png",
    url: "/media/suppliers/:id/:style/:hash.:extension",
    path: ":rails_root/public/media/suppliers/:id/:style/:hash.:extension",
    hash_secret: "G0sl3tm31n"

  validates_attachment_content_type :logo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates :email, uniqueness: true, format: { with: Const::VALID_EMAIL_REGEX }
  validates :abn, uniqueness: true

  def init_default_trading_terms_settings
    supplier = self
    # Init trading_terms_settings data
    trading_terms_settings_attributes = [
      {
        trading_term: 'Cash on Delivery',
        personal_guarantees: 'None',
        directors: '1',
        active: true,
        default: false,
        supplier_id: supplier[:id]
      },
      {
        trading_term: '7 Days',
        personal_guarantees: 'None',
        directors: '1',
        active: true,
        default: true,
        supplier_id: supplier[:id]
      },
      {
        trading_term: '14 Days',
        personal_guarantees: '1',
        directors: '2',
        active: true,
        default: false,
        supplier_id: supplier[:id]
      },
      {
        trading_term: '30 Days',
        personal_guarantees: '2',
        directors: 'All',
        active: true,
        default: false,
        supplier_id: supplier[:id]
      },
    ]

    trading_terms_settings_attributes.each do |trading_terms_setting_attributes|
      trading_terms_setting = TradingTermsSetting.new(trading_terms_setting_attributes)

      if supplier.trading_terms_settings.count < 4
        trading_terms_setting.save! unless trading_terms_setting_attributes[:default]
        trading_terms_setting.save! if trading_terms_setting_attributes[:default] && supplier.trading_terms_settings.where(default: true).count == 0
      end
    end
  end

  def get_terms_and_condition(vaf_id, trading_term_id)
    result = nil
    supplier_terms_conditions = []
    market_segment = VenueApplicationForm.find(vaf_id).market_segment

    # Get T&Cs match supplier and trading term
    supplier_terms_conditions = SupplierTermsCondition
      .where(trading_terms_setting_id: trading_term_id, supplier_id: self[:id] )
      .order("created_at desc")
    # Find T&C for vaf market segment
    result = SupplierTermsCondition.find_terms_condition_match_market_segment(supplier_terms_conditions, market_segment)
    return result if result.present?

    # Get T&Cs match supplier and all trading terms
    supplier_terms_conditions = SupplierTermsCondition
      .where(is_all_trading_terms_settings: true, supplier_id: self[:id])
      .order("created_at desc")
    # Find T&C for vaf market segment
    result = SupplierTermsCondition.find_terms_condition_match_market_segment(supplier_terms_conditions, market_segment)
    return result if result.present?

    result
  end

  def get_terms_and_condition_for_create_default_ascf_from_import_vaf(venue)
    terms_condition = nil
    trading_term_id = self.get_default_trading_terms_setting[:id]
    terms_condition = self.get_terms_and_condition(venue[:id], trading_term_id)

    if terms_condition.blank?
      terms_condition = SupplierTermsCondition.find_terms_condition_match_market_segment(
                                                  self.supplier_terms_conditions.order("created_at desc"),
                                                  venue.market_segment)
    end

    if terms_condition.blank?
      terms_condition = self.supplier_terms_conditions.first
      venue.update_attributes(market_segment_id: terms_condition.market_segments.first[:id]) unless terms_condition[:is_all_market]
    end

    terms_condition
  end

  def self.available_trading_terms_settings(supplier_id, vaf_id)
    market_segment_vaf = VenueApplicationForm.find_by_id(vaf_id).market_segment
    supplier = Supplier.find(supplier_id)

    # Martket = All - Trading Term: All
    supplier_terms_conditions = supplier.supplier_terms_conditions.where(is_all_trading_terms_settings: true,
      is_all_market: true)
    return supplier.trading_terms_settings.ids if supplier_terms_conditions.present?

    # Martket = Not All - Trading Term: All
    supplier_terms_conditions = supplier.supplier_terms_conditions.where(is_all_trading_terms_settings: true,
      is_all_market: false)

    if supplier_terms_conditions.present?
      supplier_terms_conditions.each do |supplier_terms_condition|
        if supplier_terms_condition.market_segments.include?(market_segment_vaf)
          return supplier.trading_terms_settings.ids
        end
      end
    end

    trading_term_ids = []

    # Martket = All - Trading Term: Not All
    supplier_terms_conditions = supplier.supplier_terms_conditions.where(is_all_trading_terms_settings: false,
      is_all_market: true)

    if supplier_terms_conditions.present?
      supplier_terms_conditions.each do |supplier_terms_condition|
        trading_term_ids << supplier_terms_condition.trading_terms_setting_id
      end
    end

    # Martket = Not All - Trading Term: Not All
    supplier_terms_conditions = supplier.supplier_terms_conditions.where(is_all_trading_terms_settings: false,
      is_all_market: false)

    if supplier_terms_conditions.present?
      supplier_terms_conditions.each do |supplier_terms_condition|
        if supplier_terms_condition.market_segments.include?(market_segment_vaf)
          trading_term_ids << supplier_terms_condition.trading_terms_setting_id
        end
      end
    end

    return trading_term_ids if trading_term_ids.present?
    nil
  end

  def self.get_supplier_valid_for_vaf(vaf_id)
    market_segment_id = VenueApplicationForm.find(vaf_id)[:market_segment_id]
    supplier_terms_condition_ids = SupplierTermsConditionMarketSegment.where(market_segment_id: market_segment_id)
      .map(&:supplier_terms_condition_id)
    supplier_terms_conditions = SupplierTermsCondition.where(is_all_market: true)
    supplier_ids = supplier_terms_conditions.or(SupplierTermsCondition.where(id: supplier_terms_condition_ids))
      .map(&:supplier_id)
    reject_supplier_ids = ApplySupplierCreditForm.where(vaf_id: vaf_id, active: true).map(&:supplier_id)
    reject_supplier_ids.uniq!
    Supplier.where(id: (supplier_ids - reject_supplier_ids))
  end

  def get_venue_application_forms
    venue_forms = []
    apply_supplier_credit_forms.each { |cf| venue_forms << cf.vaf }
    venue_forms.uniq! { |f| f[:id] }
    return venue_forms
  end

  def get_default_trading_terms_setting
    self.trading_terms_settings.where(default: true).first
  end

  def send_supplier_registration(options = {})
    specific_user = user
    specific_user = User.user_by_email(options[:user_email]) if options[:user_email].present?
    set_password_link = specific_user.get_setting_password_url
    sent_email = specific_user[:email]

    UserMailer.supplier_registration(
      contact_name: contact_name,
      email: sent_email,
      first_name: first_name,
      last_name: last_name,
      company_name: company_name,
      set_password_link: set_password_link
    ).deliver_later
  end

  # Get credit limit
  def get_credit_limit(market_segment_id, trading_terms_setting_id)
    credit_limit = self.credit_limits.where(
                                            market_segment_id: market_segment_id,
                                            trading_terms_setting_id: trading_terms_setting_id,
                                            is_all_market_segments: false,
                                            is_all_trading_terms: false
                                            ).first
    return credit_limit if credit_limit.present?

    credit_limit = self.credit_limits.where(
                                            market_segment_id: market_segment_id,
                                            is_all_market_segments: false,
                                            is_all_trading_terms: true
                                            ).first
    return credit_limit if credit_limit.present?


    credit_limit = self.credit_limits.where(
                                            trading_terms_setting_id: trading_terms_setting_id,
                                            is_all_market_segments: true,
                                            is_all_trading_terms: false
                                            ).first
    return credit_limit if credit_limit.present?

    credit_limit = self.credit_limits.where(
                                            is_all_market_segments: true,
                                            is_all_trading_terms: true
                                            ).first
    return credit_limit if credit_limit.present?

    return nil
  end

  # Check whether is trading_term(string) is a supplier valid trading term
  # If null return default, if valid return valid, if not valid return nil
  def get_trading_term(trading_term)
    return self.get_default_trading_terms_setting if trading_term.blank?
    trading_terms = self.trading_terms_settings.where(active: true)
    trading_term = trading_term.gsub(/\s+/, " ").strip

    trading_terms.each do |term|
      if term[:trading_term].casecmp(trading_term) == 0
        return term
      end
    end

    return nil
  end

  def has_terms_and_condition?(venue, trading_term)
    return false if self.supplier_terms_conditions.blank?
    terms_and_condition = self.get_terms_and_condition(venue[:id], trading_term[:id])
    return false if terms_and_condition.blank?
    return true
  end

  def is_valid_credit_limit?(credit_limit, trading_term, venue)
    supplier_credit_limit = self.get_credit_limit(venue[:market_segment_id], trading_term[:id])
    return true if supplier_credit_limit.blank? && credit_limit > 0

    is_valid = supplier_credit_limit.present? && credit_limit <= supplier_credit_limit[:credit_limit]
    is_valid = is_valid && credit_limit >= supplier_credit_limit[:increments]
    return true if is_valid

    return false
  end
end
