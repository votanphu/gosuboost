class PersonalGuarantee < ApplicationRecord
  include PersonalGuaranteeImportConcern

  belongs_to :user
  has_many :venue_application_form_personal_guarantees, dependent: :destroy
  has_many :venue_application_forms, through: :venue_application_form_personal_guarantees
  has_many :apply_supplier_credit_personal_guarantees, dependent: :destroy
  has_many :apply_supplier_credit_forms, through: :apply_supplier_credit_personal_guarantees

  before_save :downcase_fields

  # ascf: ApplySupplierCreditForm
  # pga: personal_guarantee_application
  def send_pga_for_supplier_credit(ascf, is_exist_user = false)
    if user.created_at == user.updated_at
      set_password_link = user.get_setting_password_url(ascf_token: ascf[:save_token])
    else
      set_password_link = ascf.sign_in_to_form_url(user)
    end

    # pga: personal_guarantee_application
    UserMailer.pga_for_supplier_credit(
      personal_guarantee_email: email,
      personal_guarantee_firstname: firstname,
      personal_guarantee_lastname: lastname,
      trading_name: ascf.vaf[:trading_name],
      company_name: ascf.supplier[:company_name],
      set_password_link: set_password_link
    ).deliver_later
  end

  def get_venue_application_forms
    vaf_ids = apply_supplier_credit_forms.map(&:vaf_id)
    VenueApplicationForm.where(id: vaf_ids)
  end

  # Check whether this pg is also a director in a ascf
  def is_director?(ascf)
    self.apply_supplier_credit_personal_guarantees.where(apply_supplier_credit_form_id: ascf[:id],
                                                          is_director: true).present?
  end

  def get_director_id(ascf)
    ascfpg = self.apply_supplier_credit_personal_guarantees.where(apply_supplier_credit_form_id: ascf[:id],
                                                          is_director: true).first
    if ascfpg.present?
      return ascfpg[:director_id]
    end

    nil
  end

  def has_missing_information?
    self.attributes.values_at(*PersonalGuarantee.required_pg_attributes).any? { |value| value.blank? }
  end

  # Send sign email, create pg if not added in ascf
  # ascf: Apply Supplier Credit Form
  def self.send_sign_email(email, ascf)
    personal_guarantee = ascf.personal_guarantees.where(email: email).first
    personal_guarantee = create_from_email(email, ascf) if personal_guarantee.blank?
    personal_guarantee.send_pga_for_supplier_credit(ascf)
  end

  # Check email whether has user yet if not create one
  # ascf: Apply Supplier Credit Form
  def self.create_from_email(email, ascf, pg_attributes = {})
    user = User.where(email: email).first
    is_exist_user = true

    if user.blank?
      is_exist_user = false
      user = User.create_user_if_not_exist(email, Const::ROLES[:personal_guarantee], pg_attributes)
    end

    temp_name = email.split("@")[0]

    pg_attributes = {
      email: email,
      firstname: temp_name,
      lastname: ""
    } if pg_attributes.blank?

    personal_guarantee = user.personal_guarantees.create(pg_attributes)

    attributes = {
      apply_supplier_credit_form_id: ascf[:id],
      personal_guarantee_id: personal_guarantee[:id]
    }

    director = ascf.vaf.directors.where(email: personal_guarantee[:email]).first

    attributes = attributes.merge(
      director_id: director[:id],
      is_director: true
    ) if director.present?

    ApplySupplierCreditPersonalGuarantee.where(apply_supplier_credit_form_id: ascf[:id],
      personal_guarantee_id: personal_guarantee[:id]).first_or_initialize.tap do |item|
      next unless item.new_record?
      item.assign_attributes(attributes)
      item.save!
    end

    if director.present?
      director.user.add_role(Const::ROLES[:personal_guarantee]) unless director.user.is_only_personal_guarantee?
    end

    personal_guarantee
  end

  def self.personal_guarantee_attributes
    %w{firstname lastname mobile phone email home_property_name home_unit_number home_street_number
      home_street_name home_street_type home_suburb home_state home_postcode postal_property_name
      postal_unit_number postal_street_number postal_street_name postal_street_type postal_suburb postal_state
      postal_postcode
      }
  end

  def self.required_pg_attributes
    %w{firstname lastname mobile email home_street_number
      home_street_name home_street_type home_suburb home_state home_postcode
      }
  end
end
