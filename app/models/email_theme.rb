class EmailTheme < ApplicationRecord
  belongs_to :created_by, class_name: "User"
  validates :name, presence: true

  CATEGORY = [
    { name: 'default', value: 'GENERAL' }
  ]

  def self.get_default
    EmailTheme.where(category: 'GENERAL').first
  end
end
