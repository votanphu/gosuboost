class CreditLimit < ApplicationRecord
  belongs_to :supplier
  belongs_to :market_segment
  belongs_to :trading_terms_setting

  validate :validate_credit_limit, :validate_increments

  def validate_credit_limit
    unless validate_currency(credit_limit)
      errors.add(:credit_limit, 'Invalid value!')
    end
  end

  def validate_increments
    unless validate_currency(increments)
      errors.add(:increments, 'Invalid value!')
    end
  end

  def validate_currency(currency, precision = 15)
    currency.to_s.gsub(".", "").length <= precision
  end

  def self.set_market_segment(credit_limit_attrs, credit_limit_params)
    if credit_limit_params[:market_segment_id].include?("All")
      return credit_limit_attrs.merge(
        market_segment_id: MarketSegment.order(name: :asc).first[:id]
      )
    end

    credit_limit_attrs.merge(
      is_all_market_segments: false,
      market_segment_id: credit_limit_params[:market_segment_id]
    )
  end

  def self.set_trading_terms_setting(credit_limit_attrs, credit_limit_params, trading_terms_setting_id)
    if credit_limit_params[:trading_terms_setting_id].include?("All")
      return credit_limit_attrs.merge(
        trading_terms_setting_id: trading_terms_setting_id
      )
    end

    credit_limit_attrs.merge(
      is_all_trading_terms: false,
      trading_terms_setting_id: credit_limit_params[:trading_terms_setting_id]
    )
  end
end
