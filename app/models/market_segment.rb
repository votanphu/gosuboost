class MarketSegment < ApplicationRecord
  has_many :venue_application_forms, dependent: :destroy
  has_many :credit_limits, dependent: :destroy

  def self.validate_market_segment_id(market_segment_id)
    return all.map(&:id).include?(market_segment_id.to_i)
  end
end
