class PagesController < ApplicationController
  def venue_registration
  end

  def home
    $current_page = 'Pencil | Home'
    @contact = Contact.new
  end

  def terms_of_use
  end

  def privacy
  end

  def faq
  end

  def not_found
  end
end
