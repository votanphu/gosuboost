class AbnController < ApplicationController
  def abn_details
    respond_to do |format|
      result = VenueApplicationForm.check_unique_abn(params[:company_abn], params[:vaf_id])
      format.json { render json: result, status: :ok }
    end
  end
end
