class DirectorsController < ApplicationController
  def upload_front_license_file
    @director = Director.where(id: params['id']).first

    if @director.blank?
      respond_to do |format|
        format.json {
          render json: {
            director: nil,
            message: 'Upload file successfully.'
          },
          status: :created
        }
      end

      return
    else
      @director.front_license_file = params['file']
      @director.email = params['email']
    end

    respond_to do |format|
      if @director.save
        director = @director.as_json

        if @director.front_license_file.present?
          director[:front_license_original_url] = @director.front_license_file.url
          director[:front_license_medium_url] = @director.front_license_file.url(:medium)
          director[:front_license_thumb_url] = @director.front_license_file.url(:thumb)
        end

        if @director.back_license_file.present?
          director[:back_license_original_url] = @director.back_license_file.url
          director[:back_license_medium_url] = @director.back_license_file.url(:medium)
          director[:back_license_thumb_url] = @director.back_license_file.url(:thumb)
        end

        format.json {
          render json: {
            director: director,
            message: 'Director has been saved successfully.'
          },
          status: :created
        }
      else
        format.json {
          render json: {
            director: nil,
            message: @director.errors
          },
          status: :unprocessable_entity
        }
      end
    end
  end

  def upload_back_license_file
    @director = Director.where(id: params['id']).first

    if @director.blank?
      respond_to do |format|
        format.json {
          render json: {
            director: nil,
            message: 'Upload file successfully.'
          },
          status: :created
        }
      end

      return
    else
      @director.back_license_file = params['file']
      @director.email = params['email']
    end

    respond_to do |format|
      if @director.save
        director = @director.as_json

        if @director.front_license_file.present?
          director[:front_license_original_url] = @director.front_license_file.url
          director[:front_license_medium_url] = @director.front_license_file.url(:medium)
          director[:front_license_thumb_url] = @director.front_license_file.url(:thumb)
        end

        if @director.back_license_file.present?
          director[:back_license_original_url] = @director.back_license_file.url
          director[:back_license_medium_url] = @director.back_license_file.url(:medium)
          director[:back_license_thumb_url] = @director.back_license_file.url(:thumb)
        end

        format.json {
          render json: {
            director: director,
            message: 'Director has been saved successfully.'
          },
          status: :created
        }
      else
        format.json {
          render json: {
            director: nil,
            message: @director.errors
          },
          status: :unprocessable_entity
        }
      end
    end
  end
end
