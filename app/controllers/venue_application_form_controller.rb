class VenueApplicationFormController < ApplicationController
  layout :set_layout
  before_action :set_venue_application_form, only: [:update, :send_form_link_to_email, :destroy]
  before_action :authenticate_user!, only: [:destroy]

  # GET ${API_URL}/venue-registration/token/${token}.json
  def get_by_token
    $current_page = 'Pencil | Venue Form'
    @venue_application_form = VenueApplicationForm.get_vaf_by_token(params['token'])
    @is_signed = false

    respond_to do |format|
      if @venue_application_form.present?
        if user_signed_in? && current_user.is_admin?
          venue_application_form = @venue_application_form.as_json
          venue_application_form[:directors] = @venue_application_form.directors.as_json

          format.html { render :venue_form }
          format.json {
            render json: {
              venue_application_form: venue_application_form,
              message: 'Get Venue Application Form successfully.'
            },
            status: :ok
          }
        else
          if !user_signed_in? && params['step'].present? && params['step'] == VenueApplicationForm::STEP_REVIEW
            flash[:alert] = 'You please sign in before review a venue form!'
            uri = URI.parse(@venue_application_form.sign_in_to_form_url)
            redirect_to "#{uri.path}?#{uri.query}"
            return
          end

          @is_signed = @venue_application_form.is_signed_by_director

          if @is_signed && (params['step'].blank? || params['step'] != VenueApplicationForm::STEP_REVIEW)
            uri = URI.parse(@venue_application_form.get_review_url)
            redirect_to "#{uri.path}?#{uri.query}"
            return
          end

          venue_application_form = @venue_application_form.as_json
          venue_application_form[:directors] = @venue_application_form.directors.as_json

          format.html { render :venue_form }
          format.json {
            render json: {
              venue_application_form: venue_application_form,
              message: 'Get Venue Application Form successfully.'
            },
            status: :ok
          }
        end
      else
        format.html { redirect_to pages_not_found_path }
        format.json {
          render json: {
            message: 'Not found.'
          },
          status: :not_found
        }
      end
    end
  end

  # POST /venue-registration
  def create
    vaf_params = params[:venue_application_form]

    if params["is_save_and_continue_later"].present?
      is_save_and_continue_later = CommonLib.convert_to_bool(params["is_save_and_continue_later"])
      status = Const::STATUS[:draft][:value]
    else
      # Submit
      check_valid = VenueApplicationForm.valid_hash_params(vaf_params)

      if check_valid[:invalid]
        respond_to do |format|
          format.json {
            render json: {
              venue_application_form: nil,
              message: check_valid[:message]
            },
            status: :unprocessable_entity
          }
        end

        return
      end

      is_save_and_continue_later = false
      status = Const::STATUS[:pending_director_signature][:value]
    end

    @venue_application_form = VenueApplicationForm.create(venue_application_form_params)
    @venue_application_form[:status] = status

    respond_to do |format|
      if @venue_application_form.save
        venue_application_form = @venue_application_form.as_json
        venue_application_form[:directors] = create_directors(params, @venue_application_form, is_save_and_continue_later)

        format.json {
          render json: {
            venue_application_form: venue_application_form,
            message: 'Venue Application Form has been saved successfully. Please click OK and then either copy your saved form URL or send a copy of your saved form URL to your email.',
            redirect_url: venue_application_form_venue_form_success_path
          },
          status: :created
        }
      else
        format.json {
          render json: {
            venue_application_form: nil,
            message: @venue_application_form.errors.full_messages.to_sentence
          },
          status: :unprocessable_entity
        }
      end
    end
  end

  # PATCH/PUT /venue_application_forms/1.json
  def update
    vaf_params = params[:venue_application_form]

    if params["is_save_and_continue_later"].present?
      is_save_and_continue_later = CommonLib.convert_to_bool(params["is_save_and_continue_later"])
      status = Const::STATUS[:draft][:value]
    else
      # Submit
      check_valid = VenueApplicationForm.valid_hash_params(vaf_params, @venue_application_form)

      if check_valid[:invalid]
        respond_to do |format|
          format.json {
            render json: {
              venue_application_form: nil,
              message: check_valid[:message]
            },
            status: :unprocessable_entity
          }
        end

        return
      end

      is_save_and_continue_later = false
      status = Const::STATUS[:pending_director_signature][:value]
    end

    @venue_application_form[:status] = status

    respond_to do |format|
      if @venue_application_form.update(venue_application_form_params)
        venue_application_form = @venue_application_form.as_json
        venue_application_form[:directors] = update_directors(params, @venue_application_form, is_save_and_continue_later)

        format.json {
          render json: {
            venue_application_form: venue_application_form,
            message: 'Venue Application Form has been saved successfully. Please click OK and then either copy your saved form URL or send a copy of your saved form URL to your email.',
            redirect_url: venue_application_form_venue_form_success_path
          },
          status: :ok
        }
      else
        format.json {
          render json: {
            venue_application_form: nil,
            message: @venue_application_form.errors.full_messages.to_sentence
          },
          status: :unprocessable_entity
        }
      end
    end
  end

  # DELETE /venue-registration/1
  def destroy
    # If current user is admin
    if current_user.is_admin?
      if @venue_application_form.destroy
        flash[:notice] = "Venue application form was successfully deleted."
        redirect_to admin_venue_details_url
      else
        flash[:notice] = "Venue application form was not successfully deleted."
        redirect_to admin_venue_details_url
      end
    end

    # If venue form belongs to current user
    if @venue_application_form.is_include_user(current_user)
      # If venue form has not signed by anyone yet
      if !@venue_application_form.is_signed
        if @venue_application_form.destroy
          flash[:notice] = "Venue application form was successfully deleted."
          redirect_to admin_venue_details_url
        else
          flash[:notice] = "Venue application form was not successfully deleted."
          redirect_to admin_venue_details_url
        end
      # The Venue Form had been signed
      else
        flash[:notice] = "Venue application form had been signed."
        redirect_to admin_venue_details_url
      end
    end
  end

  def venue_form
    $current_page = 'Pencil | Venue Form'
    @venue_application_form = VenueApplicationForm.new
  end

  def send_form_link_to_email
    link_url = "#{$prod_root_url}#{venue_application_form_venue_form_token_path(params[:token])}"

    respond_to do |format|
      # format.html { redirect_to link_url, notice: 'Sent email successfully.' }
      if params['email'].present?
        UserMailer.send_form_link(
          form_link: link_url,
          email: params['email']
        ).deliver_later

        format.json {
          render json: {
            venue_application_form: @venue_application_form,
            message: "Sent to the email #{params['email']} successfully."
          },
          status: :ok
        }
      else
        format.json {
          render json: {
            message: 'Email is invalid.'
          },
          status: :not_found
        }
      end
    end
  end

  def director_sign
    vaf = VenueApplicationForm.get_vaf_by_token(params['form_token'])

    respond_to do |format|
      if user_signed_in? && vaf.present? &&
        vaf.is_include_user(current_user) && params['signature_image'].present?

        director = vaf.get_director_by_user(current_user)
        vafd = vaf.get_vafd_by_director(director)
        vafd.set_signature(params['signature_image'], remote_ip, request_latitude, request_longitude)

        if vafd.save
          if vaf.is_signed_by_all_directors?
            vaf[:status] = Const::STATUS[:completed][:value]

            UserMailer.directors_signed_all(
              vaf: vaf
            ).deliver_later
          else
            UserMailer.director_signed_send_all_directors(
              director: director,
              vaf: vaf
            ).deliver_later

            vaf[:status] = Const::STATUS[:pending_director_signature][:value]
          end

          vaf.save

          format.json {
            render json: {
              venue_application_form: @venue_application_form,
              venue_application_form_director: vafd,
              redirect_url: admin_admin_index_path,
              message: "#{current_user[:email]} signed this form successfully."
            },
            status: :ok
          }
        else
          format.json {
            render json: {
              message: vafd.errors
            },
            status: :unprocessable_entity
          }
        end
      else
        format.json {
          render json: {
            message: 'Data is invalid.'
          },
          status: :not_found
        }
      end
    end
  end

  def venue_form_success
  end

  private
    def create_directors(params, venue_application_form, is_save_and_continue_later = false)
      if params['directors'].present?
        directors_params = params['directors'].as_json.values
        directors_params = [] << directors_params unless directors_params.instance_of?(Array)

        directors_params.each do |director_params|
          next if director_params['email'].blank?
          director = Director.where(id: director_params['id']).first
          director_attributes = director_params.reject{ |k,v| !Director.new.attributes.keys.member?(k.to_s) }

          if director.blank?
            user = User.where(email: director_params['email']).first
            is_exist_user = true

            if user.blank?
              is_exist_user = false
              new_password = SecureRandom.base64(16)

              user_attributes = {
                email: director_params['email'],
                password: new_password,
                password_confirmation: new_password,
                role: 'DIRECTOR',
                firstname: director_params['firstname'],
                lastname: director_params['lastname'],
                name: "#{director_params['firstname']} #{director_params['lastname']}"
              }

              user = User.new(user_attributes)
              user.skip_confirmation!
              user.save!
            end

            director = user.directors.create(director_attributes)
          end

          director_attributes['id'] = director[:id]
          director.attributes = director_attributes

          if director_params['front_license_file'].present?
            director.set_front_license_file(director_params['front_license_file'])
          end

          if director_params['back_license_file'].present?
            director.set_back_license_file(director_params['back_license_file'])
          end

          director.save
          director.send_director_registration(venue_application_form, is_exist_user) unless is_save_and_continue_later

          attributes = {
            venue_application_form_id: venue_application_form[:id],
            director_id: director[:id],
            signature_image: director_params['signature_image'],
            is_signed: director_params['signature_image'].present? ? true : false,
            form_data: venue_application_form,
            is_director_guarantee: CommonLib.convert_to_bool(director_params['is_director_guarantee'])
          }

          VenueApplicationFormDirector.where(venue_application_form_id: venue_application_form[:id],
            director_id: director[:id]).first_or_initialize.tap do |item|
            next unless item.new_record?
            item.assign_attributes(attributes)
            item.save!
          end
        end
      end

      venue_application_form.directors.as_json
    end

    def update_directors(params, venue_application_form, is_save_and_continue_later = false)
      if params['directors'].present?
        directors_params = params['directors'].as_json.values
        directors_params = [] << directors_params unless directors_params.instance_of?(Array)

        # Check and destroy Directors
        willKeepDirectors = directors_params.map { |obj| obj['email'] }
        willDestroyDirectors = venue_application_form.directors.where.not(email: willKeepDirectors)
        willDestroyDirectors.destroy_all

        directors_params.each do |director_params|
          next if director_params['email'].blank?
          vaf_director = VenueApplicationFormDirector.where(venue_application_form_id: @venue_application_form[:id],
            director_id: director_params['id']).first
          director_attributes = director_params.reject{ |k,v| !Director.new.attributes.keys.member?(k.to_s) }
          next if vaf_director.present? && vaf_director[:is_signed] == true

          if vaf_director.present?
            director = vaf_director.director
            director.attributes = director_attributes
            director.save

            vaf_director.update({
              signature_image: director_params['signature_image'],
              is_signed: director_params['signature_image'].present? ? true : false,
              form_data: venue_application_form
            })

            unless vaf_director[:is_signed]
              director.send_director_registration(venue_application_form, true) unless is_save_and_continue_later
            end
          else
            user = User.where(email: director_params['email']).first
            is_exist_user = true

            if user.blank?
              new_password = SecureRandom.base64(16)
              is_exist_user = false

              user_attributes = {
                email: director_params['email'],
                password: new_password,
                password_confirmation: new_password,
                role: 'DIRECTOR',
                firstname: director_params['firstname'],
                lastname: director_params['lastname'],
                name: "#{director_params['firstname']} #{director_params['lastname']}"
              }

              user = User.new(user_attributes)
              user.skip_confirmation!
              user.save!
            end

            director = Director.where(id: director_params['id']).first

            if director.blank?
              director = user.directors.create(director_attributes)
            else
              director.attributes = director_attributes
            end

            if director_params['front_license_file'].present?
              director.set_front_license_file(director_params['front_license_file'])
            end

            if director_params['back_license_file'].present?
              director.set_back_license_file(director_params['back_license_file'])
            end

            director.save
            director.send_director_registration(venue_application_form, is_exist_user) unless is_save_and_continue_later

            attributes = {
              venue_application_form_id: @venue_application_form[:id],
              director_id: director[:id],
              signature_image: director_params['signature_image'],
              is_signed: director_params['signature_image'].present? ? true : false,
              form_data: venue_application_form,
              is_director_guarantee: CommonLib.convert_to_bool(director_params['is_director_guarantee'])
            }

            VenueApplicationFormDirector.where(venue_application_form_id: @venue_application_form[:id],
              director_id: director[:id]).first_or_initialize.tap do |item|
              next unless item.new_record?
              item.assign_attributes(attributes)
              item.save!
            end
          end

          is_director_guarantees = CommonLib.convert_to_bool(venue_application_form['is_director_guarantees'])

          if !is_director_guarantees
            @venue_application_form.venue_application_form_directors.update_all(is_director_guarantee: false)
          end
        end
      end

      @venue_application_form.directors.as_json
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_venue_application_form
      @venue_application_form = VenueApplicationForm.where(save_token: params[:token]).first if params[:token].present?
      @venue_application_form = VenueApplicationForm.where(save_token: params[:id]).first if params[:id].present?
      @venue_application_form = VenueApplicationForm.find(params[:id]) if @venue_application_form.blank?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def venue_application_form_params
      params.require(:venue_application_form).permit(:business_name, :email, :firstname,
        :lastname, :mobile, :phone, :trading_address_line_1, :trading_address_line_2,
        :trading_suburb, :trading_state, :trading_postcode,
        :estimated_weekly_expected_purchased, :requested_trading_terms, :company_name,
        :company_abn, :operating_structure, :market_segment_id, :registered_address_line_1,
        :registered_address_line_2, :registered_suburb, :registered_state,
        :registered_postcode, :bank_branch, :bsb, :account_number, :pay_by_credit_card,
        :account_contact_name, :account_contact_email, :account_contact_number,
        :delivery_contact_name, :delivery_contact_number, :trade_reference_1_business_name,
        :trade_reference_1_contact_person, :trade_reference_1_contact_number, :trading_name,
        :is_director_guarantees, :trading_property_name, :trading_unit_number,
        :trading_street_number, :trading_street_name, :trading_street_type, :registered_property_name,
        :registered_unit_number, :registered_street_number, :registered_street_name, :registered_street_type)
    end

    def set_layout
      case action_name
      when 'venue_form_blank'
        'blank'
      else
        'application'
      end
    end
end
