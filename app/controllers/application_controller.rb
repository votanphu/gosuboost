class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  include CommonLib

  protected

  def authorize_admin!
    if current_user.blank? || !current_user.is_admin?
      redirect_to admin_venue_details_url
    end
  end

  def authorize_supplier!
    if current_user.blank? || !current_user.is_supplier?
      redirect_to admin_venue_details_url
    end
  end

  def authorize_director!
    if current_user.blank? || !current_user.is_director?
      redirect_to admin_venue_details_url
    end
  end

  def authorize_personal_guarantee!
    if current_user.blank? || !current_user.is_personal_guarantee?
      redirect_to admin_venue_details_url
    end
  end

  def authorize_venue_user!
    if current_user.blank? || !current_user.is_venue_user?
      redirect_to admin_venue_details_url
    end
  end

  def remote_ip
    request.remote_ip
  end

  def request_latitude
    request.location.try(:latitude)
  end

  def request_longitude
    request.location.try(:longitude)
  end

  def authorize_admin_supplier!
    if current_user.blank? || (!current_user.is_admin? && !current_user.is_supplier?)
      redirect_to admin_venue_details_url
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_in, keys: [:otp_attempt])
  end
end
