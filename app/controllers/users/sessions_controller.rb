class Users::SessionsController < Devise::SessionsController
  layout 'blank'

  # GET /resource/sign_in
  def new
    super
    $current_page = 'Pencil | Sign In'
  end

  def create
    user_params = params[:user]
    user = User.where(email: user_params[:email]).first
    return super unless User.validate_user_opt(user, user_params[:password])
    session[:temp_user_id] = user.id
    return render :otp if user.otp_required_for_login
    generate_qr(user)
  end

  def verify_otp
    user = User.find_by(id: session[:temp_user_id])

    if user.validate_and_consume_otp!(params[:otp_attempt], otp_secret: user.otp_secret)
      create_session(user)
    else
      respond_to do |format|
        format.html {
          flash.now[:alert] = 'The otp does not match!'
          render :otp
        }
      end
    end
  end

  protected

  # Override the method of devise
  def after_sign_in_path_for(resource)
    vaf = nil

    if params[:user].present? && sign_up_params[:form_token].present?
      form_token = sign_up_params[:form_token]
    else
      form_token = params[:form_token]
    end

    vaf = VenueApplicationForm.get_vaf_by_token(form_token) if form_token.present?
    return vaf.get_review_url if vaf.present?

    ascf = nil

    if params[:user].present? && sign_up_params[:ascf_token].present?
      ascf_token = sign_up_params[:ascf_token]
    else
      ascf_token = params[:ascf_token]
    end

    # ascf: ApplySupplierCreditForm
    ascf = ApplySupplierCreditForm.get_ascf_by_token(ascf_token) if ascf_token.present?
    return ascf.get_review_url if ascf.present?

    return session.delete(:previous_url) if session[:previous_url].present?

    "#{$prod_root_url}/admin/venue_details"
  end

  def sign_up_params
    params.require(:user).permit(:email, :password, :form_token, :remember_me, :ascf_token)
  end

  def create_session(user)
    sign_in(user)
    session.delete(:temp_user_id)
    flash[:notice] = 'Signed in successfully.'
    respond_with user, location: after_sign_in_path_for(user)
  end

  def generate_qr(user)
    user.update_otp_secret
    @qr_code = RQRCode::QRCode.new(two_factor_url).to_img.resize(240, 240).to_data_url
    user.activate_otp
    return render :qr
  end

  def two_factor_url
    app_id = ENV['APP_ID']
    app_name = ENV['APP_NAME']
    "otpauth://totp/#{app_id}:#{current_user.email}?secret=#{current_user.unconfirmed_otp_secret}&issuer=#{app_name}"
  end
end
