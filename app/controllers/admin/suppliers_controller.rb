class Admin::SuppliersController < Admin::AdminController
  before_action :authorize_supplier!, only: [:my_profile]
  before_action :set_supplier_user, only: [:show, :edit, :update, :destroy]
  before_action :set_title

  before_action :set_supplier, only: [
    :update_profile, :my_terms_and_conditions, :my_profile, :new_terms_and_condition, :create_terms_and_condition,
    :edit_terms_and_condition, :update_terms_and_condition, :supplier_users
  ]
  before_action :set_title_terms_and_conditions, only: [
    :my_terms_and_conditions, :new_terms_and_condition, :create_terms_and_condition, :update_terms_and_condition,
    :edit_terms_and_condition
  ]
  before_action :set_title_my_profile, only: [:my_profile]
  before_action :set_title_supplier_users, only: [:supplier_users]

  # Begin::Supplier
  def index
    @suppliers = Supplier.order(company_name: :asc)
  end

  def show
  end

  def edit
  end

  def new
    @supplier = Supplier.new
  end

  def create
    return false unless check_abn

    user = User.new_user(
      name: supplier_params[:contact_name],
      email: supplier_params[:email],
      role: 'SUPPLIER',
      is_skip_confirmation: true
    )

    @supplier = Supplier.new(supplier_params.merge(user_id: user[:id]))

    respond_to do |format|
      if @supplier.save
        format.html { redirect_to admin_suppliers_url, notice: 'The supplier was successfully created.' }
        format.json { render :show, status: :created, location: @supplier }
      else
        format.html { render :new }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    # Validate abn
    if supplier_params[:abn].present?
      abn = supplier_params[:abn]
      is_abn_valid = VenueApplicationForm.validate_abn(abn)

      unless is_abn_valid
        message = 'The ABN is not valid. Please check and try again!'
        redirect_to edit_admin_supplier_path(@supplier[:id]), alert: message
        return
      end

      supplier = Supplier.where(abn: abn).first

      unless supplier.blank?
        redirect_to edit_admin_supplier_path(@supplier[:id]), alert: 'The ABN was taken. Please check and try again!'
        return
      end
    end

    respond_to do |format|
      if @supplier.update(supplier_params)
        format.html { redirect_to admin_suppliers_path, notice: 'The supplier was successfully updated.' }
        format.json { render :show, status: :ok, location: @supplier }
      else
        format.html { render :edit }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @supplier.destroy
    respond_to do |format|
      format.html { redirect_to admin_suppliers_url, notice: 'The supplier was successfully removed.' }
      format.json { head :no_content }
    end
  end
  # End::Supplier

  # Begin::Supplier T&Cs
  def new_terms_and_condition
    @supplier_terms_condition = @supplier.supplier_terms_conditions.new
  end

  def my_terms_and_conditions
    @supplier = current_user.supplier
  end

  def create_terms_and_condition
    market_segment_ids = []
    attributes = supplier_terms_condition_attributes(@trading_terms_settings)

    if attributes[:content].blank?
      message = 'Your terms and conditions was not successfully created. Your terms and conditions is empty.'
      message += ' Please update it and try again!'
      redirect_to admin_suppliers_new_terms_and_condition_path, alert: message
      return
    end

    supplier_terms_condition = @supplier.supplier_terms_conditions.new(attributes)

    if supplier_terms_condition.save
      if terms_and_condition_params['market_segments_list'].present?
        market_segment_ids = terms_and_condition_params['market_segments_list']
      end

      market_segment_ids.each do |market_segment_id|
        supplier_terms_condition.supplier_terms_condition_market_segments.create(market_segment_id: market_segment_id)
      end

      message = 'Your terms and conditions was successfully created.'
      redirect_to admin_suppliers_my_terms_and_conditions_path, notice: message
    else
      message = 'Your terms and conditions was not successfully created.'
      redirect_to admin_suppliers_new_terms_and_condition_path, alert: message
    end
  end

  def edit_terms_and_condition
    @supplier_terms_condition = SupplierTermsCondition.find(params[:id])
    @trading_terms_settings = @supplier.trading_terms_settings.where(active: true)
  end

  def update_terms_and_condition
    market_segment_ids = []
    supplier_terms_condition = SupplierTermsCondition.find(params[:id]) if params[:id].present?
    attributes = supplier_terms_condition_attributes(@trading_terms_settings)

    if ActionController::Base.helpers.strip_tags(attributes[:content]).blank?
      message = 'Your terms and conditions was not successfully updated. Your new terms and conditions is empty.'
      message += ' Please update it and try again!'
      redirect_to admin_suppliers_new_terms_and_condition_path, alert: message
      return
    end

    if supplier_terms_condition.update(attributes)
      if terms_and_condition_params['market_segments_list'].present?
        supplier_terms_condition.supplier_terms_condition_market_segments.delete_all
        market_segment_ids = terms_and_condition_params['market_segments_list']
      end

      market_segment_ids.each do |market_segment_id|
        supplier_terms_condition.supplier_terms_condition_market_segments.create(market_segment_id: market_segment_id)
      end

      message = 'Your terms and conditions was successfully updated.'
      redirect_to admin_suppliers_my_terms_and_conditions_path, notice: message
    else
      message = 'Your terms and conditions was not successfully updated.'
      redirect_to admin_suppliers_update_terms_and_condition_path(supplier_terms_condition[:id]), alert: message
    end
  end

  def destroy_terms_and_condition
    @supplier_terms_condition = SupplierTermsCondition.find_by_id(params[:id])

    if @supplier_terms_condition.is_deletable?
      @supplier_terms_condition.destroy
      message = 'Terms and Condition was successfully deleted.'
      redirect_to admin_suppliers_my_terms_and_conditions_url, notice: message
    else
      message = 'Terms and Condition was not successfully deleted. There is/are venues using it.'
      redirect_to admin_suppliers_my_terms_and_conditions_url, alert: message
    end
  end
  # End::Supplier T&Cs

  # Begin::Supplier users
  def supplier_users
    @parent_id = current_user.supplier.user.id
    @supplier_users = User.get_supplier_users(@parent_id)
  end

  def edit_supplier_user
    @supplier_user = User.find_by_id(params[:id])
  end

  def new_supplier_user
    @supplier_user = User.new
  end

  def update_supplier_user
    supplier_user = User.find_by_id(params[:id])

    respond_to do |format|
      if supplier_user.update(user_params)
        message = 'Supplier user was successfully updated.'
        format.html { redirect_to admin_suppliers_supplier_users_path, notice: message }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # Create children supplier
  def create_supplier_user
    parent_user = current_user.supplier.user
    return false if exist_email?(parent_user)
    options = user_params
    options[:is_skip_confirmation] = true
    options[:parent_user_id] = parent_user[:id]
    supplier_user = User.new_children_user(options)
    return false unless check_exist_parent(supplier_user, parent_user[:id])

    respond_to do |format|
      if supplier_user.save!(user_params)
        current_user.supplier.send_supplier_registration(user_email: supplier_user[:email])
        message = 'Supplier user was successfully created.'
        format.html { redirect_to admin_suppliers_supplier_users_path, notice: message }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy_supplier_user
    user = User.find(params[:id])

    if user.is_only_supplier?
      user.destroy
    else
      user.remove_supplier_role
    end

    respond_to do |format|
      message = 'Supplier user was successfully deleted.'
      format.html { redirect_to admin_suppliers_supplier_users_path, notice: message }
      format.json { head :no_content }
    end
  end
  # End::Supplier users

  # Begin::Supplier Profile
  def my_profile
  end

  def update_profile
    respond_to do |format|
      if @supplier.update(supplier_profile_params)
        message = 'The Supplier has been successfully updated.'
        format.html { redirect_to admin_suppliers_my_profile_path, notice: message }
        format.json { render :show, status: :ok, location: @supplier }
      else
        format.html { render :edit }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end
  # End::Supplier Profile

  # Begin::Supplier avatar
  def update_avatar
    if params[:user].blank?
      redirect_to admin_suppliers_my_profile_path, notice: 'Your Avatar does not change.'
      return
    end

    if current_user.update(user_avatar_params)
      if user_avatar_params[:avatar].blank?
        message = 'Your Avatar was successfully updated.'
      else
        message = 'Your Avatar was cropped successfully.'
      end
    else
      message = 'Your Avatar was not successfully updated.'
    end

    redirect_to admin_suppliers_my_profile_path, notice: message
  end
  # End::Supplier avatar

  def update_logo
    if params[:supplier].blank?
      redirect_to admin_suppliers_my_profile_path, notice: 'Your Logo does not change.'
      return
    end

    supplier = current_user.supplier

    if supplier.update(supplier_logo_params)
      message = 'Your Logo was successfully updated.'
    else
      message = 'Your Logo was not successfully updated.'
    end

    redirect_to admin_suppliers_my_profile_path, notice: message
  end

  private

  def set_title
    $current_page = 'Pencil | Suppliers'
    $subheader_title = 'Suppliers'
  end

  def set_title_terms_and_conditions
    $current_page = 'Pencil | My Terms & Conditions'
    $subheader_title = 'My Terms & Conditions'
  end

  def set_title_my_profile
    $current_page = 'Pencil | My Profile'
    $subheader_title = 'My Profile'
  end

  def set_title_supplier_users
    $current_page = 'Pencil | Supplier Users'
    $subheader_title = 'Supplier Users'
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_supplier_user
    @supplier = Supplier.find(params[:id])
  end

  def set_supplier
    @supplier = current_user.supplier
    @trading_terms_settings = @supplier.trading_terms_settings.where(active: true)
  end

  def supplier_params
    params.require(:supplier).permit(
      :company_name, :contact_name, :contact_number, :user_id, :abn, :email, :address
    )
  end

  def supplier_profile_params
    params.require(:supplier).permit(
      :contact_name, :first_name, :last_name, :email, :company_name, :contact_number, :contact_mobile,
      :abn, :address, :terms_and_conditions, :business_phone, :property_name, :unit_number,
      :street_number, :street_name, :street_type, :suburb, :state, :postcode
    )
  end

  def terms_and_condition_params
    params.require(:supplier_terms_condition)
  end

  def user_params
    params.require(:user).permit(:firstname, :lastname, :email)
  end

  def user_avatar_params
    params.require(:user).permit(:avatar, :crop_x, :crop_y, :crop_w, :crop_h)
  end

  def supplier_terms_condition_attributes(trading_terms)
    attributes = {
      content: terms_and_condition_params['content']
    }

    attributes = SupplierTermsCondition.set_market_segment(attributes, terms_and_condition_params)
    SupplierTermsCondition.set_trading_terms_setting(
      attributes,
      terms_and_condition_params,
      trading_terms.first[:id]
    )
  end

  def supplier_logo_params
    params.require(:supplier).permit(:logo)
  end

  # Check abn: valid and exist
  def check_abn
    return false if supplier_params[:abn].blank?
    abn = supplier_params[:abn]
    is_abn_valid = VenueApplicationForm.validate_abn(abn)

    unless is_abn_valid
      redirect_to new_admin_supplier_path, alert: 'The ABN is not valid. Please check and try again!'
      return false
    end

    supplier = Supplier.where(abn: abn).first

    unless supplier.blank?
      redirect_to new_admin_supplier_path, alert: 'The ABN was taken. Please check and try again!'
      return false
    end

    true
  end

  def exist_email?(parent_user)
    supplier_emails = User.get_supplier_users(parent_user[:id]).map(&:email)

    if supplier_emails.include? user_params[:email]
      respond_to do |format|
        message = 'The update email of this user is exist! Please try with another email!'
        format.html { redirect_to admin_suppliers_new_supplier_user_path, alert: message }
      end

      return true
    end

    false
  end

  # Check exist parent
  def check_exist_parent(user, parent_user_id)
    unless user.exist_parent_id(parent_user_id)
      respond_to do |format|
        message = 'Supplier user was not successfully created. Supplier had another parent'
        format.html { redirect_to admin_suppliers_supplier_users_path, alert: message }
      end

      return false
    end

    true
  end
end
