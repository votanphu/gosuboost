class Admin::DashboardController < Admin::AdminController
  def index
    $current_page = 'Pencil | Dashboard'
    $subheader_title = 'Dashboard'
    redirect_to admin_venue_details_url

    # respond_to do |format|
    #   format.html { render 'index' }
    # end
  end
end
