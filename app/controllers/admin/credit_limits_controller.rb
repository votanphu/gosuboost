# Credit Limits Controller
class Admin::CreditLimitsController < Admin::AdminController
  before_action :authorize_supplier!
  before_action :set_title
  before_action :set_supplier, except: [:destroy]
  before_action :set_credit_limit, only: [:edit, :update, :destroy]
  before_action :set_trading_terms, only: [:new, :create, :edit, :update]

  def index
    @credit_limits = @supplier.credit_limits
  end

  def new
    @credit_limit = @supplier.credit_limits.new
  end

  def create
    credit_limit_attrs = credit_limit_attributes(@trading_terms_settings)
    credit_limit = @supplier.credit_limits.new(credit_limit_attrs)

    if credit_limit.save
      redirect_to admin_credit_limits_url, notice: 'Credit limit was successfully created.'
    else
      redirect_to admin_credit_limits_url, notice: 'Credit limit was not successfully created.'
    end
  end

  def edit
  end

  def update
    credit_limit_attrs = credit_limit_attributes(@trading_terms_settings)

    if @credit_limit.update(credit_limit_attrs)
      redirect_to admin_credit_limits_path, notice: 'Credit limit was successfully updated.'
    else
      redirect_to admin_credit_limits_path, notice: 'Credit limit was not successfully updated.'
    end
  end

  def destroy
    @credit_limit.destroy
    redirect_to admin_credit_limits_path, notice: 'Credit limit was successfully deleted.'
  end

  private

  def set_title
    $current_page = 'Pencil | Credit Limits'
    $subheader_title = 'Credit Limits'
  end

  def set_supplier
    @supplier = current_user.supplier
  end

  def set_trading_terms
    @trading_terms_settings = @supplier.trading_terms_settings.order(created_at: :asc)
  end

  def set_credit_limit
    param_id = params['id']
    @credit_limit = CreditLimit.find(param_id) if param_id.present?
  end

  def credit_limit_params
    params.require(:credit_limit).permit(
      :credit_limit, :increments, :market_segment_id, :trading_terms_setting_id
    )
  end

  def credit_limit_attributes(trading_terms_settings)
    credit_limit_attrs = {
      credit_limit: credit_limit_params[:credit_limit],
      increments: credit_limit_params[:increments]
    }

    credit_limit_attrs = CreditLimit.set_market_segment(credit_limit_attrs, credit_limit_params)
    CreditLimit.set_trading_terms_setting(credit_limit_attrs, credit_limit_params, trading_terms_settings.first[:id])
  end
end
