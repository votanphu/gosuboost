class Admin::VenueUsersController < Admin::AdminController
  before_action :set_venue_users, only: [:index]
  before_action :set_venue_user, only: [:show]
  before_action :set_title

  def index
    # if current user is venue user
    @venue_user_forms_quantity = count_intersect_venue_forms(@venue_users, current_user) if current_user.is_venue_user?

    # if current user is admin
    if current_user.is_admin?
      @venue_user_forms_quantity = {}
      @venue_user_forms_quantity = { current_user[:email] => 0 }

      @venue_users.each do |venue_user|
        @venue_user_forms_quantity[venue_user[:email]] = venue_user.user.get_venue_user_venue_application_forms.count
      end
    end
  end

  def show
    @venue_user_venue_forms = []

    if current_user.is_admin?
      @venue_user_venue_forms = @venue_user.user.get_venue_user_venue_application_forms
    elsif current_user.is_venue_user?
      venue_user_venue_form_ids = get_intersect_venue_form_ids(@venue_user, current_user)
      @venue_user_venue_forms = VenueApplicationForm.where(id: venue_user_venue_form_ids)
    end
  end

  private

  def set_title
    $current_page = 'Pencil | Venue Users'
    $subheader_title = 'Venue Users'
  end

  def set_venue_user
    param_id = params[:id]
    @venue_user = Director.where(id: param_id).first if param_id.present?
    @venue_user = PersonalGuarantee.where(id: param_id).first if param_id.present? && @venue_user.blank?
  end

  def set_venue_users
    @venue_users = []
    current_user_venue_forms = []

    if current_user.is_admin?
      current_user_venue_forms = VenueApplicationForm.includes(:apply_supplier_credit_forms, :directors,
                                            :venue_application_form_directors => [:director]).all
    elsif current_user.is_venue_user?
      current_user_venue_forms = current_user.get_venue_user_venue_application_forms
    end

    current_user_venue_forms.each do |venue_form|
      @venue_users << venue_form.directors
      @venue_users << venue_form.get_personal_guarantees
    end

    @venue_users.flatten!
    @venue_users.uniq! { |user| user[:email] }
  end

  def get_intersect_venue_form_ids(venue_user, user)
    intersect_venue_form_ids = user.get_venue_user_venue_application_forms.map(&:id)
    intersect_venue_form_ids = intersect_venue_form_ids & venue_user.user.get_venue_user_venue_application_forms.map(&:id)
    return intersect_venue_form_ids
  end

  def count_intersect_venue_forms(venue_users, user)
    venue_user_forms_quantity = {}
    venue_user_forms_quantity = { user[:email] => user.get_venue_user_venue_application_forms.count }

    venue_users.each do |venue_user|
      venue_user_forms_quantity[venue_user[:email]] = get_intersect_venue_form_ids(venue_user, user).count
    end

    return venue_user_forms_quantity
  end
end
