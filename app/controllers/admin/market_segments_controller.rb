class Admin::MarketSegmentsController < Admin::AdminController
  before_action :authorize_admin!
  before_action :set_market_segment, only: [:show, :edit, :update, :destroy]
  before_action :set_title

  def index
    @market_segments = MarketSegment.order(name: :asc)
  end

  def show
  end

  def new
    @market_segment = MarketSegment.new
  end

  def create
    market_segment_params[:name] = market_segment_params[:name].gsub(/\s+/, ' ').strip
    @market_segment = MarketSegment.new(market_segment_params)

    respond_to do |format|
      if @market_segment.save
        format.html { redirect_to admin_market_segments_url, notice: 'Market Segment was successfully created.' }
        format.json { render :show, status: :created, location: @market_segment }
      else
        format.html { render :new }
        format.json { render json: @market_segment.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    market_segment_params[:name] = market_segment_params[:name].gsub(/\s+/, ' ').strip

    respond_to do |format|
      if @market_segment.update(market_segment_params)
        format.html { redirect_to admin_market_segments_path, notice: 'Market Segment was successfully updated.' }
        format.json { render :show, status: :ok, location: @market_segment }
      else
        format.html { render :edit }
        format.json { render json: @market_segment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @market_segment.destroy

    respond_to do |format|
      format.html { redirect_to admin_market_segments_url, notice: 'Market Segment was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private

  def set_title
    $current_page = 'Pencil | Market Segments'
    $subheader_title = 'Market Segments'
  end

  def set_market_segment
    @market_segment = MarketSegment.find(params['id']) if params['id'].present?
  end

  def market_segment_params
    params.require(:market_segment).permit(:name)
  end
end
