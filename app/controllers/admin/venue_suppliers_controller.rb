class Admin::VenueSuppliersController < Admin::AdminController
  before_action :authorize_director!, only: [:new, :update, :destroy, :update_ascf]
  before_action :authorize_venue_user!, only: [:edit]
  before_action :set_title
  before_action :set_venue_supplier, only: [:edit, :update, :destroy, :update_ascf]
  before_action :check_user_has_venues, only: [:new]
  before_action :set_apply_supplier_credit_form, only: [:new, :edit]

  def index
    @all_venues_status = ApplySupplierCreditForm.get_all_status
    @venue_suppliers = current_user.all_apply_supplier_credit_forms
  end

  def new
    @venue_supplier = ApplySupplierCreditForm.new
  end

  def edit
    return if check_any_pg_signed
    authorize_personal_guarantee! if params['step'] == ApplySupplierCreditForm::STEP_REVIEW
    @number_of_trade_references = @venue_supplier.trading_terms_setting.get_required_trade_references_quantity
    @number_of_personal_guarantees = @venue_supplier.trading_terms_setting.get_required_personal_guarantees_quantity
  end

  def update
  end

  def destroy
    @venue_supplier.destroy
    respond_to do |format|
      format.html { redirect_to admin_suppliers_url, notice: 'The supplier was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create
    venue_credit_limit = params[:venue_credit_limit].delete(',')
    trading_terms_setting = TradingTermsSetting.where(id: params[:trading_terms_setting_id]).first
    is_save_and_continue = false

    if params['is_save_and_continue'].present?
      is_save_and_continue = CommonLib.convert_to_bool(params['is_save_and_continue'])
    end

    selected_directors_ids = params['selected_directors_ids'].present? ? params['selected_directors_ids'] : []
    selected_directors_ids.uniq!
    personal_guarantees_params = []

    if params['personal_guarantees'].present?
      personal_guarantees_params = params['personal_guarantees'].as_json.values
      is_pgs_params_a = personal_guarantees_params.instance_of?(Array)
      personal_guarantees_params = [] << personal_guarantees_params unless is_pgs_params_a
      personal_guarantees_params = ApplySupplierCreditForm.filter_pg_params(personal_guarantees_params)
    end

    pgs_quantity = Director.get_real_pg_quantity(selected_directors_ids, personal_guarantees_params)

    unless trading_terms_setting.has_enough_personal_guarantees(pgs_quantity) || is_save_and_continue
      message = 'The added personal guarantees have duplicate emails.'
      message += ' Please update it and try again!'

      respond_to do |format|
        format.json {
          render json: {
            apply_supplier_credit_form: nil,
            message: message
          },
          status: :unprocessable_entity
        }
      end

      return
    end

    @apply_supplier_credit_form = ApplySupplierCreditForm.create!(
      venue_supplier_params.merge(
        trading_terms_setting_id: params[:trading_terms_setting_id],
        venue_credit_limit: venue_credit_limit,
        credit_limit: venue_credit_limit,
        created_by_id: current_user[:id]
      )
    )

    if params['is_save_and_continue'].present?
      is_save_and_continue = CommonLib.convert_to_bool(params['is_save_and_continue'])
      @apply_supplier_credit_form[:status] = Const::STATUS[:draft][:value]
    else
      trading_terms_setting = @apply_supplier_credit_form.trading_terms_setting

      if trading_terms_setting[:personal_guarantees] == Const::SIGN_QUANTITIES[:none][:value]
        @apply_supplier_credit_form[:status] = Const::STATUS[:no_signature_required][:value]
        stc_content = @apply_supplier_credit_form.get_supplier_terms_condition[:content]
        @apply_supplier_credit_form[:supplier_terms_condition_content] = stc_content
      else
        @apply_supplier_credit_form[:status] = Const::STATUS[:pending_pg_signature][:value]
      end
    end

    respond_to do |format|
      if @apply_supplier_credit_form.save
        apply_supplier_credit_form = @apply_supplier_credit_form.as_json

        create_personal_guarantees(
          selected_directors_ids, personal_guarantees_params, @apply_supplier_credit_form, is_save_and_continue
        )

        save_trade_references(params, @apply_supplier_credit_form, is_save_and_continue)

        if is_save_and_continue == false
          UserMailer.ascf_apply_send_to_supplier(
            ascf: @apply_supplier_credit_form
          ).deliver_later
        end

        format.json {
          render json: {
            apply_supplier_credit_form: apply_supplier_credit_form,
            message: 'Your apply supplier credit has been created successfully',
            redirect_url:  admin_venue_suppliers_path
          },
          status: :created
        }
      else
        format.json {
          render json: {
            apply_supplier_credit_form: nil,
            message: @apply_supplier_credit_form.errors
          },
          status: :unprocessable_entity
        }
      end
    end
  end

  def update_ascf
    trading_terms_setting = TradingTermsSetting.where(id: params[:trading_terms_setting_id]).first
    is_save_and_continue = false

    if params['is_save_and_continue'].present?
      is_save_and_continue = CommonLib.convert_to_bool(params['is_save_and_continue'])
    end

    selected_directors_ids = params['selected_directors_ids'].present? ? params['selected_directors_ids'] : []
    selected_directors_ids.uniq!
    personal_guarantees_params = []

    if params['personal_guarantees'].present?
      personal_guarantees_params = params['personal_guarantees'].as_json.values
      is_pgs_params_a = personal_guarantees_params.instance_of?(Array)
      personal_guarantees_params = [] << personal_guarantees_params unless is_pgs_params_a
      personal_guarantees_params = ApplySupplierCreditForm.filter_pg_params(personal_guarantees_params)
    end

    pgs_quantity = Director.get_real_pg_quantity(selected_directors_ids, personal_guarantees_params)

    unless trading_terms_setting.has_enough_personal_guarantees(pgs_quantity) || is_save_and_continue
      message = 'The added personal guarantees have duplicate emails.'
      message += ' Please update it and try again!'

      respond_to do |format|
        format.json {
          render json: {
            apply_supplier_credit_form: nil,
            message: message
          },
          status: :unprocessable_entity
        }
      end

      return
    end

    if params['is_save_and_continue'].present?
      is_save_and_continue = CommonLib.convert_to_bool(params['is_save_and_continue'])
      @venue_supplier[:status] = Const::STATUS[:draft][:value]
    else
      is_save_and_continue = false
      trading_terms_setting = @venue_supplier.trading_terms_setting

      if trading_terms_setting[:personal_guarantees] == Const::SIGN_QUANTITIES[:none][:value]
        @venue_supplier[:status] = Const::STATUS[:no_signature_required][:value]
        @venue_supplier[:supplier_terms_condition_content] = @venue_supplier.get_supplier_terms_condition[:content]
      else
        @venue_supplier[:status] = Const::STATUS[:pending_pg_signature][:value]
      end
    end

    @apply_supplier_credit_form = @venue_supplier
    venue_credit_limit = params[:venue_credit_limit].delete(',')

    @venue_supplier.attributes = venue_supplier_params.merge(
      trading_terms_setting_id: params[:trading_terms_setting_id],
      venue_credit_limit: venue_credit_limit,
      credit_limit: venue_credit_limit
    )

    respond_to do |format|
      if @apply_supplier_credit_form.save
        apply_supplier_credit_form = @apply_supplier_credit_form.as_json

        if selected_directors_ids.present? && selected_directors_ids.count > 0
          update_directors(selected_directors_ids, @apply_supplier_credit_form, is_save_and_continue)
        elsif personal_guarantees_params.present?
          update_personal_guarantees(personal_guarantees_params, @apply_supplier_credit_form, is_save_and_continue)
        end

        save_trade_references(params, @apply_supplier_credit_form, is_save_and_continue)

        if is_save_and_continue == false
          UserMailer.ascf_apply_send_to_supplier(
            ascf: @apply_supplier_credit_form
          ).deliver_later
        end

        format.json {
          render json: {
            apply_supplier_credit_form: apply_supplier_credit_form,
            message: 'Your apply supplier credit has been updated successfully',
            redirect_url:  admin_venue_suppliers_path
          },
          status: :created
        }
      else
        format.json {
          render json: {
            apply_supplier_credit_form: nil,
            message: @apply_supplier_credit_form.errors
          },
          status: :unprocessable_entity
        }
      end
    end

    # Delete store location
    session.delete(:previous_url) if session[:previous_url].present?
  end

  def set_supplier
    @ascf = ApplySupplierCreditForm.new
    @ascf = ApplySupplierCreditForm.find(params[:ascf_id]) if params[:ascf_id].present?
    @supplier = Supplier.find(params[:supplier_id])
    venue_application_form_id = params[:venue_application_form_id]
    list_active = Supplier.available_trading_terms_settings(@supplier.id, venue_application_form_id)
    @trading_terms_active = TradingTermsSetting.where(active: true, id: list_active, supplier_id: @supplier.id)
    @supplier_terms_conditions = @supplier.supplier_terms_conditions
    @number_of_trade_references = 0

    if @trading_terms_active.where(default: true).present?
      @trading_terms_setting = @trading_terms_active.where(default: true).first
      @supplier_terms_condition = @supplier.get_terms_and_condition(venue_application_form_id, @trading_terms_setting[:id])

      @number_of_trade_references = @trading_terms_setting.get_required_trade_references_quantity
      @number_of_personal_guarantees = @trading_terms_setting.get_required_personal_guarantees_quantity
    end

    if @supplier_terms_condition.blank?
      @trading_terms_setting = @trading_terms_active.first
      @supplier_terms_condition = @supplier.get_terms_and_condition(venue_application_form_id, @trading_terms_setting.id)
    end

    # Getting credit limit of supplier to show at step 3
    venue = VenueApplicationForm.where(id: params[:venue_application_form_id]).first
    @credit_limits = []
    credit_limit = @supplier.get_credit_limit(venue[:market_segment_id],
                                     @trading_terms_setting[:id])
    @credit_limits = init_credit_limits(credit_limit) if credit_limit.present?
    # End of getting credit limit of supplier to show at step 3

    if @supplier_terms_condition.present?
      respond_to do |f|
        f.html { redirect_back(fallback_location: admin_admin_index_path) }
        f.js {
          render "_apply_supplier_credit_form_step_3",
          locals: {
            supplier_terms_condition: @supplier_terms_condition,
            trading_terms_active: @trading_terms_active,
            credit_limits: @credit_limits,
            ascf: @ascf,
            number_of_trade_references: @number_of_trade_references,
            trading_terms_setting: @trading_terms_setting
          }
        }
      end
    else
      respond_to do |f|
        f.json {
          render json: {
            message: "This supplier does not support your market segment!"
          },
          status: :not_found
        }
      end
    end
  end

  def set_vaf
    @venue_application_form = VenueApplicationForm.find_by_id(params[:vaf_id])
    @directors = @venue_application_form.directors
    selected_director_ids = []

    respond_to do |f|
      f.html{ redirect_back(fallback_location: admin_admin_index_path) }
      f.js { }
    end
  end

  def set_trading_term_setting
    @supplier = Supplier.find(params[:supplier_id])
    venue_application_form_id = params[:venue_application_form_id]
    trading_terms_setting_id = params[:trading_terms_setting_id]

    if params[:ascf_id].present?
      @ascf = ApplySupplierCreditForm.find(params[:ascf_id])
    else
      @ascf = ApplySupplierCreditForm.new
    end

    @trading_terms_setting = @supplier.trading_terms_settings.where(id: trading_terms_setting_id)
      .order(created_at: :asc).first
    @number_of_trade_references = @trading_terms_setting.get_required_trade_references_quantity
    @number_of_personal_guarantees = @trading_terms_setting.get_required_personal_guarantees_quantity
    @trading_terms_settings = @supplier.trading_terms_settings.where(active: true)
    @supplier_terms_conditions = @supplier.supplier_terms_conditions
    @supplier_terms_condition = @supplier.get_terms_and_condition(venue_application_form_id, trading_terms_setting_id)

    # Getting credit limit of supplier to show at step 3
    venue = VenueApplicationForm.where(id: params[:venue_application_form_id]).first
    @credit_limits = []
    credit_limit = @supplier.get_credit_limit(venue[:market_segment_id],
                                    trading_terms_setting_id)
    @credit_limits = init_credit_limits(credit_limit) if credit_limit.present?
    selected_director_ids = []
    # End of getting credit limit of supplier to show at step 3

    respond_to do |f|
      f.html { redirect_back(fallback_location: admin_admin_index_path) }
      f.js {
        render "_apply_supplier_credit_form_step_4",
        locals: {
          vaf: venue,
          credit_limits: @credit_limits,
          supplier_terms_condition: @supplier_terms_condition,
          number_of_trade_references: @number_of_trade_references,
          ascf: @ascf,
          trading_terms_setting: @trading_terms_setting,
          number_of_personal_guarantees: @number_of_personal_guarantees,
          selected_director_ids: selected_director_ids
        }
      }
    end
  end

  def personal_guarantee_sign
    ascf = ApplySupplierCreditForm.find(params['id'])

    respond_to do |format|
      if user_signed_in? && ascf.present? &&
        ascf.is_include_user(current_user) && params['signature_image'].present?

        personal_guarantee = ascf.get_pg_by_user(current_user)
        ascfpg = ascf.get_ascfpg_by_pg(personal_guarantee)
        ascfpg.set_signature(params['signature_image'], remote_ip, request_latitude, request_longitude)

        if ascfpg.save
          if ascf.is_signed_by_all_personal_guarantees? &&
            ascf.personal_guarantees.count >= 1
            UserMailer.all_pg_signed_send_to_supplier_main(
              ascf: ascf
            ).deliver_later

            UserMailer.all_pg_signed_send_to_pgs(
              ascf: ascf
            ).deliver_later

            ascf[:status] = Const::STATUS[:all_signed][:value]
          else
            ascf[:status] = Const::STATUS[:pending_pg_signature][:value]

            UserMailer.pg_signed_apply_send_to_supplier(
              personal_guarantee: personal_guarantee,
              ascf: ascf
            ).deliver_later

            UserMailer.pg_signed_send_all_pgs(
              personal_guarantee: personal_guarantee,
              ascf: ascf
            ).deliver_later
          end

          ascf[:supplier_terms_condition_content] = ascf.get_supplier_terms_condition[:content]
          ascf.save

          format.json {
            render json: {
              venue_application_form: @venue_application_form,
              venue_application_form_director: ascfpg,
              redirect_url: admin_admin_index_path,
              message: "#{current_user[:email]} signed this form successfully."
            },
            status: :ok
          }
        else
          format.json {
            render json: {
              message: ascfpg.errors
            },
            status: :unprocessable_entity
          }
        end
      else
        format.json {
          render json: {
            message: 'Data is invalid.'
          },
          status: :not_found
        }
      end
    end
  end

  def get_director_details
    director = Director.find_by_id(params[:director_id]) if params[:director_id].present?
    response = { director: director }

    respond_to do |f|
      f.html { redirect_back(fallback_location: admin_admin_index_path) }
      f.json { render :json => response }
    end
  end

  def get_vaf_directors
    vaf = VenueApplicationForm.find_by_id(params[:vaf_id]) if params[:vaf_id].present?
    trading_terms_setting = TradingTermsSetting.find_by_id(params[:trading_terms_setting_id]) if params[:trading_terms_setting_id].present?
    response = { directors: vaf.directors, required_pgs_quantity: trading_terms_setting.get_required_personal_guarantees_quantity }

    respond_to do |f|
      f.html { redirect_back(fallback_location: admin_admin_index_path) }
      f.json { render :json => response }
    end
  end

  def search_suppliers
    search_text = params["q"].present? ? params["q"] : ""
    page = params["page"].present? ? params["pages"] : 0
    limit = 10
    # Get suppliers
    suppliers = []
    vaf = VenueApplicationForm.where(id: params["vaf_id"]).first if params["vaf_id"].present?

    if vaf.present?
      # Get the suppliers ids match search text and valid
      suppliers_total_ids = Supplier.where("company_name ILIKE ?", "%#{search_text}%").map(&:id)
      suppliers_valid_ids = Supplier.get_supplier_valid_for_vaf(vaf[:id]).map(&:id)
      suppliers_total_ids = suppliers_total_ids & suppliers_valid_ids
      # Get suppliers with valid suppliers ids
      suppliers_total = Supplier.where(id: suppliers_total_ids)
      suppliers = suppliers_total.limit(limit).offset(page * limit)
      suppliers = suppliers.map { |supplier| {company_name: supplier[:company_name], id: supplier[:id]} }
    end

    response = { suppliers: suppliers, total_count: suppliers_total.count }

    respond_to do |f|
      f.html{ redirect_back(fallback_location: admin_admin_index_path) }
      f.json { render :json => response }
    end
  end

  private

  def set_venue_supplier
    @venue_supplier = ApplySupplierCreditForm.find(params[:id])
  end

  def set_title
    $current_page = 'Pencil | Suppliers'
    $subheader_title = 'Suppliers'
  end

  def set_apply_supplier_credit_form
    @directors = current_user.directors.includes(:venue_application_forms)

    # Get all Venue Application Form is valid to create ASCF from directors of current user
    @venue_application_forms = current_user.vafs_valid_to_create_ascf

    # ADD:
    if params[:id].blank?
      if @venue_application_forms.blank?
        respond_to do |format|
          format.html {
            redirect_to admin_venue_suppliers_path,
              alert: 'Not found any Venue Application Form to create Apply Supplier Credit Form.'
          }
        end

        return
      end

      @venue_application_form = @venue_application_forms.first

      # Get all valid suppliers for Venue Application Form
      @suppliers = Supplier.get_supplier_valid_for_vaf(@venue_application_form.id)

      # Check exist suppliers when new ASCF
      if @suppliers.blank? && params[:id].blank?
        respond_to do |format|
          format.html {
            redirect_to admin_venue_suppliers_path,
              alert: 'Not found any supplier. Please contact Pencil Admin to create a new supplier.'
          }
        end

        return
      end

      @supplier = @suppliers.first

      # Get list trading_terms_settings by supplier and venue form
      list_active = Supplier.available_trading_terms_settings(@supplier.id, @venue_application_form.id)

      # Get trading_terms_settings active for supplier
      @trading_terms_active = TradingTermsSetting.where(active: true, id: list_active, supplier_id: @supplier.id)
      @trading_terms_settings = @trading_terms_active
      @is_directors = nil

      # Check exist default trading_terms_setting
      if (@trading_terms_settings.where(default: true).present?)
        @supplier_terms_condition = @supplier.get_terms_and_condition(@venue_application_form[:id],
                                            @trading_terms_settings.where(default: true).first[:id])

        @trading_terms_setting = @trading_terms_settings.where(default: true).first
        @number_of_trade_references = @trading_terms_setting.get_required_trade_references_quantity
        @number_of_personal_guarantees = @trading_terms_setting.get_required_personal_guarantees_quantity
      end

      # Assign exist terms_condition if default not exist
      if @supplier_terms_condition.blank? && @trading_terms_settings.present?
        @trading_terms_setting = @trading_terms_settings.first

        @supplier_terms_condition = @supplier.get_terms_and_condition(@venue_application_form.id, @trading_terms_setting.id)
      end
    end

    # EDIT:
    if params[:id].present?
      @venue_supplier = ApplySupplierCreditForm.find(params[:id])

      # Get Venue Application Form if current user is a personal guarantee
      @venue_application_form = @venue_supplier.vaf
      @supplier = @venue_supplier.supplier

      @suppliers = Supplier.get_supplier_valid_for_vaf(@venue_application_form.id)
      @suppliers = @suppliers.or(Supplier.where(id: @supplier[:id]))
      @suppliers.uniq { |supplier| supplier[:id] }

      # Get list trading_terms_settings by supplier and venue form
      list_active = Supplier.available_trading_terms_settings(@supplier.id, @venue_application_form.id)

      # Get trading_terms_settings active for supplier
      @trading_terms_active = TradingTermsSetting.where(active: true, id: list_active, supplier_id: @supplier.id)
      @trading_terms_settings = @trading_terms_active
      @is_directors = @venue_supplier.is_directors?

      @selected_director_ids = @venue_supplier.director_ids_as_pgs
      @trading_terms_setting = @venue_supplier.trading_terms_setting

      @number_of_trade_references = @trading_terms_setting.get_required_trade_references_quantity
      @number_of_personal_guarantees = @trading_terms_setting.get_required_personal_guarantees_quantity

      # Get Venue Application Form if current user is a personal guarantee
      @venue_application_forms = @venue_application_forms.or(
        VenueApplicationForm.where(id: @venue_application_form[:id]))
      @venue_application_forms.uniq { |item| item[:id] }

      # Get Terms and conditions
      @supplier_terms_condition = @supplier.get_terms_and_condition(@venue_supplier[:vaf_id],
                                                      @venue_supplier[:trading_terms_setting_id])
    end

    # Getting credit limit of supplier to show at step 3
    @credit_limits = []
    credit_limit = @supplier.get_credit_limit(@venue_application_form[:market_segment_id],
                                     @trading_terms_setting[:id])
    @credit_limits = init_credit_limits(credit_limit) if credit_limit.present?
    # End of getting credit limit of supplier to show at step 3

    if @supplier_terms_condition.blank?
      respond_to do |format|
        format.html {
          redirect_to admin_venue_suppliers_path,
            alert: "Terms and Conditions wasn't created or Trading Terms Settings wasn't updated.
              Please contact Pencil Admin to create a new Terms and Conditions."
        }
      end

      return
    end
  end

  def create_personal_guarantees(
    selected_directors_ids, personal_guarantees_params, apply_supplier_credit_form, is_save_and_continue = false
  )
    # Check selected directors
    if selected_directors_ids.present?
      directors_personal_guarantees_params = []

      selected_directors_ids.each do |director_id|
        director = Director.find_by_id(director_id)

        directors_personal_guarantees_params << {
          firstname: director[:firstname],
          lastname: director[:lastname],
          email: director[:email],
          home_address_line_1: director[:address_line_1],
          home_address_line_2: director[:address_line_2],
          home_suburb: director[:suburb],
          home_state: director[:state],
          home_postcode: director[:postcode],
          mobile: director[:mobile],
          phone: director[:phone],
          home_property_name: director[:property_name],
          home_unit_number: director[:unit_number],
          home_street_number: director[:street_number],
          home_street_name: director[:street_name],
          home_street_type: director[:street_type],
          director_id: director_id
        }
      end

      directors_personal_guarantees_params.each do |personal_guarantee_params|
        next if personal_guarantee_params[:email].blank?
        user = User.where(email: personal_guarantee_params[:email]).first
        is_exist_user = true
        pg_new_attr = PersonalGuarantee.new.attributes
        personal_guarantee_attributes = personal_guarantee_params.reject { |k, _| !pg_new_attr.keys.member?(k.to_s) }

        if user.blank?
          is_exist_user = false
          new_password = SecureRandom.base64(16)

          user_attributes = {
            email: personal_guarantee_params[:email],
            password: new_password,
            password_confirmation: new_password,
            role: 'PERSONAL_GUARANTEE',
            firstname: personal_guarantee_params['firstname'],
            lastname: personal_guarantee_params['lastname'],
            name: "#{personal_guarantee_params['firstname']} #{personal_guarantee_params['lastname']}"
          }

          user = User.new(user_attributes)
          user.skip_confirmation!
          user.save!
        else
          user.add_role(Const::ROLES[:personal_guarantee])
        end

        personal_guarantee = user.personal_guarantees.create!(personal_guarantee_attributes)
        personal_guarantee.send_pga_for_supplier_credit(apply_supplier_credit_form, is_exist_user) unless is_save_and_continue

        attributes = {
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id],
          signature_image: personal_guarantee_params[:signature_image],
          is_signed: personal_guarantee_params[:signature_image].present? ? true : false,
          form_data: apply_supplier_credit_form,
          is_director: true,
          director_id: personal_guarantee_params[:director_id]
        }

        ApplySupplierCreditPersonalGuarantee.where(
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id]
        ).first_or_initialize.tap do |item|
          next unless item.new_record?
          item.assign_attributes(attributes)
          item.save!
        end
      end
    end

    if personal_guarantees_params.present?
      personal_guarantees_params.each do |personal_guarantee_params|
        next if personal_guarantee_params['email'].blank?
        personal_guarantee_attributes = personal_guarantee_params.reject{ |k,v| !PersonalGuarantee.new.attributes.keys.member?(k.to_s) }
        user = User.where(email: personal_guarantee_params['email']).first
        is_exist_user = true

        if user.blank?
          is_exist_user = false
          new_password = SecureRandom.base64(16)

          user_attributes = {
            email: personal_guarantee_params['email'],
            password: new_password,
            password_confirmation: new_password,
            role: 'PERSONAL_GUARANTEE',
            firstname: personal_guarantee_params['firstname'],
            lastname: personal_guarantee_params['lastname'],
            name: "#{personal_guarantee_params['firstname']} #{personal_guarantee_params['lastname']}"
          }

          user = User.new(user_attributes)
          user.skip_confirmation!
          user.save!
        else
          user.add_role(Const::ROLES[:personal_guarantee])
        end

        personal_guarantee = user.personal_guarantees.create(personal_guarantee_attributes)
        personal_guarantee.send_pga_for_supplier_credit(apply_supplier_credit_form, is_exist_user) unless is_save_and_continue

        attributes = {
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id],
          signature_image: personal_guarantee_params['signature_image'],
          is_signed: personal_guarantee_params['signature_image'].present? ? true : false,
          form_data: apply_supplier_credit_form,
        }

        # Check whether this external pg is also director
        pg_is_director = apply_supplier_credit_form.vaf.directors.where(email: personal_guarantee[:email]).present?
        attributes = attributes.merge(is_director: true) if pg_is_director

        ApplySupplierCreditPersonalGuarantee.where(apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
        personal_guarantee_id: personal_guarantee[:id]).first_or_initialize.tap do |item|
          next unless item.new_record?
          item.assign_attributes(attributes)
          item.save!
        end
      end
    end

    apply_supplier_credit_form.personal_guarantees.as_json
  end

  # ascf: apply_supplier_credit_form
  def save_trade_references(params, ascf, is_save_and_continue = false)
    if params['trade_references'].present?
      trade_references_params = params['trade_references'].as_json.values
      trade_references_params = [] << trade_references_params unless trade_references_params.instance_of?(Array)

      trade_references_params.each do |trade_reference_params|
        trade_reference = TradeReference.where(id: trade_reference_params['id']).first
        trade_reference_attributes = trade_reference_params.reject{ |k,v| !TradeReference.new.attributes.keys.member?(k.to_s) }

        if trade_reference.blank?
          trade_reference = ascf.trade_references.create(trade_reference_attributes)
        end

        trade_reference_attributes['id'] = trade_reference[:id]
        trade_reference.attributes = trade_reference_attributes
        trade_reference.save
      end
    end

    ascf.trade_references.as_json
  end

  def update_directors(selected_directors_ids, apply_supplier_credit_form, is_save_and_continue = false)
    if selected_directors_ids.present?
      # Check and remove directors
      ascpg_will_removed = apply_supplier_credit_form.apply_supplier_credit_personal_guarantees.where.not(director_id: selected_directors_ids)
      ascpg_will_removed = ascpg_will_removed.or(apply_supplier_credit_form.apply_supplier_credit_personal_guarantees.where(director_id: nil))
      personal_guarantees_will_removed = PersonalGuarantee.where(id: ascpg_will_removed.map(&:personal_guarantee_id))
      ascpg_will_removed.destroy_all
      personal_guarantees_will_removed.destroy_all

      # Check and create new dictore if have
      personal_guarantees_params = []

      selected_directors_ids.each do |director_id|
        director = Director.find_by_id(director_id)
        next if apply_supplier_credit_form.apply_supplier_credit_personal_guarantees.where(director_id: director_id).count > 0

        personal_guarantees_params << {
          firstname: director[:firstname],
          lastname: director[:lastname],
          email: director[:email],
          home_address_line_1: director[:address_line_1],
          home_address_line_2: director[:address_line_2],
          home_suburb: director[:suburb],
          home_state: director[:state],
          home_postcode: director[:postcode],
          mobile: director[:mobile],
          phone: director[:phone],
          home_property_name: director[:property_name],
          home_unit_number: director[:unit_number],
          home_street_number: director[:street_number],
          home_street_name: director[:street_name],
          home_street_type: director[:street_type],
          director_id: director_id
        }
      end

      personal_guarantees_params = personal_guarantees_params.take 1

      personal_guarantees_params.each do |personal_guarantee_params|
        next if personal_guarantee_params[:email].blank?
        personal_guarantee_attributes = personal_guarantee_params.reject{ |k,v| !PersonalGuarantee.new.attributes.keys.member?(k.to_s) }
        user = User.where(email: personal_guarantee_params[:email]).first
        is_exist_user = true

        if user.blank?
          is_exist_user = false
          new_password = SecureRandom.base64(16)

          user_attributes = {
            email: personal_guarantee_params[:email],
            password: new_password,
            password_confirmation: new_password,
            role: 'PERSONAL_GUARANTEE',
            firstname: personal_guarantee_params['firstname'],
            lastname: personal_guarantee_params['lastname'],
            name: "#{personal_guarantee_params['firstname']} #{personal_guarantee_params['lastname']}"
          }

          user = User.new(user_attributes)
          user.skip_confirmation!
          user.save!
        else
          user.add_role('PERSONAL_GUARANTEE')
        end

        personal_guarantee = user.personal_guarantees.create!(personal_guarantee_attributes)
        personal_guarantee_attributes['id'] = personal_guarantee[:id]
        personal_guarantee.attributes = personal_guarantee_attributes
        personal_guarantee.save
        personal_guarantee.send_pga_for_supplier_credit(apply_supplier_credit_form, is_exist_user) unless is_save_and_continue

        attributes = {
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id],
          signature_image: personal_guarantee_params[:signature_image],
          is_signed: personal_guarantee_params[:signature_image].present? ? true : false,
          form_data: apply_supplier_credit_form,
          is_director: true,
          director_id: personal_guarantee_params[:director_id]
        }

        ApplySupplierCreditPersonalGuarantee.where(
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id]
        ).first_or_initialize.tap do |item|
          next unless item.new_record?
          item.assign_attributes(attributes)
          item.save!
        end
      end
    end
  end

  def update_personal_guarantees(personal_guarantees_params, apply_supplier_credit_form, is_save_and_continue = false)
    if personal_guarantees_params.present?
      # Check and remove personal guarantees as directors
      apply_supplier_credit_form.personal_guarantees_as_director.destroy_all
      # personal_guarantees_params = personal_guarantees_params.take 1

      personal_guarantees_params.each do |personal_guarantee_params|
        next if personal_guarantee_params['email'].blank?
        personal_guarantee = PersonalGuarantee.where(id: personal_guarantee_params['id']).first
        personal_guarantee_attributes = personal_guarantee_params.reject{ |k,v| !PersonalGuarantee.new.attributes.keys.member?(k.to_s) }

        if personal_guarantee.blank?
          user = User.where(email: personal_guarantee_params['email']).first
          is_exist_user = true

          if user.blank?
            is_exist_user = false
            new_password = SecureRandom.base64(16)

            user_attributes = {
              email: personal_guarantee_params['email'],
              password: new_password,
              password_confirmation: new_password,
              role: 'PERSONAL_GUARANTEE',
              firstname: personal_guarantee_params['firstname'],
              lastname: personal_guarantee_params['lastname'],
              name: "#{personal_guarantee_params['firstname']} #{
                personal_guarantee_params['lastname']}"
            }

            user = User.new(user_attributes)
            user.skip_confirmation!
            user.save!
          else
            user.add_role('PERSONAL_GUARANTEE')
          end

          personal_guarantee = user.personal_guarantees.create(personal_guarantee_attributes)
        end

        personal_guarantee_attributes['id'] = personal_guarantee[:id]
        personal_guarantee.attributes = personal_guarantee_attributes
        personal_guarantee.save

        unless is_save_and_continue
          personal_guarantee.send_pga_for_supplier_credit(apply_supplier_credit_form,
            is_exist_user)
        end

        attributes = {
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id],
          signature_image: personal_guarantee_params['signature_image'],
          is_signed: personal_guarantee_params['signature_image'].present? ? true : false,
          form_data: apply_supplier_credit_form,
        }

        # Check whether this external pg is also director
        pg_is_director = apply_supplier_credit_form.vaf.directors.where(email: personal_guarantee[:email]).present?
        attributes = attributes.merge(is_director: true) if pg_is_director

        ApplySupplierCreditPersonalGuarantee.where(
          apply_supplier_credit_form_id: apply_supplier_credit_form[:id],
          personal_guarantee_id: personal_guarantee[:id]
        ).first_or_initialize.tap do |item|
          next unless item.new_record?
          item.assign_attributes(attributes)
          item.save!
        end
      end
    end

    apply_supplier_credit_form.personal_guarantees.as_json
  end

  def venue_supplier_params
    params.require(:apply_supplier_credit_form).permit(
      :supplier_id, :trading_terms_setting_id, :created_by_id, :vaf_id, :save_at, :active
    )
  end

  # Check user has venues before action new
  def check_user_has_venues
    unless current_user.has_venues?
      redirect_to admin_venue_suppliers_path, alert: "Venue application form has not been created. Please create it!"
    end
  end

  # Init the credit limit options to choose in step 3
  def init_credit_limits(credit_limit)
    temp = credit_limit[:increments]
    credit_limits = []

    while temp <= credit_limit[:credit_limit] do
      credit_limits << temp
      temp += credit_limit[:increments]
    end

    if (temp - credit_limit[:credit_limit])  < credit_limit[:increments]
      credit_limits << credit_limit[:credit_limit]
    end

    return credit_limits
  end

  # Check any personal guarantee signed
  def check_any_pg_signed
    is_signed = @venue_supplier.is_signed_by_personal_guarantee?
    is_signed = is_signed && (params['step'].blank? || params['step'] != VenueApplicationForm::STEP_REVIEW)

    if is_signed
      uri = URI.parse(@venue_supplier.get_review_url)
      message = 'This Apply Supplier Credit Form was signed by some personal guarantees.'

      respond_to do |format|
        format.html { redirect_to "#{uri.path}?#{uri.query}", alert: message }
      end

      return true
    end

    return false
  end
end
