class Admin::UserProfilesController < Admin::AdminController
  before_action :authorize_profile_user!, only: [:index]
  before_action :set_title

  def index
    if current_user.is_admin?
      @user = current_user
    elsif current_user.is_director?
      @director = current_user.directors.first
    elsif current_user.is_personal_guarantee?
      @personal_guarantee = current_user.personal_guarantees.first
    end
  end

  def update_admin
    user = User.find(params[:id]) if params[:id].present?

    respond_to do |format|
      if user.present? && user.update(admin_params)
        format.html { redirect_to admin_admin_user_profile_path, notice: 'Your profile was successfully updated.' }
      else
        flash[:alert] = user.errors.full_messages.to_sentence
        format.html { redirect_to admin_admin_user_profile_path }
        format.json { render json: user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_director
    director = Director.find(params[:id]) if params[:id].present?
    # Filter Director attributes and director user password by remove blank fields
    user_attributes = director_params.permit(:password, :password_confirmation).reject { |_, v| v.blank? }

    director_attributes = director_params.permit(
      :firstname, :middlename, :lastname, :date_of_birth, :email, :address_line_1, :address_line_2, :mobile, :phone,
      :drivers_license_number, :license_expiry
    ).reject { |_, v| v.blank? }

    respond_to do |format|
      if !director.blank? && director.update(director_attributes) && director.user.update(user_attributes)
        format.html { redirect_to admin_admin_user_profile_path, notice: 'Your profile was successfully updated.' }
      else
        flash[:alert] = (director.errors.full_messages + director.user.errors.full_messages).to_sentence
        format.html { redirect_to admin_admin_user_profile_path }
        format.json { render json: director.errors + director.user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_personal_guarantee
    personal_guarantee = PersonalGuarantee.find(params[:id]) if params[:id].present?
    # Filter Director attributes and director user password by remove blank fields
    user_attributes = pg_params.permit(:password, :password_confirmation).reject { |_, v| v.blank? }

    pg_attributes = pg_params.permit(
      :firstname, :lastname, :email, :mobile, :phone, :home_address_line_1, :home_address_line_2
    ).reject { |_, v| v.blank? }

    respond_to do |format|
      if !personal_guarantee.blank? && personal_guarantee.update(pg_attributes) &&
         personal_guarantee.user.update(user_attributes)
        format.html { redirect_to admin_admin_user_profile_path, notice: 'Your profile was successfully updated.' }
      else
        full_message = personal_guarantee.errors.full_messages + personal_guarantee.user.errors.full_messages
        full_errors = personal_guarantee.errors + personal_guarantee.user.errors
        flash[:alert] = full_message.to_sentence
        format.html { redirect_to admin_admin_user_profile_path }
        format.json { render json: full_errors, status: :unprocessable_entity }
      end
    end
  end

  def update_avatar
    if params[:user].blank?
      redirect_to admin_admin_user_profile_path, notice: 'Your Avatar does not change.'
      return
    end

    if current_user.update(user_avatar_params)
      if user_avatar_params[:avatar].blank?
        message = 'Your Avatar was successfully updated.'
      else
        message = 'Your Avatar was cropped successfully.'
      end
    else
      message = 'Your Avatar was not successfully updated.'
    end

    redirect_to admin_admin_user_profile_path, notice: message
  end

  private

  def set_title
    $current_page = 'Pencil | My Profile'
    $subheader_title = 'My Profile'
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_params
    params.require(:user).permit(
      :name, :firstname, :lastname, :email, :password, :password_confirmation
    ).reject { |_, v| v.blank? }
  end

  def director_params
    params.require(:director)
  end

  def pg_params
    params.require(:personal_guarantee)
  end

  def user_avatar_params
    params.require(:user).permit(:avatar, :crop_x, :crop_y, :crop_w, :crop_h)
  end

  def authorize_profile_user!
    unless current_user.is_admin? || current_user.is_director? || current_user.is_personal_guarantee?
      redirect_to admin_venue_details_url
    end
  end
end
