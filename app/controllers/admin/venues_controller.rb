class Admin::VenuesController < Admin::AdminController
  before_action :set_title
  before_action :authorize_admin_supplier!, only: [:import, :export, :send_email_directors, :send_email_pgs]
  before_action :set_ascf, only: [:approve, :reject, :send_credit_limit]

  def index
    @venue_forms = []
    @all_venues_status = ApplySupplierCreditForm.get_all_status

    if current_user.is_admin?
      @venue_forms = VenueApplicationForm.order(object_index: :desc).includes(:directors)
    elsif current_user.is_supplier?
      @venue_forms = current_user.supplier.get_venue_application_forms
    elsif current_user.is_venue_user?
      @venue_forms = current_user.get_venue_user_venue_application_forms
    end
  end

  # Import Venue Application Form (Venues) from csv file.
  def import
    errors_report = {}
    credit_errors_report = {}
    import_option = import_params[:import_option].present? ? import_params[:import_option] : 'all_option'
    need_create_default_ascfs = false

    if import_params[:create_default_ascfs].present? && import_params[:create_default_ascfs] == 'create_default_ascfs'
      need_create_default_ascfs = true
    end

    # If current user is admin
    if current_user.is_admin?
      if import_params[:venues_file].present? && (import_option == 'all_option' || import_option == 'venue_only')
        errors_report = VenueApplicationForm.import(import_params[:venues_file])
      end

      if import_params[:apply_credit_file].present? && current_user.is_admin? &&
          (import_option == 'all_option' || import_option == 'apply_credit_only')
        credit_errors_report = ApplySupplierCreditForm.import(import_params[:apply_credit_file])
      end
    end

    # If current user is supplier
    if current_user.is_supplier? && !current_user.is_admin?
      # If supplier has not set their T&C
      if import_params[:venues_file].present? && import_option == 'venue_only' && need_create_default_ascfs &&
          current_user.supplier.supplier_terms_conditions.blank?
        redirect_to admin_venue_details_url, alert: 'Your T&C has not set yet. Please update it and try again.'
        return
      end

      if import_params[:apply_credit_file].present? && (import_option == 'all_option' || import_option == 'apply_credit_only') &&
          current_user.supplier.supplier_terms_conditions.blank?
        redirect_to admin_venue_details_url, alert: 'Your T&C has not set yet. Please update it and try again.'
        return
      end

      if import_params[:venues_file].present? && (import_option == 'all_option' || import_option == 'venue_only')
        user = need_create_default_ascfs ? current_user : nil
        errors_report = VenueApplicationForm.import(import_params[:venues_file], user)
      end

      if import_params[:apply_credit_file].present? && (import_option == 'all_option' || import_option == 'apply_credit_only')
        credit_errors_report = ApplySupplierCreditForm.import(import_params[:apply_credit_file], current_user.supplier[:abn])
      end
    end

    if errors_report[:no_data] && credit_errors_report[:no_data]
      redirect_to admin_venue_details_url, alert: 'No Venue has been imported.'
      return
    end

    has_error = errors_report.values.any?(&:present?)
    has_error ||= credit_errors_report.values.any?(&:present?)

    if has_error
      render 'import_errors', :locals => { errors_report: errors_report,
                                          credit_errors_report: credit_errors_report }
    else
      redirect_to admin_venue_details_url, notice: 'Venue / Apply credit forms have been imported successfully.'
    end
  end

  # Export Venue Application Form (Venues) / Apply supplier credit fomrs from csv file.
  def export
    venues = []
    datetimenow = DateTime.now.strftime('%Y%m%d-%H%m%S%z')

    # If current user is admin
    if current_user.is_admin?
      # If user choose export venues and all option
      if export_params[:venue_option] == 'venue' && export_params[:export_option] == 'all'
        venues = VenueApplicationForm.order(object_index: :desc)

        respond_to do |format|
          format.html
          format.csv { send_data VenueApplicationForm.to_csv(venues), filename: "venues-#{datetimenow}.csv" }
        end

        return
      end

      # If user choose export venues and supplier option
      if export_params[:venue_option] == 'venue' && export_params[:export_option] == 'supplier'
        supplier = Supplier.where(id: export_params[:supplier_id]).first
        venues = supplier.get_venue_application_forms

        respond_to do |format|
          format.html
          format.csv { send_data VenueApplicationForm.to_csv(venues),
                       filename: "venues-#{supplier[:company_name]}-#{datetimenow}.csv" }
        end

        return
      end

      # If user choose export apply credit forms and all option
      if export_params[:venue_option] == 'apply_credit' && export_params[:export_option] == 'all'
        ascfs = ApplySupplierCreditForm.all

        respond_to do |format|
          format.html
          format.csv { send_data ApplySupplierCreditForm.to_csv(ascfs),
                       filename: "apply-credit-forms-#{datetimenow}.csv" }
        end

        return
      end

      # If user choose export apply credit forms and supplier option
      if export_params[:venue_option] == 'apply_credit' && export_params[:export_option] == 'supplier'
        supplier = Supplier.where(id: export_params[:supplier_id]).first
        ascfs = supplier.apply_supplier_credit_forms

        respond_to do |format|
          format.html
          format.csv { send_data ApplySupplierCreditForm.to_csv(ascfs),
                       filename: "apply-credit-forms-#{supplier[:company_name]}-#{datetimenow}.csv" }
        end

        return
      end
    end

    # If current user is supplier
    if current_user.is_supplier?
      # If user choose export venues
      if export_params[:venue_option] == 'venue'
        venues = current_user.supplier.get_venue_application_forms

        respond_to do |format|
          format.html
          format.csv { send_data VenueApplicationForm.to_csv(venues),
                       filename: "venues-#{current_user.supplier[:company_name]}-#{datetimenow}.csv" }
        end

        return
      end

      # If user choose export apply credit forms
      if export_params[:venue_option] == 'apply_credit'
        ascfs = current_user.supplier.apply_supplier_credit_forms

        respond_to do |format|
          format.html
          format.csv { send_data ApplySupplierCreditForm.to_csv(ascfs),
                       filename: "apply-credit-forms-#{current_user.supplier[:company_name]}-#{datetimenow}.csv" }
        end

        return
      end
    end
  end

  def send_email_directors
    venue = VenueApplicationForm.where(id: params['id']).first if params['id'].present?

    # Check if venue not exist
    if venue.blank?
      redirect_to admin_venue_details_url, alert: 'Error! Venue has not been found.'
      return
    end

    # Send emails
    has_sent_emails = venue.send_directors_emails(send_email_params)

    # If no email has been sent
    unless has_sent_emails
      redirect_to admin_venue_details_url, alert: 'No emails has been sent.'
      return
    end

    redirect_to admin_venue_details_url, notice: 'Emails have been sent.'
  end

  # pgs: personal guarantees
  def send_email_pgs
    # ascf: Apply Supplier Credit Form
    ascf = ApplySupplierCreditForm.where(id: params['id']).includes(:vaf).first if params['id'].present?

    if ascf.blank?
      redirect_to admin_venue_details_url, alert: 'Error! Apply Supplier Credit Form has not been found.'
      return
    end

    # Send emails
    has_sent_directors_emails = ascf.vaf.send_directors_emails(send_email_params)
    has_sent_pgs_emails = ascf.send_pgs_emails(send_email_params)

    # If no email has been sent
    unless has_sent_directors_emails || has_sent_pgs_emails
      redirect_to admin_venue_details_url, alert: 'No emails has been sent.'
      return
    end

    redirect_to admin_venue_details_url, notice: 'Emails have been sent.'
  end

  # Approve venues (apply credit)
  def approve
    redirect_url = admin_venue_details_url

    unless current_user.is_director? || current_user.is_supplier?
      redirect_to redirect_url
    end

    redirect_url = admin_venue_suppliers_path if current_user.is_director?
    is_approved = @ascf.approve_by_director(current_user) || @ascf.approve_by_supplier(current_user)

    if is_approved
      redirect_to redirect_url, notice: 'Apply credit has been approved successfully.'
    else
      redirect_to redirect_url, alert: 'Apply credit has not been approved successfully.'
    end
  end

  # Reject venues (apply credit)
  def reject
    redirect_url = admin_venue_details_url

    unless current_user.is_director? || current_user.is_supplier?
      redirect_to redirect_url
    end

    redirect_url = admin_venue_suppliers_path if current_user.is_director?
    is_rejected = @ascf.reject_by_director(current_user) || @ascf.reject_by_supplier(current_user)

    if is_rejected
      redirect_to redirect_url, notice: 'Apply credit has been rejected successfully.'
    else
      redirect_to redirect_url, notice: 'Apply credit has not been rejected successfully.'
    end
  end

  # Send new credit limit
  def send_credit_limit
    if @ascf.send_credit_limit(current_user, reject_apply_credit_params)
      redirect_to admin_venue_details_url, notice: 'New credit limit has been sent successfully.'
    else
      redirect_to admin_venue_details_url, alert: 'New credit limit has not been sent successfully.'
    end
  end

  private

    def set_title
      $current_page = 'Pencil | Venues'
      $subheader_title = 'Venues'
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def import_params
      params.require(:venues).permit(:venues_file, :apply_credit_file, :import_option, :create_default_ascfs)
    end

    def export_params
      params.require(:venue_export_options).permit(:venue_option, :export_option, :supplier_id)
    end

    def send_email_params
      params.require(:send_email).permit(:directors_option, :director_id, :director_email, :director_mobile,
                :pgs_option, :personal_guarantee_id, :personal_guarantee_email, :personal_guarantee_mobile,
                :pg_director_id)
    end

    def reject_apply_credit_params
      params.require(:reject_apply_credit).permit(:supplier_credit_limit)
    end

    def set_ascf
      @ascf = ApplySupplierCreditForm.where(id: params['id']).first if params['id'].present?
    end
end
