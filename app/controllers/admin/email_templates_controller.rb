class Admin::EmailTemplatesController < Admin::AdminController
  before_action :authorize_admin!
  before_action :set_email_template, only: [:show, :edit, :update, :destroy]
  before_action :set_title

  # GET /admin/email_templates
  # GET /admin/email_templates.json
  def index
    @email_templates = EmailTemplate.order(created_at: :asc).includes(:created_by)
  end

  # GET /admin/email_templates/1
  # GET /admin/email_templates/1.json
  def show
  end

  # GET /admin/email_templates/new
  def new
    @email_template = EmailTemplate.new
  end

  # GET /admin/email_templates/1/edit
  def edit
  end

  # POST /admin/email_templates
  # POST /admin/email_templates.json
  def create
    params[:email_template][:created_by_id] = current_user.id
    @email_template = EmailTemplate.new(email_template_params)

    respond_to do |format|
      if @email_template.save
        format.html { redirect_to admin_email_templates_url, notice: 'Email Template was successfully created.' }
        format.json { render :show, status: :created, location: @email_template }
      else
        format.html { render :new }
        format.json { render json: @email_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/email_templates/1
  # PATCH/PUT /admin/email_templates/1.json
  def update
    respond_to do |format|
      if @email_template.update(email_template_params)
        format.html { redirect_to admin_email_templates_path, notice: 'Email Template was successfully updated.' }
        format.json { render :show, status: :ok, location: @email_template }
      else
        format.html { render :edit }
        format.json { render json: @email_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/email_templates/1
  # DELETE /admin/email_templates/1.json
  def destroy
    @email_template.destroy

    respond_to do |format|
      format.html { redirect_to admin_email_templates_url, notice: 'Email Template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_title
    $current_page = 'Pencil | Email Templates'
    $subheader_title = 'Email Templates'
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_email_template
    @email_template = EmailTemplate.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def email_template_params
    params.require(:email_template).permit(
      :name, :subject, :plain_content, :html_content, :category, :active, :created_by_id
    )
  end
end
