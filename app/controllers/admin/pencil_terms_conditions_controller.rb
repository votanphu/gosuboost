class Admin::PencilTermsConditionsController < Admin::AdminController
  before_action :authorize_admin!
  before_action :set_title
  before_action :set_pencil_terms_conditions, only: [:edit, :update]

  def index
    @pencil_terms_condition = PencilTermsCondition.first || []
  end

  # def edit
  # end

  def update
    respond_to do |format|
      if @pencil_terms_condition.update(pencil_terms_conditions_params)
        message = 'Pencil Terms & Condition was successfully updated.'
        format.html { redirect_to admin_pencil_terms_conditions_path, notice: message }
        format.json { render :show, status: :ok, location: @pencil_terms_condition }
      else
        format.html { render :edit }
        format.json { render json: @pencil_terms_condition.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_title
    $current_page = 'Pencil | Pencil Venue T&Cs'
    $subheader_title = 'Pencil Venue T&Cs'
  end

  def set_pencil_terms_conditions
    @pencil_terms_condition = PencilTermsCondition.find(params['id']) if params['id'].present?
  end

  def pencil_terms_conditions_params
    params.require(:pencil_terms_condition).permit(:content)
  end
end
