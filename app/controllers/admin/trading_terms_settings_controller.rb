class Admin::TradingTermsSettingsController < Admin::AdminController
  before_action :authorize_supplier!
  before_action :set_trading_terms_settings, only: [:index, :update]
  before_action :set_title

  def index
  end

  def update
    trading_terms_settings_params.each do |id, trading_terms_setting_params|
      next if @trading_terms_settings.where(id: id.to_i).blank?

      trading_terms_setting_attributes = trading_terms_setting_params.permit(
        :trading_term, :personal_guarantees, :directors, :trade_references, :active, :default
      )

      trading_terms_setting = TradingTermsSetting.find(id.to_i)

      unless trading_terms_setting_attributes[:default]
        trading_terms_setting.update_attributes!(trading_terms_setting_attributes)
        trading_terms_setting.update_attributes!(default: false)
      end

      if trading_terms_setting_attributes[:default]
        trading_terms_setting_attributes[:active] = true
        trading_terms_setting.update_attributes!(trading_terms_setting_attributes)
      end
    end

    flash[:notice] = 'Your trading terms setting has been saved.'
    redirect_to controller: 'admin/trading_terms_settings', action: 'index'
  end

  private

  def set_title
    $current_page = 'Pencil | Trading Terms Settings'
    $subheader_title = 'Trading Terms Settings'
  end

  def set_trading_terms_settings
    @trading_terms_settings = current_user.supplier.trading_terms_settings.order(created_at: :asc)
  end

  def trading_terms_settings_params
    params.require(:trading_terms_settings)
  end
end
