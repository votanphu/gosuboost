class Admin::AdminController < ApplicationController
  layout 'admin'
  before_action :store_location
  before_action :authenticate_user!

  protected
    def store_location
      session[:previous_url] = request.fullpath if (request.fullpath =~ /\/admin\/venue_suppliers\/.*\/edit/).present?
    end
end
