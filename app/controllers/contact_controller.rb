class ContactController < ApplicationController
  def create
    if !params["g-recaptcha-response"].present?
      flash[:alert] = "reCAPTCHA verification failed, please try again."
      redirect_to :controller => 'pages', :action => 'home'
      return
    end

  	contact = Contact.new(contact_params)

  	if contact.save!
  		flash[:notice] = "Your message has been sent. Thank you!"
      redirect_to :controller => 'pages', :action => 'home'
  	else
  		redirect_to :controller => 'pages', :action => 'home'
  	end
  end

  private

  def contact_params
    params.require(:contact).permit(:firstname, :lastname, :email, :subject, :message)
  end
end
