class UserController < ApplicationController
  layout 'blank'
  before_action :set_user, only: [:update_password]

  def create_users
    users = params[:users]

    users.each do |item_user|
      user = User.where(email: item_user[:email]).first

      if user.blank?
        new_password = SecureRandom.base64(16)

        user_attributes = {
          email: item_user[:email],
          password: new_password,
          password_confirmation: new_password
        }

        user = User.new(user_attributes)
        user.skip_confirmation!
        user.save!
      end
    end

    respond_to do |format|
      format.json {
        render json: {
          message: 'Users has been created successfully.'
        },
        status: :created
      }
    end
  end

  def set_password
    @user = User.where(id: params['user_token']).first if @user.blank?
    @vaf = VenueApplicationForm.get_vaf_by_token(params['form_token']) if params['form_token'].present?
    @ascf = ApplySupplierCreditForm.get_ascf_by_token(params['ascf_token']) if params['ascf_token'].present?

    if @user.present? && params['form_token'].present? && @vaf.present? && @user.created_at != @user.updated_at
      uri = URI.parse(@vaf.sign_in_to_form_url(@user))
      redirect_to "#{uri.path}?#{uri.query}"
    end

    if @user.present? && params['ascf_token'].present? && @ascf.present? && @user.created_at != @user.updated_at
      uri = URI.parse(@ascf.sign_in_to_form_url(@user))
      redirect_to "#{uri.path}?#{uri.query}"
    end
  end

  def edit_password
  end

  def update_password
    @user.password = user_params[:password]
    @user.password_confirmation = user_params[:confirm_password]
    @vaf = VenueApplicationForm.get_vaf_by_token(user_params[:form_token])
    @ascf = ApplySupplierCreditForm.get_ascf_by_token(user_params[:ascf_token])

    respond_to do |format|
      if @user.save
        sign_in(@user, scope: :user)

        if user_params[:form_token].present? && @vaf.present?
          uri = URI.parse(@vaf.get_review_url)
          format.html { redirect_to "#{uri.path}?#{uri.query}", notice: 'The password has been set successfully.' }
          format.json { render :set_password, status: :ok, location: @user }
        elsif user_params[:ascf_token].present? && @ascf.present?
          uri = URI.parse(@ascf.get_review_url)
          format.html { redirect_to "#{uri.path}?#{uri.query}", notice: 'The password has been set successfully.' }
          format.json { render :set_password, status: :ok, location: @user }
        else
          format.html { redirect_to root_path, notice: 'The password has been set successfully.' }
        end
      else
        flash[:alert] = @user.errors.full_messages.to_sentence
        format.html { render :edit_password }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:email, :user_token, :form_token, :password,
        :confirm_password, :ascf_token)
    end
end
