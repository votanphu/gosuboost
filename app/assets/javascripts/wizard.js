//== Class definition
var WizardVAF = function () {
  //== Base elements
  var wizardEl = $('#m_wizard');
  var formEl = $('#venue_form');
  var validator;
  var wizard;

  var isDuplicateEmail = function isDuplicateEmail() {
    var errorCounterDupInput = 0;
    var email_values = [];

    formEl.find('.email').each(function (i, el) {
      var current_val = $(el).val();

      if (current_val) {
        if (email_values.indexOf(current_val) === -1) {
          email_values.push(current_val);
        } else {
          errorCounterDupInput++;
        }
      }
    });

    if (errorCounterDupInput > 0) {
      swal({
        "title": "",
        "text": "The emails are duplicated. Please update them.",
        "type": "error",
        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
      });

      return true;
    }

    return false;
  }

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? false : sParameterName[1];
      }
    }
  };

  var scrollToError = function scrollToError() {
    var findErrorClass = setInterval(function () {
      if ($(".form-control-feedback").length > 0) {
        $('html, body').animate({
          scrollTop: $(".form-control-feedback").offset().top - 150
        }, 500);

        clearInterval(findErrorClass);
      }
    }, 500);
  }

  //== Private functions
  var initWizard = function () {
    var startStep = 1;

    if (getUrlParameter('step') && getUrlParameter('step') == 'review') {
      startStep = 7
    }

    //== Initialize form wizard
    wizard = wizardEl.mWizard({
      startStep: startStep
    });

    //== Validation before going to next page
    wizard.on('beforeNext', function(wizard) {
      if (isDuplicateEmail()) {
        return false;
      }

      var currentStep = $('.m-wizard__form-step--current');

      if (currentStep.attr('id') == 'm_wizard_form_step_3') {
        var btn = $('#check-abn');
        var groupParent = btn.closest('.m-form__group-sub');
        groupParent.removeClass('has-success');
        groupParent.removeClass('has-danger');
        groupParent.find('.form-control-feedback').remove();
      }

      if (validator.form() !== true) {
        return false;  // don't go to the next step
      }

      if (currentStep.attr('id') == 'm_wizard_form_step_3' && $('#company_name').val() == '') {
        checkABN();
        return false;
      }
    })

    //== Change event = passed current step, move to next step
    wizard.on('change', function(wizard) {
      scrollToError();
      showReviewStep();
    });

    if (getUrlParameter('step') && getUrlParameter('step') == 'review') {
      showReviewStep();
    }
  }

  var initValidation = function() {
    validator = formEl.validate({
      //== Validate only visible fields
      ignore: ":hidden",

      //== Validation rules
      rules: {
        // === Client Information(step 1)
        'venue_application_form[trading_name]': {
          required: true
        },
        'venue_application_form[email]': {
          required: true,
          email: true
        },

        // === Client Information(step 2)
        'venue_application_form[firstname]': {
          required: true
        },
        'venue_application_form[lastname]': {
          required: true
        },
        'venue_application_form[mobile]': {
          required: true
        },
        'venue_application_form[trading_street_number]': {
          required: true
        },
        'venue_application_form[trading_street_name]': {
          required: true
        },
        'venue_application_form[trading_street_type]': {
          required: true
        },
        'venue_application_form[trading_suburb]': {
          required: true
        },
        'venue_application_form[trading_state]': {
          required: true
        },
        'venue_application_form[trading_postcode]': {
          required: true
        },
        'venue_application_form[estimated_weekly_expected_purchases]': {
          required: true,
          number: true
        },
        'venue_application_form[requested_trading_terms]': {
          required: true
        },
        termsConditions: {
          required: true
        },

        //=== Client Information(step 3)
        'venue_application_form[company_abn]': {
          required: true
        },
        'venue_application_form[company_name]': {
          required: true
        },
        'venue_application_form[operating_structure]': {
          required: true
        },
        'venue_application_form[registered_street_number]': {
          required: true
        },
        'venue_application_form[registered_street_name]': {
          required: true
        },
        'venue_application_form[registered_street_type]': {
          required: true
        },
        'venue_application_form[registered_suburb]': {
          required: true
        },
        'venue_application_form[registered_state]': {
          required: true
        },
        'venue_application_form[registered_postcode]': {
          required: true
        },

        //=== Confirmation(step 4)
        'venue_application_form[bank_branch]': {
          required: true
        },
        'venue_application_form[bsb]': {
          required: true
        },
        'venue_application_form[account_number]': {
          required: true
        },

        //=== Client Information(step 5)
        'venue_application_form[account_contact_name]': {
          required: true
        },
        'venue_application_form[account_contact_email]': {
          required: true,
          email: true
        },
        'venue_application_form[account_contact_number]': {
          required: true
        },
        'venue_application_form[delivery_contact_name]': {
          required: true
        },
        'venue_application_form[delivery_contact_number]': {
          required: true
        },

        //=== Client Information(step 6)
        'directors[0][firstname]': {
          required: true
        },
        'directors[0][lastname]': {
          required: true
        },
        'directors[0][email]': {
          required: true,
          email: true
        },
        'directors[0][date_of_birth]': {
          required: true
        },
        'directors[0][street_number]': {
          required: true
        },
        'directors[0][street_name]': {
          required: true
        },
        'directors[0][street_type]': {
          required: true
        },
        'directors[0][suburb]': {
          required: true
        },
        'directors[0][state]': {
          required: true
        },
        'directors[0][postcode]': {
          required: true
        },
        'directors[0][mobile]': {
          required: true
        },
        'directors[0][drivers_license_number]': {
          required: true
        },
        'directors[0][license_expiry]': {
          required: true
        },

        //=== Client Information(step 7)

        //=== Client Information(step review)
        termsConditions2: {
          required: true
        }
      },

      //== Validation messages
      messages: {
        termsConditions: {
          required: "You must accept the Terms and Conditions agreement!"
        },
        termsConditions2: {
          required: "You must accept the Terms and Conditions agreement!"
        }
      },

      //== Display error
      invalidHandler: function(event, validator) {
        scrollToError();

        swal({
          "title": "",
          "text": "There are some errors in your submission. Please correct them.",
          "type": "error",
          "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
        });
      },

      //== Submit valid form
      submitHandler: function (form) { }
    });

    //=== Client Information(step 6) - for repeater of director
    formEl.find('.required').each(function() {
      $(this).rules('add', {
        required: true
      });
    });

    formEl.find('.email').each(function() {
      $(this).rules('add', {
        email: true
      });
    });
  }

  var initSubmit = function() {
    var btn = formEl.find('[data-wizard-action="submit"]');

    btn.on('click', function(e) {
      e.preventDefault();

      if (isDuplicateEmail()) {
        return false;
      }

      if (validator.form()) {
        var currentStep = $('.m-wizard__form-step--current');

        if (currentStep.attr('id') == 'm_wizard_form_step_3' && $('#company_name').val() == '') {
          checkABN();
          return;
        }

        mApp.progress(btn);

        formEl.ajaxSubmit({
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          success: function(responseText, statusText, xhr, $form) {
            mApp.unprogress(btn);

            swal({
              "title": "",
              "text": "The venue application form has been successfully submitted!",
              "type": "success",
              "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });

            var responseText = responseText;

            setTimeout(function() {
              window.location.href = responseText.redirect_url;
            }, 2000);
          },
          error: function(response) {
            mApp.unprogress(btn);
            var data = response.responseJSON;
            var message = "There are some errors in your submission. Please correct them.";

            if (data.message) {
              message = data.message
            }

            swal({
              "title": "",
              "text": message,
              "type": "error",
              "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
          }
        });
      }
    });
  }

  var initInputMask = function() {
    $(".mobileAU").inputmask("mask", {
      "mask": "0499 999 999",
      "clearIncomplete": true
    });

    $(".phoneAU, .phoneNumber").inputmask("mask", {
      "mask": "(09) 9999 9999",
      "clearIncomplete": true
    });

    $(".postcodeAU").inputmask("mask", {
      "mask": "9{4}",
      "clearIncomplete": true
    });

    $(".numer10Digits").inputmask("mask", {
      "mask": "9{10}",
      "clearIncomplete": true
    });

    $(".number1To20").inputmask("mask", {
      "mask": "9{1,20}",
      "clearIncomplete": true
    });

    $(".date_picker, .date_of_birth, .license_expiry").inputmask("99/99/9999", {
      "placeholder": "dd/mm/yyyy",
      "clearIncomplete": true
    });

    $('.phoneNumber').each(function() {
      var input_mask = 0;

      $(this).on('input', function() {
        if($(this).val().substr(1,2) == "04") {
          $(this).inputmask("unmask", {
            "mask": "(09) 9999 9999",
            "clearIncomplete": true
          });

          $(this).inputmask("mask", {
            "mask": "0499 999 999",
            "clearIncomplete": true
          });
        }
      });
    });
  }

  var showReviewStep = function() {
    var buildElement = '';
    var director_count = 1;
    var trade_reference_count = 1;
    var is_front_license_image = false;
    var is_back_license_image = false;

    $('[id^=m_wizard_form_step_]').not('#m_wizard_form_step_review').each(function() {
      if (is_front_license_image && !is_back_license_image) {
        is_back_license_image = true;
        buildElement += '</div>';
      }

      var title = $(this).find('h3.m-form__heading-title').text().trim();
      buildElement += '<div class="card mb-3"><div class="card-header font-weight-bold">' + title + '</div><div class="card-body pb-1">';
      is_front_license_image = false;
      is_back_license_image = false;

      $(this).find('input.form-control, select.form-control, .signature-image, .signed-at, .front-license-image, .back-license-image').each(function() {
        var label = $(this).data('label') ? $(this).data('label') : '';

        if (title && title.toLowerCase() == 'directors' && label.toLowerCase() == 'first name') {
          if (director_count > 1) {
            buildElement += '<hr/>';
          }

          buildElement += '<h5 class="card-title">Director ' + director_count + '</h5>';
          director_count++;
          is_front_license_image = false;
          is_back_license_image = false;
        }

        if (label.toLowerCase() != 'front of license' && label.toLowerCase() != 'back of license') {
          if (label.toLowerCase() == 'signed') {
            if (is_front_license_image && !is_back_license_image) {
              is_back_license_image = true;
              buildElement += '</div>';
            }

            var src = $(this).attr('src');
            buildElement += '<p>' + label + ':</p>';
            buildElement += '<p><img class="signature-image" alt="signature-image" src="' + src + '" width="300">';
          } else if (label.toLowerCase() == 'date' || label.toLowerCase() == 'time' ||
                     label.toLowerCase() == 'ip address' || label.toLowerCase() == 'geo') {
            var signed_info = $(this).text();
            buildElement += '<p style="margin:0">' + label + ': ' + signed_info + '</p>';
          } else if (label.toLowerCase() == 'market segment') {
            var market_segment = $(this).children(':selected').text();
            buildElement += '<p>' + label + ': ' + market_segment + '</p>';
          } else if (label.toLowerCase() == 'front of drivers license or passport number') {
            is_front_license_image = true;
            var frontLicenseImageSrc = $(this).attr('src');
            buildElement += '<div class="row"><div class="col-lg-6"><p class="text-center font-weight-bold">' + label + '</p><img class="front-license-image img-fluid mb-3" alt="front-license-image" src="' + frontLicenseImageSrc + '"></div>';
          } else if (label.toLowerCase() == 'back of drivers license or passport number') {
            is_back_license_image = true;
            var backLicenseImageSrc = $(this).attr('src');
            var beginWrapper = '';

            if (!is_front_license_image) {
              beginWrapper = '<div class="row">';
            }

            buildElement += beginWrapper + '<div class="col-lg-6"><p class="text-center font-weight-bold">' + label + '</p><img class="back-license-image img-fluid mb-3" alt="back-license-image" src="' + backLicenseImageSrc + '"></div></div>';
          } else if (label.toLowerCase() == 'has been bankrupt') {
            var value = $(this).val();
            has_been_bankrupt = "No";

            if (value == "true") {
              has_been_bankrupt = "Yes"
            }

            buildElement += '<p>' + label + ': ' + has_been_bankrupt + '</p>';
          } else {
            var value = $(this).val();
            buildElement += '<p>' + label + ': ' + value + '</p>';
          }
        }
      });

      buildElement += '</div></div>';
    });

    $('.form-reviews').html(buildElement);
  }

  var datepicker = function () {
    var dobMaxDate = new Date(new Date().getFullYear(), 11, 31);
    dobMaxDate.setFullYear(dobMaxDate.getFullYear() - 18);

    $('.date_picker').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      orientation: "bottom left",
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
      },
      autoclose: true
    });

    $('.date_of_birth').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      orientation: "bottom left",
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
      },
      autoclose: true,
      endDate: dobMaxDate
    });

    $('.license_expiry').datepicker({
      format: 'dd/mm/yyyy',
      todayHighlight: true,
      orientation: "bottom left",
      templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
      },
      autoclose: true,
      startDate: '+1d'
    });
  }

  var repeater = function() {
    var repeaterDirectors = $('#m_repeater_directors').repeater({
      initEmpty: false,
      show: function () {
        $(this).slideDown();
        updateItemIndex(this);
        datepicker();
        initValidation();
        initInputMask();
        initSelect2();
        changeIdsToApplyDropzone(this);
      },
      isFirstItemUndeletable: true,
      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  }

  var updateItemIndex = function(item) {
    var newIndex = $(item).parent('div').find('div[data-repeater-item]').length - 1;
    $(item).data('index', newIndex);
    $(item).find('.data-repeater-item-index').text(newIndex + 1);
  }

  var initDropZoneItem = function(index) {
    // 2 upload fields
    var dropZoneFrontId = '#dropzoneFront' + index;
    var dropZoneBackId = '#dropzoneBack' + index;

    if ($(dropZoneFrontId).length) {
      var dropzoneFront = new Dropzone(dropZoneFrontId, {
        url: "/directors/upload-front-license-file.json",
        method: "POST",
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        withCredentials: false,
        acceptedFiles: '.png,.jpg,.jpeg,.gif,.bmp',
        maxFiles: 1,
        maxFilesize: 100, // MB
        addRemoveLinks: true
      });

      dropzoneFront.on("sending", function(file, xhr, formData) {
        // console.log('file', file);
        // console.log('xhr', xhr);
        // console.log('formData', formData);

        var id = $(dropZoneFrontId).closest('.director-item').find('.director-id').val();
        var email = $(dropZoneFrontId).closest('.director-item').find('.email').val();
        formData.append("id", id);
        formData.append("email", email);
      });

      dropzoneFront.on("success", function(file, response) {
        console.log('file', file);
        console.log('response', response);

        var frontLicenseFileImg = $(dropZoneFrontId).closest('.director-item').find('.front-license-file-preview img');
        var frontLicenseFileInput = $(dropZoneFrontId).closest('.director-item').find('.front-license-file-preview input');
        var itemIndex = $(dropZoneFrontId).closest('.director-item').data('index');

        if (frontLicenseFileImg.length > 0) {
          frontLicenseFileImg.attr('src', file.dataURL);
          frontLicenseFileInput.val(file.dataURL);
        } else {
          var inputFrontLicenseFile = '<input type="hidden" value="' + file.dataURL + '" name="directors[' + itemIndex + '][front_license_file]" class="front-license-file-input" />';
          var frontLicenseHtml = '<div class="mb-3 front-license-file-preview">' + inputFrontLicenseFile + '<img alt="front-license-file" src="' + file.dataURL + '" class="front-license-image img-fluid" data-label="Front Of License"></div>';
          $(dropZoneFrontId).before(frontLicenseHtml);
        }

        if (file.previewElement) {
          return file.previewElement.classList.add("dz-success");
        }
      });

      dropzoneFront.on("error", function(file, message) {
        if (file.previewElement) {
          file.previewElement.classList.add("dz-error");

          if (typeof message !== "String" && message.error) {
            message = message.error;
          }

          for (var _iterator7 = file.previewElement.querySelectorAll("[data-dz-errormessage]"), _isArray7 = true, _i7 = 0, _iterator7 = _isArray7 ? _iterator7 : _iterator7[Symbol.iterator]();;) {
            var _ref6;

            if (_isArray7) {
              if (_i7 >= _iterator7.length) break;
              _ref6 = _iterator7[_i7++];
            } else {
              _i7 = _iterator7.next();
              if (_i7.done) break;
              _ref6 = _i7.value;
            }

            var node = _ref6;
            node.textContent = message;
          }
        }
      });
    }

    if ($(dropZoneBackId).length) {
      var dropzoneBack = new Dropzone(dropZoneBackId, {
        url: "/directors/upload-back-license-file.json",
        method: "POST",
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        withCredentials: false,
        acceptedFiles: '.png,.jpg,.jpeg,.gif,.bmp',
        maxFiles: 1,
        maxFilesize: 100, // MB
        addRemoveLinks: true
      });

      dropzoneBack.on("sending", function(file, xhr, formData) {
        var id = $(dropZoneBackId).closest('.director-item').find('.director-id').val();
        var email = $(dropZoneBackId).closest('.director-item').find('.email').val();
        formData.append("id", id);
        formData.append("email", email);
      });

      dropzoneBack.on("success", function(file, response) {
        var backLicenseFileImg = $(dropZoneBackId).closest('.director-item').find('.back-license-file-preview img');
        var backLicenseFileInput = $(dropZoneBackId).closest('.director-item').find('.back-license-file-preview input');
        var itemIndex = $(dropZoneBackId).closest('.director-item').data('index');

        if (backLicenseFileImg.length > 0) {
          backLicenseFileImg.attr('src', file.dataURL);
          backLicenseFileInput.val(file.dataURL);
        } else {
          var inputBackLicenseFile = '<input type="hidden" value="' + file.dataURL + '" name="directors[' + itemIndex + '][back_license_file]" class="back-license-file-input" />';
          var backLicenseHtml = '<div class="mb-3 back-license-file-preview">' + inputBackLicenseFile + '<img alt="back-license-file" src="' + file.dataURL + '" class="back-license-image img-fluid" data-label="Back Of License"></div>';
          $(dropZoneBackId).before(backLicenseHtml);
        }

        if (file.previewElement) {
          return file.previewElement.classList.add("dz-success");
        }
      });

      dropzoneBack.on("error", function(file, message) {
        if (file.previewElement) {
          file.previewElement.classList.add("dz-error");

          if (typeof message !== "String" && message.error) {
            message = message.error;
          }

          for (var _iterator7 = file.previewElement.querySelectorAll("[data-dz-errormessage]"), _isArray7 = true, _i7 = 0, _iterator7 = _isArray7 ? _iterator7 : _iterator7[Symbol.iterator]();;) {
            var _ref6;

            if (_isArray7) {
              if (_i7 >= _iterator7.length) break;
              _ref6 = _iterator7[_i7++];
            } else {
              _i7 = _iterator7.next();
              if (_i7.done) break;
              _ref6 = _i7.value;
            }

            var node = _ref6;
            node.textContent = message;
          }
        }
      });
    }
  }

  var dropZone = function(fromIndex) {
    $('.director-item').each(function (index, element) {
      if (index >= fromIndex) {
        initDropZoneItem(index);
      }
    });
  }

  var changeIdsToApplyDropzone = function(newElement) {
    var newIndex = $("#m_repeater_directors div[data-repeater-item]").length - 1;
    $(newElement).find('.dropzone-front').attr('id', 'dropzoneFront' + newIndex);
    $(newElement).find('.dropzone-back').attr('id', 'dropzoneBack' + newIndex);
    $(newElement).find('.front-license-file-preview').remove();
    $(newElement).find('.back-license-file-preview').remove();
    dropZone(newIndex);
  }

  var saveAndContinueLater = function() {
    var btn = formEl.find('[data-wizard-action="save"]');

    btn.on('click', function(e) {
      e.preventDefault();
      mApp.progress(btn);

      formEl.ajaxSubmit({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          is_save_and_continue_later: true
        },
        success: function(responseText, statusText, xhr, $form) {
          mApp.unprogress(btn);

          swal({
            "title": "",
            "text": responseText.message,
            "type": "success",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });

          var formElLink = $('#form_link_send_email');
          $('#m_wizard').hide();
          $('#save-continue-later').removeClass('m--hide');
          var form_link_url = $('#form_link').attr('data-url') + responseText.venue_application_form.save_token;
          $('#form_link').html(form_link_url);
          $('#form_link').attr('href', form_link_url);
          var send_email_url = formElLink.attr('action').replace('__token__', responseText.venue_application_form.save_token);
          formElLink.attr('action', send_email_url);
          var sendLinkBtn = $('#send_link_to_email_btn');

          // Validate
          validatorFormElLink = formElLink.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
              'email': {
                required: true,
                email: true
              }
            },

            //== Display error
            invalidHandler: function(event, validator) {
              scrollToError();

              swal({
                "title": "",
                "text": "There are some errors in your submission. Please correct them.",
                "type": "error",
                "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
              });
            }
          });

          // Send
          sendLinkBtn.on('click', function(e) {
            e.preventDefault();

            if (validatorFormElLink.form() !== true) {
              return false;
            }

            mApp.progress(sendLinkBtn);

            formElLink.ajaxSubmit({
              headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(responseText, statusText, xhr, $form) {
                mApp.unprogress(sendLinkBtn);

                swal({
                  "title": "",
                  "text": responseText.message,
                  "type": "success",
                  "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
              },
              error: function(response) {
                mApp.unprogress(sendLinkBtn);
                var data = response.responseJSON;
                var message = "There are some errors in your submission. Please correct them.";

                swal({
                  "title": "",
                  "text": message,
                  "type": "error",
                  "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
              }
            });
          });
        },
        error: function(response) {
          mApp.unprogress(btn);
          var data = response.responseJSON;
          var message = "There are some errors in your submission. Please correct them.";

          if (data.message && data.message.company_abn &&
            Array.isArray(data.message.company_abn)) {
            message = "Company ABN " + data.message.company_abn.join(" and ");
          }

          swal({
            "title": "",
            "text": message,
            "type": "error",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });
        }
      });
    });
  }

  var lockEditWizard = function() {
    $('.m-wizard__head').closest('div.col-left').remove();
    $('.venue_form_submit').remove();
    var formSubmitSignature = $('#venue_form');
    var submitSignatureBtn = formSubmitSignature.find('[data-wizard-action="submit-signature"]');

    //
    var canvas = document.getElementById('signature-pad');

    if (!canvas) {
      $('.submit_signature_wrapper').remove();
      return;
    }

    var signaturePad = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255)', // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
      penColor: 'rgb(0, 0, 0)'
    });

    // clear-signature
    var clearButton = $('#clear-signature');

    clearButton.click(function (event) {
      signaturePad.clear();
    });

    function resizeCanvas() {
      var signRatio = 2.4;
      var ratio =  Math.max(window.devicePixelRatio || 1, 1);
      var signWidth = $('.signature-pad-wrapper').width();
      var signHeight = signWidth / signRatio;
      $('.signature-pad-wrapper').height(signHeight);
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext("2d").scale(ratio, ratio);
      var tempData = signaturePad.toDataURL('image/png');

      if (tempData) {
        signaturePad.fromDataURL(tempData);
      }

      signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }

    window.addEventListener("resize", resizeCanvas);
    resizeCanvas();

    submitSignatureBtn.on('click', function(e) {
      e.preventDefault();

      if (validator.form() !== true) {
        return false;
      }

      if (signaturePad.isEmpty()) {
        swal({
          "title": "",
          "text": "Please provide a signature first.",
          "type": "error",
          "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
        });

        return false;
      }

      var dataSignaturePad = signaturePad.toDataURL('image/png');
      mApp.progress(submitSignatureBtn);

      formSubmitSignature.ajaxSubmit({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          signature_image: dataSignaturePad
        },
        success: function(responseText, statusText, xhr, $form) {
          mApp.unprogress(submitSignatureBtn);

          swal({
            "title": "",
            "text": responseText.message,
            "type": "success",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });

          window.location.href = responseText.redirect_url;
        },
        error: function(response) {
          mApp.unprogress(submitSignatureBtn);
          var data = response.responseJSON;
          var message = "There are some errors in your submission. Please correct them.";

          swal({
            "title": "",
            "text": message,
            "type": "error",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });
        }
      });
    });
  }

  var ABNButtonClick = function() {
    var btn = formEl.find('#check-abn');

    btn.on('click', function(e) {
      e.preventDefault();
      checkABN();
    });
  }

  var checkABN = function() {
    var btn = $('#check-abn');
    var companyAbn = $('#company_abn').val().replace(/\s/g,'');
    var groupParent = btn.closest('.m-form__group-sub');
    groupParent.removeClass('has-success');
    groupParent.removeClass('has-danger');
    groupParent.find('.form-control-feedback').remove();

    if (!companyAbn && $('#company_abn').valid() == false) {
      return false;
    }

    $('#company_abn').val(companyAbn);
    mApp.progress(btn);

    var ajaxData = {
      company_abn: companyAbn,
      vaf_id: $('#vaf_id').val()
    }

    return $.ajax({
      headers: {
        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      },
      method: 'POST',
      url: '/abn/abn-details',
      data: ajaxData,
      success: function(response) {
        var messageHtml = '';

        if (!response.is_exist && response.is_active) {
          messageHtml = '<div class="form-control-feedback">' + response.message + '</div>';
          $('#company_name').closest('.m-form__group').removeClass('m--hide');
          $('#company_name').val(response.response.EntityName);
          groupParent.addClass('has-success');
          groupParent.removeClass('has-danger');
        } else {
          messageHtml = '<div class="form-control-feedback">' + response.message + '</div>';
          $('#company_name').closest('.m-form__group').addClass('m--hide');
          $('#company_name').val('');
          groupParent.addClass('has-danger');
          groupParent.removeClass('has-success');
        }

        groupParent.append(messageHtml);
        mApp.unprogress(btn);
      }
    });
  }

  var initSelect2 = function() {
    // Operating Structure
    var no_search_combobox = formEl.find('.no-search-select2');

    no_search_combobox.select2({
      placeholder: "Please select",
      minimumResultsForSearch: Infinity
    });

    // Market segment
    var search_combobox = formEl.find('.search-select2');

    search_combobox.select2({
      placeholder: "Please select"
    });
  }

  return {
    // public functions
    init: function() {
      wizardEl = $('#m_wizard');
      formEl = $('#venue_form');
      initWizard();
      initValidation();
      initSubmit();
      initInputMask();
      ABNButtonClick();
      datepicker();
      repeater();
      dropZone(0);
      saveAndContinueLater();
      initSelect2();

      if (getUrlParameter('step')) {
        lockEditWizard();
      }
    }
  }
}();

$(document).ready(function() {
  Dropzone.autoDiscover = false;
  WizardVAF.init();

  // PO-126 When click next the window go to to
  $('#pencil-wizard-next-btn').click(function() {
    $('html,body').animate({ scrollTop: 0 }, 500);
  });

  // Handle invalid data when click back/next on the browser
  top.name = 'not_fire_back';

  $(window).bind("pageshow", function() {
    if (top.name === "not_fire_back") {
      // Reset venue form if exist
      if ($('#venue_form').length > 0) {
        $('#venue_form')[0].reset();
      }

      top.name = 'fire_back';
    }
  });
});
