//== Class definition
var WizardCredit = function() {
  //== Base elements
  var wizardEl = $('#m_wizard_apply_supplier_credit');
  var formEl = $('#ascf_form');
  var validator;
  var wizard;

  var isDuplicateEmail = function isDuplicateEmail() {
    var errorCounterDupInput = 0;
    var email_values = [];

    formEl.find('.email').each(function (i, el) {
      var current_val = $(el).val();

      if (current_val) {
        if (email_values.indexOf(current_val) === -1) {
          email_values.push(current_val);
        } else {
          errorCounterDupInput++;
        }
      }
    });

    if (errorCounterDupInput > 0) {
      swal({
        "title": "",
        "text": "The emails are duplicated. Please update them.",
        "type": "error",
        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
      });

      return true;
    }

    return false;
  }

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? false : sParameterName[1];
      }
    }
  };

  var scrollToError = function scrollToError() {
    var findErrorClass = setInterval(function () {
      if ($(".form-control-feedback").length > 0) {
        $('html, body').animate({
          scrollTop: $(".form-control-feedback").offset().top - 150
        }, 500);

        clearInterval(findErrorClass);
      }
    }, 500);
  }

  var showReviewStep = function() {
    var buildElement = '';
    var personal_guarantees_count = 1;
    var trade_reference_count = 1;
    var is_begin_pg = false;

    $('[id^=m_wizard_form_step_]').not('#m_wizard_form_step_review, #m_wizard_form_step_personal_guarantee').each(function() {
      var title = $(this).find('h3.m-form__heading-title').text().trim();
      buildElement += '<div class="card mb-3"><div class="card-header font-weight-bold">' +
                      title + '</div><div class="card-body pb-1">';

      $(this).find('input.form-control, select.form-control, .signature-image, ' +
        '.signed-at, .textareaTermsConditions-asvf, .no-data-message').each(function() {
        if ($(this).attr('aria-label') && $(this).attr('aria-label').toLowerCase() == 'search') {
          return;
        }

        var label = $(this).data('label') ? $(this).data('label') : '';

        if (title && title.toLowerCase() == 'trade references' && label.toLowerCase() == 'business name') {
          if (trade_reference_count > 1) {
            buildElement += '<hr/>';
          }

          buildElement += '<h5 class="card-title">Trade Reference ' + trade_reference_count + '</h5>';
          trade_reference_count++;
        }

        if (label.toLowerCase() == 'market segment') {
          var market_segment = $(this).children(':selected').text();
          buildElement += '<p>' + label + ': ' + market_segment + '</p>';
        } else {
          var value = $(this).val();

          if ($(this).is('select')) {
            switch (label.toLowerCase()) {
              case 'supplier':
                value = $('.div-supplier-selectpicker span.select2-selection__rendered').text();
                break;
              default:
                value = $(this).find("option:selected").text();
            }
          }

          if ($(this).hasClass('textareaTermsConditions-asvf')) {
            buildElement += '<p>' + $(this).html() + '</p>';
          }

          if ($(this).hasClass('no-data-message')) {
            buildElement += '<p>' + $(this).html() + '</p>';
          } else {
            buildElement += '<p>' + label + ': ' + value + '</p>';
          }
        }
      });

      buildElement += '</div></div>';
    });

    $('#m_wizard_form_step_personal_guarantee').each(function() {
      var title = $(this).find('h3.m-form__heading-title').text().trim();
      buildElement += '<div class="card mb-3"><div class="card-header font-weight-bold">' +
                      title + '</div><div class="card-body pb-1">';
      var flag = 0;

      $('.personal-guarantee-item').each(function() {
        flag++;
        var outerThis = $(this);

        if (flag > 1) {
          buildElement += '</br></br>'
        }
       
        outerThis.find('input.form-control, select.form-control, .signature-image, .signed-at').each(function() {
          var label = $(this).data('label') ? $(this).data('label') : '';

          if (label.toLowerCase() == 'signed') {
            if ($(this).parents('.pg-details')[0] && outerThis.find('.no-pg').hasClass('active') ||
                $(this).parents('.dt-details')[0] && outerThis.find('.yes-pg').hasClass('active')) {
              var src = $(this).attr('src');
              buildElement += '<p>' + label + ':</p>';
              buildElement += '<p><img class="signature-image" alt="signature-image" src="' + src + '" width="300">';
            }
          } else if (label.toLowerCase() == 'date' || label.toLowerCase() == 'time' ||
                     label.toLowerCase() == 'ip address' || label.toLowerCase() == 'geo') {
            if ($(this).parents('.pg-details')[0] && outerThis.find('.no-pg').hasClass('active') ||
            $(this).parents('.dt-details')[0] && outerThis.find('.yes-pg').hasClass('active')) {
            var signed_info = $(this).text();
            buildElement += '<p style="margin:0">' + label + ': ' + signed_info + '</p>';
            }
          } else {
            var value = $(this).val();
          }

          if ($(this).is('select')) {
            value = $(this).find("option:selected").text();
          }

          if(outerThis.find('.no-pg').hasClass('active'))
          {
            if(label.toLowerCase()== 'first name') {
              buildElement +='<h5 class="card-title">Personal Guarantee ' + flag + '</h5>';
            }
            if (outerThis.find('.m-checkbox-postal').hasClass('cb-active')) {
              switch (label.toLowerCase()) {
                case 'first name':
                case 'last name':
                case 'mobile number':
                case 'home phone':
                case 'email':
                case 'postal building/property name':
                case 'postal unit number':
                case 'postal street number':
                case 'postal street name':
                case 'postal street type':
                case 'postal suburb':
                case 'postal state':
                case 'postal postcode':
                case 'home building/property name':
                case 'home unit number':
                case 'home street number':
                case 'home street name':
                case 'home street type':
                case 'home suburb':
                case 'home state':
                case 'home postcode':
                  buildElement += '<p>' + label + ': ' + value + '</p>'; break;

                default: break;
              }
            } else {
              switch (label.toLowerCase()) {
                case 'first name':
                case 'last name':
                case 'mobile number':
                case 'home phone':
                case 'email':
                case 'home building/property name':
                case 'home unit number':
                case 'home street number':
                case 'home street name':
                case 'home street type':
                case 'home suburb':
                case 'home state':
                case 'home postcode':
                  buildElement += '<p>' + label + ': ' + value + '</p>'; break;

                default: break;
              }

              if(label.toLowerCase() == 'postal postcode')
              {
                buildElement += '<h5 class="notification small">' +"Postal address is the same as Address (Home)" + '</h5>';
              }
            }
          }

          if(outerThis.find('.yes-pg').hasClass('active'))
          {
            if(label.toLowerCase()== 'directors') {
              buildElement +='<h5 class="card-title">Personal Guarantee ' + flag + ' (Director)</h5>';
            }
            switch (label.toLowerCase()) {
              case 'director mobile number':
                buildElement += '<p>' + 'Mobile Number' + ': ' + value + '</p>'; break;

              case 'director home phone':
                buildElement += '<p>' + 'Home Phone' + ': ' + value + '</p>'; break;

              case 'director email':
                buildElement += '<p>' + 'Email' + ': ' + value + '</p>'; break;

              case 'directors':
                buildElement += '<p>' + 'Director' + ': ' + value + '</p>'; break;

              case 'building/property name':
              case 'unit number':
              case 'street number':
              case 'street name':
              case 'street type':
              case 'suburb':
              case 'state':
              case 'postcode':
                buildElement += '<p>' + label + ': ' + value + '</p>'; break;

              default: break;
            }
          }
        })
      })
    })

    $('.form-reviews').html(buildElement);
  }

  var isCreditLimitAdded = function() {
    var credit_limit_input = formEl.find('#venue_credit_limit_text')[0];

    if ($(credit_limit_input).length > 0) {
      if ($(credit_limit_input).val()) {
        var credit_limit = $(credit_limit_input).val();
        credit_limit = credit_limit.replace(/,/g, "");
        credit_limit = credit_limit.replace(/\./, "");

        if (credit_limit.length <= 15) {
          return true;
        } else {
          swal({
            "title": "",
            "text": "Requested Credit Limit is not valid. Please update it!",
            "type": "error",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });

          return false;
        }
      } else {
        swal({
          "title": "",
          "text": "Requested Credit Limit has not been added yet. Please update it!",
          "type": "error",
          "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
        });

        return false;
      }
    } else {
      return true;
    }
  }

  var isSupplierSelected = function() {
    var supplier_selected = $(".supplier_selectpicker").val()

    if (supplier_selected) {
      return true;
    } else {
      swal({
        "title": "",
        "text": "Please Choose a Supplier!",
        "type": "error",
        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
      });

      return false;
    }
  }

  var getVafDirectors = function() {
    $.ajax({
      url: '/admin/get_vaf_directors',
      type: 'GET',
      headers: { 'X-CSRF-Token': csrfToken },
      dataType: 'json',
      data: {
        vaf_id: $('.vaf_selectpicker').val(),
        trading_terms_setting_id: $('.trading_term_selectpicker').val()
      },
      success: function(data) {
        // Save data to directors
        directors = data.directors;
        required_pgs_quantity = data.required_pgs_quantity;
      },
      error: function(data) {
        console.log(data);
      }
    });
  }

  var isPGAdded = function(item) {
    var yes_pg = $(item).find('.yes-pg');
    var no_pg = $(item).find('.no-pg');

    if ($(yes_pg).hasClass("active") || $(no_pg).hasClass("active") || !$('.render-question-pg').is(":visible")) {
      return true;
    } else {
      return false;
    }
  }

  var updateSelectedDirectors = function() {
    var items = $(formEl).find('div.personal-guarantee-item[data-repeater-item]');
    selected_directors_ids = [];
    var i;

    for (i = 0; i < items.length; i++) {
      var yes_pg = $(items[i]).find('.yes-pg.active');
      var directors_select = $(items[i]).find('select.directors-select');
      var selected_director_hidden = $(items[i]).find('input.selected_director_id[type="hidden"]');
      var director_id = $(directors_select[0]).val();

      if (yes_pg.length > 0 && director_id != "" ) {
        selected_directors_ids.push(director_id);
        $(selected_director_hidden).val(director_id);
      } else {
        $(selected_director_hidden).val(" ");
      }
    }
  }

  var checkPGsQuantity = function() {
    var pg_wrapper = $(formEl).find('.render_pg_step_personal_guarantee');
    var items = $(pg_wrapper).find('div[data-repeater-item]');
    var valid = items.length >= required_pgs_quantity;
    var i;

    for (i = 0; i < items.length; i++) {
      valid = valid && isPGAdded(items[i]);
    }

    if (valid == true) {
      // update selected directors before step 7
      updateSelectedDirectors();
      return true;
    } else {
      swal({
        "title": "",
        "text": "Required Personal Guarantees quantity is " + required_pgs_quantity + ". Please update it!",
        "type": "error",
        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
      });

      return false;
    }
  }

  var showDirectorDetails = function(director_details, director) {
    $(director_details).find('input.director_firstname').val(director.firstname);
    $(director_details).find('input.director_lastname').val(director.lastname);
    $(director_details).find('input.director_mobile').val(director.mobile);
    $(director_details).find('input.director_phone').val(director.phone);
    $(director_details).find('input.director_email').val(director.email);
    // Home address
    $(director_details).find('input.director_home_property_name').val(director.property_name);
    $(director_details).find('input.director_home_unit_number').val(director.unit_number);
    $(director_details).find('input.director_home_street_number').val(director.street_number);
    $(director_details).find('input.director_home_street_name').val(director.street_name);
    $(director_details).find('input.director_home_street_type').val(director.street_type);
    $(director_details).find('input.director_home_suburb').val(director.suburb);
    $(director_details).find('input.director_home_state').val(director.state);
    $(director_details).find('input.director_home_postcode').val(director.postcode);
  }

  var clearDirectorDetails = function(director_details) {
    $(director_details).find('input.director_firstname').val('');
    $(director_details).find('input.director_lastname').val('');
    $(director_details).find('input.director_mobile').val('');
    $(director_details).find('input.director_phone').val('');
    $(director_details).find('input.director_email').val('');
    // Home address
    $(director_details).find('input.director_home_property_name').val('');
    $(director_details).find('input.director_home_unit_number').val('');
    $(director_details).find('input.director_home_street_number').val('');
    $(director_details).find('input.director_home_street_name').val('');
    $(director_details).find('input.director_home_street_type').val('');
    $(director_details).find('input.director_home_suburb').val('');
    $(director_details).find('input.director_home_state').val('');
    $(director_details).find('input.director_home_postcode').val('');
  }

  var updateDirectorsSelect = function(item) {
    var directors_combobox_wrapper = $(item).find('div.directors-select-wrapper');
    var selected_director_hidden = $(item).find('input.selected_director_id[type="hidden"]');
    var selected_director_id = $(selected_director_hidden).val();
    var director_html = '<select class="form-control m-bootstrap-select m_selectpicker required directors-select" data-label="Directors">';
    remain_directors = [];
    var i;

    for (i = 0; i < directors.length; i++) {
      if (selected_directors_ids.includes(directors[i].id) == false || directors[i].id == selected_director_id) {
        remain_directors.push(directors[i]);
      }
    }

    var selected_director = remain_directors[0];
    var selected = '';

    for (i = 0; i < remain_directors.length; i++) {
      if (remain_directors[i].id == selected_director_id) {
        selected = 'selected';
        selected_director = remain_directors[i];
      } else {
        selected = '';
      }

      director_html = director_html + '<option value="' + remain_directors[i].id + '"' + ' ' + selected + '>';
      director_html += remain_directors[i].firstname + ' ' + remain_directors[i].lastname;
      director_html += '</option>';
    }

    director_html = director_html + '</select>';
    $(directors_combobox_wrapper).empty();
    $(directors_combobox_wrapper).html(director_html);
    $('.m_selectpicker').selectpicker();

    // Show first remain director
    var director_details = $(item).find('.detail-director');

    if (remain_directors.length > 0) {
      showDirectorDetails(director_details, selected_director);
    } else {
      clearDirectorDetails(director_details);
    }
  }

  var syncDirectorsSelects = function() {
    var items = $(formEl).find('div.personal-guarantee-item[data-repeater-item]');
    var i;

    for (i = 0; i < items.length; i++) {
      updateDirectorsSelect(items[i]);
    }
  }

  var initBeforeStepPG = function() {
    updateSelectedDirectors();
    syncDirectorsSelects();
  }

  //== Private functions
  var initWizard = function () {
    var startStep = 1;

    if (getUrlParameter('step') && getUrlParameter('step') == 'review') {
      startStep = 7
    }

    //== Initialize form wizard
    wizard = wizardEl.mWizard({
      startStep: startStep
    });

    //== Validation before going to next page
    wizard.on('beforeNext', function(wizard) {
      var currentStep = $('.m-wizard__form-step--current');

      if (currentStep.attr('id') == 'm_wizard_form_step_personal_guarantee' && checkPGsQuantity() !== true) {
        return false;
      }

      if (currentStep.attr('id') == 'm_wizard_form_step_trade_references') {
        initBeforeStepPG();
      }

      if (currentStep.attr('id') == 'm_wizard_form_step_4') {
        getVafDirectors();
      }

      if (currentStep.attr('id') == 'm_wizard_form_step_3' && isCreditLimitAdded() !== true) {
        return false;
      }

      if (currentStep.attr('id') == 'm_wizard_form_step_2' && isSupplierSelected() !== true) {
        return false;
      }

      if (validator.form() !== true) {
        return false;  // don't go to the next step
      }
    });

    //== Change event (change step)
    wizard.on('change', function(wizard) {
      scrollToError();
      showReviewStep();
    });

    if (getUrlParameter('step') && getUrlParameter('step') == 'review') {
      showReviewStep();
    }
  }

  var initValidation = function() {
    validator = formEl.validate({
      //== Validate only visible fields
      ignore: ":hidden",

      //== Validation rules
      rules: {
        'personal_guarantees[0][mobile]': {
          required: true
        },
        'personal_guarantees[0][email]': {
          required: true,
          email: true
        },
        termsConditions2: {
          required: true
        }
      },

      //== Validation messages
      messages: {
        termsConditions2: {
          required: "You must accept the Terms and Conditions agreement!"
        }
      },

      //== Display error
      invalidHandler: function(event, validator) {
        scrollToError();

        swal({
          "title": "",
          "text": "There are some errors in your submission. Please correct them.",
          "type": "error",
          "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
        });
      },

      //== Submit valid form
      submitHandler: function (form) {
      }
    });

    formEl.find('.required').each(function() {
      $(this).rules('add', {
        required: true
      });
    });

    formEl.find('.email').each(function() {
      $(this).rules('add', {
        email: true
      });
    });
  }

  var initSubmit = function() {
    var btn = formEl.find('[data-wizard-action="submit"]');

    btn.on('click', function(e) {
      e.preventDefault();

      // if (isDuplicateEmail()) {
      //   return false;
      // }

      if (validator.form()) {
        mApp.progress(btn);

        formEl.ajaxSubmit({
          headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          data: {
            trading_terms_setting_id: $('.trading_term_selectpicker').val(),
            selected_directors_ids: selected_directors_ids
          },
          success: function(responseText, statusText, xhr, $form) {
            mApp.unprogress(btn);

            swal({
              "title": "",
              "text": "The apply supplier credit form has been successfully submitted!",
              "type": "success",
              "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });

            var responseText = responseText;

            setTimeout(function() {
              window.location.href = "/admin/venue_suppliers";
            }, 2000);
          },
          error: function(response) {
            mApp.unprogress(btn);
            var data = response.responseJSON;
            var message = "There are some errors in your submission. Please correct them.";

            if (data.message) {
              message = data.message
            }

            swal({
              "title": "",
              "text": message,
              "type": "error",
              "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
            });
          }
        });
      }
    });
  }

  var initInputMask = function() {
    $(".mobileAU").inputmask("mask", {
      "mask": "0499 999 999",
      "clearIncomplete": true
    });

    $(".phoneAU, .phoneNumber").inputmask("mask", {
      "mask": "(09) 9999 9999",
      "clearIncomplete": true
    });

    $(".postcodeAU").inputmask("mask", {
      "mask": "9{4}",
      "clearIncomplete": true
    });

    $(".numer10Digits").inputmask("mask", {
      "mask": "9{10}",
      "clearIncomplete": true
    });

    $(".number1To20").inputmask("mask", {
      "mask": "9{1,20}",
      "clearIncomplete": true
    });

    $(".date_picker, .date_of_birth, .license_expiry").inputmask("99/99/9999", {
      "placeholder": "dd/mm/yyyy",
      "clearIncomplete": true
    });

    $('.phoneNumber').each(function() {
      var input_mask = 0;

      $(this).on('input', function() {
        if($(this).val().substr(1,2) == "04") {
          $(this).inputmask("unmask", {
            "mask": "(09) 9999 9999",
            "clearIncomplete": true
          });

          $(this).inputmask("mask", {
            "mask": "0499 999 999",
            "clearIncomplete": true
          });
        }
      });
    });

    $(".currency").inputmask("decimal", {
      radixPoint: ".",
      autoGroup: true,
      groupSeparator: ",",
      groupSize: 3,
      removeMaskOnSubmit: !0,
    });
  }

  var initSelect2 = function() {
    // Credit limit
    var credit_limits_combobox = formEl.find('#venue_credit_limit');

    credit_limits_combobox.select2({
        placeholder: "Select a credit limit"
    });

    // Street type
    var street_type_combobox = formEl.find('.street-type-select2');

    street_type_combobox.select2({
      placeholder: "Select one",
      allowClear: true
    });

    // State
    var state_combobox = formEl.find('.state-select2');

    state_combobox.select2({
      placeholder: "Select one",
      allowClear: true,
      minimumResultsForSearch: Infinity
    });
  }

  var yesNoButtonsEvents = function(item) {
    $(item).find('.yes-pg').click(function() {
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).siblings('.no-pg').removeClass('active');
        $(this).parents("div[data-repeater-item]").find(".pg-details").hide();
        $(this).parents("div[data-repeater-item]").find(".dt-details").show();
        updateSelectedDirectors();
        syncDirectorsSelects();
      }
    });

    $(item).find('.no-pg').click(function() {
      if (!$(this).hasClass('active')) {
        $(this).addClass('active');
        $(this).siblings('.yes-pg').removeClass('active');
        $(this).parents('div[data-repeater-item]').find('.pg-details').show();
        $(this).parents('div[data-repeater-item]').find('.dt-details').hide();
        updateSelectedDirectors();
        syncDirectorsSelects();
      }
    });

    $(item).find('.m-checkbox-postal').click(function() {
      $actionOnclickCheckPostal = $(this)
        .parents('.m-checkbox-postal-div')
        .siblings('.pa-details');

      if (!$(this).hasClass('cb-active')) {
        $(this).addClass('cb-active');
        $actionOnclickCheckPostal.removeClass('pa-deactive');
        $actionOnclickCheckPostal.show();
      } else if ($(this).hasClass('cb-active')) {
        $(this).removeClass('cb-active');
        $actionOnclickCheckPostal.hide();
      }
    });
  }

  var directorDetails = function() {
    $(document).on('changed.bs.select', '.directors-select', function(e) {
      objects = e.target.selectedOptions;
      directors_id = [];

      for (let option of e.target.selectedOptions) {
        directors_id.push(option.value);
      }

      var director_details = $(this).parents('.list-directors').find('.detail-director');

      $.ajax({
        url: '/admin/get_director_details',
        type: 'GET',
        headers: { 'X-CSRF-Token': csrfToken },
        dataType: 'json',
        data: {
          director_id: directors_id[0],
        },
        success: function(data) {
          director = data.director;
          showDirectorDetails(director_details, director);
          updateSelectedDirectors();
          syncDirectorsSelects();
        },
        error: function(data) {
          console.log(data);
        }
      });
    });
  }

  var syncAfterDeletePGItem = function(item) {
    var item_id = $(item).data('index');
    var items = $(formEl).find('div.personal-guarantee-item[data-repeater-item]');
    selected_directors_ids = [];
    var i;

    // update selected directors ids
    for (i = 0; i < items.length; i++) {
      if ($(items[i]).data('index') == item_id) {
        break;
      }

      var yes_pg = $(items[i]).find('.yes-pg.active');
      var directors_select = $(items[i]).find('select.directors-select');
      var selected_director_hidden = $(items[i]).find('input.selected_director_id[type="hidden"]');
      var director_id = $(directors_select[0]).val();

      if (yes_pg.length > 0 && director_id != "" ) {
        selected_directors_ids.push(director_id);
        $(selected_director_hidden).val(director_id);
      } else {
        $(selected_director_hidden).val(" ");
      }
    }

    // sync directors selects
    for (i = 0; i < items.length; i++) {
      if ($(items[i]).data('index') == item_id) {
        break;
      }

      updateDirectorsSelect(items[i]);
    }
  }

  var updateItemIndex = function(item) {
    var newIndex = $(item).parent('div').find('div[data-repeater-item]').length - 1;
    $(item).data('index', newIndex);
    $(item).find('.data-repeater-item-index').text(newIndex + 1);
  }

  var updateItemsIndexAfterDeleteItem = function(item) {
    var item_id = $(item).data('index');
    var items = $(formEl).find('div.personal-guarantee-item[data-repeater-item]');
    var new_items = [];
    var i;

    for (i = 0; i < items.length; i++) {
      if ($(items[i]).data('index') != item_id) {
        new_items.push(items[i]);
      }
    }

    for (i = 0; i < new_items.length; i++) {
      $(new_items[i]).data('index', i);
      $(new_items[i]).find('.data-repeater-item-index').text(i + 1);
    }
  }

  var initItem = function(item) {
    var yes_pg = $(item).find('.yes-pg');
    var no_pg = $(item).find('.no-pg');
    $(yes_pg).removeClass('active');
    $(no_pg).removeClass('active');
    $(item).find('.pg-details').hide();
    $(item).find('.dt-details').hide();
  }

  var repeater = function() {
    var repeater = $('#m_repeater_personal_guarantees').repeater({
      initEmpty: false,
      show: function () {
        $(this).slideDown();
        updateItemIndex(this);
        initInputMask();
        initSelect2();
        initItem(this);
        yesNoButtonsEvents(this);
        updateSelectedDirectors();
        directorDetails();
        syncDirectorsSelects();
      },
      //isFirstItemUndeletable: true,
      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
        syncAfterDeletePGItem(this);
        updateItemsIndexAfterDeleteItem(this);
      }
    });

    var repeaterTradeReferences = $('#m_repeater_trade_references').repeater({
      initEmpty: false,
      show: function () {
        $(this).slideDown();
        updateItemIndex(this);
        initValidation();
        initInputMask();
      },
      isFirstItemUndeletable: true,
      hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
      }
    });
  }

  var saveAndContinueLater = function() {
    var btn = formEl.find('[data-wizard-action="save"]');

    btn.on('click', function(e) {
      e.preventDefault();
      mApp.progress(btn);

      formEl.ajaxSubmit({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          trading_terms_setting_id: $('.trading_term_selectpicker').val(),
          selected_directors_ids: selected_directors_ids,
          is_save_and_continue: true
        },
        success: function(responseText, statusText, xhr, $form) {
          mApp.unprogress(btn);

          swal({
            "title": "",
            "text": "The apply supplier credit form has been successfully submitted!",
            "type": "success",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });

          var responseText = responseText;

          setTimeout(function() {
            window.location.href = "/admin/venue_suppliers";
          }, 2000);
        },
        error: function(response) {
          mApp.unprogress(btn);
          var data = response.responseJSON;
          var message = "There are some errors in your submission. Please correct them.";

          swal({
            "title": "",
            "text": message,
            "type": "error",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });
        }
      });
    });
  }

  var lockEditWizard = function() {
    $('.m-wizard__head').closest('div.col-left').remove();
    $('.ascf_form_submit').remove();
    var formSubmitSignature = $('#ascf_form');
    var submitSignatureBtn = formSubmitSignature.find('[data-wizard-action="submit-signature"]');

    //
    var canvas = document.getElementById('signature-pad');

    if (!canvas) {
      $('.submit_signature_wrapper').remove();
      return;
    }

    var signaturePad = new SignaturePad(canvas, {
      backgroundColor: 'rgb(255, 255, 255)', // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
      penColor: 'rgb(0, 0, 0)'
    });

    // clear-signature
    var clearButton = $('#clear-signature');

    clearButton.click(function (event) {
      signaturePad.clear();
    });

    function resizeCanvas() {
      var signRatio = 2.4;
      var ratio =  Math.max(window.devicePixelRatio || 1, 1);
      var signWidth = $('.signature-pad-wrapper').width();
      var signHeight = signWidth / signRatio;
      $('.signature-pad-wrapper').height(signHeight);
      canvas.width = canvas.offsetWidth * ratio;
      canvas.height = canvas.offsetHeight * ratio;
      canvas.getContext("2d").scale(ratio, ratio);
      var tempData = signaturePad.toDataURL('image/png');

      if (tempData) {
        signaturePad.fromDataURL(tempData);
      }

      signaturePad.clear(); // otherwise isEmpty() might return incorrect value
    }

    window.addEventListener("resize", resizeCanvas);
    resizeCanvas();

    submitSignatureBtn.on('click', function(e) {
      e.preventDefault();

      if (validator.form() !== true) {
        return false;
      }

      if (signaturePad.isEmpty()) {
        swal({
          "title": "",
          "text": "Please provide a signature first.",
          "type": "error",
          "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
        });

        return false;
      }

      var dataSignaturePad = signaturePad.toDataURL('image/png');
      mApp.progress(submitSignatureBtn);

      formSubmitSignature.ajaxSubmit({
        headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          signature_image: dataSignaturePad
        },
        success: function(responseText, statusText, xhr, $form) {
          mApp.unprogress(submitSignatureBtn);

          swal({
            "title": "",
            "text": responseText.message,
            "type": "success",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });

          window.location.href = responseText.redirect_url;
        },
        error: function(response) {
          mApp.unprogress(submitSignatureBtn);
          var data = response.responseJSON;
          var message = "There are some errors in your submission. Please correct them.";

          swal({
            "title": "",
            "text": message,
            "type": "error",
            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
          });
        }
      });
    });
  }

  return {
    // public functions
    init: function() {
      wizardEl = $('#m_wizard_apply_supplier_credit');
      formEl = $('#ascf_form');
      initWizard();
      initValidation();
      initSubmit();
      saveAndContinueLater();
      initInputMask();
      repeater();
      updateItemIndex();

      // Init for each pg items
      $('.personal-guarantee-item').each(function(index) {
        yesNoButtonsEvents(this);
      });

      initSelect2();

      if (getUrlParameter('step')) {
        lockEditWizard();
      }
    },
    inputMask: function() {
      initInputMask();
    },
    initValidation: function() {
      initValidation();
    },
    repeater: function() {
      repeater();
    },
    yesNoButtonsEvents: function() {
      // Init for each pg items
      $('.personal-guarantee-item').each(function(index) {
        yesNoButtonsEvents(this);
      });
    },
    updateSelectedDirectors: function() {
      updateSelectedDirectors();
    },
    directorDetails: function() {
      directorDetails();
    }
  };
}();

jQuery(document).ready(function() {
  WizardCredit.init();
  WizardCredit.updateSelectedDirectors();
  WizardCredit.directorDetails();

  $('[data-wizard-action="next"]').click(function() {
    $('html,body').animate({ scrollTop: $("#credit-form").offset().top }, 500);
  });
});
