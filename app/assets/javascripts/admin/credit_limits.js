$(document).ready(function() {
  // Init credit limits table
  if ($('#credit_limits_table').length > 0) {
    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },
        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: true,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:50,
          sortable:!1
        },
        {
          field:"Credit Limit",
          title:"Credit Limit",
          width:200,
          sortable:!1
        },
        {
          field:"Increments",
          title:"Increments",
          width:100,
          sortable:!1
        },
        {
          field:"Market Segment",
          title:"Market Segment",
          width:150,
          sortable:!1
        },
        {
          field:"Trading Term",
          title:"Trading Term",
          width:124,
          sortable:!1
        },
        {
          field:"Actions",
          title:"Actions",
          width:70,
          sortable:!1
        },
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },
        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },
              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    $('#credit_limits_table').mDatatable(options);
  }
  // End of init credit limits table

  // Currency input mask
  $(".currency").inputmask("decimal", {
    radixPoint: ".",
    autoGroup: true,
    groupSeparator: ",",
    groupSize: 3,
    removeMaskOnSubmit: !0
  });

  // Validate new credit limit form
  if ($("#new_credit_limit_form").length > 0) {
    $("#new_credit_limit_form").validate({
      // define validation rules
      rules: {
          'credit_limit[credit_limit]': {
              required: true
          },
          'credit_limit[increments]': {
              required: true
          },
          'credit_limit[market_segment_id]': {
              required: true
          },
          'credit_limit[trading_terms_setting_id]': {
              required: true
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          //var alert = $('#update_admin_user_form_msg');
          //alert.removeClass('m--hide').show();
          //mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
          form[0].submit(); // submit the form
        }
    });
  }
  // End of validate new credit limit form

  // Select2 for select
  $('#market_segments_select').select2({
      placeholder: "Select a market segment"
  });

   $('#trading_terms_select').select2({
      placeholder: "Select a trading term",
      minimumResultsForSearch: Infinity
  });
  // End of init Select2
});
