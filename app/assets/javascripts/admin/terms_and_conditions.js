var MarketSegmentsSelect = function () {
  //== Private functions
  var initMarketSegmentSelect2 = function() {
    $('.market_segments_sp, .trading_terms_setting_sp').select2({
      placeholder: "Please select"
    });

    $('.trading_terms_setting_sp').select2({
      placeholder: "Please select",
      minimumResultsForSearch: Infinity
    });
  }

  return {
    // public functions
    init: function() {
      initMarketSegmentSelect2();
    }
  };
}();

jQuery(document).ready(function() {
  MarketSegmentsSelect.init();
});

$(document).ready(function() {
  if ($("#new_supplier_terms_condition_form").length > 0) {
    $("#new_supplier_terms_condition_form").validate({
      // display error alert on form submit
      invalidHandler: function(event, validator) {
        //var alert = $('#update_admin_user_form_msg');
        //alert.removeClass('m--hide').show();
        //mApp.scrollTo(alert, -200);
      },

      submitHandler: function (form) {
        $('#content').val($('.note-editable').html());
        form[0].submit(); // submit the form
      }
    });

    $('.summernote').summernote({
      height: 300
    });
    //== Class definition
  }

  if ($('#terms_and_condition_table').length > 0) {
    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },

        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: false,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:34,
          sortable:!1
        },
        {
          field:"Market Segment",
          title:"Market Segment",
          width:124,
          sortable:!1
        },
        {
          field:"Trading Terms",
          title:"Trading Terms",
          width:124,
          sortable:!1
        },
        {
          field:"Terms & Conditions",
          title:"Terms & Conditions",
          width:440,
          sortable:!1
        },
        {
          field:"Active",
          title:"Active",
          width:50,
          sortable:!1
        },
        {
          field:"Actions",
          title:"Actions",
          width:84,
          sortable:!1
        }
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },

              tablet: {
                layout: 'default',
                pagesNumber: 3
              },

              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },

        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },

              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    $('#terms_and_condition_table').mDatatable(options);
  }
});
