var changeMask = false;
var supplierDetailsTable, t;

$(document).ready(function() {
  //admin
  $('#contact_number').on('input', function() {
    if(($("#contact_number").val().substr(0,2) == "04") && changeMask == false){
      $("#contact_number").inputmask("0499 999 999",{
        "clearIncomplete": true,
        removeMaskOnSubmit: !0
      });
      changeMask = true;
    }
  });

  if ($('#suppliers_table').length > 0) {

    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },
        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: true,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:34,
          sortable:!1
        },
        {
          field:"Venue Trading Name",
          title:"Venue Trading Name",
          width:134,
          sortable:!1
        },
        {
          field:"Supplier Trading Name",
          title:"Supplier Trading Name",
          width:134,
          sortable:!1
        },
        {
          field:"Contact Name",
          title:"Contact Name",
          width:134,
          sortable:!1
        },
        {
          field:"ABN",
          title:"ABN",
          width:124,
          sortable:!1
        },
        {
          field:"Contact Email",
          title:"Contact Email",
          width:184,
          sortable:!1
        },
        {
          field:"Contact number",
          title:"Contact number",
          width:120,
          sortable:!1
        },
        {
          field:"Contact Address",
          title:"Contact Address",
          width:184,
          sortable:!1
        },
        {
          field:"Trading Term",
          title:"Trading Term",
          width:124,
          sortable:!1
        },
        {
          field:"Credit Limit",
          title:"Credit Limit",
          width:84,
          sortable:!1
        },
        {
          field:"Personal Guarantees Signed",
          title:"Personal Guarantees Signed",
          width:94,
          sortable:!1
        },
        {
          field:"Active",
          title:"Active",
          width:50,
          sortable:!1
        },
        {
          field:"Created At",
          title:"Created At",
          width:84,
          sortable:!1
        },
        {
          field:"Status",
          title:"Status",
          width:84,
          sortable:!1
        },
        {
          field:"Actions",
          title:"Actions",
          width:184,
          sortable:!1
        },
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },
        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },
              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    supplierDetailsTable = $('#suppliers_table').mDatatable(options);
    t = supplierDetailsTable.getDataSourceQuery();

    $('#m_list_credit_form_status').on('change', function() {
      var t = supplierDetailsTable.getDataSourceQuery();
      t.Status = $(this).val().toLowerCase();
      supplierDetailsTable.setDataSourceQuery(t);
        supplierDetailsTable.load();
    }).val(void 0 !== t.Status ? t.Status : '');

    $('#m_list_pg_sign').on('change', function() {
      var t = supplierDetailsTable.getDataSourceQuery();
      t['Personal Guarantees Signed'] = $(this).val().toLowerCase();
        supplierDetailsTable.setDataSourceQuery(t);
        supplierDetailsTable.load();
    }).val(void 0 !== t['Personal Guarantees Signed'] ? t['Personal Guarantees Signed'] : '');

    $('#m_list_credit_form_status, #m_list_pg_sign').select2({
        minimumResultsForSearch: Infinity
    });

    //seclection all
    // $(datatable.tableHead).find('.m-checkbox--all > [type="checkbox"]').click(function(e) {
    //   // clear selected and unselected rows
    //   Extension.selectedRows = Extension.unselectedRows = [];
    //   datatable.stateRemove('checkbox');

    //   // select all rows
    //   if ($(this).is(':checked')) {
    //     Extension.selectedAllRows = true;
    //   } else {
    //     Extension.selectedAllRows = false;
    //   }

    //   // local select all current page rows
    //   if (!options.vars.requestIds) {
    //     if ($(this).is(':checked')) {
    //       Extension.selectedRows = $.makeArray($(datatable.tableBody).find('.m-checkbox--single > [type="checkbox"]').
    //         map(function(i, chk) {
    //           return $(chk).val();
    //         }));
    //     }
    //     var storage = {};
    //     storage['selectedRows'] = $.unique(Extension.selectedRows);
    //     datatable.stateKeep('checkbox', storage);
    //   }

    //   // keep selectedAllRows in datasource params
    //   datatable.setDataSourceParam(options.vars.selectedAllRows, Extension.selectedAllRows);

    //   $(datatable).trigger('m-datatable--on-click-checkbox', [$(this)]);
    // });

    // // single row checkbox click
    // $(datatable.tableBody).find('.m-checkbox--single > [type="checkbox"]').click(function(e) {
    //   var id = $(this).val();
    //   if ($(this).is(':checked')) {
    //     Extension.selectedRows.push(id);
    //     // remove from unselected rows
    //     Extension.unselectedRows = Extension.remove(Extension.unselectedRows, id);
    //   } else {
    //     Extension.unselectedRows.push(id);
    //     // remove from selected rows
    //     Extension.selectedRows = Extension.remove(Extension.selectedRows, id);
    //   }

    //   // local checkbox header check
    //   if (!options.vars.requestIds && Extension.selectedRows.length < 1) {
    //     // remove select all checkbox, if there is no checked checkbox left
    //     $(datatable.tableHead).find('.m-checkbox--all > [type="checkbox"]').prop('checked', false);
    //   }

    //   var storage = {};
    //   storage['selectedRows'] = $.unique(Extension.selectedRows);
    //   storage['unselectedRows'] = $.unique(Extension.unselectedRows);
    //   datatable.stateKeep('checkbox', storage);

    //   $(datatable).trigger('m-datatable--on-click-checkbox', [$(this)]);
    // });
  }

  if ($("#new_supplier_form").length > 0) {
    $("#contact_number").inputmask("99 9999 9999",{
      "clearIncomplete": true,
      removeMaskOnSubmit: !0
    });
    $("#abn").inputmask("mask", {
      "mask": "9{11}",
      "clearIncomplete": true,
      removeMaskOnSubmit: !0
    });


    $("#new_supplier_form").validate({
      // define validation rules
      rules: {
        'supplier[name]': {
          required: true
        },
        'supplier[email]': {
          email: true
        },
        'supplier[category]': {
          required: true
        },
        'supplier[abn]': {
        },
        'supplier[contact_number]':{
        }
      },


      // display error alert on form submit
      invalidHandler: function(event, validator) {
        var alert = $('#m_form_1_msg');
        alert.removeClass('m--hide').show();
        mApp.scrollTo(alert, -200);
      },
    });
  }

  if ($("#new_my_terms_and_conditions_form").length > 0) {
    $("#new_my_terms_and_conditions_form").validate({
      submitHandler: function (form) {
        $('#terms_and_conditions').val($('.note-editable').html())
        form[0].submit(); // submit the form
      }
    })
    $('.summernote').summernote({
      height: 300,
      width: 600,
    });
  }
});



