$(document).ready(function() {
  $('.password').val('');

  // Input mask
  $(".mobileAU").inputmask("mask", {
    "mask": "0499 999 999",
    "clearIncomplete": true
  });

  $(".phoneAU").inputmask("mask", {
    "mask": "(09) 9999 9999",
    "clearIncomplete": true
  });

  $(".date_picker").inputmask("99/99/9999", {
    "placeholder": "dd/mm/yyyy",
    "clearIncomplete": true
  });
  // End input mask

  // Validate update_admin_user_form
  if ($("#update_admin_user_form").length > 0) {
    $("#update_admin_user_form").validate({
      // define validation rules
      rules: {
          'user[name]': {
              required: true
          },
          'user[firstname]': {
              required: true
          },
          'user[lastname]': {
              required: true
          },
          'user[email]': {
              required: true,
              email: true,
              minlength: 10
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          //var alert = $('#update_admin_user_form_msg');
          //alert.removeClass('m--hide').show();
          //mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
          form[0].submit(); // submit the form
        }
    });

    $('#update_admin_user_form #password').on('input', function() {
      $('#update_admin_user_form #password_confirmation').rules('add', {
        required: true
      });
    });
  }

  if ($("#update_admin_director_form").length > 0) {
    $("#update_admin_director_form").validate({
      // define validation rules
      rules: {
          'user[firstname]': {
              required: true
          },
          'user[lastname]': {
              required: true
          },
          'user[date_of_birth]': {
              required: true
          },
          'user[email]': {
              required: true,
              email: true,
              minlength: 10
          },
          'user[mobile]': {
              required: true
          },
          'user[drivers_license_number]': {
              required: true
          },
          'user[license_expiry]': {
              required: true
          },
          'user[address_line_1]': {
              required: true
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          //var alert = $('#update_admin_user_form_msg');
          //alert.removeClass('m--hide').show();
          //mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
          form[0].submit(); // submit the form
        }
    });

    $('#update_admin_director_form #password').on('input', function() {
      $('#update_admin_director_form #password_confirmation').rules('add', {
        required: true
      });
    });
  }

  if ($("#update_admin_personal_guarantee_form").length > 0) {
    $("#update_admin_personal_guarantee_form").validate({
      // define validation rules
      rules: {
          'user[firstname]': {
              required: true
          },
          'user[lastname]': {
              required: true
          },
          'user[email]': {
              required: true,
              email: true,
              minlength: 10
          },
          'user[mobile]': {
              required: true
          },
          'user[home_address_line_1]': {
              required: true
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          //var alert = $('#update_admin_user_form_msg');
          //alert.removeClass('m--hide').show();
          //mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
          form[0].submit(); // submit the form
        }
    });

    $('#update_admin_personal_guarantee_form #password').on('input', function() {
      $('#update_admin_personal_guarantee_form #password_confirmation').rules('add', {
        required: true
      });
    });
  }

  // Crop image
  var cropPicture, updateCrop;

  cropPicture = function() {
    return $("#cropbox").Jcrop({
      onChange: updateCrop,
      onSelect: updateCrop,
      setSelect: [0, 0, 500, 500],
      aspectRatio: 1
    });
  };

  updateCrop = function(coords) {
    var avatarHeight, avatarWidth, rx, ry;
    rx = 100 / coords.w;
    ry = 100 / coords.h;
    avatarWidth = parseInt(document.getElementById('avatar_width').value);
    avatarHeight = parseInt(document.getElementById('avatar_height').value);

    $('#preview').css({
      width: Math.round(rx * avatarWidth) + 'px',
      height: Math.round(ry * avatarHeight) + 'px',
      marginLeft: '-' + Math.round(rx * coords.x) + 'px',
      marginTop: '-' + Math.round(ry * coords.y) + 'px'
    });

    $('#crop_x').val(coords.x);
    $('#crop_y').val(coords.y);
    $('#crop_w').val(coords.w);
    $('#crop_h').val(coords.h);
    return true;
  };

  cropPicture();
});
