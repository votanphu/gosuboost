$(document).ready(function() {
  if ($("#pencil_terms_condition_form").length > 0) {
    $("#pencil_terms_condition_form").validate({
      submitHandler: function (form) {
        $('#content').val($('.note-editable').html());

        form.submit(); // submit the form
      }
    })

    $('.summernote').summernote({
      height: 300
    });
    //== Class definition
  }
})
