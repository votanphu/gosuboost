//== Class definition
var AdminVenues = function() {
  //== Private functions
  var venueDetailsTable;

  var venueDetails = function() {
    if ($('#venue_details_table').length > 0) {
      var options = {
        data: {
          type: 'local',
          pageSize: 10,
          saveState: {
            cookie: true,
            webstorage: true
          },
          serverPaging: false,
          serverFiltering: false,
          serverSorting: false
        },

        layout: {
          theme: 'default',
          class: 'm-datatable--brand',
          scroll: false,
          height: null,
          footer: false,
          header: true,

          smoothScroll: {
            scrollbarShown: true
          },

          spinner: {
            overlayColor: '#000000',
            opacity: 0,
            type: 'loader',
            state: 'brand',
            message: true
          },

          icons: {
            sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
            pagination: {
              next: 'la la-angle-right',
              prev: 'la la-angle-left',
              first: 'la la-angle-double-left',
              last: 'la la-angle-double-right',
              more: 'la la-ellipsis-h'
            },
            rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
          }
        },

        sortable: false,

        pagination: true,

        search: {
          // enable trigger search by keyup enter
          onEnter: false,
          // input text for search
          input: $('#generalSearch'),
          // search delay in milliseconds
          delay: 400,
        },

        rows: {
          callback: function() {},
          // auto hide columns, if rows overflow. work on non locked columns
          autoHide: false,
        },

        columns:[
          {
            field:"#",
            title:"No",
            width:34,
            sortable:!1
          },
          {
            field:"Company Name",
            title:"Company Name",
            width:134,
            sortable:!1
          },
          {
            field:"ABN",
            title:"ABN",
            width:104,
            sortable:!1
          },
          {
            field:"Trading Address",
            title:"Trading Address",
            width:184,
            sortable:!1
          },
          {
            field:"Directors Signed",
            title:"Directors Signed",
            width:78,
            sortable:!1
          },
          {
            field:"Directors have been bankrupt",
            title:"Directors have been bankrupt",
            width:78,
            sortable:!1
          },
          {
            field:"Supplier",
            title:"Supplier",
            width:114,
            sortable:!1
          },
          {
            field:"Trading Term",
            title:"Trading Term",
            width:124,
            sortable:!1
          },
          {
            field:"Credit Limit",
            title:"Credit Limit",
            width:84,
            sortable:!1
          },
          {
            field:"Personal Guarantees Signed",
            title:"Personal Guarantees Signed",
            width:94,
            sortable:!1
          },
          {
            field:"Created By",
            title:"Created By",
            width:184,
            sortable:!1
          },
          {
            field:"Created At",
            title:"Created At",
            width:84,
            sortable:!1
          },
          {
            field:"Status",
            title:"Status",
            width:104,
            sortable:!1
          },
          {
            field:"Actions",
            title:"Actions",
            width:144,
            sortable:!1
          },
        ],

        toolbar: {
          layout: ['pagination', 'info'],

          placement: ['bottom'],  //'top', 'bottom'

          items: {
            pagination: {
              type: 'default',

              pages: {
                desktop: {
                  layout: 'default',
                  pagesNumber: 6
                },
                tablet: {
                  layout: 'default',
                  pagesNumber: 3
                },
                mobile: {
                  layout: 'compact'
                }
              },

              navigation: {
                prev: true,
                next: true,
                first: true,
                last: true
              },

              pageSizeSelect: [10, 20, 30, 50, 100]
            },

            info: true
          }
        },

        translate: {
          records: {
            processing: 'Please wait...',
            noRecords: 'No records found'
          },
          toolbar: {
            pagination: {
              items: {
                default: {
                  first: 'First',
                  prev: 'Previous',
                  next: 'Next',
                  last: 'Last',
                  more: 'More pages',
                  input: 'Page number',
                  select: 'Select page size'
                },
                info: 'Displaying {{start}} - {{end}} of {{total}} records'
              }
            }
          }
        }
      }

      // Init venue details table
      venueDetailsTable = $('#venue_details_table').mDatatable(options);

      // Filter venues
      t = venueDetailsTable.getDataSourceQuery();

      $('#m_list_credit_form_status').on('change', function() {
        var t = venueDetailsTable.getDataSourceQuery();
        t.Status = $(this).val().toLowerCase();
          venueDetailsTable.setDataSourceQuery(t);
          venueDetailsTable.load();
      }).val(void 0 !== t.Status ? t.Status : '');

      $('#m_list_ds_sign').on('change', function() {
        var t = venueDetailsTable.getDataSourceQuery();
        t['Directors Signed'] = $(this).val().toLowerCase();
          venueDetailsTable.setDataSourceQuery(t);
          venueDetailsTable.load();
      }).val(void 0 !== t['Directors Signed'] ? t['Directors Signed'] : '');

      $('#m_list_credit_form_status, #m_list_ds_sign').select2({
        minimumResultsForSearch: Infinity
      });
      // End filter venues
    }
  }

  var importVenues = function() {
    $('#import_venues_modal').on('shown.bs.modal', function () {
      if ($("#import_venues_form #apply_credit_only").is(":checked")) {
        $("#import_venues_form #apply_credit_file").rules('add', {
          required: true,
          extension: "csv"
        });

        $("#import_venues_form #venues_file").rules('add', {
          required: true,
          extension: "csv"
        });
      }

      if ($("#import_venues_form #venue_only").is(":checked")) {
        $("#import_venues_form #venues_file").rules('add', {
          required: true,
          extension: "csv"
        });

        $("#import_venues_form #apply_credit_file").rules('add', {
          required: false
        });
      }
    });

    // Validate import venue form
    if ($("#import_venues_form").length > 0) {
      $("#import_venues_form").validate({
        // define validation rules
        rules: {
          'venues[venues_file]': {
            required: true,
            extension: "csv"
          },
          'venues[apply_credit_file]': {
            required: true,
            extension: "csv"
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          //var alert = $('#update_admin_user_form_msg');
          //alert.removeClass('m--hide').show();
          //mApp.scrollTo(alert, -200);
        },

        submitHandler: function (form) {
          // Add loading icon to import button
          var import_btn = $(form).find('button[type="submit"]');
          mApp.progress(import_btn);
          // submit the form
          form[0].submit();
        }
      });

      $("#import_venues_form #all_option").on("change", function() {
        if ($(this).is(":checked")) {
          $("#import_venues_form #venues_file").rules('add', {
            required: true,
            extension: "csv"
          });

          $("#import_venues_form #apply_credit_file").rules('add', {
            required: true,
            extension: "csv"
          });
        }
      });

      $("#import_venues_form #venue_only").on("change", function() {
        if ($(this).is(":checked")) {
          $("#import_venues_form #venues_file").rules('add', {
            required: true,
            extension: "csv"
          });

          $("#import_venues_form #apply_credit_file").rules('add', {
            required: false
          });
        }
      });

      $("#import_venues_form #apply_credit_only").on("change", function() {
        if ($(this).is(":checked")) {
          $("#import_venues_form #apply_credit_file").rules('add', {
            required: true,
            extension: "csv"
          });

          $("#import_venues_form #venues_file").rules('add', {
            required: false
          });
        }
      });
    }
  }

  var exportVenues = function() {
    // Use Select2 to show Supplier list
    $('#export_venues_modal').on('shown.bs.modal', function () {
      $('#export_venues_modal #export_suppliers_select').select2({
        placeholder: "Select a supplier"
      });
    });
  }

  var sendEmail = function() {
    $(".send_email_modal").on('shown.bs.modal', function () {
      var send_email_form = $(this).find("form");
      var submit_btn = $(this).find("button[type='submit']");
      var radio_btn = $(this).find("input[type='radio']");

      radio_btn.on('click', function() {
        submit_btn.removeAttr("disabled");
      });

      // Input mask
      $(".mobileAU").inputmask("mask", {
        "mask": "0499 999 999",
        "clearIncomplete": true,
        removeMaskOnSubmit: !0
      });

      // Init select 2
      $('.director-mail-select2').select2({
        placeholder: "Please select a director",
        minimumResultsForSearch: Infinity
      });

      $('.pg-mail-select2').select2({
        placeholder: "Please select a personal guarantee",
        minimumResultsForSearch: Infinity
      });
    });
  }

  var rejectCreditLimit = function() {
    $(".reject_credit_limit").on('shown.bs.modal', function () {
      // Input mask
      $(".currency").inputmask("decimal", {
        radixPoint: ".",
        autoGroup: true,
        groupSeparator: ",",
        groupSize: 3,
        removeMaskOnSubmit: !0
      });
    });
  }

  //== Public functions
  return {
    init: function() {
      venueDetails();
      importVenues();
      exportVenues();
      sendEmail();
      rejectCreditLimit();
    }
  };
}();

//== Initialization
$(document).ready(function() {
  AdminVenues.init();
});
