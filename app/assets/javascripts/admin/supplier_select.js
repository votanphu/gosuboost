var csrfToken = document.querySelector('meta[name=csrf-token]').content;
var directors = [];
var selected_directors_ids = [];
var remain_directors = [];
var required_pgs_quantity = 0;
var directors_id = [];

//== Class definition
var VafSelectStep = function() {
  // Private functions
  var selectVaf = function() {
    // select venue application form
    $(document).on('change', '.vaf_selectpicker', function(evt) {
      $.ajax({
        url: '/admin/set_vaf',
        type: 'GET',
        headers: { 'X-CSRF-Token': csrfToken },
        dataType: 'script',
        data: {
          vaf_id: $('.vaf_selectpicker').val()
        }
      });
    });
  }

  return {
    // Public functions
    init: function() {
      selectVaf();
    }
  };
}();

var SupplierSelectStep = function() {
  //== Private functions
  var formatSupplier = function(supplier) {
    var markup = "<div>" + supplier.company_name + "</div>";

    return markup;
  }

  var formatSupplierSelection = function(supplier) {
    return supplier.company_name;
  }

  var initSelectPicker = function() {
    // minimum setup
    $('.m_selectpicker').selectpicker();

    // Suppliers 4 characters search
    $('.supplier_selectpicker').select2({
      placeholder:"Please select a supplier",
      ajax: {
          url: "/admin/search_suppliers",
          dataType: 'json',
          delay: 500, // deplay before issuing ajax request
          data: function(params) {
              return {
                  q: params.term, // search term
                  page: params.page,
                  vaf_id: $('.vaf_selectpicker').val()
              };
          },
          processResults: function(data, params) {
              params.page = params.page || 1;

              return {
                  results: data.suppliers,
                  pagination: {
                    more: (params.page * 10) < data.total_count
                  }
              };
          },
          cache: true
      },
      escapeMarkup: function(markup) {
          return markup;
      }, // let our custom formatter work
      minimumInputLength: 4,
      templateResult: formatSupplier,
      templateSelection: formatSupplierSelection
    });

    if ($('.div-supplier-selectpicker .select2-selection__placeholder').text() == "") {
      $('.div-supplier-selectpicker .select2-selection__placeholder').text("Please select a supplier");
    }
    // End search suppliers
  }

  var selectSupplier = function() {
    // select supplier
    $(document).on('change', '.supplier_selectpicker', function(evt) {
      $.ajax({
        url: '/admin/set_supplier',
        type: 'GET',
        headers: { 'X-CSRF-Token': csrfToken },
        dataType: 'script',
        data: {
          supplier_id: $('.supplier_selectpicker').val(),
          venue_application_form_id: $('.vaf_selectpicker').val(),
          ascf_id: $('#ascf_id').val()
        }
      });
    });
  }

  return {
    // public functions
    init: function() {
      initSelectPicker();
      selectSupplier();
    }
  };
}();

var TradingTermSelectStep = function() {
  // Private functions
  var selectTradingTerm = function() {
    // select trading term
    $(document).on('change', '.trading_term_selectpicker', function(evt) {
      $.ajax({
        url: '/admin/set_trading_term_setting',
        type: 'GET',
        headers: { 'X-CSRF-Token': csrfToken },
        dataType: 'script',
        data: {
          supplier_id: $('.supplier_selectpicker').val(),
          venue_application_form_id: $('.vaf_selectpicker').val(),
          trading_terms_setting_id: $('.trading_term_selectpicker').val(),
          ascf_id: $('#ascf_id').val()
        }
      });
    });
  }

  return {
    // Public functions
    init: function() {
      selectTradingTerm();
    }
  };
}();

jQuery(document).ready(function() {
  VafSelectStep.init();
  SupplierSelectStep.init();
  TradingTermSelectStep.init();
});
