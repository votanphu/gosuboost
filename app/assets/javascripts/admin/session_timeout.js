var SessionTimeout = function () {
  var csrfToken = document.querySelector("meta[name=csrf-token]").content;

  var initSessionTimeout = function () {
    $.sessionTimeout({
      title: 'Pencil Session Timeout Notification',
      message: 'Your session is about to expire.',
      keepAliveUrl: '/',
      redirUrl: '/',
      logoutUrl: 'users/sign_in',
      warnAfter: 900000, //warn after 15 Minutes
      redirAfter: 960000, //redirect after 16 Minutes

      onRedir: function () {
        $.ajax({
          url: '/users/sign_out',
          method: 'DELETE',
          headers: { 'X-CSRF-Token': csrfToken },
          success: function(data) {
          },
        }).done(function() {
          window.location.href = "/";
        })
      },

      ignoreUserActivity: true,
      countdownMessage: 'Redirecting in {timer} seconds.',
      countdownBar: true
    });
  }

  return {
  //main function to initiate the module
  init: function () {
      initSessionTimeout();
    }
  };

}();

jQuery(document).ready(function() {
  SessionTimeout.init();

  $('#session-timeout-dialog-logout').click(function() {
    $.ajax({
      url: '/users/sign_out',
      method: 'DELETE',
      headers: { 'X-CSRF-Token': csrfToken },
      success: function(data) {
      },
    }).done(function() {
      window.location.reload();
    })
  })
});
