$(document).ready(function() {
  var csrfToken = document.querySelector("meta[name=csrf-token]").content;

  // Add click handler to method_link hyperlinks
  var method_link = {
    init: function(elem) {
      elem.unbind().click(function(e) {
        var self = $(this);
        e.preventDefault();

        if (self.attr('data-confirm') && !confirm(self.attr('data-confirm'))) {
          return false;
        }

        $.ajax({
          url: self.attr('href'),
          method: self.attr('data-method').toUpperCase(),
          headers: { 'X-CSRF-Token': csrfToken },
          success: function(data) {
            var alert = $('#m_form_alert_msg');
            alert.addClass('alert-success');
            alert.removeClass('alert-danger');
            alert.html('Do successfull.');
            alert.removeClass('m--hide').show();
            mApp.scrollTo(alert, -200);
            location.reload();
          },
          error: function(data) {
            var alert = $('#m_form_alert_msg');
            alert.addClass('alert-danger');
            alert.removeClass('alert-success');
            alert.html('Something went wrong.');
            alert.removeClass('m--hide').show();
            mApp.scrollTo(alert, -200);
          }
        });
      });
    }
  }

  var methodLinkInterval = setInterval(function() { registryMethodLink() }, 500);

  function registryMethodLink() {
      method_link.init($('a[data-method=delete]'));
  }
});
