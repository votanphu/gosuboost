$(document).ready(function() {
  if ($('#venue_users_table').length > 0) {
    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },
        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: true,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:30,
          sortable:!1
        },
        {
          field:"Mobile",
          title:"Mobile",
          width:100,
          sortable:!1
        },
        {
          field:"Email",
          title:"Email",
          width:200,
          sortable:!1
        },
        {
          field:"Role",
          title:"Role",
          width:80,
          sortable:!1
        },
        {
          field:"Active",
          title:"Active",
          width:50,
          sortable:!1
        },
        {
          field:"Number of Venues",
          title:"Number of Venues",
          width:80,
          sortable:!1
        },
        {
          field:"View",
          title:"View Venue User",
          width:50,
          sortable:!1
        },
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },
        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },
              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    venueUsersTable = $('#venue_users_table').mDatatable(options);
    t = venueUsersTable.getDataSourceQuery();

    $('#m_list_role_venue_user').on('change', function() {
      var t = venueUsersTable.getDataSourceQuery();
      t.Role = $(this).val().toLowerCase();
        venueUsersTable.setDataSourceQuery(t);
        venueUsersTable.load();
    }).val(void 0 !== t.Role ? t.Role : '');

    $('#m_list_role_venue_user').selectpicker();
  }

  if ($('#venue_user_venue_details_table').length > 0) {
    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },
        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: false,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:34,
          sortable:!1
        },
        {
          field:"Company Name",
          title:"Company Name",
          width:134,
          sortable:!1
        },
        {
          field:"ABN",
          title:"ABN",
          width:104,
          sortable:!1
        },
        {
          field:"Trading Address",
          title:"Trading Address",
          width:184,
          sortable:!1
        },
        {
          field:"Director Signed",
          title:"Director Signed",
          width:68,
          sortable:!1
        },
        {
          field:"Active",
          title:"Venue Active",
          width:50,
          sortable:!1
        },
        {
          field:"Supplier",
          title:"Supplier",
          width:114,
          sortable:!1
        },
        {
          field:"Trading Term",
          title:"Trading Term",
          width:124,
          sortable:!1
        },
        {
          field:"Personal Guarantee Signed",
          title:"Personal Guarantee Signed",
          width:84,
          sortable:!1
        },
        {
          field:"Created By",
          title:"Created By",
          width:184,
          sortable:!1
        },
        {
          field:"Created At",
          title:"Created At",
          width:84,
          sortable:!1
        },
        {
          field:"Status",
          title:"Status",
          width:84,
          sortable:!1
        },
        {
          field:"Actions",
          title:"Actions",
          width:108,
          sortable:!1
        },
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },
        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },
              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    $('#venue_user_venue_details_table').mDatatable(options);
  }
});
