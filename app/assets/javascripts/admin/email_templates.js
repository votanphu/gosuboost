$(document).ready(function() {
  if ($('#email_templates_table').length > 0) {
    var options = {
      data: {
        type: 'local',
        pageSize: 10,
        saveState: {
          cookie: true,
          webstorage: true
        },
        serverPaging: false,
        serverFiltering: false,
        serverSorting: false
      },

      layout: {
        theme: 'default',
        class: 'm-datatable--brand',
        scroll: false,
        height: null,
        footer: false,
        header: true,

        smoothScroll: {
          scrollbarShown: true
        },

        spinner: {
          overlayColor: '#000000',
          opacity: 0,
          type: 'loader',
          state: 'brand',
          message: true
        },

        icons: {
          sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
          pagination: {
            next: 'la la-angle-right',
            prev: 'la la-angle-left',
            first: 'la la-angle-double-left',
            last: 'la la-angle-double-right',
            more: 'la la-ellipsis-h'
          },
          rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
        }
      },

      sortable: false,

      pagination: true,

      search: {
        // enable trigger search by keyup enter
        onEnter: false,
        // input text for search
        input: $('#generalSearch'),
        // search delay in milliseconds
        delay: 400,
      },

      rows: {
        callback: function() {},
        // auto hide columns, if rows overflow. work on non locked columns
        autoHide: false,
      },

      columns:[
        {
          field:"#",
          title:"No",
          width:30,
          sortable:!1
        },
        {
          field:"Category",
          title:"Category",
          width:150,
          sortable:!1
        },
        {
          field:"Subject",
          title:"Subject",
          width:150,
          sortable:!1
        },
        {
          field:"Active",
          title:"Active",
          width:60,
          sortable:!1
        },
        {
          field:"Created By",
          title:"Created By",
          width:200,
          sortable:!1
        },
        {
          field:"Actions",
          title:"Actions",
          width:70,
          sortable:!1
        },
      ],

      toolbar: {
        layout: ['pagination', 'info'],

        placement: ['bottom'],  //'top', 'bottom'

        items: {
          pagination: {
            type: 'default',

            pages: {
              desktop: {
                layout: 'default',
                pagesNumber: 6
              },
              tablet: {
                layout: 'default',
                pagesNumber: 3
              },
              mobile: {
                layout: 'compact'
              }
            },

            navigation: {
              prev: true,
              next: true,
              first: true,
              last: true
            },

            pageSizeSelect: [10, 20, 30, 50, 100]
          },

          info: true
        }
      },

      translate: {
        records: {
          processing: 'Please wait...',
          noRecords: 'No records found'
        },
        toolbar: {
          pagination: {
            items: {
              default: {
                first: 'First',
                prev: 'Previous',
                next: 'Next',
                last: 'Last',
                more: 'More pages',
                input: 'Page number',
                select: 'Select page size'
              },
              info: 'Displaying {{start}} - {{end}} of {{total}} records'
            }
          }
        }
      }
    }

    $('#email_templates_table').mDatatable(options);
  }

  if ($("#new_email_template_form").length > 0) {
    $("#new_email_template_form").validate({
      // define validation rules
      rules: {
        'email_template[name]': {
          required: true
        },
        'email_template[subject]': {
          required: true
        },
        'email_template[category]': {
          required: true
        }
      },

      // display error alert on form submit
      invalidHandler: function(event, validator) {
        var alert = $('#m_form_1_msg');
        alert.removeClass('m--hide').show();
        mApp.scrollTo(alert, -200);
      },

      submitHandler: function (form) {
        $('#html_content').val($('.note-editable').html())
        form[0].submit(); // submit the form
      }
    });

    $('.summernote').summernote({
      height: 300,
    });
  }
});
