// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// no require jquery3
// no require rails-ujs
// no require popper
// no require bootstrap
// no require turbolinks
// no require bootstrap-datepicker.min
// no require jquery.inputmask.bundle
// no require_tree .
//= require metronic/assets/vendors/base/vendors.bundle.js
//= require metronic/assets/demo/default/base/scripts.bundle.js
//= require metronic/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js
//= require signature_pad.min-2.3.2.js
//= require wizard.js
//= require bootstrap-select
//= require common
//= require pages
//= require home_page
//= require venue_application_form
//= require privacy_policy
