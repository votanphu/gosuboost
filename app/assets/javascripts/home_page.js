$(document).ready(function() {
  // Get the sticky-nav
  var sticky_nav = document.getElementById("sticky-nav");
  var stickyNav = $('#sticky-nav');
  var stickyNavHeight = 0;
  var sticky_position = document.getElementById("sticky-position");

  var gotop_button = document.getElementById("go-top");

  // Get the offset position of the navbar
  //var sticky = sticky_position.offsetTop;
  var sticky = 274;

  function checkSignature() {
    if ($('#signature-pad').length > 0) {
      var signaturePad = $('#signature-pad');
      var signaturePadOffset = signaturePad.offset().top;
      var stickyNavOffset = stickyNav.offset().top;

      if (window.pageYOffset + stickyNavHeight + 10 >= signaturePadOffset) {
        stickyNav.addClass('m--hide');
      } else {
        stickyNav.removeClass('m--hide');
      }
    }
  }

  // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky"
  // when you leave the scroll position
  function scrolling() {
    if (stickyNavHeight <= 0) {
      stickyNavHeight = stickyNav.height();
    }

    // Sticky navigation
    if (window.pageYOffset >= sticky) {
      stickyNav.addClass("sticky");
      stickyNav.removeClass("home-invisible");
      gotop_button.classList.remove("home-invisible");
    } else {
      stickyNav.addClass("home-invisible");
      stickyNav.removeClass("sticky");
      gotop_button.classList.add("home-invisible");
    }

    checkSignature();
  }

  // When the user scrolls the page, execute scrolling
  window.onscroll = function() {
    scrolling();
  }

  // Go to top of the page
  $("#go-top").click(function(){
    $('html,body').animate({ scrollTop: 0 }, 'slow');
  });

  // Pointer on hover Send Message Button
  $('#send-msg-bt').hover(function() {
      $(this).css('cursor','pointer');
  });
  $('#ins-follow-bt').hover(function() {
      $(this).css('text-decoration','none');
  });
  $('.home-button').hover(function() {
      $(this).css('cursor','pointer');
  });

  // Window scroll to flash if exits
  if ( $(".alert").text() ) {
    if ($("#send-msg-bt").length > 0 && $("#home-part-6").length > 0) {
      $('html, body').animate({
        scrollTop: ($("#send-msg-bt").offset().top + $("#home-part-6").offset().top) / 2
      }, 1000);
    }
  }

  // Validate contact_form
  if ($("#contact_form").length > 0) {
    $("#contact_form").validate({
        // define validation rules
      rules: {
          'contact[firstname]': {
              required: true
          },
          'contact[lastname]': {
              required: true
          },
          'contact[email]': {
              required: true,
              email: true,
              minlength: 10
          },
          'contact[subject]': {
              required: true
          },
          'contact[message]': {
              required: true
          }
        },

        // display error alert on form submit
        invalidHandler: function(event, validator) {
          var alert = $('#contact-form-msg');
          alert.removeClass('home-invisible').show();
        },

        submitHandler: function (form) {
          form[0].submit(); // submit the form
        }
    });
  }

  // Fetch latest instagram photos from Pencil.1nc
  if ($('#ins-images').length > 0) {
    fetch_instagram_photos();
  }

  // Load more photos
  var access_token = "6278867119.3a81a9f.e4feb7acb3954659befab6ed9baedded";
  var user_id = "6278867119";
  var max_id = "";
  var photos_number = 8;
  var has_next_page = false;
  //var cursor = "";
  //var has_next_page = false;

  $('#load-more-bt').click(function() {
    if(has_next_page) {
      load_more_instagram_photos();
    } else {
      console.log("Has next page:" + has_next_page);
    }
  });

  function fetch_instagram_photos() {
    $.ajax({
      url: 'https://api.instagram.com/v1/users/6278867119/media/recent',
      dataType: 'jsonp',
      type: 'GET',
      data: {access_token: '6278867119.3a81a9f.e4feb7acb3954659befab6ed9baedded', count: 8},
      success: function(data){
        if (data.pagination.next_max_id) {
          max_id = data.pagination.next_max_id;
          has_next_page = true;

          for (i = 0; i < photos_number; i++) {
              var ins_images_html = '<div class="home-item-image home-ins-img-width home-ins-img-padding">';
              ins_images_html = ins_images_html + '<img alt="Pencil" class="home-ins-image" src="';
              ins_images_html = ins_images_html + data.data[i].images.standard_resolution.url;
              ins_images_html = ins_images_html + '"></div>';
              $('#ins-images').append(ins_images_html);
          }
        }
      },
      error: function(data){
        console.log(data);
      }
    });
  }

  function load_more_instagram_photos() {
    $.ajax({
      url: 'https://api.instagram.com/v1/users/6278867119/media/recent',
      dataType: 'jsonp',
      type: 'GET',
      data: {access_token: '6278867119.3a81a9f.e4feb7acb3954659befab6ed9baedded', count: 8, max_id: max_id},
      success: function(data){
        if (data.pagination.next_max_id) {
          max_id = data.pagination.next_max_id;
          has_next_page = true;

          for (i = 0; i < photos_number; i++) {
              var ins_images_html = '<div class="home-item-image home-ins-img-width home-ins-img-padding">';
              ins_images_html = ins_images_html + '<img alt="Pencil" class="home-ins-image" src="';
              ins_images_html = ins_images_html + data.data[i].images.standard_resolution.url;
              ins_images_html = ins_images_html + '"></div>';
              $('#ins-images').append(ins_images_html);
          }
        }
      },
      error: function(data){
        console.log(data);
      }
    });
  }
});
