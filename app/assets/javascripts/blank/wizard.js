//== Class definition
var WizardDemo = function () {
    //== Base elements
    var wizardEl = $('#m_wizard');
    var formEl = $('#m_form');
    var validator;
    var wizard;

    //== Private functions
    var initWizard = function () {
        //== Initialize form wizard
        wizard = wizardEl.mWizard({
            startStep: 1
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function(wizard) {
            if (validator.form() !== true) {
                return false;  // don't go to the next step
            }
        })

        //== Change event
        wizard.on('change', function(wizard) {
            mApp.scrollTop();
            showReviewInStep8();
        });
    }

    var initValidation = function() {

        validator = formEl.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                // === Client Information(step 1)
                trading_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },

                // === Client Information(step 2)
                firstname: {
                    required: true
                },
                lastname: {
                    required: true
                },
                mobile: {
                    required: true
                },
                property_name: {
                    required: true
                },
                suburdApplicant: {
                    required: true
                },
                state: {
                    required: true
                },
                postcode: {
                    required: true
                },
                estimated_weekly_expected_purchases: {
                    required: true,
                    number: true
                },
                requested_trading_terms: {
                    required: true
                },
                termsConditions: {
                    required: true
                },

                //=== Client Information(step 3)
                inputCompanyAbn: {
                    required: true
                },
                selectOperatingStructure: {
                    required: true
                },
                inputRegisteredAddressLine1: {
                    required: true
                },
                inputRegisteredSuburb: {
                    required: true
                },
                inputRegisteredState: {
                    required: true
                },
                inputRegisteredPostcode: {
                    required: true
                },

                //=== Confirmation(step 4)
                inputBankBranch: {
                    required: true
                },
                inputBsb: {
                    required: true
                },
                inputAccountNumber: {
                    required: true
                },

                //=== Client Information(step 5)
                inputAccountsPersonContactName: {
                    required: true
                },
                inputAccountsPersonContactEmail: {
                    required: true,
                    email: true
                },
                inputAccountsPersonContactNumber: {
                    required: true
                },
                inputDeliveryContactName: {
                    required: true
                },
                inputDeliveryContactNumber: {
                    required: true
                },

                //=== Client Information(step 6)
                'directors[0][inputDirectorFirstName]': {
                    required: true
                },
                'directors[0][inputDirectorLastName]': {
                    required: true
                },
                'directors[0][inputEmail]': {
                    required: true,
                    email: true
                },
                'directors[0][inputDateOfBirth]': {
                    required: true
                },
                'directors[0][inputDirectorAddressLine1]': {
                    required: true
                },
                'directors[0][inputDirectorSuburb]': {
                    required: true
                },
                'directors[0][inputDirectorState]': {
                    required: true
                },
                'directors[0][inputDirectorPostcode]': {
                    required: true
                },
                'directors[0][inputDirectorMobile]': {
                    required: true
                },
                'directors[0][inputDriversLicenseNumber]': {
                    required: true
                },
                'directors[0][inputDirectorLicenseExpiry]': {
                    required: true
                },
                'directors[0][inputUploadDirectorFrontLicense]': {
                    required: true
                },
                'directors[0][inputUploadDirectorBackLicense]': {
                    required: true
                },

                //=== Client Information(step 7)
                inputTradeReference1BusinessName: {
                    required: true
                },
                inputTradeReference1ContactPerson: {
                    required: true
                },
                inputTradeReference1ContactNumber: {
                    required: true
                },

                //=== Client Information(step 8)
                termsConditions2: {
                    required: true
                }

            },

            //== Validation messages
            messages: {
                accept: {
                    required: "You must accept the Terms and Conditions agreement!"
                }
            },

            //== Display error
            invalidHandler: function(event, validator) {
                mApp.scrollTop();

                swal({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {

            }
        });

        //=== Client Information(step 6) - for repeater of director

        formEl.find('[name^="directors["]').each(function() {
            $(this).rules('add', {
                required: true
            });
        });

        formEl.find('[name*="[inputEmail]"]').each(function() {
            $(this).rules('add', {
                email: true
            });
        });
    }

    var initSubmit = function() {
        var btn = formEl.find('[data-wizard-action="submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                //== See: src\js\framework\base\app.js
                mApp.progress(btn);
                //mApp.block(formEl);

                //== See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function() {
                        mApp.unprogress(btn);
                        //mApp.unblock(formEl);

                        swal({
                            "title": "",
                            "text": "The application has been successfully submitted!",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                        });
                    }
                });
            }
        });
    }

    var initInputMask = function() {
        $(".mobileAU").inputmask("mask", {
            "mask": "0499 999 999"
        });

        $(".phoneAU").inputmask("mask", {
            "mask": "(09) 9999 9999"
        });

        $(".postcodeAU").inputmask("mask", {
            "mask": "9999"
        });

        $(".numer10Digits").inputmask("mask", {
            "mask": "9999999999"
        });

        $(".number1To20").inputmask("mask", {
            "mask": "9{1,20}"
        });
    }

    var showReviewInStep8 = function() {

        var buildElement = '';

        $('[id^=m_wizard_form_step_]').not('#m_wizard_form_step_review').each(function() {

            var title = $(this).find('h3.m-form__heading-title').text().trim();

            buildElement += '<div class="card mb-3"><div class="card-body pb-1"><p class="font-weight-bold">'+title+'</p>';

            $(this).find('input.form-control, select.form-control').each(function() {
                var label = ($(this).prev('label').text() !== '')
                ? $(this).prev('label').text().replace('*', '').trim()
                : $(this).next('span.m-form__help').text().replace('*', '').trim();
                var value = $(this).val();
                buildElement += '<p>'+label+': '+value+'</p>';
            });
            buildElement += '</div></div>';
        });

        $('.form-reviews').html(buildElement);
    }

    var fadeTermsConditions = function() {

        var textareaTermsConditions = $(".textareaTermsConditions");


        $(".checkboxTermsConditions").prop("disabled", true)
        $(".checkboxTermsConditions").css("opacity", 0.5)
        $("label.form-check-label").css("opacity", 0.5)

        $(".textareaTermsConditions").on("scroll", function() {
            var checkboxTermsConditions = $(this).next().find(".checkboxTermsConditions");
            var label = $(this).next().find("label");
            if ($(this).scrollTop() < $(this).height()) {
                checkboxTermsConditions.prop("disabled", true)
                checkboxTermsConditions.css("opacity", 0.5)
                label.css("opacity", 0.5)
            } else {
                checkboxTermsConditions.prop("disabled", false)
                checkboxTermsConditions.css("opacity", 1)
                label.css("opacity", 1)
            }
        })
    }

    var datepicker = function () {

        $('.date_picker').datepicker({
            format: 'dd/mm/yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            },
            autoclose: true
        });
    }

    var repeater = function() {

        var repeater = $('#m_repeater_directors').repeater({
            initEmpty: false,

            show: function () {
                $(this).slideDown();
                datepicker();
                initValidation();
                initInputMask();

                changeIdsToApplyDropzone(this);
            },
            isFirstItemUndeletable: true,
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    }

    var dropZone = function() {
      // 2 upload fields
      if ($('#dropzoneFront').length) {
        new Dropzone('#dropzoneFront', { url: "/file/post" });
      }

      if ($('#dropzoneBack').length) {
        new Dropzone('#dropzoneBack', { url: "/file/post" });
      }
    }

    var changeIdsToApplyDropzone = function(newElement) {

        index = $("#m_repeater_directors div[data-repeater-item]").length - 1;

        $(newElement).find('#dropzoneFront').attr('id', 'dropzoneFront'+index);
        new Dropzone('#dropzoneFront'+index, { url: "/file/post" });
        $(newElement).find('#dropzoneBack').attr('id', 'dropzoneBack'+index);
        new Dropzone('#dropzoneBack'+index, { url: "/file/post" });
    }


    return {
        // public functions
        init: function() {
            wizardEl = $('#m_wizard');
            formEl = $('#m_form');

            initWizard();
            initValidation();
            initSubmit();
            initInputMask();
            fadeTermsConditions();
            datepicker();
            repeater();
            dropZone();
        }
    };
}();

jQuery(document).ready(function() {
    WizardDemo.init();
});