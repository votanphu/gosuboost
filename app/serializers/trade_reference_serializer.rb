class TradeReferenceSerializer < ActiveModel::Serializer
  attributes :id, :business_name, :contact_person, :contact_number
  has_one :vaf
end
