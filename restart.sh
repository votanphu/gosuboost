set -e

CMD_COMPILE_ASSETS="RAILS_ENV=production rails assets:precompile"
CMD_STOP="./stop.sh"
CMD_START="./start.sh"

echo -e "Precompiling assets..."
eval $CMD_COMPILE_ASSETS

eval $CMD_STOP

sleep 2

eval $CMD_START

echo -e "\n------------> Restart completed! \n"
