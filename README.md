# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* Review Code
Checked at: 24/5/2018 - 12:26
<!-- Check best practices of code. New code must <= this -->
rails_best_practices -f html .
=> 61 warnings
<!-- Check security in code. New code must <= this -->
brakeman -o brakeman-report.html
=> 0 warnings
<!-- Check coding standard, performance in code. New code must <= this -->
rubocop --format html -o rubocop-report.html
=> 1196 warnings
<!-- Check routes. New code must <= this -->
rails traceroute > traceroute-report.txt
=> Unused routes (0). Unreachable action methods (22) of devise
<!-- Check performace, using memory, complexity. New code must (this - 1% <= score <= this + 1%), maximum 100% -->
http://quickar-local.me:3000/?pp=flamegraph
http://quickar-local.me:3000/?pp=profile-memory
rubycritic --no-browser -f html .
=> 82.96

* All in one
rails_best_practices -f html . &&
echo '==> Check best practices of code => done' &&
brakeman -o brakeman-report.html &&
echo '==> Check security in code => done' &&
rubocop --format html -o rubocop-report.html &&
echo '==> Check coding standard, performance in code => done' &&
rails traceroute > traceroute-report.txt &&
echo '==> Check routes => done' &&
rubycritic --no-browser -f html . &&
echo '==> Check performace, using memory, complexity => done'

* Testing
MUST test on production mode

* Upgrade
bundle update rails-html-sanitizer => 1.0.4

* Reset DB
RAILS_ENV=production DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rails db:drop db:create db:migrate db:seed

* Diagram
<!-- Generate to Entity Relationship Diagram -->
bundle exec erd
