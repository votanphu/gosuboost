require 'test_helper'

class Admin::VenueDetailsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_venue_details_index_url
    assert_response :success
  end

  test "should get show" do
    get admin_venue_details_show_url
    assert_response :success
  end
end
