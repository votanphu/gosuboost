require 'test_helper'

class Admin::TradingTermsSettingsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get admin_trading_terms_settings_index_url
    assert_response :success
  end

  test "should get create" do
    get admin_trading_terms_settings_create_url
    assert_response :success
  end

  test "should get update" do
    get admin_trading_terms_settings_update_url
    assert_response :success
  end

  test "should get destroy" do
    get admin_trading_terms_settings_destroy_url
    assert_response :success
  end

end
