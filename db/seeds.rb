# Init users data
users_attributes = [
  {
    name: 'Greg Armstrong', firstname: 'Greg', lastname: 'Armstrong', is_admin: true, role: 'ADMIN',
    email: 'greg.armstrong@remerce.com.au', password: 'Pencil@2018', password_confirmation: 'Pencil@2018'
  },
  {
    name: 'Nhan Nguyen', firstname: 'Nhan', lastname: 'Nguyen', is_admin: true, role: 'ADMIN',
    email: 'nnhansg@gmail.com', password: 'Pencil@2018', password_confirmation: 'Pencil@2018'
  },
  {
    name: 'Greg Armstrong', firstname: 'Greg', lastname: 'Armstrong', is_admin: true, role: 'ADMIN',
    email: 'greg@gos.com.vn', password: 'Pencil@2018', password_confirmation: 'Pencil@2018'
  }
]

users_attributes.each do |user_attributes|
  user = User.new(user_attributes)
  user.skip_confirmation!
  user.save! if User.where(email: user_attributes[:email]).first.blank?
end

puts "Init #{users_attributes.count} users data => done"

user = User.where(email: 'nnhansg@gmail.com').first

# Init email_templates data
email_templates_attributes = [
  {
    name: 'Directors registration',
    subject: 'Director application for supplier credit for {{trading_name}}',
    plain_content: "
      Hello {{director_first_name}},\n\r\n\r
      You have been added as a director of {{company_name}} for venue {{trading_name}} to apply for supplier credit through the pencil platform.\n\r\n\r
      Please click on this link {{set_password_link}} and set your password, then you'll be promoted to review the information provided in the credit application form and digitally sign and submit the form.\n\r\n\r
      Thanks,\n\r
      Pencil Team\n\r
    ",
    html_content: "
      Hello {{director_first_name}},<br/><br/>
      You have been added as a director of {{company_name}} for venue {{trading_name}} to apply for supplier credit through the pencil platform.<br/><br/>
      Please click on this <a href='{{set_password_link}}'>link</a> and set your password, then you'll be promoted to review the information provided in the credit application form and digitally sign and submit the form.<br/><br/>
      Thanks,<br/>
      Pencil Team<br/>
    ",
    category: 'DIRECTOR_REGISTRATION',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Send venue form link',
    subject: 'Link to continue Venue Application Form',
    plain_content: "
      Thank you for saving Venue Application Form. Please use the unique link below to return to the form from any computer.\n\r\n\r
      {{form_link}} \n\r\n\r
      Remember that the link will expire after 30 days so please return via the provided link to complete your form submission.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Thank you for saving Venue Application Form. Please use the unique link below to return to the form from any computer.<br/><br/>
      {{form_link}}<br/><br/>
      Remember that the link will expire after 30 days so please return via the provided link to complete your form submission.<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'VENUE_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Director sign venue form',
    subject: 'Director {{firstname}} {{lastname}} has signed the form {{trading_name}}',
    plain_content: "
      Director {{firstname}} {{lastname}} has signed the form {{trading_name}}.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Director {{firstname}} {{lastname}} has signed the form {{trading_name}}.<br/><br/>
      {{review_link}}<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'DIRECTOR_SIGN_VENUE_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Personal Guarantee Application for supplier credit',
    subject: 'Personal guarantee application for supplier credit for {{trading_name}}',
    plain_content: "
      Hello {{firstname}} {{lastname}},\n\r\n\r
      You have been added as a personal guarantee of {{company_name}} for venue {{trading_name}} to apply for supplier credit through the pencil platform.\n\r\n\r
      Please click on this link {{set_password_link}} and set your password, then you'll be promoted to review the information provided in the credit application form and digitally sign and submit the form.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Hello {{firstname}} {{lastname}},<br /><br />
      You have been added as a personal guarantee of {{company_name}} for venue {{trading_name}} to apply for supplier credit through the pencil platform.<br /><br />
      Please click on this <a href='{{set_password_link}}'>link</a> and set your password, then you'll be promoted to review the information provided in the credit application form and digitally sign and submit the form.<br /><br />
      Thanks,<br />
      Pencil Team
    ",
    category: 'PERSONAL_GUARANTEE_APPLICATION_FOR_SUPPLIER_CREDIT',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Personal Guarantee sign Apply Supplier Credit Form',
    subject: 'Personal Guarantee {{firstname}} {{lastname}} has signed the apply form {{trading_name}}',
    plain_content: "
      Personal Guarantee {{firstname}} {{lastname}} has signed the apply form {{trading_name}}.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Personal Guarantee {{firstname}} {{lastname}} has signed the apply form {{trading_name}}.<br/><br/>
      {{review_link}}<br /><br />
      Thanks,<br />
      Pencil Team
    ",
    category: 'PERSONAL_GUARANTEE_SIGN_ASCF',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'A venue has applied Supplier Credit Form',
    subject: 'A venue {{firstname}} {{lastname}} from {{company_name}} has applied a credit form for {{market_segment}} market segment',
    plain_content: "
      A venue {{firstname}} {{lastname}} from {{company_name}} has applied a credit form for {{market_segment}} market segment.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      The director {{firstname}} {{lastname}} from {{company_name}} has applied a credit form for {{market_segment}} market segment.<br/><br/>
      {{review_link}}<br/><br/>
      Thanks,<br />
      Pencil Team
    ",
    category: 'VENUE_APPLY_TO_APPLY_SUPPLIER_CREDIT_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Personal Guarantee has signed Apply Supplier Credit Form',
    subject: 'Personal Guarantee {{firstname}} {{lastname}} - {{company_name}} has signed the apply form',
    plain_content: "
      Personal Guarantee {{firstname}} {{lastname}} from {{company_name}} has signed the credit form for {{market_segment}} market segment'.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Personal Guarantee {{firstname}} {{lastname}} has signed the credit form for {{market_segment}} market segment.<br/><br/>
      {{review_link}}<br/><br/>
      Thanks,<br />
      Pencil Team
    ",
    category: 'PERSONAL_GUARANTEE_SIGN_SUPPLIER_CREDIT_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'All Personal Guarantees have signed the supplier credit form',
    subject: 'All Personal Guarantees have signed the supplier credit form for {{trading_name}}',
    plain_content: "
      All Personal Guarantees have signed the supplier credit form for {{trading_name}}.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      All Personal Guarantees have signed the supplier credit form for {{trading_name}}.<br /><br />
      {{review_link}}<br/><br/>
      Thanks,<br />
      Pencil Team
    ",
    category: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Supplier registration',
    subject: 'Supplier in Pencil Platform',
    plain_content: "
      Hello {{first_name}} {{last_name}},\n\r\n\r
      You have been added as a supplier through the pencil platform.\n\r\n\r
      Please click on this link {{set_password_link}} and set your password.\n\r\n\r
      Thanks,\n\r
      Pencil Team\n\r
    ",
    html_content: "
      Hello {{first_name}} {{last_name}},<br/><br/>
      You have been added as a supplier through the pencil platform.<br/><br/>
      Please click on this link {{set_password_link}} and set your password.<br/><br/>
      Thanks,<br/>
      Pencil Team<br/>
    ",
    category: 'SUPPLIER_REGISTRATION',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Send the apply supplier credit form link',
    subject: 'Link to continue Apply Supplier Credit Form',
    plain_content: "
      Thank you for saving Apply Supplier Credit Form. Please use the unique link below to return to the form from any computer.\n\r\n\r
      {{form_link}} \n\r\n\r
      Remember that the link will expire after 30 days so please return via the provided link to complete your form submission.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Thank you for saving Apply Supplier Credit Form. Please use the unique link below to return to the form from any computer.<br/><br/>
      {{form_link}}<br/><br/>
      Remember that the link will expire after 30 days so please return via the provided link to complete your form submission.<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'APPLY_SUPPLIER_CREDIT_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Send new credit limit notify to directors personal guarantees',
    subject: 'Review New Credit Limit of Apply Supplier Credit Form for {{trading_name}}',
    plain_content: "
      Credit Limit of Apply Supplier Credit Form for {{trading_name}} has been changed by {{trading_name}}.\n\r\n\r
      Venue credit limit: ${{venue_credit_limit}}\n\r
      Supplier credit limit: ${{supplier_credit_limit}}\n\r\n\r
      Please access to Pencil platform to review the apply supplier credit form.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Credit Limit of Apply Supplier Credit Form for {{trading_name}} has been changed by {{trading_name}}.<br/><br/>
      Venue credit limit: {{venue_credit_limit}}<br/>
      Supplier credit limit: {{supplier_credit_limit}}<br/><br/>
      Please access to Pencil platform to review the apply supplier credit form.<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'APPLY_SUPPLIER_CREDIT_FORM_NEW_CREDIT_LIMIT',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Send approve notify to directors personal guarantees supplier',
    subject: 'Apply supplier credit form for {{trading_name}} has been approved',
    plain_content: "
      The Apply Supplier Credit Form for {{trading_name}} has been approved by {{approved_by}}.\n\r\n\r
      Please access to Pencil platform to review the apply supplier credit form.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      The Apply Supplier Credit Form for {{trading_name}} has been approved by {{approved_by}}.<br/><br/>
      Please access to Pencil platform to review the apply supplier credit form.<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'APPLY_SUPPLIER_CREDIT_FORM_APPROVED',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Send reject notify to directors personal guarantees supplier',
    subject: 'Apply supplier credit form for {{trading_name}} has been rejected',
    plain_content: "
      The Apply Supplier Credit Form for {{trading_name}} has been rejected by {{rejected_by}}.\n\r\n\r
      Please access to Pencil platform to review the apply supplier credit form.\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      The Apply Supplier Credit Form for {{trading_name}} has been rejected by {{rejected_by}}.<br/><br/>
      Please access to Pencil platform to review the apply supplier credit form.<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'APPLY_SUPPLIER_CREDIT_FORM_REJECTED',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'All directors have signed the form',
    subject: 'All directors have signed the form {{trading_name}}',
    plain_content: "
      All directors have signed the form {{trading_name}}.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      All directors have signed the form {{trading_name}}.<br/><br/>
      {{review_link}}<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'ALL_DIRECTORS_SIGNED_VENUE_FORM',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Your company has signed the supplier credit form',
    subject: 'Your company has signed the supplier credit form for {{trading_name}}',
    plain_content: "
      Your company has signed the supplier credit form for {{trading_name}}.\n\r\n\r
      {{review_link}}\n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      Your company has signed the supplier credit form for {{trading_name}}.<br /><br />
      {{review_link}}<br/><br/>
      Thanks,<br />
      Pencil Team
    ",
    category: 'ALL_PERSONAL_GUARANTEES_SIGNED_SUPPLIER_CREDIT_FORM_PGS',
    active: true,
    created_by_id: user[:id]
  },
  {
    name: 'Notify directors and PGs when supplier T&Cs has been changed',
    subject: '{{supplier_company_name}} has changed their Terms and Conditions',
    plain_content: "
      {{supplier_company_name}} has changed their Terms & Conditions. Please access to Pencil platform to review your Apply Supplier Credit Form.\n\r\n\r
      {{form_link}} \n\r\n\r
      Thanks,\n\r
      Pencil Team
    ",
    html_content: "
      {{supplier_company_name}} has changed their Terms & Conditions. Please access to Pencil platform to review your Apply Supplier Credit Form.<br/><br/>
      {{form_link}}<br/><br/>
      Thanks,<br/>
      Pencil Team
    ",
    category: 'NOTIFY_DIRECTORS_PGS_T&CS_CHANGED',
    active: true,
    created_by_id: user[:id]
  }
]

email_templates_attributes.each do |email_template_attributes|
  EmailTemplate.create(email_template_attributes) if EmailTemplate.where(category: email_template_attributes[:category]).first.blank?
end

puts "Init #{email_templates_attributes.count} email templates data => done"

email_themes_attributes = [
  {
    name: 'default',
    plain_content: "",
    html_content: "
      <div style='width: 630px; background-color: white; padding-left:15px; padding-right:15px; padding-bottom:20px; padding-top:10px; border-top: 1px solid #EFEAE9; border-left: 1px solid #EFEAE9; border-right: 1px solid #EFEAE9;'>
        <div style='width: 100%; height:55px'>
          <table  style='width: 630px'>
            <tr>
              <td width='20%'>
                <a href='{{base_url}}' style='text-decoration:none;'>
                  <img src='{{base_url}}{{logo}}' height='41' alt='Pencil'>
                </a>
              </td>
              <td width='46%'>
              </td>
              <td width='9%'>
                <a href='{{base_url}}/users/sign_in' style='text-decoration:none; color: #212529;'>LOGIN</a>
              </td>
              <td width='18%'>
                <a href='{{base_url}}/venue-form' style='text-decoration:none; color: #212529;'>GET STARTED</a>
              </td>
              <td width='7%'>
                <a href='{{base_url}}/faq' style='text-decoration:none; color: #212529;'>FAQs</a>
              </td>
            <tr>
          </table>
        </div>
        <div style='padding: 4px; border: 1px solid #EFEAE9;'>
          <div style='background-color: #938E90; min-height: 500px; padding-top:15px; padding-bottom:15px;'>
            <div style='margin:auto; height: auto; width: 70%; background-color: white; padding-top:20px; padding-left:20px; padding-right:20px'>
            {{yield}}
            </div>
          </div>
        </div>
      </div>
      <div style='width: 630px; background-color: #ecaa90; height:61px; padding-top:34px; padding-left:15px; padding-right:15px; border: 1px solid #EFEAE9;'>
        <table  style='width: 630px'>
          <tr>
            <td width='40%'>
              Copyright 2018 by Pencil.One.
            </td>
            <td width='26%'>
            </td>
            <td width='17%'>
              <a href='{{base_url}}/terms_of_use' style='text-decoration:none; color: #212529;'>Terms of Use</a>
            </td>
            <td width='10%'>
              <a href='{{base_url}}/privacy' style='text-decoration:none; color: #212529;'>Privacy</a>
            </td>
            <td width='7%'>
              <a href='{{base_url}}/faq' style='text-decoration:none; color: #212529;'>FAQs</a>
            </td>
          <tr>
        </table>
      </div>
    ",
    category: 'GENERAL',
    active: true,
    created_by_id: user[:id]
  }
]

email_themes_attributes.each do |email_theme_attributes|
  EmailTheme.create(email_theme_attributes) if EmailTheme.where(category: email_theme_attributes[:category]).first.blank?
end

puts "Init #{email_themes_attributes.count} email themes data => done"

# Init market segments data
market_segments_attributes = [
  { name: 'Cafe' }, { name: 'Bar' }, { name: 'Restaurant' },
  { name: 'Nightclub' }, { name: 'Bistro' }, { name: 'Pub' },
  { name: 'Hotel' }, { name: 'Motel' },{ name: 'Serviced Apartments' },
  { name: 'Serviced Offices' }, { name: 'Massage' }, { name: 'Medical' },
  { name: 'Beauty Salon' }, { name: 'Hairdresser' }, { name: 'Finance' },
  { name: 'Corporate' }
]

market_segments_attributes.each do |ms_attributes|
  market_segment = MarketSegment.new(ms_attributes)
  market_segment.save! if MarketSegment.where(name: ms_attributes[:name]).first.blank?
end

puts "Init #{market_segments_attributes.count} market segments data => done"

# Init Pencil Terms Conditions

pencil_terms_conditions_attributes = [
  {
    content: "
    <div>Supplier Terms and Conditions</div>
    <div>
      <br>
    </div>
    <div>
      <br>
    </div>
    <div>1. General acknowledgements&nbsp;</div>
    <div>
      <br>
    </div>
    <div>The Applicant acknowledges and agrees that:</div>
    <div>
      <br>
    </div>
    <div>if accepted by (SUPPLIER NAME - SUPPLIER ACN) (Supplier), the Terms and Conditions in this agreement, all accepted orders
      placed by the Applicant, and the guarantee and indemnity (if applicable), will constitute the contract between the Supplier
      and the Applicant pursuant to which the Supplier makes all supplies of goods and services to the Applicant (Contract);</div>
    <div>
      <br>
    </div>
    <div>(b) credit may be reviewed, altered or withdrawn at any time without prior notice from the Supplier at the Supplier's absolute
      discretion and that the Supplier&nbsp; will have no liability or responsibility for any loss, however arising, incurred
      by the Applicant due to that review, alteration or withdrawal;</div>
    <div>
      <br>
    </div>
    <div>(c) any change in respect to ownership, legal entity, name or address must be notified in writing by the Applicant&nbsp;
      to the Supplier at least 7 days before that change occurs;</div>
    <div>
      <br>
    </div>
    <div>(d) if the account is overdue, the Supplier at its discretion, reserves the right to refer the account to a debt collection
      agency (and to disclose any information required to be provided to the debt collection agent in order to do so) for collection
      and the Applicant agrees to be responsible to meet all reasonable costs and commissions in employing the debt collection
      agent to collect the overdue account;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>
      <br>
    </div>
    <div>
      <br>
    </div>
    <div>(e) it grants security interests to the Supplier under the Contract and that the Contract constitutes a ‘security agreement’
      pursuant to which all such security interests are granted for the purposes of the Personal Property Securities Act 2009
      (Cth).</div>
    <div>
      <br>
    </div>
    <div>2. Credit Information</div>
    <div>
      <br>
    </div>
    <div>The Applicant, and each individual who signs this application as a director, partner or proprietor (each a Principal) acknowledges
      and agrees that the Supplier may to the extent permitted by law:</div>
    <div>
      <br>
    </div>
    <div>obtain credit eligibility information containing information about the Applicant's, and each Principal's consumer or commercial
      credit arrangements from a credit reporting body for the purposes of assessing this application or in connection with any
      attached Guarantee and Indemnity;</div>
    <div>
      <br>
    </div>
    <div>(b) give a credit reporting body information to allow the credit reporting body to create and maintain credit reporting information
      about the Applicant and each Principal;</div>
    <div>
      <br>
    </div>
    <div>(c) disclose information about the Applicant and each Principal (including credit eligibility information or any personal
      information as defined in the Privacy Act 1988 (Cth) (as amended) derived from the credit information, and any information
      about the Applicant's or a Principal's personal or commercial arrangements to any agent of the Supplier assisting in processing
      the application and any other provider of credit to the Applicant to the extent that it is contained in&nbsp; credit reporting
      information from a credit reporting body; and&nbsp;</div>
    <div>(d) exchange information about the Applicant and its Principals (including banker’s opinions, credit eligibility information&nbsp;
      and other information relating to creditworthiness) with other credit providers and any collection agent of the Supplier
      for purposes including:</div>
    <div>
      <br>
    </div>
    <div>assessing the Applicant's application for credit;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(ii) notifying other credit providers of the Applicant's defaults;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(iii) exchanging information about the Applicant's credit status where the Applicant is in default with another credit provider;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(iiii) assessing the Principals' creditworthiness;</div>
    <div>
      <br>
    </div>
    <div>(v) seeking to recover any debt; and&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(vi) any other purpose authorised by law.</div>
    <div>
      <br>
    </div>
    <div>The Applicant warrants and represents that it has the future ability to pay all debts as and when they fall due.</div>
    <div>
      <br>
    </div>
    <div>3. Privacy</div>
    <div>
      <br>
    </div>
    <div>The Supplier collects the personal information requested in or relating this application (including through its contractors
      and agents) for the purpose of determining whether or not to extend trade credit, to administer the Contract and to communicate
      with the Applicant and its personnel. The Supplier may disclose such personal information (including to recipients based
      outside Australia) to its related companies, agents, merchants, contractors (including credit reporting bodies and debt
      collection agents) for these purposes and such other purposes permitted by law.&nbsp; The Applicant (and each Principal)
      understands that it (and they) need not give any of the personal information requested in this application. However, without
      this information the Supplier may not be able to process this application or provide the Applicant with an appropriate
      level of service or extend trade credit to the Applicant. The Applicant and each Principal have the right to request that
      credit reporting bodies do not use or disclose credit reporting information for the purposes of pre-screening or direct
      marketing by credit providers. If the Applicant or any Principal has been or is likely to be a victim of fraud, it can
      ask a credit reporting body not to disclose credit reporting information about it.</div>
    <div>
      <br>
    </div>
    <div>The Supplier's Privacy Policy, located at [Link to SUPPLIER Privacy Policy] contains information about:&nbsp;</div>
    <div>
      <br>
    </div>
    <div>how to seek access to personal information the Supplier holds and seek the correction of such information; and&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(b) how to complain about a privacy breach or a credit reporting complaint and how the Supplier will deal with those complaints.</div>
    <div>
      <br>
    </div>
    <div>By signing this application, the Applicant:&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(c) confirms that it has obtained the authorisation of each of the Applicant (if they are an individual) and all individuals
      connected with the Applicant whose personal information may be handled in respect of this application and any resultant
      credit provided (including directors, officers and representatives) (Affected Individuals) for the Supplier to collect,
      maintain, use and disclose each Affected Individual's personal information in accordance with the Privacy Act 1988 (Cth)
      and the terms of this clause 3;&nbsp;</div>
    <div>(d) authorises the Supplier to collect, maintain, use and disclose the personal information of Affected Individuals in accordance
      with the Privacy Act 1988 (Cth) and the terms of this clause 3; and</div>
    <div>
      <br>
    </div>
    <div>(e) agrees that information about transactions under the Contract may be used by the Supplier or its subsidiaries in a de-identified
      manner for marketing and other commercial purposes.</div>
    <div>
      <br>
    </div>
    <div>4. Guarantee and Indemnity</div>
    <div>
      <br>
    </div>
    <div>Where the Applicant is a company, the Directors (hereafter Guarantors) of the Applicant agree:</div>
    <div>
      <br>
    </div>
    <div>to be jointly and severally liable for all indebtedness of the Applicant to the Supplier, in consideration of the Supplier
      supplying goods and services on credit to the Applicant;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(b) that the Supplier may exercise its rights under the guarantee in this clause 4 at any time;</div>
    <div>
      <br>
    </div>
    <div>(c) the Guarantors will pay any amount owing to the Supplier by the Applicant within 3 days of receipt or delivery of a written
      demand to the last known address of the Guarantors from the Supplier;</div>
    <div>
      <br>
    </div>
    <div>(d) the Supplier is not required to have exercised or exhausted its legal rights against the Applicant prior to making a
      demand under this Guarantee;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(e) the liability of Guarantors will continue despite the Supplier entering into any sort of arrangement with the Applicant;</div>
    <div>(f) the guarantee under this clause 4, will continue despite the death of one or all of the Guarantors and may be enforced
      against a Guarantor's heirs and executors; and&nbsp;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(g) the provisions of the guarantee under this clause 4 will continue to apply despite any concession or indulgence granted
      by the Supplier to the Applicant.&nbsp;</div>
    <div>
      <br>
    </div>
    <div>5. Acceptance and Acknowledgement</div>
    <div>
      <br>
    </div>
    <div>The Applicant acknowledges and agrees that:&nbsp;</div>
    <div>
      <br>
    </div>
    <div>the Applicant is not aware of any information, notice, or court proceedings that may lead to an Insolvency Event as defined
      in the Laundry Agreement;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(b) none of the Principals have been a director of a company which has experienced an Insolvency Event or have personally
      been declared bankrupt or entered into an arrangement under the Bankruptcy Act 1966 (Cth) (as amended);&nbsp;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(c) the Applicant has read and understood the information provided within this application and the Applicant agrees to be
      bound by the Contract;&nbsp;</div>
    <div>
      <br>
    </div>
    <div>(d) all information in this application is true and correct in every detail and, if credit is given, it will be provided
      in reliance upon the information supplied in this application; and</div>
    <div>
      <br>
    </div>
    <div>(e) the Applicant has received and agreed to the Terms of Credit and the Laundry Agreement.&nbsp;</div>
      "
  }
]

PencilTermsCondition.create!(pencil_terms_conditions_attributes.first) if PencilTermsCondition.first.blank?

puts "Init #{pencil_terms_conditions_attributes.count} pencil terms conditions data => done"
