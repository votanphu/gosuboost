class AddLicenseExpiryToDirector < ActiveRecord::Migration[5.1]
  def change
    add_column :directors, :license_expiry, :datetime
  end
end
