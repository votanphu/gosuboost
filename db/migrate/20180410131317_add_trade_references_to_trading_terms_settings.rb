class AddTradeReferencesToTradingTermsSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :trading_terms_settings, :trade_references, :string
  end
end
