class AddFieldsToSupplier < ActiveRecord::Migration[5.1]
  def up
    add_attachment :suppliers, :logo
  end

  def down
    remove_attachment :suppliers, :logo
  end
end
