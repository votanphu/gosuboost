class AddSignatureImageToVenueApplicationFormDirector < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_form_directors, :signature_image, :string
    add_column :venue_application_form_directors, :is_signed, :boolean, default: false
    add_column :venue_application_form_directors, :form_data, :json, default: {}
    add_column :venue_application_form_personal_guarantees, :signature_image, :string
    add_column :venue_application_form_personal_guarantees, :is_signed, :boolean, default: false
    add_column :venue_application_form_personal_guarantees, :form_data, :json, default: {}
  end
end
