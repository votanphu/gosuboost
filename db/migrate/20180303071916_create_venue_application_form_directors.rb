class CreateVenueApplicationFormDirectors < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_application_form_directors do |t|
      t.references :venue_application_form, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_directors_on_vaf_id'
      }

      t.references :director, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_directors_on_director_id'
      }

      t.timestamps
    end
  end
end
