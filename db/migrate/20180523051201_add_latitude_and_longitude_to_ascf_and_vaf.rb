# add latitude and longtitude to ASCF and VAF
class AddLatitudeAndLongitudeToAscfAndVaf < ActiveRecord::Migration[5.1]
  def change
    add_column :apply_supplier_credit_personal_guarantees, :latitude, :float
    add_column :apply_supplier_credit_personal_guarantees, :longitude, :float
    add_column :venue_application_form_directors, :latitude, :float
    add_column :venue_application_form_directors, :longitude, :float
  end
end
