class AddMarketSegmentToVenueApplicationForm < ActiveRecord::Migration[5.1]
  def change
    add_reference :venue_application_forms, :market_segment, foreign_key: true
  end
end
