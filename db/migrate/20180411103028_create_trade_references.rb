class CreateTradeReferences < ActiveRecord::Migration[5.1]
  def change
    create_table :trade_references, id: :uuid do |t|
      t.string :business_name
      t.string :contact_person
      t.string :contact_number
      t.references :vaf, references: :venue_application_forms, type: :uuid
      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE trade_references_object_index_seq START 1;
    ALTER SEQUENCE trade_references_object_index_seq OWNED BY trade_references.object_index;
    ALTER TABLE trade_references ALTER COLUMN object_index SET DEFAULT nextval('trade_references_object_index_seq');
    SQL
    add_index :trade_references, :object_index
  end
end
