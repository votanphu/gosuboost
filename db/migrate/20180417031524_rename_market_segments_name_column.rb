class RenameMarketSegmentsNameColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :market_segments, :market_segment, :name
  end
end
