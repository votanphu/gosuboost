class CreateDirectors < ActiveRecord::Migration[5.1]
  def change
    create_table :directors, id: :uuid do |t|
      t.string :firstname
      t.string :lastname
      t.datetime :date_of_birth
      t.string :email
      t.string :address_line_1
      t.string :address_line_2
      t.string :suburb
      t.string :state
      t.string :postcode
      t.string :mobile
      t.string :phone
      t.string :drivers_license_number
      t.string :license_file
      t.references :user, foreign_key: true, type: :uuid

      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE directors_object_index_seq START 1;
    ALTER SEQUENCE directors_object_index_seq OWNED BY directors.object_index;
    ALTER TABLE directors ALTER COLUMN object_index SET DEFAULT nextval('directors_object_index_seq');
    SQL

    add_index :directors, :object_index
  end
end
