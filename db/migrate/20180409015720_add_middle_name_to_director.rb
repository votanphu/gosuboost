class AddMiddleNameToDirector < ActiveRecord::Migration[5.1]
  def change
    add_column :directors, :middlename, :string
  end
end
