class CreateCreditLimits < ActiveRecord::Migration[5.1]
  def change
    create_table :credit_limits do |t|
      t.decimal :credit_limit, precision: 15, scale: 2
      t.decimal :increments, precision: 15, scale: 2
      t.references :supplier, foreign_key: true, type: :uuid
      t.references :market_segment, foreign_key: true
      t.references :trading_terms_setting, foreign_key: true

      t.timestamps
    end
  end
end
