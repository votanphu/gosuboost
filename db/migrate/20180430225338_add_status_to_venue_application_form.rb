class AddStatusToVenueApplicationForm < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_forms, :status, :string, default: 'DRAFT'
  end
end
