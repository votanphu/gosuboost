class CreateSuppliers < ActiveRecord::Migration[5.1]
  def change
    create_table :suppliers, id: :uuid do |t|
      t.string :company_name
      t.string :contact_name
      t.string :contact_number
      t.references :user, foreign_key: true, type: :uuid

      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE suppliers_object_index_seq START 1;
    ALTER SEQUENCE suppliers_object_index_seq OWNED BY suppliers.object_index;
    ALTER TABLE suppliers ALTER COLUMN object_index SET DEFAULT nextval('suppliers_object_index_seq');
    SQL

    add_index :suppliers, :object_index
  end
end
