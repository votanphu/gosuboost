class RenameKitchenContactNameToVenueApplicationForm < ActiveRecord::Migration[5.1]
  def change
    rename_column :venue_application_forms, :kitchen_contact_name, :delivery_contact_name
    rename_column :venue_application_forms, :kitchen_contact_number, :delivery_contact_number
  end
end
