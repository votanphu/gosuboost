class CreatePencilTermsConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :pencil_terms_conditions do |t|
      t.text :content
      t.references :last_updated_by, references: :users, type: :uuid
      t.datetime :last_updated_at
      t.inet :last_updated_ip

      t.timestamps
    end
  end
end
