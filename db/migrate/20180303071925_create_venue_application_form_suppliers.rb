class CreateVenueApplicationFormSuppliers < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_application_form_suppliers do |t|
      t.references :venue_application_form, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_suppliers_on_vaf_id'
      }

      t.references :supplier, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_suppliers_on_supplier_id'
      }

      t.timestamps
    end
  end
end
