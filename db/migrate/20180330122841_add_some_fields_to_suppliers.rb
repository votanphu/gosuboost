class AddSomeFieldsToSuppliers < ActiveRecord::Migration[5.1]
  def change
    add_column :suppliers, :first_name, :string
    add_column :suppliers, :last_name, :string
    add_column :suppliers, :business_phone, :string
    add_column :suppliers, :property_name, :string
    add_column :suppliers, :unit_number, :string
    add_column :suppliers, :street_number, :string
    add_column :suppliers, :street_name, :string
    add_column :suppliers, :street_type, :string
    add_column :suppliers, :suburb, :string
    add_column :suppliers, :state, :string
    add_column :suppliers, :postcode, :string
    add_column :suppliers, :contact_mobile, :string
  end
end
