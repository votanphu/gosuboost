class CreateApplySupplierCreditPersonalGuarantees < ActiveRecord::Migration[5.1]
  def change
    create_table :apply_supplier_credit_personal_guarantees, id: :uuid do |t|
      t.uuid :apply_supplier_credit_form_id
      t.uuid :personal_guarantee_id
      t.uuid :director_id
      t.string :signature_image
      t.boolean :is_signed, default: false
      t.json :form_data, default: {}
      t.boolean :is_director, default: false
      t.timestamps
    end
  end
end
