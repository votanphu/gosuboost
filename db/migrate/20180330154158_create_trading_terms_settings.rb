class CreateTradingTermsSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :trading_terms_settings do |t|
      t.string :trading_term
      t.string :personal_guarantees
      t.string :directors
      t.boolean :active, default: true
      t.boolean :default, default: false
      t.references :supplier, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
