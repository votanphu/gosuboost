class AddAddressToPersonalGuarantee < ActiveRecord::Migration[5.1]
  def change
    add_column :personal_guarantees, :home_property_name, :string
    add_column :personal_guarantees, :home_unit_number, :string
    add_column :personal_guarantees, :home_street_number, :string
    add_column :personal_guarantees, :home_street_name, :string
    add_column :personal_guarantees, :home_street_type, :string
    add_column :personal_guarantees, :postal_property_name, :string
    add_column :personal_guarantees, :postal_unit_number, :string
    add_column :personal_guarantees, :postal_street_number, :string
    add_column :personal_guarantees, :postal_street_name, :string
    add_column :personal_guarantees, :postal_street_type, :string
  end
end
