class AlterCreatedByOfApplySupplierCreditForm < ActiveRecord::Migration[5.1]
  def change
    remove_reference :apply_supplier_credit_forms, :created_by, references: :directors, type: :uuid
    add_reference :apply_supplier_credit_forms, :created_by, references: :users, type: :uuid
  end
end
