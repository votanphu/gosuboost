class AddCreditLimitToApplySupplierCredit < ActiveRecord::Migration[5.1]
  def change
    add_column :apply_supplier_credit_forms, :venue_credit_limit, :decimal, precision: 15, scale: 2
    add_column :apply_supplier_credit_forms, :supplier_credit_limit, :decimal, precision: 15, scale: 2
    add_column :apply_supplier_credit_forms, :credit_limit, :decimal, precision: 15, scale: 2
  end
end
