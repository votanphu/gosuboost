class AddOneMoreFieldToSupplierTermsCondition < ActiveRecord::Migration[5.1]
  def change
    add_column :supplier_terms_conditions, :is_all_trading_terms_settings, :boolean, default: true
  end
end
