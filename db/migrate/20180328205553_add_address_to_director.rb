class AddAddressToDirector < ActiveRecord::Migration[5.1]
  def change
    add_column :directors, :property_name, :string
    add_column :directors, :unit_number, :string
    add_column :directors, :street_number, :string
    add_column :directors, :street_name, :string
    add_column :directors, :street_type, :string
  end
end
