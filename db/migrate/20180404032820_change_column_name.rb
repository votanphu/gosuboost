class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :suppliers, :terms_and_condtions, :terms_and_conditions
  end
end
