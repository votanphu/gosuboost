class AddAttachmentBackLicenseFileToDirectors < ActiveRecord::Migration[5.1]
  def self.up
    change_table :directors do |t|
      t.attachment :back_license_file
    end
  end

  def self.down
    remove_attachment :directors, :back_license_file
  end
end
