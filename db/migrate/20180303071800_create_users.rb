class CreateUsers < ActiveRecord::Migration[5.1]
  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')
  enable_extension 'uuid-ossp' unless extension_enabled?('uuid-ossp')

  def change
    create_table :users, id: :uuid do |t|
      t.string :name
      t.string :firstname
      t.string :lastname
      t.boolean :is_admin, default: false
      t.string :role, default: 'CUSTOMER'

      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE users_object_index_seq START 1;
    ALTER SEQUENCE users_object_index_seq OWNED BY users.object_index;
    ALTER TABLE users ALTER COLUMN object_index SET DEFAULT nextval('users_object_index_seq');
    SQL

    add_index :users, :object_index
  end
end
