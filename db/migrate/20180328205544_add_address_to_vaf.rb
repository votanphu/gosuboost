class AddAddressToVaf < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_forms, :trading_property_name, :string
    add_column :venue_application_forms, :trading_unit_number, :string
    add_column :venue_application_forms, :trading_street_number, :string
    add_column :venue_application_forms, :trading_street_name, :string
    add_column :venue_application_forms, :trading_street_type, :string
    add_column :venue_application_forms, :registered_property_name, :string
    add_column :venue_application_forms, :registered_unit_number, :string
    add_column :venue_application_forms, :registered_street_number, :string
    add_column :venue_application_forms, :registered_street_name, :string
    add_column :venue_application_forms, :registered_street_type, :string
  end
end
