class AddTradingNameToVenueApplicationForm < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_forms, :trading_name, :string
  end
end
