class AddIpSignedToVafd < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_form_directors, :signed_ip, :inet
  end
end
