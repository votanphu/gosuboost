class CreateApplySupplierCreditForms < ActiveRecord::Migration[5.1]
  def change
    create_table :apply_supplier_credit_forms, id: :uuid do |t|
      t.string :save_token
      t.datetime :save_at
      t.boolean :active, default: true
      t.string :status, default: 'PENDING_PG_SIGNATURE'
      t.integer :object_index
      t.references :vaf, references: :venue_application_forms, type: :uuid
      t.references :supplier, type: :uuid
      t.references :created_by, references: :directors, type: :uuid
      t.references :trading_terms_setting
      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE apply_supplier_credit_forms_object_index_seq START 1;
    ALTER SEQUENCE apply_supplier_credit_forms_object_index_seq OWNED BY apply_supplier_credit_forms.object_index;
    ALTER TABLE apply_supplier_credit_forms ALTER COLUMN object_index SET DEFAULT nextval('apply_supplier_credit_forms_object_index_seq');
    SQL
    add_index :apply_supplier_credit_forms, :object_index
  end
end
