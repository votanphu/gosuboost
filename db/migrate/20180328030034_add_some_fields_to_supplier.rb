class AddSomeFieldsToSupplier < ActiveRecord::Migration[5.1]
  def change
    add_column :suppliers, :email, :string
    add_column :suppliers, :abn, :string
    add_column :suppliers, :address, :string
  end
end
