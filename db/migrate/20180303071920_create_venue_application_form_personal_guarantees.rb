class CreateVenueApplicationFormPersonalGuarantees < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_application_form_personal_guarantees do |t|
      t.references :venue_application_form, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_personal_guarantees_on_vaf_id'
      }

      t.references :personal_guarantee, foreign_key: true, type: :uuid, index: {
        name: 'index_vaf_personal_guarantees_on_personal_guarantee_id'
      }

      t.timestamps
    end
  end
end
