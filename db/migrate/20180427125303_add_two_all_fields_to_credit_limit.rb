class AddTwoAllFieldsToCreditLimit < ActiveRecord::Migration[5.1]
  def change
    add_column :credit_limits, :is_all_market_segments, :boolean, default: true
    add_column :credit_limits, :is_all_trading_terms, :boolean, default: true
  end
end
