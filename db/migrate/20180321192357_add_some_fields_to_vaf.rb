class AddSomeFieldsToVaf < ActiveRecord::Migration[5.1]
  def change
    add_column :venue_application_forms, :is_director_guarantees, :boolean, default: false
    add_column :personal_guarantees, :has_different_postal_address, :boolean, default: false
    add_column :venue_application_form_directors, :is_director_guarantee, :boolean, default: false
  end
end
