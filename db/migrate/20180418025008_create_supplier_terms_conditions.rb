class CreateSupplierTermsConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_terms_conditions do |t|
      t.references :supplier, type: :uuid
      t.boolean :is_all_market, default: true
      t.text :content
      t.boolean :active, default: true
      t.timestamps
      t.integer :object_index
    end
    execute <<-SQL
    CREATE SEQUENCE supplier_terms_conditions_object_index_seq START 1;
    ALTER SEQUENCE supplier_terms_conditions_object_index_seq OWNED BY supplier_terms_conditions.object_index;
    ALTER TABLE supplier_terms_conditions ALTER COLUMN object_index SET DEFAULT nextval('supplier_terms_conditions_object_index_seq');
    SQL
    add_index :supplier_terms_conditions, :object_index
  end
end
