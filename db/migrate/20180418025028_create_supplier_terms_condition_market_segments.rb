class CreateSupplierTermsConditionMarketSegments < ActiveRecord::Migration[5.1]
  def change
    create_table :supplier_terms_condition_market_segments do |t|
      t.references :supplier_terms_condition, foreign_key: true, index: { name: 'index_stcms_on_stc_id' }
      t.references :market_segment, foreign_key: true, index: { name: 'index_stcms_on_ms_id' }
      t.timestamps
    end
  end
end
