class CreateMarketSegments < ActiveRecord::Migration[5.1]
  def change
    create_table :market_segments do |t|
      t.string :market_segment

      t.timestamps
    end
  end
end
