class AddSignedAtToAscfpg < ActiveRecord::Migration[5.1]
  def change
    add_column :apply_supplier_credit_personal_guarantees, :signed_at, :datetime
  end
end
