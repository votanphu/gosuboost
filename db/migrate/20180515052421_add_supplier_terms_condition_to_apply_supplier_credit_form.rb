class AddSupplierTermsConditionToApplySupplierCreditForm < ActiveRecord::Migration[5.1]
  def change
    add_column :apply_supplier_credit_forms, :supplier_terms_condition_content, :text
  end
end
