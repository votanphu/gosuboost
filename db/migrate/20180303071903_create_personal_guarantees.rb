class CreatePersonalGuarantees < ActiveRecord::Migration[5.1]
  def change
    create_table :personal_guarantees, id: :uuid do |t|
      t.string :firstname
      t.string :lastname
      t.string :home_address_line_1
      t.string :home_address_line_2
      t.string :home_suburb
      t.string :home_state
      t.string :home_postcode
      t.string :postal_address_line_1
      t.string :postal_address_line_2
      t.string :postal_suburb
      t.string :postal_state
      t.string :postal_postcode
      t.string :mobile
      t.string :phone
      t.string :email
      t.references :user, foreign_key: true, type: :uuid

      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE personal_guarantees_object_index_seq START 1;
    ALTER SEQUENCE personal_guarantees_object_index_seq OWNED BY personal_guarantees.object_index;
    ALTER TABLE personal_guarantees ALTER COLUMN object_index SET DEFAULT nextval('personal_guarantees_object_index_seq');
    SQL

    add_index :personal_guarantees, :object_index
  end
end
