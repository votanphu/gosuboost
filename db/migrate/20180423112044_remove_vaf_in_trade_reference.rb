class RemoveVafInTradeReference < ActiveRecord::Migration[5.1]
  def change
    remove_reference :trade_references, :vaf
    add_reference :trade_references, :ascf, references: :apply_supplier_credit_forms, type: :uuid
    remove_column :supplier_terms_conditions, :object_index
  end
end
