class AddIndexCompanyAbnToVaf < ActiveRecord::Migration[5.1]
  def change
    add_index :venue_application_forms, :company_abn, unique: true
    add_index :suppliers, :abn, unique: true
  end
end
