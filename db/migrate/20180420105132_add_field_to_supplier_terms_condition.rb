class AddFieldToSupplierTermsCondition < ActiveRecord::Migration[5.1]
  def change
    add_reference :supplier_terms_conditions, :trading_terms_setting, foreign_key: true
  end
end
