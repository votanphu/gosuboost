class AddAttachmentFrontLicenseFileToDirectors < ActiveRecord::Migration[5.1]
  def self.up
    change_table :directors do |t|
      t.attachment :front_license_file
    end
  end

  def self.down
    remove_attachment :directors, :front_license_file
  end
end
