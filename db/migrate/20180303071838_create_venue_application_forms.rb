class CreateVenueApplicationForms < ActiveRecord::Migration[5.1]
  def change
    create_table :venue_application_forms, id: :uuid do |t|
      t.string :business_name
      t.string :email
      t.string :firstname
      t.string :lastname
      t.string :mobile
      t.string :phone
      t.string :trading_address_line_1
      t.string :trading_address_line_2
      t.string :trading_suburb
      t.string :trading_state
      t.string :trading_postcode
      t.integer :estimated_weekly_expected_purchased
      t.string :requested_trading_terms
      t.string :company_name
      t.string :company_abn
      t.string :operating_structure
      t.string :registered_address_line_1
      t.string :registered_address_line_2
      t.string :registered_suburb
      t.string :registered_state
      t.string :registered_postcode
      t.string :bank_branch
      t.string :bsb
      t.string :account_number
      t.boolean :pay_by_credit_card, default: false
      t.string :account_contact_name
      t.string :account_contact_email
      t.string :account_contact_number
      t.string :kitchen_contact_name
      t.string :kitchen_contact_number
      t.string :trade_reference_1_business_name
      t.string :trade_reference_1_contact_person
      t.string :trade_reference_1_contact_number
      t.string :save_token
      t.datetime :save_at
      t.boolean :active, default: true

      t.timestamps
      t.integer :object_index
    end

    execute <<-SQL
    CREATE SEQUENCE venue_application_forms_object_index_seq START 1;
    ALTER SEQUENCE venue_application_forms_object_index_seq OWNED BY venue_application_forms.object_index;
    ALTER TABLE venue_application_forms ALTER COLUMN object_index SET DEFAULT nextval('venue_application_forms_object_index_seq');
    SQL

    add_index :venue_application_forms, :object_index
    add_index :venue_application_forms, :created_at
  end
end
