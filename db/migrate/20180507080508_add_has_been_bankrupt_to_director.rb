class AddHasBeenBankruptToDirector < ActiveRecord::Migration[5.1]
  def change
    add_column :directors, :has_been_bankrupt, :boolean, default: false
  end
end
