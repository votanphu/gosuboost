class CreateEmailThemes < ActiveRecord::Migration[5.1]
  def change
    create_table :email_themes, id: :uuid do |t|
      t.string :name
      t.text :plain_content
      t.text :html_content
      t.string :category, default: 'GENERAL'
      t.boolean :active, default: false
      t.references :created_by, references: :users, type: :uuid

      t.timestamps
    end
  end
end
