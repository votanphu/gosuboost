# Generate tables/models
rails g model VenueApplicationForm business_name:string email:string firstname:string lastname:string mobile:string phone:string trading_address_line_1:string trading_address_line_2:string trading_suburb:string trading_state:string trading_postcode:string estimated_weekly_expected_purchased:integer requested_trading_terms:string company_name:string company_abn:string operating_structure:string registered_address_line_1:string registered_address_line_2:string registered_suburb:string registered_state:string registered_postcode:string bank_branch:string bsb:string account_number:string pay_by_credit_card:boolean account_contact_name:string account_contact_email:string account_contact_number:string kitchen_contact_name:string kitchen_contact_number:string trade_reference_1_business_name:string trade_reference_1_contact_person:string trade_reference_1_contact_number:string save_token:string save_at:datetime active:boolean

rails g model Director firstname:string lastname:string date_of_birth:datetime email:string address_line_1:string address_line_2:string suburb:string state:string postcode:string mobile:string phone:string drivers_license_number:string license_file:string

rails g model PersonalGuarantee firstname:string lastname:string home_address_line_1:string home_address_line_2:string home_suburb:string home_state:string home_postcode:string postal_address_line_1:string postal_address_line_2:string postal_suburb:string postal_state:string postal_postcode:string mobile:string phone:string email:string

rails g model Supplier company_name:string contact_name:string contact_number:string

rails g model User name:string firstname:string lastname:string is_admin:boolean role:string

rails g model VenueApplicationFormDirector venue_application_form:references director:references

rails g model VenueApplicationFormPersonalGuarantee venue_application_form:references personal_guarantee:references

rails g model VenueApplicationFormSupplier venue_application_form:references supplier:references

rails g model EmailTemplate name:string subject:string plain_content:text html_content:text type:string category:string active:boolean user:references

rails g model TradeReference business_name:string contact_person:string contact_number:string vaf:references

# Generate

# Future
rails g migration AddAddressToVAF trading_property_name:string trading_unit_number:string trading_street_number:string trading_street_name:string trading_street_type:string registered_property_name:string registered_unit_number:string registered_street_number:string registered_street_name:string registered_street_type:string

rails g migration AddAddressToDirector property_name:string unit_number:string street_number:string street_name:string street_type:string

rails g migration AddAddressToPersonalGuarantee home_property_name:string home_unit_number:string home_street_number:string home_street_name:string home_street_type:string postal_property_name:string postal_unit_number:string postal_street_number:string postal_street_name:string postal_street_type:string

rails g model Profile businness_name:string email:string firstname:string lastname:string name:string mobile:string phone:string date_of_birth:datetime

rails g model DriverLicense drivers_license_number:string front_license_file:attachment back_license_file:attachment license_expiry:datetime

rails g model Address property_name:string unit_number:string street_number:string street_name:string street_type:string address_line_1:string address_line_2:string suburb:string state:string postcode:string