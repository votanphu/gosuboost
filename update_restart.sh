set -e
CMD_COMPILE_GIT="git pull"
CMD_BUNDLE_INSTALL="bundle install"
CMD_COMPILE_ASSETS="RAILS_ENV=production rails assets:precompile"
CMD_MIGRATE="RAILS_ENV=production rails db:migrate"
CMD_STOP="./stop.sh"
CMD_START="./start.sh"

echo -e "Updating code..."
eval $CMD_COMPILE_GIT

echo -e "Bundle install..."
eval $CMD_BUNDLE_INSTALL

echo -e "Precompiling assets..."
eval $CMD_COMPILE_ASSETS

echo -e "Migrating database..."
eval $CMD_MIGRATE

eval $CMD_STOP

sleep 2

eval $CMD_START

echo -e "\n------------> Updated successful! \n"
