set -e

CMD_SERVER="RAILS_MAX_THREADS=73 WEB_CONCURRENCY=36 bundle exec puma -p 3000 -e production -d"

echo -e "\nStarting server ${CMD_SERVER} \n"

eval $CMD_SERVER &

for value in {1..1}
do
CMD_WORKER="RAILS_ENV=production VVERBOSE=1 PIDFILE=./tmp/pids/resque${value}.pid BACKGROUND=yes QUEUE=* rails environment resque:work"
eval $CMD_WORKER &
echo -e $CMD_WORKER
done

wait

echo -e "\nStart both server completed! \n"
