require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Pencilone
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths << Rails.root.join('lib')

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get, :post, :options, :delete, :head]
      end
    end

    config.time_zone = ENV['TIME_ZONE']
    config.i18n.default_locale = :en
    config.i18n.available_locales = [:en]

    $prod_root_url = ENV['PROD_ROOT_URL'] if Rails.env.production?
    $prod_root_url = ENV['DEV_ROOT_URL'] unless Rails.env.production?
    $default_per_page = 25
    WillPaginate.per_page = $default_per_page
  end
end
