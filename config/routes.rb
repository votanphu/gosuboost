Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords',
    unlocks: 'users/unlocks'
  }

  devise_scope :user do
    post "/users/sessions/verify_otp" => "users/sessions#verify_otp"
  end

  root to: "pages#home"
  get '/venue-registration' => 'pages#venue_registration', as: :pages_venue_registration
  get '/not-found' => 'pages#not_found', as: :pages_not_found

  post '/venue-registration' => 'venue_application_form#create', as: :venue_application_form_create
  post '/venue-registration/:id' => 'venue_application_form#update', as: :venue_application_form_update
  get '/venue-registration/token/:token' => 'venue_application_form#get_by_token', as: :venue_application_form_get_by_token
  get '/venue-form/token/:token' => 'venue_application_form#get_by_token', as: :venue_application_form_venue_form_token
  get '/venue-form' => 'venue_application_form#venue_form', as: :venue_application_form_venue_form
  post '/venue-form/send-form-link-to-email/:token' => 'venue_application_form#send_form_link_to_email', as: :venue_application_form_send_form_link_to_email
  post '/venue-form/:token/director-sign' => 'venue_application_form#director_sign', as: :venue_application_form_director_sign
  get '/venue-form/success' => 'venue_application_form#venue_form_success', as: :venue_application_form_venue_form_success
  delete '/venue-registration/:id' => 'venue_application_form#destroy', as: :venue_application_form_destroy

  post '/directors/upload-front-license-file' => 'directors#upload_front_license_file', as: :directors_upload_front_license_file
  post '/directors/upload-back-license-file' => 'directors#upload_back_license_file', as: :directors_upload_back_license_file

  post '/abn/abn-details' => 'abn#abn_details', as: :abn_abn_details

  get '/terms_of_use' => 'pages#terms_of_use', as: :terms_of_use
  get '/privacy' => 'pages#privacy', as: :privacy
  get '/faq' => 'pages#faq', as: :faq

  post '/contact' => 'contact#create', as: :contacts
  post 'contact/create' => 'contact#create', as: :contacts_create_contact

  scope :users do
    post '/create-users' => 'user#create_users', as: :users_create_users
    get '/set-password' => 'user#set_password', as: :users_set_password
    get '/set-password/:id' => 'user#edit_password', as: :users_edit_password
    post '/set-password/:id' => 'user#update_password', as: :users_update_password
  end

  namespace :admin do
    get '/' => 'dashboard#index', as: :admin_index
    get '/dashboard/' => 'dashboard#index', as: :admin_dashboard_index

    resources :users
    resources :email_templates
    resources :market_segments
    resources :credit_limits, except: [:show]

    get '/trading_terms_settings' => 'trading_terms_settings#index', as: :trading_terms_settings
    post '/update_trading_terms_settings' => 'trading_terms_settings#update', as: :update_trading_terms_settings

    resources :venue_suppliers, except: [:show]
    get '/set_supplier' => 'venue_suppliers#set_supplier', as: :set_supplier_venue_suppliers
    post '/venue_suppliers/:id/update' => 'venue_suppliers#update_ascf', as: :venue_suppliers_update_ascf
    post '/venue_suppliers/:id/pg-sign' => 'venue_suppliers#personal_guarantee_sign', as: :venue_suppliers_personal_guarantee_sign
    get '/set_vaf' => 'venue_suppliers#set_vaf', as: :set_vaf_venue_suppliers
    get '/get_director_details' => 'venue_suppliers#get_director_details', as: :venue_suppliers_get_director_details
    get '/get_vaf_directors' => 'venue_suppliers#get_vaf_directors', as: :venue_suppliers_get_vaf_directors
    get '/set_trading_term_setting' => 'venue_suppliers#set_trading_term_setting', as: :set_trading_term_setting_venue_suppliers
    get '/search_suppliers' => 'venue_suppliers#search_suppliers', as: :venue_suppliers_search_suppliers

    get '/venue_details' => 'venues#index', as: :venue_details
    post '/venues/import' => 'venues#import', as: :admin_import_venues
    post '/venues/admin_export' => 'venues#export', as: :admin_export_venues
    post '/venues/send_email_directors/:id' => 'venues#send_email_directors', as: :admin_venues_send_email_directors
    post '/venues/send_email_pgs/:id' => 'venues#send_email_pgs', as: :admin_venues_send_email_pgs
    get '/venues/approve/:id' => 'venues#approve', as: :admin_venues_approve
    get '/venues/reject/:id' => 'venues#reject', as: :admin_venues_reject
    post '/venues/send_credit_limit/:id' => 'venues#send_credit_limit', as: :admin_venues_send_credit_limit

    get '/venue_users' => 'venue_users#index', as: :venue_users
    get '/venue_users/:id' => 'venue_users#show', as: :show_venue_user

    get '/my_profile' => 'user_profiles#index', as: :admin_user_profile
    post '/update_director/:id' => 'user_profiles#update_director', as: :admin_update_director
    post '/update_personal_guarantee/:id' => 'user_profiles#update_personal_guarantee', as: :admin_update_personal_guarantee
    post '/update_admin/:id' => 'user_profiles#update_admin', as: :admin_update_admin
    post '/update_avatar/:id' => 'user_profiles#update_avatar', as: :user_profiles_update_avatar

    get '/pencil_terms_conditions' => 'pencil_terms_conditions#index', as: :pencil_terms_conditions
    post '/pencil_terms_conditions/:id/update' => 'pencil_terms_conditions#update', as: :pencil_terms_conditions_update

    resources :suppliers
    get '/suppliers_new_terms_and_condition' => 'suppliers#new_terms_and_condition', as: :suppliers_new_terms_and_condition
    get '/suppliers_my_terms_and_conditions' => 'suppliers#my_terms_and_conditions', as: :suppliers_my_terms_and_conditions
    post '/suppliers_create_terms_and_condition' => 'suppliers#create_terms_and_condition', as: :suppliers_create_terms_and_condition
    get '/suppliers_edit_terms_and_condition/:id' => 'suppliers#edit_terms_and_condition', as: :suppliers_edit_terms_and_condition
    post '/suppliers_update_terms_and_condition/:id' => 'suppliers#update_terms_and_condition', as: :suppliers_update_terms_and_condition
    delete '/suppliers_destroy_terms_and_condition/:id' => 'suppliers#destroy_terms_and_condition', as: :suppliers_destroy_terms_and_condition
    get '/suppliers_edit_supplier_user/:id' => 'suppliers#edit_supplier_user', as: :suppliers_edit_supplier_user
    get '/suppliers_new_supplier_user' => 'suppliers#new_supplier_user', as: :suppliers_new_supplier_user
    post '/suppliers_update_supplier_user/:id' => 'suppliers#update_supplier_user', as: :suppliers_update_supplier_user
    post '/suppliers_create_supplier_user' => 'suppliers#create_supplier_user', as: :suppliers_create_supplier_user
    delete '/suppliers_destroy_supplier_user/:id' => 'suppliers#destroy_supplier_user', as: :suppliers_destroy_supplier_user
    get '/suppliers_my_profile' => 'suppliers#my_profile', as: :suppliers_my_profile
    get '/suppliers_supplier_users' =>  'suppliers#supplier_users', as: :suppliers_supplier_users
    post '/suppliers_update_profile' => 'suppliers#update_profile', as: :suppliers_update_profile
    post '/suppliers_update_avatar/:id' => 'suppliers#update_avatar', as: :suppliers_update_avatar
    post '/suppliers_update_logo/:id' => 'suppliers#update_logo', as: :suppliers_update_logo
  end

  get '*path', :to => 'pages#not_found'
end
