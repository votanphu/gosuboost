Resque.logger = Logger.new(Rails.root.join('log', "#{Rails.env}_resque.log"))
Resque.redis = Redis.new(host: "localhost", port: 6379)
