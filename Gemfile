source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.5'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 3.2.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'active_model_serializers', '0.10.7'
gem 'activerecord-session_store', '1.1.1'
gem 'attr_encrypted', '3.1.0'
gem 'awesome_print', '1.8.0'
gem 'bootsnap', '1.3.0', require: false
gem 'devise', '4.4.1'
gem 'devise-i18n', '1.6.0'
gem 'devise-two-factor', '3.0.3'
gem 'dotenv-rails', '2.2.1'
gem 'geocoder', '1.4.8'
gem 'jwt', '2.1.0'
gem 'paperclip', '5.0.0'
gem 'rack-cors', '1.0.2', require: 'rack/cors'
gem 'rails-i18n', '5.1.1'
gem 'recaptcha', '4.6.4', require: 'recaptcha/rails'
gem 'resque', '1.27.4'
gem 'resque-scheduler', '4.3.1'
gem 'rqrcode_png', '0.1.5'

gem 'ruby-transmitsms', '~> 0.1.3'
gem 'whenever', '0.10.0', require: false

gem 'bootstrap', '4.0.0'
gem 'devise-bootstrapped', github: 'king601/devise-bootstrapped', branch: 'bootstrap4'
gem 'font-awesome-rails', '4.7.0.3'
gem 'htmlcompressor', '0.4.0'
gem 'httparty', '0.16.0'
gem 'jcrop-rails-v2', '0.9.12.3'
gem 'jquery-rails', '4.3.1'
gem 'popper_js', '~> 1.12.9'

gem 'ffaker', '2.7.0'
gem 'rinku', '2.0.4'
gem 'swagger-blocks', '2.0.2'
gem 'wicked_pdf', '~> 1.1'
gem 'will_paginate', '3.1.6'
gem 'will_paginate-bootstrap4', '0.2.2'
gem 'wkhtmltopdf-binary', '0.12.3.1'
gem 'wkhtmltopdf-binary-edge', '0.12.4.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'pry', '~> 0.11.3'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '2.0.2'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'brakeman', '4.3.0', require: false
  gem 'bullet', '5.7.5', group: 'development'
  gem 'rails-erd', '1.5.2', require: false
  gem 'rails_best_practices', '1.19.2', group: 'development'
  gem 'rubocop', '0.55.0', require: false, group: 'development'
  gem 'traceroute', '0.6.1'

  gem 'rack-mini-profiler', '1.0.0', require: false

  # For memory profiling
  gem 'memory_profiler', '0.9.10'

  # For call-stack profiling flamegraphs
  gem 'flamegraph', '0.9.5'
  gem 'stackprof', '0.2.11'

  gem 'rubycritic', '3.4.0', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
