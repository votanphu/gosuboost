#!/usr/bin/env bash
kill -9 `cat tmp/pids/puma.pid`
echo -e "\nPuma stopped."

for value in {1..1}
do
kill -9 `cat ./tmp/pids/resque${value}.pid`
echo -e "\nResque ${value} stopped."
done

echo -e "\nPuma server and Resque stopped! \n"
