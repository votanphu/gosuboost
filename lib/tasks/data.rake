namespace :data do
  desc 'Downcase all emails'
  task downcase_all_emails: :environment do
    puts '### Begin to downcase all emails'

    User.update_all('email = LOWER(email)')
    puts '==> Downcase all user emails => done'

    Director.update_all('email = LOWER(email)')
    puts '==> Downcase all director emails => done'

    PersonalGuarantee.update_all('email = LOWER(email)')
    puts '==> Downcase all personal guarantee emails => done'

    VenueApplicationForm.update_all('email = LOWER(email)')
    VenueApplicationForm.update_all('email = LOWER(account_contact_email)')
    puts '==> Downcase all vaf emails => done'

    Contact.update_all('email = LOWER(email)')
    puts '==> Downcase all contact emails => done'

    Supplier.update_all('email = LOWER(email)')
    puts '==> Downcase all supplier emails => done'

    puts '### End to downcase all emails'
  end
end
