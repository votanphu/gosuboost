module CommonLib
  def self.convert_to_bool(value)
    return false if value.blank?
    value = value.strip.downcase
    return true if value.to_s == 'true' || value.to_s == '1'
    false
  end

  def self.validate_phoneAU(phone_number)
    phone_number = phone_number.gsub(/\s+/, '').strip if phone_number.present?
    return true if (phone_number =~ /\([0]{1}[0-9]{1}\)[0-9]{8}/).present?
    false
  end

  def self.validate_mobileAU(mobile_number)
    mobile_number = mobile_number.gsub(/\s+/, '').strip if mobile_number.present?
    return true if (mobile_number =~ /[0]{1}[4]{1}[0-9]{8}/).present?
    false
  end

  def self.validate_email(email)
    return false if email.blank?
    email.strip!.downcase!
    return true if (email =~ Const::VALID_EMAIL_REGEX).present?
    false
  end

  def self.validate_postcodeAU(postcode)
    postcode = postcode.strip if postcode.present?
    return true if (postcode =~ /[0-9]{4}/).present?
    false
  end

  def self.number_to_currency(number)
    number.to_s.gsub('.0', '').reverse.scan(/(\d*\.\d{1,3}|\d{1,3})/).join(',').reverse
  end

  def self.currency_to_number(currency)
    return 0 if currency.blank?
    currency.delete(',').strip
  end

  def self.validate_number(value)
    return false if value.blank?
    true if Float(value) rescue false
  end

  def self.validate_street_type(value)
    return false if value.blank?
    value.strip!
    Const::STREET_TYPES.any? { |item| item[:value].casecmp(value) == 0 }
  end

  def self.validate_state(value)
    return false if value.blank?
    value.strip!
    Const::STATES.any? { |item| item[:value].casecmp(value) == 0 }
  end

  def self.validate_operating_structure(value)
    return false if value.blank?
    value = value.gsub(/\s+/, ' ').strip
    Const::OPERATING_STRUCTURES.any? { |item| item[:value].casecmp(value) == 0 }
  end

  def self.validate_datetime(value)
    return false if value.blank?
    true if DateTime.parse(value) rescue false
  end

  def self.validate_currency(value, precision = 15)
    return false if value < 0
    value.to_s.delete('.').length <= precision
  end

  def self.text_different?(text1, text2)
    return false if text1.blank? && text2.blank?
    return true if text1.blank? && text2.present?
    return true if text1.present? && text2.blank?
    text1 = text1.gsub(/\s+/, ' ').strip
    text2 = text2.gsub(/\s+/, ' ').strip
    return true unless text1.eql? text2
    false
  end
end
